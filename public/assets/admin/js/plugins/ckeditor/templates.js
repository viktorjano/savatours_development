/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

// Register a templates definition set named "default".
CKEDITOR.addTemplates( 'default', {
	// The name of sub folder which hold the shortcut preview images of the
	// templates.
	imagesPath: CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),

	// The templates definitions.
	templates: [
		{
		title: 'Hotel overview',
		image: 'template1.gif',
		description: 'Text + 3 rreshta me foto',
		html: 
			'<article><div style="width: 75%; float:left;"><p>' +
				'description </p></div>' +
				'<div style="width: 21%; float:left; margin-left: 2%; border-left: 1px dotted #ccc; padding-left: 10px;">' +
				'<p><em>Wifi Falas</em></p>' +
				'<p><em>494 Dhoma</em></p>' +
				'<p><em>I rindertuar</em></p>' +
				'</div></article>' +
			 '<article  class="overview-block">' +
					'<h2>Ultra All Inclusive</h2>' +
					'<div class="text">' +
						'<p>Ushqimi i larmishem...</p>' +
					'</div>' +
					'<img src="http://placehold.it/280x250" width="280px;" style="float:right;">' +
			'</article>'+
			 '<article  class="overview-block">' +
					'<h2>Plazhi dhe Pishinat</h2>' +
					'<div class="text">' +
						'<p>Pazh i papare...</p>' +
					'</div>' +
					'<img src="http://placehold.it/280x250" width="280px;" style="float:right;">' +
			'</article>'+
			 '<article  class="overview-block">' +
					'<h2>Dicka tjeter</h2>' +
					'<div class="text">' +
						'<p>Ushqimi i larmishem...</p>' +
					'</div>' +
					'<img src="http://placehold.it/280x250" width="280px;" style="float:right;">' +
			'</article>'					

		}, {
			title: 'Hotel overview 2',
			image: 'template1.gif',
			description: 'Text + 4 rreshta me foto',
			html: 
				'<article><div style="width: 75%; float:left;"><p>' +
					'description </p></div>' +
					'<div style="width: 21%; float:left; margin-left: 2%; border-left: 1px dotted #ccc; padding-left: 10px;">' +
					'<p><em>Wifi Falas</em></p>' +
					'<p><em>494 Dhoma</em></p>' +
					'<p><em>I rindertuar</em></p>' +
					'</div></article>' +
				 '<article  class="overview-block">' +
						'<h2>Ultra All Inclusive</h2>' +
						'<div class="text">' +
							'<p>Ushqimi i larmishem...</p>' +
						'</div>' +
						'<img src="http://placehold.it/280x250" width="280px;" style="float:right;">' +
				'</article>'+
				 '<article  class="overview-block">' +
						'<h2>Plazhi dhe Pishinat</h2>' +
						'<div class="text">' +
							'<p>Pazh i papare...</p>' +
						'</div>' +
						'<img src="http://placehold.it/280x250" width="280px;" style="float:right;">' +
				'</article>'+
				 '<article  class="overview-block">' +
						'<h2>Dicka tjeter</h2>' +
						'<div class="text">' +
							'<p>Ushqimi i larmishem...</p>' +
						'</div>' +
						'<img src="http://placehold.it/280x250" width="280px;" style="float:right;">' +
				'</article>'					

		}, {
		title: 'Hotel overview pa txt',
		image: 'template1.gif',
		description: '3 rreshta me foto',
		html: 
			 '<article  class="overview-block">' +
					'<h2>Ultra All Inclusive</h2>' +
					'<div class="text">' +
						'<p>Ushqimi i larmishem...</p>' +
					'</div>' +
					'<img src="http://placehold.it/280x250" width="280px;" style="float:right;">' +
			'</article>'+
			 '<article  class="overview-block">' +
					'<h2>Plazhi dhe Pishinat</h2>' +
					'<div class="text">' +
						'<p>Pazh i papare...</p>' +
					'</div>' +
					'<img src="http://placehold.it/280x250" width="280px;" style="float:right;">' +
			'</article>'+
			 '<article  class="overview-block">' +
					'<h2>Dicka tjeter</h2>' +
					'<div class="text">' +
						'<p>Ushqimi i larmishem...</p>' +
					'</div>' +
					'<img src="http://placehold.it/280x250" width="280px;" style="float:right;">' +
			'</article>'					

		}, {
			title: 'Hotel overview pa txt 2',
			image: 'template1.gif',
			description: '4 rreshta me foto',
			html: 
				 '<article  class="overview-block">' +
						'<h2>Ultra All Inclusive</h2>' +
						'<div class="text">' +
							'<p>Ushqimi i larmishem...</p>' +
						'</div>' +
						'<img src="http://placehold.it/280x250" width="280px;" style="float:right;">' +
				'</article>'+
				 '<article  class="overview-block">' +
						'<h2>Plazhi dhe Pishinat</h2>' +
						'<div class="text">' +
							'<p>Pazh i papare...</p>' +
						'</div>' +
						'<img src="http://placehold.it/280x250" width="280px;" style="float:right;">' +
				'</article>'+
				 '<article  class="overview-block">' +
						'<h2>Dicka tjeter</h2>' +
						'<div class="text">' +
							'<p>Ushqimi i larmishem...</p>' +
						'</div>' +
						'<img src="http://placehold.it/280x250" width="280px;" style="float:right;">' +
				'</article>'+
				 '<article  class="overview-block">' +
						'<h2>Dicka tjeter</h2>' +
						'<div class="text">' +
							'<p>Ushqimi i larmishem...</p>' +
						'</div>' +
						'<img src="http://placehold.it/280x250" width="280px;" style="float:right;">' +
				'</article>'							
		}, {
			title: 'Main Banner',
			image: 'template1.gif',
			description: 'Main website banner',
			html: 
				'<div class="main-banner-description">'+
				'<h1>Sirius Deluxe Hotel</h1>'+
				'<div style="width: 400px;">'+
				'<h2>Neverojatna Ultra All Inclusive Ponuda</h2>'+
				'<h2>21.06, 7 dena, total <span class="price"><i>€</i>383 pp</span></h2>'+
				'<h2>i mnogu drugi super ponudi</h2>'+
				'</div>'+
				'</div>	'		
		}
	]
} );
