<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Samo {

    public function __construct() {
        $this->_db_connect();
    }

    /**
     * @todo
     *
     *
     * @param int $town_from
     * @param int $state
     * @param smalldate $checkin
     * @param int $hotel
     * @param int $tour
     *
     * @return array
     */
    public function get_spos($town_from = NULL, $state = NULL, $checkin = NULL, $hotel = NULL, $tour = NULL) {


            $query = mssql_query("sava.dbo.up_WEB_3_search_Spog @STATE = $state, @TOWNFROM = $town_from, @CHECKIN = $checkin, @HOTEL = $hotel, @TOUR = $tour");

            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                    $result[] = $row;
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        return NULL;
    }

    /**
     * Function to list all valid SPOS
     *
     * @param type $state
     * @param type $town_from
     * @param type $tour
     * @param type $hotel
     * @param type $checkin
     * @param type $order_by_name
     *
     * @return array
     */
    public function list_all_spos($state, $town_from, $tour, $hotel = NULL, $checkin = NULL, $order_by_name = NULL) {

        // $key = md5('list_all_spos');

        // if ($result = $this->CI->cache->memcached->get($key)) {

        //     return $result;
        // } else {
            $stmt = mssql_init('sava.dbo.up_WEB_3_spo_List');
            mssql_bind($stmt, '@STATE', $state, SQLINT4, false, $state == null ? true : false);
            mssql_bind($stmt, '@TOWNFROM', $town_from, SQLINT4, false, $town_from == null ? true : false);
            mssql_bind($stmt, '@TOUR', $tour, SQLINT4, false, $tour == null ? true : false);
            mssql_bind($stmt, '@CHECKIN', $spo, SQLINT4, false, $checkin == null ? true : false);
            mssql_bind($stmt, '@HOTEL', $spo, SQLINT4, false, $hotel == null ? true : false);
            mssql_bind($stmt, '@ORDER_BY_NAME', $order_by_name, SQLINT1, false, $order_by_name == null ? true : false);

            $query = mssql_execute($stmt);
            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                    $result[] = $row;
                }

                // $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        // }
        return NULL;
    }

   /**
     *
     * @param int $state
     * @param int $town_from
     * @param bit $order_by_name
     *
     * @return array
     */
    public function get_tours($state, $town_from, $order_by_name = 0) {

        $this->CI->cache->memcached->clean();
        $key = md5($state . $town_from . $order_by_name);

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {
            $stmt = mssql_init('sava.dbo.up_WEB_3_search_Tour');

            mssql_bind($stmt, '@STATE', $state, SQLINT4, false, $state == null ? true : false);
            mssql_bind($stmt, '@TOWNFROM', $town_from, SQLINT4, false, $town_from == null ? true : false);

            mssql_bind($stmt, '@ORDER_BY_NAME', $order_by_name, SQLINT1, false, $order_by_name == null ? true : false);

            $query = mssql_execute($stmt);
            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                    $result[] = $row;
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        }

        return NULL;
    }

    /**
     *
     * @param type $id
     * @param array $all_tours
     *
     * @return array
     */
//    public function get_tour_by_id($id, $all_tours) {
//        foreach($tours as $tour) {
//            if($tour['Inc'] ==  $id) {
//                return $tour;
//            }
//        }
//        return NULL;
//    }

    /**
     *
     * @param int $town_from
     * @param int $state
     * @param int $tour
     * @param int $spo
     * @param bit $order_by_name
     *
     * @return array
     */
    public function get_price_type($town_from, $state, $tour = NULL, $spo = NULL, $order_by_name = NULL) {

        $key = md5($town_from . $state . $tour . $spo . $order_by_name);

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {

            $query = mssql_query("sava.dbo.up_WEB_3_search_Ptype @TOWNFROM = $town_from, @STATE = $state, @TOUR = $tour, @SPOG = $spo, @ORDER_BY_NAME = $order_by_name");

            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                    $result[] = $row;
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        }
        return NULL;
    }

    /**
     *
     * @param int $town_from
     * @param int $state
     * @param smalldate $begin_date
     * @param int $spo
     *
     * @return null
     */
//    public function get_departure_dates($town_from = NULL, $state = NULL, $begin_date = NULL, $spo = NULL) {
//
//        $key = md5($town_from . $state . $begin_date . $spo);
//
//        if ($result = $this->CI->cache->memcached->get($key)) {
//
//            return $result;
//        } else {
//
//            $query = mssql_query("sava.dbo.up_WEB_3_search_CheckinString @STATE = $state, @TOWNFROM = $town_from, @DATEBEG = $begin_date, @SPOG = $spo");
//
//            if (mssql_num_rows($query) > 0) {
//
//                $result = array();
//
//                while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
//                    $result[] = $row;
//                }
//
//                $this->CI->cache->memcached->save($key, $result);
//
//                return $result;
//            }
//
//            return NULL;
//        }
//    }

    /**
     * @param int $state
     * @return array
     */
    public function get_meal_types($state) {

        $key = md5("get_meal_types" . $state);

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {

            $query = mssql_query("SELECT gm.inc AS Inc, gm.name AS Name, gm.name AS LName FROM ADMIN_SITE.dbo.group_meal AS gm
                                    WHERE gm.State = $state ORDER BY Name");

            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query)) {
                    $result[] = $row;
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        }
        return NULL;
    }

    /**
     * Get meal id from the meal group id
     *
     * @param int $meal_group
     * @return int
     */
    public function get_meal_id($meal_group) {

        $key = md5("get_meal_id" . $meal_group);

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {

            $query = mssql_query("SELECT meal from ADMIN_SITE.dbo.group_meal_list WHERE group_meal=$meal_group");
            //var_dump(mssql_fetch_row($query));

            if (mssql_fetch_row($query) !== FALSE) {
                $result = mssql_result($query, 0, 0);

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }

            return null;
        }
    }

    /**
     * What the hell is this used for?
     *
     * @param int $state
     * @return array
     */
    public function get_star_types($state) {

        $key = md5("get_star_types" . $state);

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {

            $query = mssql_query("SELECT  gs.inc AS Inc, gs.name AS Name, gs.name AS LName
                            FROM ADMIN_SITE.dbo.group_star AS gs WHERE gs.state = $state
                            ORDER BY  Name");

            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                    $result[] = $row;
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        }

        return NULL;
    }

    /**
     *
     * @param int $state
     * @param int $region
     * @param int $spo
     * @param int $tour
     * @param int $town_from
     * @param bit $order_by_name
     *
     * @return array
         */
    public function get_destination_towns($state, $town_from, $region = NULL, $tour = NULL, $spo = NULL, $order_by_name = NULL) {

        $key = md5("get_destination_towns" . $state . $town_from . $region . $tour . $spo . $order_by_name);

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {

            $query = mssql_query("sava.dbo.up_WEB_3_search_Townto @STATE = $state, @TOWNFROM = $town_from, @REGION=$region,
                                @TOUR = $tour, @SPOG = $spo, @ORDER_BY_NAME = $order_by_name");

            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                    $result[] = $row;
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        }
        return NULL;
    }

    /**
     * @todo This is ambiguous. Needs to be clarified why we fetch these star types as well. Perhaps attaching to the hotels?
     *
     * @return array
     */
    public function get_all_stars() {

        $key = md5("get_all_stars");

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;

        } else {

            $query = mssql_query("SELECT * FROM sava.dbo.Star WHERE Inc > 0");

            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                    $result[] = $row;
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        }

        return NULL;
    }

    /**
     *
     * @param int $state
     * @return null
     */
    public function get_star_groups($state) {

        $key = md5("get_star_groups" . $state);

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;

        } else {

            $query = mssql_query("SELECT gsl.star AS StarInc, gs.Inc AS GroupStarInc
                                FROM ADMIN_SITE.dbo.group_star AS gs, ADMIN_SITE.dbo.group_star_list AS gsl
                                WHERE gs.state = $state AND gsl.group_star = gs.Inc");

            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                    $result[] = $row;
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        }

        return NULL;
    }

    /**
     * Get the currency rates
     *
     * @return array
     */
    public function get_currency_rate() {

        $key = md5("get_currency_rate");

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {

            $query = mssql_query("sava.dbo.up_WEB_3_CurrencyRates");

            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                    $result[] = $row;
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        }

        return NULL;
    }

    /**
     * @todo this is ambiguous. what is this currency list used for? perhaps this is the list of the alternative currencies?
     *
     * @param int $state
     * @param int $town_from
     * @param int $tour
     *
     * @return array
     */
    public function get_currency_list($state, $town_from, $tour) {

        $key = md5("get_currency_list" . $state . $town_from . $tour);

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {

            $stmt = mssql_init('up_WEB_3_search_CurrencyList');

            mssql_bind($stmt, '@STATE', $state, SQLINT2);
            mssql_bind($stmt, '@TOWNFROM', $state, SQLINT2);
            mssql_bind($stmt, '@TOUR', $state, SQLINT2);

            var_dump(mssql_fetch_row(mssql_execute($stmt)));

            //$query = mssql_query("sava.dbo.up_WEB_3_search_CurrencyList @STATE = $state, @TOWNFROM = $town_from, @TOUR = $tour");

            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                    $result[] = $row;
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        }
        return NULL;
    }

    *
     *
     * @param int $currency
     * @return array

    public function get_currency($currency_id) {

        $key = md5("get_currency" . $currency_id);

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {

            $query = mssql_query("SELECT Alias AS Name, Alias AS LName,Inc FROM sava.dbo.currency WHERE Inc=$currency_id");

            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                    $result[] = $row;
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        }

        return NULL;
    }

    /**
     *
     *
     * @param int $state
     * @param int $town_from
     * @param int $region
     * @param int $destination_town
     * @param int $tour
     * @param int $spo
     * @param bit $order_by_name
     *
     * @return array
     */
    public function get_hotels($state, $town_from, $tour, $spo=null, $region = NULL, $destination_town = NULL, $order_by_name = NULL) {

        $key = md5("get_hotels" . $state . $town_from . $tour . $spo . $region . $destination_town . $order_by_name);

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {

            $stmt = mssql_init('sava.dbo.up_WEB_3_search_Hotel');

            mssql_bind($stmt, '@STATE', $state, SQLINT4, false, $state == null ? true : false);
            mssql_bind($stmt, '@TOWNFROM', $town_from, SQLINT4, false, $town_from == null ? true : false);
            mssql_bind($stmt, '@REGION', $region, SQLINT4, false, $region == null ? true : false);
            mssql_bind($stmt, '@TOWN', $destination_town, SQLINT4, false, $destination_town == null ? true : false);
            mssql_bind($stmt, '@TOUR', $tour, SQLINT4, false, $tour == null ? true : false);
            mssql_bind($stmt, '@SPOG', $spo, SQLINT4, false, $spo == null ? true : false);
            mssql_bind($stmt, '@ORDER_BY_NAME', $order_by_name, SQLINT1, false, $order_by_name == null ? true : false);

            $query = mssql_execute($stmt);

            if (mssql_num_rows($query) > 0) {
                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                    $result[] = $row;
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        }
        return NULL;
    }

    /**
     *
     * @TODO:   - Debug with different input parameters.
     *          - Where an SPO exists, return both SPO and normal price
     *
     * @param varchar $form
     * @param int $spo
     * @param int $owner
     * @param int $fr_place
     * @param smalldatetime $checkin_begin
     * @param smalldatetime $checkin_end
     * @param varchar $hotel_list
     * @param smallint $nights_min
     * @param smallint $nights_max
     * @param varchar $meal_list
     * @param smallint $adults
     * @param smallint $children
     * @param int $price_min
     * @param int $price_max
     * @param smallint $filter
     * @param smallint $price_page
     * @param smallint $sort_type
     * @param smallint $records_on_page
     * @param varchar $catalog_db
     * @param varchar $site_db
     * @param int $min
     * @param int $max
     * @param bit $show_request
     * @param int $tour
     * @param int $business_fr_place
     * @param int $business_min
     * @param int $business_max
     * @param int $max_records
     * @param int $town_from
     * @param int $state
     * @param int $child1_age
     * @param int $child2_age
     * @param int $child3_age
     * @param int $price_type
     *
     * @return array
     */
    public function get_prices($form, $spo, $owner, $fr_place, $checkin_begin, $checkin_end, $hotel_list, $nights_min, $nights_max, $meal_list, $adults, $children, $price_min, $price_max, $filter, $price_page, $sort_type, $records_on_page, $catalog_db, $site_db, $min, $max, $show_request, $tour, $business_fr_place, $business_min, $business_max, $max_records, $town_from, $state, $child1_age, $child2_age, $child3_age, $price_type) {

        ini_set("memory_limit", "512M");

        $stmt = mssql_init('sava.dbo.up_WEB_3_search_Price');

        mssql_bind($stmt, '@FORM', $form, SQLVARCHAR);
        mssql_bind($stmt, '@SPOG', $spo, SQLINT2, false, $spo == null ? true : false);
        mssql_bind($stmt, '@OWNER', $owner, SQLINT2);
        mssql_bind($stmt, '@FRPLACE', $fr_place, SQLINT2);
        mssql_bind($stmt, '@CHECKIN_BEG', $checkin_begin, SQLVARCHAR);
        mssql_bind($stmt, '@CHECKIN_END', $checkin_end, SQLVARCHAR);
        mssql_bind($stmt, '@HOTELLIST', $hotel_list, SQLVARCHAR);
        mssql_bind($stmt, '@NIGHTMIN', $nights_min, SQLINT1);
        mssql_bind($stmt, '@NIGHTMAX', $nights_max, SQLINT1);
        mssql_bind($stmt, '@MEALLIST', $meal_list, SQLVARCHAR, false, $meal_list == null ? true : false);
        mssql_bind($stmt, '@ADULT', $adults, SQLINT1);
        mssql_bind($stmt, '@CHILD', $children, SQLINT1);
        mssql_bind($stmt, '@COSTMIN', $price_min, SQLINT4, false, $price_min == null ? true : false);
        mssql_bind($stmt, '@COSTMAX', $price_max, SQLINT4, false, $price_max == null ? true : false);
        mssql_bind($stmt, '@FILTER', $filter, SQLINT1);
        mssql_bind($stmt, '@PRICE_PAGE', $price_page, SQLINT1);
        mssql_bind($stmt, '@SORT_TYPE', $sort_type, SQLINT1);
        mssql_bind($stmt, '@RecOnPage', $records_on_page, SQLINT4);
        mssql_bind($stmt, '@CATALOGDB', $catalog_db, SQLVARCHAR);
        mssql_bind($stmt, '@DBSITE', $site_db, SQLVARCHAR);
        mssql_bind($stmt, '@MIN', $min, SQLINT4);
        mssql_bind($stmt, '@MAX', $max, SQLINT4);
        mssql_bind($stmt, '@SHOW_REQUEST', $show_request, SQLBIT, false, $show_request == null ? true : false);
        mssql_bind($stmt, '@TOUR', $tour, SQLINT4);
        mssql_bind($stmt, '@BFRPLACE', $business_fr_place, SQLINT4);
        mssql_bind($stmt, '@BMIN', $business_min, SQLINT4);
        mssql_bind($stmt, '@BMAX', $business_max, SQLINT4);
        mssql_bind($stmt, '@MAXRECORD', $max_records, SQLINT4);
        mssql_bind($stmt, '@TOWNFROM', $town_from, SQLINT4);
        mssql_bind($stmt, '@STATE', $state, SQLINT4);
        mssql_bind($stmt, '@AGE1', $child1_age, SQLINT2, false, $child1_age == null ? true : false);
        mssql_bind($stmt, '@AGE2', $child2_age, SQLINT2, false, $child2_age == null ? true : false);
        mssql_bind($stmt, '@AGE3', $child3_age, SQLINT2, false, $child3_age == null ? true : false);
        mssql_bind($stmt, '@PTYPE', $price_type, SQLINT4, false, $price_type == null ? true : false);

        $result = mssql_execute($stmt);

        if (mssql_num_rows($result) > 0) {

            $return_val = array();

            while ($row = mssql_fetch_array($result, MSSQL_ASSOC)) {
                $return_val[] = $row;
            }


            return $return_val;
        }

        return NULL;
    }

    /**
     *
     * @param type $id
     *
     * @return array
     */
    public function get_state($id) {

        $key = md5("get_state" . $id);

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {

            $query = mssql_query("SELECT * FROM sava.dbo.state WHERE inc=$id");

            if (mssql_num_rows($query) > 0) {

                $result = mssql_fetch_array($query, MSSQL_ASSOC);

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        }

        return NULL;

    }

    /**
     * returns array containing IDs of hotels having the selected starcount
     *
     * @param int $star_samo_id
     * @param array $tours
     *
     * @return array
     */
    private function filter_stars($star_samo_id, $tours) {

        $key = md5("filter_stars" . $star_samo_id . $tours);

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {

            $hotel_list = array();
            if ($star_samo_id !== NULL) {
                foreach ($tours as $tour) {
                    if ($tour['StarInc'] == $star_samo_id) {
                        $hotel_list[] = $tour['HotelInc'];
                    }
                }

                $this->CI->cache->memcached->save($key, $hotel_list);

                return $hotel_list;
            }
        }

        return null;
    }

    /**
     *
     * @param array $all_tours
     * @param array $stars
     * @return array
     */
    public function filter_hotels_by_stars($all_hotels, $stars) {
        $hotels = array();
        foreach ($all_hotels as $hotel) {
            if (in_array($hotel['StarInc'], $stars)) {
                $hotels[] = $hotel;
            }
        }

        return $hotels;
    }

    public function filter_by_concept($all_tours, $concepts) {
        $filtered = array();
        foreach ($all_tours as $key => $tour) {
            foreach ($concepts as $k => $v) {
                if ($tour['MealInc'] !== $k) {
                    $filtered[] = $tour;
                }
            }
        }

        return $filtered;
    }

    /**
     * Returns the hotel details
     *
     * @param int $id
     * @return array
     */
    public function get_hotel_details($id) {

        $key = md5("get_hotel_details" . $id);

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {

            $query = mssql_query("SELECT * FROM [sava].[dbo].[hotel] WHERE inc = $id");

            if (mssql_num_rows($query) > 0) {

                $result = mssql_fetch_array($query, MSSQL_ASSOC);

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }
        }
        return NULL;
    }

    /**
     * Get Departure dates from the calculated price list
     *
     * @param type $town_from
     * @param type $state
     * @param type $begin_date
     * @param type $spo
     * @return null
     */
    public function get_departure_dates($tour, $town_from, $state) {

        $key = md5($town_from . $state . $tour . 'checkin');

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {
            $query = mssql_query("SELECT DISTINCT CONVERT(varchar(10),[CheckIn], 121) 'checkin'  FROM [wwwbron].[dbo].[Cat_Claim] WHERE Tour = $tour AND townfrom = $town_from AND [state] = $state");

            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_NUM)) {
                    $result[] = $row[0];
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }

            return NULL;
        }
    }

    public function get_checkout_dates($tour, $town_from, $state) {

        $key = md5($town_from . $state . $tour . 'checkout');

        if ($result = $this->CI->cache->memcached->get($key)) {

            return $result;
        } else {
            $query = mssql_query("SELECT DISTINCT CONVERT(varchar(10),[dateout], 121) 'checkout'  FROM [wwwbron].[dbo].[Cat_Claim] WHERE Tour = $tour AND townfrom = $town_from AND [state] = $state");

            if (mssql_num_rows($query) > 0) {

                $result = array();

                while ($row = mssql_fetch_array($query, MSSQL_NUM)) {
                    $result[] = $row[0];
                }

                $this->CI->cache->memcached->save($key, $result);

                return $result;
            }

            return NULL;
        }
    }

    /**
     * Get Tour state. Needed for as a search param
     *
     * @param int $tour
     */
    public function get_tour_state($tour) {
        $query = mssql_query("SELECT state FROM [sava].[dbo].[tour] WHERE inc = $tour");


        if (mssql_num_rows($query) > 0) {
            return mssql_result($query, 0, 0);
        }

        return NULL;
    }

    /**
     * Returns prices without SPO
     *
     * @param array $claim
     * @return null
     */
    public function get_normal_price($claim) {
        $hotel = $claim['HotelInc'];
        $nights = $claim['Nights'];
        $tour = $claim['TourInc'];
        $ht_place = $claim['HtPlaceInc'];
        $meal = $claim['MealInc'];
        $room  = $claim['RoomInc'];
        $adult = $claim['Adult'];
        $child = $claim['Child'];
        $date = explode('/', $claim['CheckIn']);

        $checkin = $date[2] . "-" . $date[1] . "-" . $date[0];
        $checkin = date("Y-m-d H:i:s", strtotime($checkin));

//        var_dump($checkin);
        $query = mssql_query("SELECT Price FROM [wwwbron].[dbo].[Cat_Claim] WHERE Hotel = $hotel AND HtPlace = $ht_place AND Meal = $meal AND room = $room
            AND adult = $adult AND child = $child  AND Tour = $tour AND Nights = $nights AND CheckIn = '$checkin' AND Spog = NULL");

            if (mssql_fetch_row($query) !== FALSE) {
                $result = mssql_result($query, 0, 0);

                return $result;
            }
            return null;
    }

    private function _db_connect() {
        mssql_connect($this->CI->config->item('server'), $this->CI->config->item('uid'), $this->CI->config->item('pwd'))
                or die("Couldn't connect to SQL Server on $server. Error: " . mssql_get_last_message());
    }

}

/* End of file Samo.php */
