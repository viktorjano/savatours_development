<?php namespace Savatours\Samo;

use Illuminate\Support\ServiceProvider;

class SamoServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['samo'] = $this->app->share(function($app)
        {
            return new Samo;
        });
    }
}