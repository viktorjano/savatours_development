<?php namespace Savatours\Samo;

class Samo implements SamoInterface {

    private $key;
    private $office_db;

   public function __construct() {

        $this->key  = \Config::get('site.cache_key_prefix');

        $this->office_db = \Config::get('site.office_db');

        mssql_connect( \Config::get('site.online_db'), \Config::get('site.db_user'), \Config::get('site.db_pass') )
                or die("Couldn't connect to SQL Server on $server. Error: " . mssql_get_last_message());
    }

    /**
     * Get hotels
     *
     * @param int $state
     * @param int $town_from
     * @param int $region
     * @param int $destination_town
     * @param int $tour
     * @param int $spo
     * @param bit $order_by_name
     *
     * @return array
     */
    public function getHotels($state = 6, $town_from = 3, $tour = 131, $spo = NULL, $region = NULL, $destination_town = NULL, $order_by_name = NULL) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        $this->key = $this->key .  __FUNCTION__ . implode('.',  func_get_args());

        // if exists in cache return
        if(\Cache::has($this->key )) {
            \Debugbar::stopMeasure('time');
            return \Cache::get($this->key );
        }

        $stmt = mssql_init($this->office_db . '.dbo.up_WEB_3_search_Hotel');

        mssql_bind($stmt, '@STATE', $state, SQLINT4, false, $state == null ? true : false);
        mssql_bind($stmt, '@TOWNFROM', $town_from, SQLINT4, false, $town_from == null ? true : false);
        mssql_bind($stmt, '@REGION', $region, SQLINT4, false, $region == null ? true : false);
        mssql_bind($stmt, '@TOWN', $destination_town, SQLINT4, false, $destination_town == null ? true : false);
        mssql_bind($stmt, '@TOUR', $tour, SQLINT4, false, $tour == null ? true : false);
        mssql_bind($stmt, '@SPOG', $spo, SQLINT4, false, $spo == null ? true : false);
        mssql_bind($stmt, '@ORDER_BY_NAME', $order_by_name, SQLINT1, false, $order_by_name == null ? true : false);

        $query = mssql_execute($stmt);

        if (mssql_num_rows($query) > 0) {
            $result = array();

            while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                $result[] = $row;
            }

            \Cache::add($this->key , $result, 360);
            \Debugbar::stopMeasure('time');

            return $result;
        }

        \Debugbar::stopMeasure('time');

    	return NULL;
    }

   /**
     * Get details of the hotel
     *
     * @param int $id
     * @return array
     */
    public function getHotel($id) {

        // \Debugbar::startMeasure('time', __FUNCTION__);
        //
        // $this->key = $this->key .  __FUNCTION__ . implode('.',  func_get_args());
        //
        // // if exists in cache return
        // if(\Cache::has($this->key )) {
        //     \Debugbar::stopMeasure('time');
        //     return \Cache::get($this->key );
        // }

        $query = mssql_query("SELECT * FROM [$this->office_db].[dbo].[hotel] WHERE inc = $id");

        if (mssql_num_rows($query) > 0) {

            $result = mssql_fetch_array($query, MSSQL_ASSOC);

            // \Cache::add($this->key , $result, 360);
            // \Debugbar::stopMeasure('time');

            return $result;
        }

        // \Debugbar::stopMeasure('time');

        return NULL;
    }

    /**
     * @todo
     *
     *
     * @param int $town_from
     * @param int $state
     * @param smalldate $checkin
     * @param int $hotel
     * @param int $tour
     *
     * @return array
     */
    public function getSpecialOffers($town_from = NULL, $state = NULL, $checkin = NULL, $hotel = NULL, $tour = NULL) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        $this->key = $this->key .  __FUNCTION__ . implode('.',  func_get_args());

        // if exists in cache return
        if(\Cache::has($this->key )) {
            \Debugbar::stopMeasure('time');
            return \Cache::get($this->key );
        }

        $stmt = mssql_init($this->office_db . '.dbo.up_WEB_3_search_Spog');

        mssql_bind($stmt, '@STATE', $state, SQLINT4, false, $state == null ? true : false);
        mssql_bind($stmt, '@TOWNFROM', $town_from, SQLINT4, false, $town_from == null ? true : false);
        mssql_bind($stmt, '@CHECKIN', $checkin, SQLINT4, false, $checkin == null ? true : false);
        mssql_bind($stmt, '@HOTEL', $hotel, SQLINT4, false, $hotel == null ? true : false);
        mssql_bind($stmt, '@TOUR', $tour, SQLINT4, false, $tour == null ? true : false);

        $query = mssql_execute($stmt);

        if (mssql_num_rows($query) > 0) {

            $result = array();

            while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                $result[] = $row;
            }

            \Cache::add($this->key , $result, 360);
            \Debugbar::stopMeasure('time');

            return $result;
        }

        \Debugbar::stopMeasure('time');

        return NULL;
    }

   /**
     *
     * @param int $state
     * @param int $town_from
     * @param bit $order_by_name
     *
     * @return array
     */
    public function getTours($state, $town_from, $order_by_name = 0) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        // dd($state, $town_from);

        $this->key = $this->key .  __FUNCTION__ .  implode('.',  func_get_args());

        // if exists in cache return
        if(\Cache::has($this->key )) {
            return \Cache::get($this->key );
        }

        $stmt = mssql_init($this->office_db . '.dbo.up_WEB_3_search_Tour');

        mssql_bind($stmt, '@STATE', $state, SQLINT4, false, $state == null ? true : false);
        mssql_bind($stmt, '@TOWNFROM', $town_from, SQLINT4, false, $town_from == null ? true : false);

        mssql_bind($stmt, '@ORDER_BY_NAME', $order_by_name, SQLINT1, false, $order_by_name == null ? true : false);

        $query = mssql_execute($stmt);

        if (mssql_num_rows($query) > 0) {

            $result = array();

            while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                $result[] = $row;
            }

            \Cache::add($this->key , $result, 360);

            return $result;
        }

        \Debugbar::stopMeasure('time');

        return NULL;
    }

    /**
     * Get Departure dates from the calculated price list
     *
     * @param type $town_from
     * @param type $state
     * @param type $begin_date
     * @param type $spo
     * @return null
     */
    public function getDepartureDates($tour, $town_from, $state) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        $this->key = $this->key .  __FUNCTION__ . implode('.',  func_get_args());

        // if exists in cache return
        if(\Cache::has($this->key )) {
            return \Cache::get($this->key );
        }

        $query = mssql_query("SELECT DISTINCT CONVERT(varchar(10),[CheckIn], 121) 'checkin'  FROM [wwwbron].[dbo].[Cat_Claim] WHERE Tour = 131 AND townfrom = $town_from AND [state] = $state");

        if (mssql_num_rows($query) > 0) {

            $result = array();

            while ($row = mssql_fetch_array($query, MSSQL_NUM)) {
                $result[] = $row[0];
            }

            \Cache::add($this->key , $result, 360);

            return $result;
        } else {
            $result = array();
            return $result;
        }

        \Debugbar::stopMeasure('time');

        return NULL;
    }

    /**
     * @param int $state
     * @return array
     */
    public function getMealTypes($state) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        $this->key = $this->key .  __FUNCTION__ . $state;

        // if exists in cache return
        if(\Cache::has($this->key )) {

            return \Cache::get($this->key );
        }

        $query = mssql_query("SELECT gm.inc AS Inc,
            gm.name AS Name,
            gm.name AS LName
            FROM ADMIN_SITE.dbo.group_meal
            AS gm WHERE gm.State = $state ORDER BY Name");

        if (mssql_num_rows($query) > 0) {

            $result = array();

            while ($row = mssql_fetch_array($query)) {
                $result[] = $row;
            }

            \Cache::add($this->key , $result, 360);

            return $result;
        }

        \Debugbar::stopMeasure('time');

        return NULL;
    }

   /**
     * Get meal id from the meal group id
     *
     * @param int $meal_group
     * @return int
     */
    public function getMealId($meal_group) {
        \Debugbar::startMeasure('time', __FUNCTION__);

        $this->key = $this->key .  __FUNCTION__ . $meal_group;

        // if exists in cache return
        if(\Cache::has($this->key )) {
            \Debugbar::stopMeasure('time');
            return \Cache::get($this->key );
        }

        $query = mssql_query("SELECT meal from ADMIN_SITE.dbo.group_meal_list WHERE group_meal=$meal_group");

        if (mssql_fetch_row($query) !== FALSE) {

            $result = mssql_result($query, 0, 0);

            \Cache::add($this->key , $result, 360);
            \Debugbar::stopMeasure('time');

            return $result;
        }

        return null;
    }


   /**
     *
     * @TODO:   - Debug with different input parameters.
     *          - Where an SPO exists, return both SPO and normal price
     *
     * @param varchar $form
     * @param int $spo
     * @param int $owner
     * @param int $fr_place
     * @param smalldatetime $checkin_begin
     * @param smalldatetime $checkin_end
     * @param varchar $hotel_list
     * @param smallint $nights_min
     * @param smallint $nights_max
     * @param varchar $meal_list
     * @param smallint $adults
     * @param smallint $children
     * @param int $price_min
     * @param int $price_max
     * @param smallint $filter
     * @param smallint $price_page
     * @param smallint $sort_type
     * @param smallint $records_on_page
     * @param varchar $catalog_db
     * @param varchar $site_db
     * @param int $min
     * @param int $max
     * @param bit $show_request
     * @param int $tour
     * @param int $business_fr_place
     * @param int $business_min
     * @param int $business_max
     * @param int $max_records
     * @param int $town_from
     * @param int $state
     * @param int $child1_age
     * @param int $child2_age
     * @param int $child3_age
     * @param int $price_type
     *
     * @return array
     */
    public function getPrices($data) {
        \Debugbar::startMeasure('render', __FUNCTION__);
        $this->key = $this->key .  __FUNCTION__ . implode('.', $data);

        // if exists in cache return
        if(\Cache::has($this->key )) {

            $results = \Cache::get($this->key);

            \Debugbar::stopMeasure('render');

            return $results;
        }


        ini_set("memory_limit", "512M");

        $stmt = mssql_init( $this->office_db . '.dbo.up_WEB_3_search_Price');

        mssql_bind($stmt, '@FORM', $data['form'], SQLVARCHAR);
       // mssql_bind($stmt, '@SPOG', $data['spo'], SQLINT2, false, false);
        mssql_bind($stmt, '@OWNER', $data['owner'], SQLINT2);
       // mssql_bind($stmt, '@FRPLACE', $data['fr_place'], SQLINT2);
        mssql_bind($stmt, '@CHECKIN_BEG', $data['checkin_start'], SQLVARCHAR);
        mssql_bind($stmt, '@CHECKIN_END', $data['checkin_end'], SQLVARCHAR);
        mssql_bind($stmt, '@HOTELLIST', $data['hotel_list'], SQLVARCHAR);
        mssql_bind($stmt, '@NIGHTMIN', $data['nights_min'], SQLINT1);
        mssql_bind($stmt, '@NIGHTMAX', $data['nights_max'], SQLINT1);
        mssql_bind($stmt, '@MEALLIST', $data['meal_list'], SQLVARCHAR, false, $data['meal_list'] == null ? true : false);
        mssql_bind($stmt, '@ADULT', $data['adults'], SQLINT1);
        mssql_bind($stmt, '@CHILD', $data['children'], SQLINT1);
        mssql_bind($stmt, '@COSTMIN', $data['price_min'], SQLINT4, false, $data['price_min'] == null ? true : false);
        mssql_bind($stmt, '@COSTMAX', $data['price_max'], SQLINT4, false, $data['price_max']== null ? true : false);
        mssql_bind($stmt, '@FILTER', $data['filter'], SQLINT1);
        mssql_bind($stmt, '@PRICE_PAGE', $data['price_page'], SQLINT1);
        mssql_bind($stmt, '@SORT_TYPE', $data['sort_type'], SQLINT1);
        mssql_bind($stmt, '@RecOnPage', $data['records_on_page'], SQLINT4);
        mssql_bind($stmt, '@CATALOGDB', $data['catalog_db'], SQLVARCHAR);
        mssql_bind($stmt, '@DBSITE', $data['site_db'], SQLVARCHAR);
        // mssql_bind($stmt, '@MIN', $data['min'], SQLINT4);
        // mssql_bind($stmt, '@MAX', $data['max'], SQLINT4);
        // mssql_bind($stmt, '@SHOW_REQUEST', $data['show_request'], SQLBIT, false, $data['show_request'] == null ? true : false);
        mssql_bind($stmt, '@TOUR', $data['tour'], SQLINT4);
        // mssql_bind($stmt, '@BFRPLACE', $data['business_fr_place'], SQLINT4);
        // mssql_bind($stmt, '@BMIN', $data['business_min'], SQLINT4);
        // mssql_bind($stmt, '@BMAX', $data['business_max'], SQLINT4);
        mssql_bind($stmt, '@MAXRECORD', $data['max_records'], SQLINT4);
        mssql_bind($stmt, '@TOWNFROM', $data['town_from'], SQLINT4);
        mssql_bind($stmt, '@STATE', $data['state'], SQLINT4);
        mssql_bind($stmt, '@AGE1', $data['child1_age'], SQLINT2, false, $data['child1_age'] == null ? true : false);
        mssql_bind($stmt, '@AGE2', $data['child2_age'], SQLINT2, false, $data['child2_age'] == null ? true : false);
        mssql_bind($stmt, '@AGE3', $data['child3_age'], SQLINT2, false, $data['child3_age'] == null ? true : false);
        mssql_bind($stmt, '@PTYPE', $data['price_type'], SQLINT4, false, $data['price_type'] == null ? true : false);

        $result = mssql_execute($stmt);

        if (mssql_num_rows($result) > 0) {

            $return_val = array();

            while ($row = mssql_fetch_array($result, MSSQL_ASSOC)) {
                $return_val[] = $row;
            }

            // Save to cache
            \Cache::add($this->key , $return_val, 15);
            \Debugbar::stopMeasure('render');
            mssql_free_statement($stmt);
            return $return_val;
        }

        \Debugbar::stopMeasure('render');

        mssql_free_statement($stmt);

        return NULL;
    }

    /**
     * Returns prices without SPO
     *
     * @param array $claim
     * @return null
     */
    public function getNormalPrice($claim) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        $hotel = $claim['HotelInc'];
        $nights = $claim['Nights'];
        $tour = $claim['TourInc'];
        $ht_place = $claim['HtPlaceInc'];
        $meal = $claim['MealInc'];
        $room  = $claim['RoomInc'];
        $adult = $claim['Adult'];
        $child = $claim['Child'];
        $date = $claim['CheckIn'];

        // $checkin = $date[2] . "-" . $date[1] . "-" . $date[0];
        // $checkin = date("Y-m-d", strtotime($checkin));

        $this->key = $this->key .  __FUNCTION__ . $hotel . $nights . $tour . $ht_place . $meal . $room . $adult . $child . $date;

        // if exists in cache return
        if(\Cache::has($this->key )) {
            \Debugbar::stopMeasure('time');
            return \Cache::get($this->key );
        }

        $query = mssql_query("SELECT Price FROM [wwwbron].[dbo].[Cat_Claim] WHERE Hotel = $hotel AND HtPlace = $ht_place AND Meal = $meal AND room = $room
            AND adult = $adult AND child = $child  AND Tour = $tour AND Nights = $nights AND CheckIn = '$date' AND Spog = NULL");

        if (mssql_fetch_row($query) !== FALSE) {
            $result = mssql_result($query, 0, 0);

            \Cache::add($this->key , $result, 360);
            \Debugbar::stopMeasure('render');

            return $result;
        }

        \Debugbar::stopMeasure('render');

        return null;
    }

    /**
     *
     * @param type $id
     *
     * @return array
     */
    public function getState($id) {
        \Debugbar::startMeasure('time', __FUNCTION__);

        $this->key = $this->key .  __FUNCTION__ . $id;

        // if exists in cache return
        if(\Cache::has($this->key )) {
            \Debugbar::stopMeasure('time');
            return \Cache::get($this->key );
        }

        $query = mssql_query("SELECT * FROM $this->office_db.dbo.state WHERE inc=$id");

        if (mssql_num_rows($query) > 0) {

            $result = mssql_fetch_array($query, MSSQL_ASSOC);

            \Cache::add($this->key , $result, 360);
            \Debugbar::stopMeasure('render');

            return $result;
        }

        \Debugbar::stopMeasure('render');

        return NULL;

    }

    public function getDestinationTowns($state, $town_from, $region, $tour, $spo, $order_by_name)
    {
        \Debugbar::startMeasure('time', __FUNCTION__);


        // // $this->key = $this->key .  __FUNCTION__ . implode('.', $params);
        // $this->key = $this->key .  __FUNCTION__ . $params['state'] . $params['town_from'] . $params['tour'];
        // // if exists in cache return
        // if(\Cache::has($this->key )) {
        //     \Debugbar::stopMeasure('time');
        //     return \Cache::get($this->key );
        // }

        // $state = $params['state'];
        // $town_from = $params['town_from'];
        // $region = $params['region'];
        // $tour = $params['tourmssql_init($this->office_db . ''];
        // $spo = $params['spo'];
        // $order_by_name = $params['order_by_name'];

        $stmt = mssql_init($this->office_db . '.dbo.up_WEB_3_search_Townto');

        mssql_bind($stmt, '@STATE', $state, SQLINT2);
        mssql_bind($stmt, '@TOWNFROM', $town_from, SQLINT2);
        mssql_bind($stmt, '@REGION', $region, SQLINT2, false, $region == null ? true : false);
        mssql_bind($stmt, '@TOUR', $tour, SQLINT2);
        mssql_bind($stmt, '@SPOG', $spo, SQLINT2, false, $spo == null ? true : false);
        mssql_bind($stmt, '@ORDER_BY_NAME', $order_by_name, SQLINT1, false, $order_by_name == null ? true : false);

        $query = mssql_execute($stmt);

        if (mssql_num_rows($query) > 0) {

            $result = array();

            while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                $result[] = $row;
            }

            // \Cache::add($this->key , $result, 360);
            \Debugbar::stopMeasure('render');

            return $result;
        }

        \Debugbar::stopMeasure('render');

        return NULL;
    }

    public function getPartnersInfo()
    {


        $stmt = mssql_init($this->office_db . '.dbo.listAllPartners');

        $query = mssql_execute($stmt);

        if (mssql_num_rows($query) > 0) {

            $result = array();

            while ($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                $result[] = $row;
            }

            \Cache::add($this->key , $result, 360);
            \Debugbar::stopMeasure('render');

            return $result;
        }

        return NULL;

    }

    public function getPacketInfo($params)
    {
        \Debugbar::startMeasure('time', __FUNCTION__);

        $this->key = $this->key .  implode('.', $params);

        // if exists in cache return
        // if(\Cache::has($this->key )) {
        //     \Debugbar::stopMeasure('time');
        //     return \Cache::get($this->key );
        // }

        $stmt = mssql_query("EXEC " . $this->office_db . ".dbo.up_WEB_3_bron_PacketInfo @Cat_Claim = ". (binary)$params['Cat_Claim'] . ", @Partner = 0, @INTERNET_PARTNER = 0, @TOWNFROM = 3, @STATE = 6");


        if (mssql_num_rows($stmt) > 0) {

            $result = array();

            while ($row = mssql_fetch_array($stmt, MSSQL_ASSOC)) {
                $result[] = $row;
            }

            foreach($result as $res) {
                $result['normal_price'] = $this->getNormalPrice(array(
                    'HotelInc' => $res['HotelInc'],
                    'Nights' => $res['Nights'],
                    'TourInc' => $res['TourInc'],
                    'HtPlaceInc' => $res['HtPlaceInc'],
                    'MealInc' => $res['MealInc'],
                    'RoomInc' => $res['RoomInc'],
                    'Adult' => $res['Adult'],
                    'Child' => $res['Child'],
                    'CheckIn' => $res['CheckIn']
                ));

            }

            // \Cache::add($this->key , $result, 360);
            // \Debugbar::stopMeasure('render');

            return $result;
        }

        // \Debugbar::stopMeasure('render');

        return NULL;
    }

    public function getFlightInfo($params)
    {
       \Debugbar::startMeasure('time', __FUNCTION__);

        $this->key = $this->key .  implode('.', $params);

        // if exists in cache return
        if(\Cache::has($this->key )) {
            \Debugbar::stopMeasure('time');
            return \Cache::get($this->key );
        }

        $stmt = mssql_init($this->office_db . '.dbo.up_WEB_3_bron_PacketFreight');

        mssql_bind($stmt, '@Cat_Claim', $params['Cat_Claim'], SQLINT4);
        mssql_bind($stmt, '@TOUR', $params['TOUR'], SQLINT2, false, $params['TOUR'] == null ? true : false);
        mssql_bind($stmt, '@TOWNFROM', $params['TOWNFROM'], SQLINT2, false, $params['TOWNFROM'] == null ? true : false);
        mssql_bind($stmt, '@STATE', $params['STATE'], SQLINT2, false, $params['STATE'] == null ? true : false);

        $query = mssql_execute($stmt);

        if (mssql_num_rows($query) > 0) {

            $flights = array();

            while($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                $flights[] = $row;
            }

            \Cache::add($this->key , $flights, 360);
            \Debugbar::stopMeasure('render');

            // $flightDetails = flightDetails($array_push($params, ));


            return $flights;
        }

        \Debugbar::stopMeasure('render');

        return NULL;
    }

    private function flightDetails($params) {
       \Debugbar::startMeasure('time', __FUNCTION__);

        $this->key = $this->key .  implode('.', $params);

        // if exists in cache return
        if(\Cache::has($this->key )) {
            \Debugbar::stopMeasure('time');
            return \Cache::get($this->key );
        }

        $stmt = mssql_init($this->office_db . '.dbo.up_WEB_3_bron_FreightOnline');

        mssql_bind($stmt, '@CLAIM', $params['Cat_Claim'], SQLINT4);
        mssql_bind($stmt, '@TOUR', $params['TOUR'], SQLINT2, false, $params['TOUR'] == null ? true : false);
        mssql_bind($stmt, '@TOWNFROM', $params['TOWNFROM'], SQLINT2, false, $params['TOWNFROM'] == null ? true : false);
        mssql_bind($stmt, '@STATE', $params['STATE'], SQLINT2, false, $params['STATE'] == null ? true : false);

        $query = mssql_execute($stmt);

        if (mssql_num_rows($query) > 0) {

            $result = array();

            while($row = mssql_fetch_array($query, MSSQL_ASSOC)) {
                $result[] = $row;
            }

            \Cache::add($this->key , $result, 360);
            \Debugbar::stopMeasure('render');


            return $result;
        }

        \Debugbar::stopMeasure('render');

        return NULL;
    }



    public function getBestOffers() {
        $spos = $this->getSpecialOffers();
        $prices = array();

        foreach($spos as $k=>$spo) {

            $data = array(
                    'form' => \Config::get('site.form'),
                    'spo' => $spo['Inc'],
                    'owner' => \Config::get('site.owner'),
                    'fr_place' => \Config::get('site.fr_place'),
                    'checkin_start' => $spo['datebeg112'],
                    'checkin_end' => $spo['dateend112'],
                    'hotel_list' => NULL,
                    'nights_min' => 5,
                    'nights_max' => 10,
                    'meal_list' => NULL,
                    'adults' => \Config::get('site.adults_default'),
                    'children' => \Config::get('site.children_default'),
                    'price_min' => NULL,
                    'price_max' => NULL,
                    'filter' => 0,
                    'price_page' => 1,
                    'sort_type' => 1,
                    'records_page' => \Config::get('site.records_on_page'),
                    'catalog_db' => \Config::get('site.catalog_db'),
                    'admin_db' => \Config::get('site.admin_db'),
                    'min' => \Config::get('site.min'),
                    'max' => \Config::get('site.max'),
                    'show_request' => \Config::get('site.show_request'),
                    'tour' => \Config::get('site.tour_default'),
                    'business_fr_place' => \Config::get('site.business_fr_place'),
                    'business_min' => \Config::get('site.business_min'),
                    'business_max' => \Config::get('site.business_max'),
                    'max_records' => \Config::get('site.max_records'),
                    'town_from' => \Config::get('site.departure_default'),
                    'state' => \Config::get('site.state_default'),
                    'child1_age' => NULL,
                    'child2_age' => NULL,
                    'child3_age' => NULL,
                    'price_page' => NULL
            );

            $prices[] = $this->getPrices($data);
        }

        // $bestOffers = array();

        foreach($prices as $k=>$price) {
            if($price !== NULL) {
                $bestOffers[] = $this->filter_cheapest_rooms($price);
            }
        }
        //dd($prices);
        return $prices;
        // foreach($data as $row) {

        // }

        //return $spos;
    }


    public function filter_cheapest_rooms($tours) {

        $this->key = $this->key .  __FUNCTION__ . $tours[0]['CheckIn'] . $tours[0]['CheckIn'] . $tours[0]['RoomInc'] . $tours[0]['Nights'] . $tours[0]['HotelInc'];

        if(\Cache::has($this->key )) {
            return \Cache::get($this->key );
        }

        // Init empty array
        $cheapest = array();

        if($tours) {
            // Iterate tours
            foreach ($tours as $tour) {

                // If tour is found in array
                if (array_key_exists($tour['HotelInc'], $cheapest)) {

                    // If new price is cheaper than existing
                    if ($tour['Price'] < $cheapest[$tour['HotelInc']]['Price']) {

                        // Replace
                        $cheapest[$tour['HotelInc']] = $tour;
                    }
                } else { // Tour not in array
                    // Insert tour in array
                    $cheapest[$tour['HotelInc']] = $tour;
                }
            }
        }

        \Cache::add($this->key , $cheapest, 360);
        return $cheapest;
    }


    public function paging($input, $page, $show_per_page)
    {
        $start = ($page-1) * $show_per_page;
        $end = $start + $show_per_page;
        $count = count($input);

        // Conditionally return results
        if ($start < 0 || $count <= $start)
            // Page is out of range
            return array();
        else if ($count <= $end)
            // Partially-filled page
            return array('start' => $start+1, 'end' => $end, 'tours' => array_slice($input, $start));
        else
            // Full page
            return array('start' => $start+1, 'end' => $end, 'tours' => array_slice($input, $start, $end - $start));
    }

    public function filter_nights($tours, $nights)
    {
        $result = array();

        foreach($tours as $k=>$v) {
            if($v['Nights'] == $nights) {
                $result[$k] = $v;
            }
        }

        return $result;
    }

    /**
     * Takes as input the star count (1-5) and returns array of ids that fall in the given star category.
     * We use this to filter the hotels having the samo star_id in the set of returned values
     * If min is true the function will also return star categories above the indicated
     *
     * @param int $star_count
     * @return array
     */
    public function convert_stars($star_count) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        // Get all star types
        $samo_stars = $this->samo->get_all_stars();

        // Init empty
        $converted_stars = array();

        // For the given number of stars up to five (to include upper categories)
        for ($i = $star_count; $i <= 5; $i++) {

            // Iterate through star type
            foreach ($samo_stars as $star) {

                // If star category starts with given star category, add to return array
                if (substr($star['name'], 0) == $i) {
                    $converted_stars[] = $star['inc'];
                }
            }
        }

        \Debugbar::stopMeasure('time');

        return $converted_stars;
    }

}
