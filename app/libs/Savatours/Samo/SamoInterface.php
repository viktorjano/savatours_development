<?php namespace Savatours\Samo;

interface SamoInterface {

	/**
	 * Get tours from Samo
     * 
     * @param int $state
     * @param int $town_from
     * @param bit $order_by_name
     *
     * @return array
     */
	public function getTours($state, $town_from);


    /**
     * Get all hotels from Samo
     *
     * @return array
     */
    public function getHotels($state, $town_from, $tour, $spo=null, $region = NULL, $destination_town = NULL, $order_by_name = NULL);

	/**
     * Get available offers
     *
     * @param int $town_from
     * @param int $state
     * @param smalldate $checkin
     * @param int $hotel
     * @param int $tour
     * 
	 * @return array
	 */ 
	// public function getSpecialOffers();

    /**
     * Get Hotel by id
     * 
     * @param int $id
     * 
     * @return array
     */ 
    public function getHotel($id);


    public function getSpecialOffers();

    public function getDepartureDates($tour, $town_from, $state);

    public function getMealTypes($state);

    public function getMealId($meal_group);    

 //    *
 //     * Function to list all valid SPOS
 //     *
 //     * @param type $state
 //     * @param type $town_from
 //     * @param type $tour
 //     * @param type $hotel
 //     * @param type $checkin
 //     * @param type $order_by_name
 //     *
 //     * @return array
     
    // public function listSpecialOffers();

    // /**
    //  * Get destinations in point B when departing from point A
    //  *
    //  * @param int $state
    //  * @param int $region
    //  * @param int $spo
    //  * @param int $tour
    //  * @param int $town_from
    //  * @param bit $order_by_name
    //  *
    //  * @return array
    //  */
     public function getDestinationTowns($state, $region, $spo, $tour,$town_from, $order_by_name);

    /**
     * Get hotels
     *
     * @param int $state
     * @param int $town_from
     * @param int $region
     * @param int $destination_town
     * @param int $tour
     * @param int $spo
     * @param bit $order_by_name
     *
     * @return array
     */
    // public function getHotels($state, $town_from, $region, $destination_town, $tour, $spo, $order_by_name);

    // /**
    //  *
    //  * 
    //  * @TODO:   - Debug with different input parameters.
    //  *          - Where an SPO exists, return both SPO and normal price
    //  *
    //  * @param varchar $form
    //  * @param int $spo
    //  * @param int $owner
    //  * @param int $fr_place
    //  * @param smalldatetime $checkin_begin
    //  * @param smalldatetime $checkin_end
    //  * @param varchar $hotel_list
    //  * @param smallint $nights_min
    //  * @param smallint $nights_max
    //  * @param varchar $meal_list
    //  * @param smallint $adults
    //  * @param smallint $children
    //  * @param int $price_min
    //  * @param int $price_max
    //  * @param smallint $filter
    //  * @param smallint $price_page
    //  * @param smallint $sort_type
    //  * @param smallint $records_on_page
    //  * @param varchar $catalog_db
    //  * @param varchar $site_db
    //  * @param int $min
    //  * @param int $max
    //  * @param bit $show_request
    //  * @param int $tour
    //  * @param int $business_fr_place
    //  * @param int $business_min
    //  * @param int $business_max
    //  * @param int $max_records
    //  * @param int $town_from
    //  * @param int $state
    //  * @param int $child1_age
    //  * @param int $child2_age
    //  * @param int $child3_age
    //  * @param int $price_type
    //  *
    //  * @return array
    //  */
    public function getPrices($data);

    public function getNormalPrice($claim);

    public function getState($state_id);

    
}