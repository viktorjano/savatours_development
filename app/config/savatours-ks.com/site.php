<?php

return array(

	'locale' => 'sq',

	'multilanguage' => false,

	'facebook_url' => 'https://www.facebook.com/savatours.al',

	'site_name' => 'savatours_ks',

	'b2b_link'=> 'http://kosova.savatours.com',

	'phone1' => '+381 38 77 22 22',

	'phone2' => '+381 38 77 55 55',

	'mobile' => '+386 49 93 55 55',

	'email' => 'info@savatours-ks.com',

	'db_user' => 'web',

	'db_pass' => 'PMoY2i7)[lf={sXeyDso',

	'online_db' => 'serveralbania',

	'office_db' => 'sava',

	'catalog_db' => 'WWWBRON',

	'admin_db' => 'ADMIN_SITE',

	'cache_key_prefix' => 'ks_',

	'town_from' => 1782,

	'tour_default' => 117,

	'state_default' => 6,

	'departure_default' => 1782,

	'star_map' => array(
			    '3*' => '3',
			    '3*+' => '3',
			    '4*' => '4',
			    '4*Q' => '4',
			    '4*+' => '4',
			    '5*' => '5',
			    '5*DELUXE' => '5',
			    'HV1' => '5'
    ),

	'concept_map' => array(
		    'UAI' => 'Ultra All Inclusive',
		    'AI' => 'All Inclusive',
		    'HB' => 'Mengjes dhe darke',
		    'FB' => 'Mengjes, dreke, darke',
		    'BB' => 'Me mengjes',
		    'RO' => 'Vetem dhome'
    ),

	'form' => 'PRICE',

	'owner' => 1,

	'fr_place' => 2,

	'records_on_page' => 10000,

	'min' => 5,

	'max' => 10,

	'max_records' => 10000,

	'business_max' => 5,

	'business_min' => 1,

	'business_fr_place' => 3,

	'show_request' => 0,

	'min' => 5,

	'max' => 10,

	'nights_min_default' => 3,

	'nights_max_default' => 10,

	'child_age_options' => array(
	    '-1' => '---',
	    '0-1' => '0-1',
	    '2' =>  '2',
	    '3' =>  '3',
	    '4' =>  '4',
	    '5' =>  '5',
	    '6' =>  '6',
	    '7' =>  '7',
	    '8' =>  '8',
	    '9' =>  '9',
	    '10' =>  '10',
	    '11' =>  '11',
	    '12' =>  '12',
	    '13' =>  '13',
	    '14' =>  '14',
    ),

    'adults_default' => 2,

    'children_default' => 0,

	'durations' => array(
		'6-9' => Lang::get('searchform.about_1_week'),
		'3-6' => Lang::get('searchform.few_days'),
		'9-12' => Lang::get('searchform.about_10_days'),
		'12-16' => Lang::get('searchform.about_2_weeks')
	),

	'duration_default' => '7-9',

	'results_per_page' => 12,

	'lhc' => "var LHCChatOptions = {};
		LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500};
		(function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		var refferer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
		var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
		po.src = '//livehelp.savatours.com/index.php/alb/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(hide_offline)/true/(top)/350/(units)/pixels/(leaveamessage)/true/(department)/6?r='+refferer+'&l='+location;
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		})();"
);
