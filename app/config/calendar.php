<?php
return array(
	'monthNames' => array('Janar','Shkurt','Mars','Prill','Maj','Qershor','Korrik','Gusht','Shtator','Tetor','Nëntor','Dhjetor'),
	'monthNamesShort' => array('Jan','Shk','Mar','Pri','Maj','Qer','Kor','Gus','Sht','Tet','Nën','Dhj'),
	'dayNames' => array('E Diel','E Hënë','E Martë','E Mërkurë','E Enjte','E Premte','E Shtune'),
	'dayNamesShort' => array('Di','Hë','Ma','Më','En','Pr','Sh'),
	'dayNamesMin' => array('Di','Hë','Ma','Më','En','Pr','Sh'),
);