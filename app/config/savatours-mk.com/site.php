<?php

return array(
	
	'locale' => 'mk',

	'multilanguage' => true,

	'site_name' => 'savatours_mk',

	'b2b_link'=> 'http://partners.savatours-mk.com',

	'facebook_url' => 'https://www.facebook.com/savatours.com.mk',
	
	'phone1' => '+3892 3116 690',

	'phone2' => '+3892 3116 177', 

	'mobile' => '+389 75 411 103',

	'email' => 'info@savatours-mk.com',

	'db_user' => 'web',

	'db_pass' => '#u%Elm>_:6am>!u',

	'online_db' => 'macedonia',

	'office_db' => 'savatours_mk',

	'catalog_db' => 'WWWBRON',

	'admin_db' => 'ADMIN_SITE', 

	'cache_key_prefix' => 'mk_',

	'town_from' => 50,

	'tour_default' => 118,

	'state_default' => 6,

	'departure_default' => 50,

	'star_map' => array(
			    '3*' => '3',
			    '3*+' => '3',
			    '4*' => '4',
			    '4*Q' => '4',
			    '4*+' => '4',
			    '5*' => '5',
			    '5*DELUXE' => '5',
			    'HV1' => '5'
    ),

	'concept_map' => array(
		    'UAI' => 'Ultra All Inclusive',
		    'AI' => 'All Inclusive',
		    'HB' => 'Mengjes dhe darke',
		    'FB' => 'Mengjes, dreke, darke',
		    'BB' => 'Me mengjes',
		    'RO' => 'Vetem dhome'
    ),

	'form' => 'PRICE',
	
	'owner' => 1,
	
	'fr_place' => 2, 

	'records_on_page' => 10000,

	'min' => 5,

	'max' => 10,

	'max_records' => 10000,

	'business_max' => 5,

	'business_min' => 1,

	'business_fr_place' => 3,

	'show_request' => 0,

	'min' => 5,

	'max' => 10,

	'nights_min_default' => 3,

	'nights_max_default' => 10,

	'child_age_options' => array(
	    '-1' => '---',
	    '0-1' => '0-1',
	    '2' =>  '2',
	    '3' =>  '3',
	    '4' =>  '4',
	    '5' =>  '5',
	    '6' =>  '6',
	    '7' =>  '7',
	    '8' =>  '8',
	    '9' =>  '9',
	    '10' =>  '10',
	    '11' =>  '11',
	    '12' =>  '12',
	    '13' =>  '13',
	    '14' =>  '14',
    ),

    'adults_default' => 2,
    
    'children_default' => 0,

	'durations' => array(
		'6-9' => Lang::get('searchform.about_1_week'),
		'3-6' => Lang::get('searchform.few_days'),
		'9-12' => Lang::get('searchform.about_10_days'),
		'12-16' => Lang::get('searchform.about_2_weeks')
	),

	'duration_default' => '6-9',

	'results_per_page' => 12,

	'lhc' => "var LHCChatOptions = {};
		LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500,domain:'www.savatours.com'};
		(function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		var refferer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
		var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
		po.src = '//livehelp.savatours.com/index.php/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(hide_offline)/true/(top)/350/(units)/pixels/(leaveamessage)/true/(department)/4?r='+refferer+'&l='+location;
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		})();"		
);