<?php

return array(
	// 'bg' => 'http://brolik.com/images/backgroundClouds.jpg',
	'db_user' => 'web',

	'db_pass' => '#u%Elm>_:6am>!u',

	'online_db' => 'server',

	'catalog_db' => 'WWWBRON',

	'admin_db' => 'ADMIN_SITE', 

	'cache_key_prefix' => 'mk_',

	'town_from' => 3,

	'tour_default' => 131,

	'state_default' => 6,

	'departure_default' => 3,

	'star_map' => array(
			    '3*' => '3',
			    '3*+' => '3',
			    '4*' => '4',
			    '4*Q' => '4',
			    '4*+' => '4',
			    '5*' => '5',
			    '5*DELUXE' => '5',
			    'HV1' => '5'
    ),

	'concept_map' => array(
		    'UAI' => 'Ultra All Inclusive',
		    'AI' => 'All Inclusive',
		    'HB' => 'Mengjes dhe darke',
		    'FB' => 'Mengjes, dreke, darke',
		    'BB' => 'Me mengjes',
		    'RO' => 'Vetem dhome'
    ),

	'form' => 'PRICE',
	
	'owner' => 1,
	
	'fr_place' => 2, 

	'records_on_page' => 10000,

	'min' => 5,

	'max' => 10,

	'max_records' => 10000,

	'business_max' => 5,

	'business_min' => 1,

	'business_fr_place' => 3,

	'show_request' => 0,

	'min' => 5,

	'max' => 10,

	'nights_min_default' => 3,

	'nights_max_default' => 10,

	'child_age_options' => array(
	    '-1' => '---',
	    '0-1' => '0-1',
	    '2' =>  '2',
	    '3' =>  '3',
	    '4' =>  '4',
	    '5' =>  '5',
	    '6' =>  '6',
	    '7' =>  '7',
	    '8' =>  '8',
	    '9' =>  '9',
	    '10' =>  '10',
	    '11' =>  '11',
	    '12' =>  '12',
	    '13' =>  '13',
	    '14' =>  '14',
    ),

    'adults_default' => 2,
    
    'children_default' => 0,

	// Facebook Page URL
	'facebook_url' => 'https://www.facebook.com/savatours.al',

	'durations' => array(
		'6-9' => 'rreth 1 jave',
		'3-6' => 'disa dite',
		'9-12' => 'rreth 10 dite',
		'12-16' => 'rreth 2 jave'
	),

	'duration_default' => '7-9',
	
);