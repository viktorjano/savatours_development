<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('language/{lang}', function($lang)
{
	Session::put('lang', $lang);

	if ( isset( $_SERVER['HTTP_REFERER'] ) )
		return Redirect::back();
	else
		return Redirect::to('/');
});

Route::get('/', array('as'=> 'home', 'uses' => 'HomeController@home'));

Route::get('/pushime', 'SearchController@pushime');

Route::get('/pushime/hotel/', 'SearchController@accomodation');

/****iframe routes for partners****/

Route::get('/partner/{partner_id}', 'HomeIframeController@home');

Route::get('/pushime/partner/{partner_id}', 'PartnersIframeController@pushime');

Route::get ('/pushime/partner/{partner_id}/hotel/', 'PartnersIframeController@accomodation');

/*********************************/


Route::get('/pushime/rezervo/{claim_id}', 'SearchController@book');

Route::get('/spos', 'SPOController@bestOffers');

Route::get('image/{size}/{image}', function($size, $image_path)
{
	ini_set('max_execution_time', 300);

 	$d = explode("x", $size);

   	$img = Image::make($image_path)
					   ->fit((int)$d[0], (int)$d[1])
					   ->sharpen(5)
					   ->brightness(5);

    return $img->response('jpg');

})->where('image', '(.*)');

Route::get('/blog', 'BlogController@index');
Route::get('/blog/{slug}', 'BlogController@show');

Route::get('/hotel/{hotelid}', 'HotelController@show');

Route::get('/collection/{name}', 'CollectionController@index');

// Contact page
Route::get('contact', 'HomeController@contact');

Route::post('contact', function()
{
	$rules = array(
		// 'name' => 'required',
		// 'email' => 'required',
		// 'message' => 'required'
		);

	$validator = \Validator::make($data = \Input::all(), $rules);

	if($validator->fails()) {
		return \Redirect::back()->withErrors($validator)->withInput();
	}
	// Send email
	Mail::queue('emails.form_message', array('name' => $data['name'], 'email' => $data['email'], 'the_message' => $data['the_message']), function($message) use ($data)
	{
	    $message->to(Config::get('site.email'), 'Savatours')->subject('Mesazh i ri nga website!');
	});


	return Response::json(array('success'=> true, 'message' => Lang::get('contact.thank_you_message')), 200);

});

Route::get('/flydays', function()
{
	App::setlocale('en');

	$samo = new \Savatours\Samo\Samo;

	$key = Config::get('site.site_name') . Config::get('site.tour_default') . Config::get('site.departure_default') . Config::get('site.state_default');

    // if(Cache::has($key)) {
    // 	return Response::json(array('departures' => Cache::get($key)));
    // }

    $departures = $samo->getDepartureDates(Config::get('site.tour_default'), Config::get('site.departure_default'), Config::get('site.state_default'));

    sort($departures);

//       echo "<pre>";
	// dd($departures);
    foreach ($departures as $k=>$v) {
    	// $date = DateTime::createFromFormat('Y-m-d', $v);
    	// $v = $date->format('d/m/Y');
    	// if($v > date("d/m/Y", strtotime('today'))) {
        	$departures[$k] = date("d/m/Y", strtotime($v));
        // /}
    }

    // dd($departures);


    Cache::add($key, $departures, 60);

	return Response::json(array('departures' => $departures));
});

// Route::get('/hotele', function()
// {
// 	return View::make('pages.hotels');
// });

// ADMIN ROUTES
Route::group(array('prefix' => 'admin', 'before' => 'admin'), function()
{

	// Dashboard
	Route::get('/', 'Admin\HomeController@dashboard');

	// Apps
	Route::get('/apps/{app_name}', 'Admin\ApplicationController@index');


	Route::get('/hotels/json_photos/{hotel_id?}', 'Admin\HotelController@jsonPhotos');

	// Hotels
	Route::resource('hotels', 'Admin\HotelController');


	// Albums
	Route::post('/albums/setCover', 'Admin\AlbumController@ajaxSetCover');
	Route::post('/albums/order', 'Admin\AlbumController@ajaxOrder');

	// Photos
	Route::resource('photos', 'Admin\PhotoController');

	// Menus
	Route::resource('menu', 'Admin\MenuController');

	Route::post('menu/sort', function()
	{
		return Menu::sort(Input::get('order'));
	});

	// Pages
	Route::resource('pages', 'Admin\PageController');

	// Collections
	Route::resource('collections', 'Admin\CollectionController');

	// Banners
	Route::resource('banners', 'Admin\BannerController');

	Route::resource('settings', 'Admin\SettingsController');

	Route::resource('posts', 'Admin\PostsController');

	Route::post('/posts/restore/{id}', 'Admin\PostsController@restore');

	Route::resource('topoffers', 'Admin\TopOffersController');

	Route::resource('users', 'Admin\UsersController');

	Route::resource('partners', 'Admin\PartnersController');

	Route::post('image/crop', function()
	{
		if(Request::ajax()) {
			$img = Image::make(Input::file('image'));
			$img->crop(100, 100, 25, 25);

			return $img->response('jpg');
		}
	});

	// return View::make('admin.dashboard');
	// return 200;
});

Route::post('/login', 'Admin\UsersController@login');
Route::get('/logout', 'Admin\UsersController@logout');

// WISHLIST LOGIC
Route::group(array('prefix' => 'wishlist'), function()
{
	if(Request::ajax())
	{
		// Add to wishlist
		Route::get('/add/{hotelId}', function($hotelId)
		{
			if( ! Session::has('wishlist')) {
				Session::push('wishlist', $hotelId);
			} else if( ! in_array( $hotelId, Session::get('wishlist') ) ) {
				Session::push('wishlist', $hotelId);
			}

			return Response::json(array(
				'success' => true,
				'data' => Session::get('wishlist')
			));
		});

		// Get items in wishlist
		Route::get('/get', function()
		{
			return Response::json(array(
				'success' => true,
				'data' => Session::get('wishlist')
			));
		});

		// Remove specific item
		Route::get('/remove/{hotelId}', function($hotelId)
		{
			$wishlist = Session::get('wishlist');
			if(($key = array_search($hotelId, $wishlist)) !== false) {
			    unset($wishlist[$key]);
			}

			Session::forget('wishlist');

			foreach($wishlist as $wish) {
				Session::push('wishlist', $wish);
			}

			return Response::json(array(
				'success' => true,
				'data' => Session::get('wishlist')
			));
		});

		// Clear the wishlist
		Route::get('/clear', function()
		{
			Session::clear();
		});

	} else {
		Redirect::to('/');
	}
});

Route::resource('topoffers', 'TopOffersController');

Route::get('/login', function()
{
	return View::make('pages.login');
});


// Route::get('mongo', function() {
// 	// $top = New TopOfferMongo;
// 	// $top->title = 'Test 1';
// 	// $top->save();
// 	$top = TopOfferMongo::all();
// 	echo "<pre>";
// 	dd($top->first());
// });


Route::get('oferta-speciale/{spo}', function()
{
	/*
	$sandbox = new PHPSandbox\PHPSandbox;
	$sandbox->set_option('allow_escaping', true); // or $sandbox->validate_functions = false;
	$sandbox->execute('<?php $echo $i = 1; ?>');
	*/

	$samo = new \Savatours\Samo\Samo;

	dd($samo->getHotels(Config::get('site.state_default'), Config::get('site.town_from'), Config::get('site.tour_default'), $spo));
});

	Route::get('api/gettours', 'ApiController@gettours');
	Route::get('api/gethotels', 'ApiController@gethotels');
	Route::get('api/getflights', 'ApiController@getflights');
	Route::get('api/getprinttypes', 'ApiController@getprinttypes');
	Route::get('api/getstars', 'ApiController@getstars');
	Route::get('api/gettowns', 'ApiController@gettowns');
	Route::get('api/getpartners', 'ApiController@getpartners');
	Route::get('api/getrooms', 'ApiController@getrooms');
	Route::get('api/getroomsdesc', 'ApiController@getroomsdesc');
	Route::get('api/gethtpartpr', 'ApiController@gethtpartpr');
	Route::get('api/gethotelpr', 'ApiController@gethotelpr');
	Route::get('api/getregions', 'ApiController@getregions');
	Route::get('api/gethtroutes', 'ApiController@gethtroutes');
	Route::get('api/gethpicts', 'ApiController@gethpicts');
	Route::get('api/gethtplaces', 'ApiController@gethtplaces');
	Route::get('api/getstates', 'ApiController@getstates');
//Route::get('/partners', 'PartnersController@index');

// Route::get('/holiday_alert', 'TravelAlertController@create');

App::missing(function($exception)
{
    // return Response::view('errors.missing', array(), 404);
    return Redirect::to('/');
});
