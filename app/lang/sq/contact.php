<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Contact page Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'write_to_us' => 'Na shkruani',
	'contact_now' => 'Na kontaktoni',
	'name' => 'Emri juaj',
	'email' => 'Emaili juaj',
	'message' => 'Mesazhi juaj',
	'send' => 'Dergo',
	'visit_us' => 'Na vizitoni ne nje nga zyrat tona',
	'thank_you_message' => '<p>Thank you for your message. We will get back to you as soon as possible. If this is an emergent matter please contact us via the phone numbers or email address above.</p>'	
); 