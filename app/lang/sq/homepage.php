<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Homepage Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	"ask_question" => "Bëj një pyetje",
	'travel_advice' => 'Këshilla udhëtimi',
	'b2b_login' => 'B2B Login',
	'holidays' => 'Pushime',
	'tours' => 'Ture',
	'blog' => 'Blog',
	'contact' => 'Kontakt',
	'signup_for_offers' => 'Rregjistrohu për oferta',
	'personalized_offers' => 'Oferta të personalizuara, drejt e në inbox tuaj.',
	'signup' => 'Regjistrohuni',
	'customer_care' => 'Shërbimi i kujdesit për klientin.',
	'faq' => 'Pyetje të shpeshta',
	'how_to_book' => 'Si mund te rezervoj?',
	'payment_methods' => 'Mënyrat e pagesës',
	'follow_us' => 'Na ndiqni',
	'about_us' => 'Rreth nesh',
	'partners' => 'Partnerët',
	'career' => 'Karriera',
	'tos' => 'Termat dhe kushtet',
	'privacy_policy' => 'Rregullore e Privatësisë',
	'all' => 'Të gjitha',
	'get_results' => 'Rezultatet',
);
