<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Hotel Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'save' => 'ruaj',
	'package_price' => 'Cmimi i paketës',
	'accommodation' => 'akomodim',
	'change_room' => 'Ndrysho dhomën: ',
	'whats_included' => 'Çfarë përfshihet?',
	'flights' => 'Fluturimet',
	'transfers' => 'Transfertat',
	'have_a_question' => 'Keni pyetje?',
	'contact_customer_care' => 'Kontaktoni ekipin tonë të kujdesit ndaj klientit në numrin e mëposhtëm për tu konsultuar me një nga specialistët tanë të pushimeve.',
	'to_book_call' => 'Për të rezervuar këtë paketë telefono: ',
	'or_email_to' => 'Ose e-mail: ',
	'in_total' => 'në total',
	'save_wishlist' => 'Ruaj',
	'book' => 'Rezervo',
	'adults' => 'të rritur',
	'children' => 'fëmijë',	
	'number_of_nights' => 'Numri i netëve: ',
	'nights' => 'Net',
);
 