<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Search results Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'number_of_nights' => 'Numri i netëve',
	'night' => 'Nat',
	'nights' => 'Net',
	'days' => 'Ditë',
	'filter_results' => 'Fitlro rezultatet',
	'price' => 'Çmimi',
	'concept' => 'Koncepti',
	'town' => 'Qyteti',
	'results' => 'rezultate',
	'total_price' => 'Çmimi total',
	'departure' => 'Nisja',
	'adults' => 'të rritur',
	'children' => 'fëmijë',
	'child' => 'Fëmijë',
	'room' => 'Dhomë',
	'in' => 'në',
	'for' => 'për',
	'first_page' => 'E para',
	'last_page' => 'E fundit',
	'of' => 'nga',
	'clear_filters' => 'Pastro filtrat',
);
 