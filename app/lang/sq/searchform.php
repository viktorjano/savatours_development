<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Search form Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'destination' => 'Destinacioni',
	'hotel' => 'Hoteli',
	'departure_date' => 'Data e nisjes',
	'duration' => 'Kohëzgjatja',
	'adults' => 'Të rritur',
	'children' => 'Fëmijë',
	'children_ages' => 'Moshat e fëmijëve',
	'search' => 'Kërko',
	'choose_hotels' => 'Zgjidh hotelet',
	'days' => 'Ditë',
	'filter' => 'Filtro',
	'all' => 'Të gjitha',
	'none' => 'Asnjë',
	'about_1_week' => 'rreth 1 jave',
	'few_days' => 'disa dite',
	'about_10_days' => 'rreth 10 dite',
	'about_2_weeks' => 'rreth 2 jave',
	'of' => 'nga',
);