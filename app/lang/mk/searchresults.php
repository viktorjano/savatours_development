<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Search results Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'number_of_nights' => 'Број на ноќи',
	'night' => 'Ноќ',
	'nights' => 'Ноќи',
	'days' => 'Денови',
	'filter_results' => 'Филтрирани резултати',
	'price' => 'Цена',
	'concept' => 'Концепт',
	'town' => 'Град',
	'results' => 'Резултати',
	'total_price' => 'Цена во тотал',
	'departure' => 'Поаѓање',
	'adults' => 'Возрасни',
	'children' => 'Деца',
	'child' => 'Дете',
	'room' => 'Соба',
	'in' => 'Во',
	'for' => 'За',
	'first_page' => 'First page',
	'last_page' => 'Last page',
	'of' => 'of',
	'clear_filters' => 'Clear filters',
);
 