<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Homepage Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	"ask_question" => "Ask a question",
	'travel_advice' => 'Travel Advice',
	'b2b_login' => 'B2B Login',
	'holidays' => 'Holidays',
	'tours' => 'Tours',
	'blog' => 'Blog',
	'contact' => 'Contact',
	'filter' => 'Filter',
	'all' => 'All',
	'none' => 'None',
	'signup_for_offers' => 'Signup for offers',
	'personalized_offers' => 'Personalized offers, straight to your inbox',
	'signup' => 'Signup',
	'customer_care' => 'Customer care',
	'faq' => 'FAQ',
	'how_to_book' => 'How can I book?',	
	'payment_methods' => 'Payment methods',
	'follow_us' => 'Follow Us',
	'about_us' => 'About Us',
	'partners' => 'Partners',
	'career' => 'Career',
	'tos' => 'Terms & Conditions',
	'privacy_policy' => 'Privacy Policy',
	'get_results' => 'Get Results',
);
