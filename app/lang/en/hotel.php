<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Hotel Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'save' => 'Save',
	'package_price' => 'Package price',
	'accommodation' => 'Accomodation',
	'change_room' => 'Change room: ',
	'whats_included' => 'What\'s included?',
	'flights' => 'Flights',
	'transfers' => 'Transfers',
	'have_a_question' => 'Have a question?',
	'contact_customer_care' => 'Contact our customer care team via the number below to speak to one of our vaccation agents',
	'to_book_call' => 'To book this package call ',
	'or_email_to' => 'Or email to ',
	'in_total' => 'in total',
	'save_wishlist' => 'Save',
	'book' => 'Book',
	'adults' => 'adults',
	'children' => 'children',	
	'number_of_nights' => 'Number of nights: ',
	'nights' => 'Nights'	
);
 
