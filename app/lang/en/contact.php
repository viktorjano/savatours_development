<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Contact page Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'write_to_us' => 'Send us a message',
	'contact_now' => 'Call or email',
	'name' => 'Your name',
	'email' => 'Email',
	'message' => 'Your message',
	'send' => 'Send',
	'visit_us' => 'Visit us at one of our locations',
	'thank_you_message' => '<p>Thank you for your message. We will get back to you as soon as possible. If this is an emergent matter please contact us via the phone numbers or email address above.</p>'	
); 