<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Search results Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'number_of_nights' => 'Number of nights',
	'night' => 'Night',
	'nights' => 'Nights',
	'days' => 'Days',
	'filter_results' => 'Filter results',
	'price' => 'Price',
	'concept' => 'Concept',
	'town' => 'Town',
	'results' => 'results',
	'total_price' => 'Price',
	'departure' => 'Departure',
	'adults' => 'adults',
	'children' => 'children',
	'child' => 'Child',
	'room' => 'Room',
	'in' => 'in',
	'for' => 'for',
	'first_page' => 'First page',
	'last_page' => 'Last page',
	'of' => 'of',
	'clear_filters' => 'Clear filters',
);
 