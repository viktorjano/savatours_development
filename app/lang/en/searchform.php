<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Search form Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'destination' => 'Destination',
	'hotel' => 'Hotel',
	'departure_date' => 'Departure date',
	'duration' => 'Duration',
	'adults' => 'Adults',
	'children' => 'Children',
	'children_ages' => 'Children ages',
	'search' => 'Search',
	'choose_hotels' => 'Choose hotels',
	'filter' => 'Filter',
	'days' => 'Days',
	'none' => 'None',	
	'about_1_week' => 'about 1 week',
	'few_days' => 'few days',
	'about_10_days' => 'about 10 week',
	'about_2_weeks' => 'about 2 weeks',
	'of' => 'од',	
);