<?php

class Photo extends Eloquent {
	protected $guarded = array();

	public static $rules = array(

	);

	/**
	 * Relation with Album
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */	
	public function albums()
	{
		return $this->belongsToMany('Album');
	}
}
