<?php
Use \Illuminate\Database\Eloquent\SoftDeletingTrait;

class Post extends \Eloquent {
	protected $fillable = [];
	use SoftDeletingTrait;

	public static $rules = array(
			'title' => 'required|min:15|max:255',
			'image' => 'image',
			'image_alt'	=> 'max:255',
			'youtube_video' => 'max:255',
			'summary' => 'required',
			'content' => 'required',
			'link_text' => 'max:255',
			'link_url' => 'max:255|url',
			'is_sticky' => 'boolean',
			'slug' => 'required|unique:posts|unique:pages',
			'meta_description' => 'required|max:155',
			'meta_keywords' => 'required',
			'status' => 'in:PUBLISHED,DRAFT'
			);

	public function Photos() {
		return $this->belongsToMany('Photo');
	}
}