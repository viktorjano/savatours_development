<?php

class Collection extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'title' => 'required:unique',
		'slug'	=> 'required:unique',
		'description' =>'required'
	);

	/**
	 * Relations with Album
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function albums()
	{
		return $this->belongsToMany('Album');
	}

	/**
	 * Relations with Hotel
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function hotels()
	{
		return $this->belongsToMany('Hotel');
	}
	
}
