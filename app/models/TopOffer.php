<?php

class TopOffer extends \Eloquent {
    
	protected $fillable = [];

	public function top_offers($id)
	{

		$topOffer = \TopOffer::find($id);

		$date = explode("/", $topOffer->checkin);
		
		$checkin = $date[2] . $date[1] . $date[0];

		$samo = new \Savatours\Samo\Samo;		
		
        $selected_meals = array();

        $state = \Config::get('site.state_default') ;
        $data['meals'] = $samo->getMealTypes($state);

        foreach ($data['meals'] as $meal) {
            $selected_meals[] = $meal['Inc'];
        }
        $meals = array();

        foreach ($selected_meals as $k => $v) {
            $meals[] = $samo->getMealId($v);
        }

        $data['selected_meals'] = $selected_meals;

        $meal_list = implode(',', $meals);		

        $hotel_list = implode(",", json_decode($topOffer->hotels));

        $data['tours'] = $samo->getPrices(array(
                'form' => \Config::get('site.form'), 
                'spo' => null, 
                'owner' => \Config::get('site.owner'), 
                'fr_place' => \Config::get('site.fr_place'), 
                'checkin_start' => $checkin, 
                'checkin_end' => $checkin, 
                'hotel_list' => $hotel_list, 
                'nights_min' => $topOffer->nights, 
                'nights_max' => $topOffer->nights+4, 
                'meal_list' => $meal_list, 
                'adults' => 2, 
                'children' => 0, 
                'price_min' => null, 
                'price_max' => null, 
                'filter' => 0, 
                'price_page' => 1, 
                'sort_type' => 1, 
                'records_page' => \Config::get('site.records_on_page'), 
                'catalog_db' => \Config::get('site.catalog_db'), 
                'admin_db' => \Config::get('site.admin_db'), 
                'min' => \Config::get('site.min'), 
                'max' => \Config::get('site.max'), 
                'show_request' => \Config::get('site.show_request'), 
                'tour' => \Config::get('site.tour_default'), 
                'business_fr_place' => \Config::get('site.business_fr_place'), 
                'business_min' => \Config::get('site.business_min'), 
                'business_max' => \Config::get('site.business_max'), 
                'max_records' => \Config::get('site.max_records'), 
                'town_from' => \Config::get('site.departure_default'), 
                'state' => \Config::get('site.state_default'), 
                'child1_age' => null, 
                'child2_age' => null, 
                'child3_age' => null, 
                'price_page' => 1
        ));		

		$data['tours'] = $samo->filter_cheapest_rooms($data['tours']);

		// echo "<pre>";
		// dd($data['tours']);

		$data['date'] = explode("/", strftime("%A/%-e/%B", strtotime("$checkin")));        

        $data['stars'] = \Config::get('site.star_map');

        return View::make('widgets.top_offers', $data);

	}	
}