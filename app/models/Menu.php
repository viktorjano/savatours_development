<?php

class Menu extends Eloquent {
	protected $guarded = array();

	public static $rules = array();

    public function parent() {

        return $this->hasOne('menu', 'id', 'parent_id');
    }

    public function children() {

        return $this->hasMany('menu', 'parent_id', 'id');

    }  

    public static function tree() {

        return static::with(implode('.', array_fill(0, 100, 'children')))->where('parent_id', '=', 0)->get();

    }

	// Update the menu
	public static function sort($order)
	{
		$order = Input::get('order');

		foreach($order as $k=>$v) {
			Menu::find($v)->update(array('order' => $k+1));
		}

		return Response::json('ok');
	}
}
