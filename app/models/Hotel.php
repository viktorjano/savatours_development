<?php

class Hotel extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'samo_id'	=> 'unique:hotels|required|numeric',
		// 'slogan'	=> '',
		// 'features'	=> '',
		// 'short_description'	=> '',
		// 'full_description'	=> ''
	);

	/**
	 * Relations with Album
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function albums()
	{
		return $this->belongsToMany('Album');
	}

	/**
	 * Relations with Collection
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function collections()
	{
		return $this->belongsToMany('Collection');
	}

	public static function getAll()
	{
		$samo = new \Savatours\Samo\Samo;		
		$hotels = $samo->getHotels(Config::get('site.state_default'), Config::get('site.town_from'), Config::get('site.tour_default'));

		return $hotels;
	}


	/**
	* Get the hotel by reference Id
	*
	*/
	public static function get($id)
	{
		$samo = new \Savatours\Samo\Samo;
		
		return $samo->getHotel($id);
	}
}
