<?php

class Tour extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'samo_id' => 'unique|numeric',
		'title' => 'required',
		'slug' => 'required',
		// 'slogan' => 'text',
		// 'features' => 'features',
		// 'short_description' => 'text',
		// 'full_description' => 'full description',
		'price' => 'numeric',
		// 'published' => '0',
		// 'spo' => '0',
	);

	/**
	* Relation to albums
	*
	* @return \Illuminate\Database\Eloquent\Relations\BelongsToMany 
	*/
	public function albums() 
	{
		return $this->belongsToMany('Album');
	}

}
