<?php

class Album extends Eloquent {
	protected $guarded = array();

	public static $rules = array();

	/**
	 * Relation with Photo
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function photos()
	{
		return $this->belongsToMany('Photo')->orderBy('sort_order', 'asc');
	}

	/**
	 * Relation with Tour
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function tours()
	{
		return $this->belongsToMany('Tour');
	}

	/**
	 * Relation with Hotel
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function hotels() 
	{
		return $this->belongsToMany('Hotel');
	}

	/**
	 * Relation with Page
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function pages() 
	{
		return $this->belongsToMany('Page');
	}

	/**
	 * Relation with Location
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function locations()
	{
		return $this->belongsToMany('Location');
	}

	/**
	 * Relation with Collection
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	 public function collections() 
	 {
	 	return $this->belongsToMany('Collection');
	 }

}
