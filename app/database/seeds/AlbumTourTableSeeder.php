<?php

class AlbumTourTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('albumtour')->truncate();

		$albumtour = array(
			array('album_id' => 1, 'tour_id' => 1),
			array('album_id' => 2, 'tour_id' => 2),			
		);

		// Uncomment the below to run the seeder
		DB::table('album_tour')->insert($albumtour);
	}

}
