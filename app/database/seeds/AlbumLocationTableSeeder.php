<?php

class AlbumLocationTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('albumlocation')->truncate();

		$albumlocation = array(
			array('album_id' => 1,'location_id' => 1),
			array('album_id' => 2,'location_id' => 1),
		);

		// Uncomment the below to run the seeder
		DB::table('album_location')->insert($albumlocation);
	}

}
