<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('SentryTableSeeder');
		$this->call('PagesTableSeeder');
		$this->call('BannersTableSeeder');
		$this->call('LocationsTableSeeder');
		$this->call('ToursTableSeeder');
		$this->call('AlbumsTableSeeder');
		$this->call('PhotosTableSeeder');
		$this->call('AlbumphotoTableSeeder');
		$this->call('HotelsTableSeeder');
		$this->call('AlbumHotelTableSeeder');
		$this->call('AlbumLocationTableSeeder');
		$this->call('CollectionTableSeeder');
		$this->call('CollectionHotelTableSeeder');
		$this->call('AlbumTourTableSeeder');
		$this->call('MenusTableSeeder');
	}

}