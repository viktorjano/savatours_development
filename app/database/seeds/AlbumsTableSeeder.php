<?php

class AlbumsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('albums')->truncate();

		$albums = array(
			array('name' => 'Album 1','description' => 'desc1'),
			array('name' => 'Album 2','description' => 'desc2'),
		);

		// Uncomment the below to run the seeder
		DB::table('albums')->insert($albums);
	}

}
