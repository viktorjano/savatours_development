<?php

class CollectionHotelTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('collectionhotel')->truncate();

		$collectionhotel = array(
			array('hotel_id' => 1,'collection_id' => 1),
			array('hotel_id' => 2,'collection_id' => 1),

		);

		// Uncomment the below to run the seeder
		DB::table('collection_hotel')->insert($collectionhotel);
	}

}
