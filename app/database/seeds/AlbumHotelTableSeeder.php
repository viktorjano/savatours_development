<?php

class AlbumHotelTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('albumhotel')->truncate();

		$albumhotel = array(
			array('album_id' => 1,'hotel_id' => 1),
			array('album_id' => 2,'hotel_id' => 1),
		);

		// Uncomment the below to run the seeder
		DB::table('album_hotel')->insert($albumhotel);
	}

}
