<?php

class ToursTableSeeder extends Seeder {

	public function run()
	{
		// wipe
		DB::table('tours')->truncate();
		
		// load
		$tours = array(
			array(
				'samo_id' => '1',
				'name' => 'Dubrovnik',
				'slug' => 'dubrovnik',
				'slogan' => 'Bregdeti Dalmat',
				'features' => 'features',
				'short_description' => 'description',
				'full_description' => 'full description',
				'price' => '99',
				'published' => '0',
				'spo' => '0',
			),
			array(
				'samo_id' => '2',
				'names' => 'Antalja',
				'slug' => 'antalja',
				'slogan' => 'pushime_ne_antalja',
				'features' => 'features',
				'short_description' => 'description',
				'full_description' => 'full description',
				'price' => '265',
				'published' => '0',
				'spo' => '0',
			),			

		);

		// run
		DB::table('tours')->insert($tours);
	}

}
