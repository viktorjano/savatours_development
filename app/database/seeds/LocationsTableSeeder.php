<?php

class LocationsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('locations')->truncate();

		$locations = array(
			array('samo_id' => 1, 'name' => 'Tirane', 'slug'  => 'slug', 'short_description' => 'dsafad', 'full_description' => 'adsfdasfas'),		
			array('samo_id' => 4, 'name' => 'Antalja', 'slug'  => 'slug2', 'short_description' => 'short description', 'full_description' => 'adsfdasfas')		
		);

		// Uncomment the below to run the seeder
		DB::table('locations')->insert($locations);
	}

}
