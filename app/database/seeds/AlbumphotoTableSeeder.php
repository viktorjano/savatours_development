<?php

class AlbumphotoTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('albumphoto')->truncate();

		$albumphoto = array(
			array('album_id' => '1','photo_id' => '1'),
			array('album_id' => '1','photo_id' => '2'),
		);

		// Uncomment the below to run the seeder
		DB::table('album_photo')->insert($albumphoto);
	}

}
