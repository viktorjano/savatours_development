<?php

class HotelsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('hotel')->truncate();

		$hotel = array(
			array(
				'samo_id' => 1, 
				'slug' => 'is_unique',				
				'slogan'  => 'slogan', 
				'features' => '["asdf"]', 
				'short_description' => 'short_description', 
				'resort_description' => 'resort_description',
				'rooms_description' => 'rooms_description',
				'beach_pools_description' => 'beach_pools_description',
				'concept_description' => 'concept_description',
			),
			array(
				'samo_id' => 2, 
				'slug' => 'hotel_slug',				
				'slogan'  => 'slogan', 
				'features' => '["feature 1"]', 
				'short_description' => 'short_description', 
				'resort_description' => 'resort_description',
				'rooms_description' => 'rooms_description',
				'beach_pools_description' => 'beach_pools_description',
				'concept_description' => 'concept_description',
			),
		);

		// Uncomment the below to run the seeder
		DB::table('hotels')->insert($hotel);
	}

}
