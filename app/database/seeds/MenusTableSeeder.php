<?php

class MenusTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		//DB::table('menus')->truncate();

		$menus = array(
			array('title' => 'Dashboard', 'url' => '#', 'parent_id' => null, 'order' => 0, 'group' => 'admin_menu' ),
			array('title' => 'Hotels', 'url' => '#', 'parent_id' => 0, 'order' => 0, 'group' => 'admin_menu' ),
			array('title' => 'Hotel descriptions', 'url' => 'admin/hotels', 'parent_id' => 1, 'order' => 0, 'group' => 'admin_menu' ),						
			array('title' => 'Hotel collections', 'url' => 'admin/collections', 'parent_id' => 1, 'order' => 0, 'group' => 'admin_menu' ),						
			array('title' => 'Settings', 'url' => 'admin/settings', 'parent_id' => 0, 'order' => 0, 'group' => 'admin_menu' )			
		);

		// Uncomment the below to run the seeder
		DB::table('menus')->insert($menus);
	}

}
