<?php

class CollectionTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('collection')->truncate();

		$collections = array(
			array('name' => 'Select', 'slug' => 'select', 'description' => 'Hotele te perzgjedhura nga Savatours ne baze te kritereve strikte per sherbimin, facilitetet etj', 'color' => '#000'),
			array('name' => 'Couples', 'slug' => 'couples', 'description' => 'Hotele te perzgjedhura nga Savatours ne baze te kritereve strikte per sherbimin, facilitetet etj', 'color' => '#fff'),
		
		);

		// Uncomment the below to run the seeder
		DB::table('collections')->insert($collections);
	}

}
