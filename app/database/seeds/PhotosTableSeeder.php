<?php

class PhotosTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('photos')->truncate();

		$photos = array(
			array('filename' => 'image1.jpg',	'title' => 'Image 1', 'description' => 'Image 1 description', 'sort_order' => '0'),
			array('filename' => 'image2.jpg',	'title' => 'Image 2', 'description' => 'Image 2 description', 'sort_order' => '0'),		
		);

		// Uncomment the below to run the seeder
		DB::table('photos')->insert($photos);
	}

}
