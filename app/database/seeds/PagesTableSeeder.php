<?php

class PagesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('pages')->truncate();

		$pages = array(
			array('layout' => 'tours','title' => 'All Tours','slug' => 'all_tours','body' => 'sdfadsfsa'),
			array('layout' => 'default','title' => 'Blog','slug' => 'Blog','body' => 'sdfadsfsa')
		);

		// Uncomment the below to run the seeder
		DB::table('pages')->insert($pages);
	}

}
