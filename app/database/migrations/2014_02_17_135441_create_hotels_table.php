<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHotelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hotels', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('samo_id')->unique();
			$table->string('slug')->unique();
			$table->string('slogan')->nullable();
			$table->text('features')->nullable();
			$table->text('short_description')->nullable();
			$table->text('resort_description')->nullable();
			$table->text('rooms_description')->nullable();
			$table->text('beach_pools_description')->nullable();
			$table->text('concept_description')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hotels');
	}

}
