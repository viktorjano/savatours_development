<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->string('image')->nullable();
			$table->string('image_alt')->nullable();
			$table->string('youtube_video')->nullable();
			$table->text('summary');
			$table->text('content');
			$table->string('link_text')->nullable();
			$table->string('link_url')->nullable();
			$table->boolean('is_sticky');
			$table->string('slug')->unique();
			$table->string('page_title')->nullable();
			$table->text('meta_description');
			$table->text('meta_keywords');
			$table->enum('status', array('DRAFT', 'PUBLISHED'))->default('DRAFT');
			$table->datetime('published_date')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
