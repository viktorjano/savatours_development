<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateToursTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tours', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('samo_id');
			$table->string('name');
			$table->string('slug');
			$table->string('slogan')->nullable();
			$table->text('features')->nullable();
			$table->text('short_description')->nullable();
			$table->text('full_description')->nullable();
			$table->integer('price');
			$table->boolean('published')->default(0);
			$table->boolean('spo')->defalut(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tours');
	}

}
