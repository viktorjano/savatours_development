<?php

// banner
Widget::register('banner', function() {

    return View::make('includes.slider');
});

Widget::register('homepage_section', function($name) {
	
	return View::make('widgets.'. $name);
});

Widget::register('homepage_full', function($name, $data = null) {

	return View::make('widgets.' . $name, compact('data', $data));	
}); 

// Widget::register('news-rotator', function ($name, $data= null) {
// 	return View::make('widgets.' . $name, compact('data', $data));	

// });