<?php

class HomePageTest extends TestCase {

	/**
	* Test that Homepage returns 200 OK
	*
	* @return void
	*/
	public function testResponseOK()
	{
		$this->client->request('GET', '/');				
		$this->assertResponseOk();
	}

}