<?php

class TourTest extends TestCase {

	/**
	* Test that Tours returns 200 OK
	*
	* @return void
	*/
	public function testResponseOK()
	{
		$this->client->request('GET', '/tours');				
		$this->assertResponseOk();
	}

}