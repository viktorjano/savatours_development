<?php 

class CollectionController extends SearchableController {

	public function index($name) {

		$this->data['name'] = $name;
		
		$this->data['collection'] = Collection::where('slug', '=', $name)->first();

		return View::make('pages.' . $name . '-collection' . '_' . Config::get('app.locale'), $this->data);
	}
}