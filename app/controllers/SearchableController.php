<?php

class SearchableController extends BaseController {

		public function __construct()
		{
	        $lang = Session::get('lang');

	        switch ($lang) {
	        	case 'mk':
	        		setlocale(LC_ALL, 'mk_MK.utf8');
	        		break;
	        	case 'sq':
	        		setlocale(LC_ALL, 'sq_AL.utf8');
	        		break;
	        	default:
	        		setlocale(LC_ALL, 'sq_AL.utf8');
	        		break;
	        }


			$today = date("Y-m-d");

			$samo = new \Savatours\Samo\Samo;

	        $this->data['form']['tours'] = $samo->getTours(Config::get('site.state_default'), Config::get('site.departure_default'));
	        //dd($data);
	        $departures = $samo->getDepartureDates($this->data['form']['tours'][0]['TourInc'], Config::get('site.departure_default'), Config::get('site.state_default'));

	       	$all_hotels = $samo->getHotels(Config::get('site.state_default', 6), Config::get('site.departure_default', 3), NULL, NULL, NULL, NULL, NULL);
			if(isset($all_hotels)) {
		       	$this->data['hotels'] = null;

		       	foreach($all_hotels as $hotel) {
		       		$this->data['hotels'][$hotel['HotelInc']] = $hotel['HotelName'];
		       	}
	       	}
	        !isset($all_hotels) ? $this->data['hotels'] = array() : $this->data['hotels'];
	        $this->data['destinationTowns'] = $samo->getDestinationTowns(Config::get('site.state_default'), Config::get('site.departure_default'), NULL, Config::get('site.tour_default'), NULL, 0);

	        if(isset($departures)) sort($departures);

	        $data['departures'] = $departures;

	        if( $checkin = Input::get('checkin') ) {


				$date = DateTime::createFromFormat('d/m/Y', $checkin);
				$checkin = $date->format('m/d/y');

		        $week = array();

		        for( $i = -3; $i <= 3; $i++ ) {
		            $week[date("d/m/Y", strtotime("$checkin +$i days"))] =array(
		            	'day' => explode("/", strftime("%A/%-e/%B", strtotime("$checkin +$i days"))),
    					'flyDay' => in_array(date("Y-m-d", strtotime("$checkin +$i days")), $departures),
    					// 'current' => strtotime($checkin) == strtotime($)
	            	);
		        }

		        $this->data['week'] = $week;
	        }

			$this->data['form']['durations'] = Config::get('site.durations');

			if (Session::has('lastSearch') && !empty(Session::get('lastSearch'))) {

				$lastSearch = Session::get('lastSearch');

				if(isset($lastSearch['hotels'])) {
					$this->data['selectedHotels'] =  $lastSearch['hotels'];
				} else {
					$this->data['selectedHotels'] = null;
				}

		        $this->data['form']['t'] = $lastSearch['t'];

		        if($lastSearch['checkin'] <= date("d/m/Y", strtotime('today'))) {
		        	$this->data['form']['checkin'] = date("d/m/Y", strtotime($departures[0]));
		        } else {
		        	$this->data['form']['checkin'] = $lastSearch['checkin'];
		        }

	       		$this->data['form']['duration'] = $lastSearch['duration'];
		        $this->data['form']['adults'] = $lastSearch['adults'];
		        $this->data['form']['children'] = $lastSearch['children'];
		        $this->data['form']['child1_age'] = $lastSearch['child1_age'];
		        $this->data['form']['child2_age'] = $lastSearch['child2_age'];
		        $this->data['form']['child3_age'] = $lastSearch['child3_age'];

			} else {
	     		$this->data['selectedHotels'] = null;
		        $this->data['form']['t'] = Config::get('site.tour_default', 131);
	       		$this->data['form']['checkin'] = date("d/m/Y", strtotime(isset($departures[0]) ? $departures[0] : 'today'));
	       		$this->data['form']['duration'] = Config::get('site.duration_default');
		        $this->data['form']['adults'] = Config::get('site.adults_default');
		        $this->data['form']['children'] = Config::get('site.children_default');
		        $this->data['form']['child1_age'] = $this->data['form']['child2_age'] = $this->data['form']['child3_age'] = -1;
			}
        }

		protected function setupLayout()
		{
		    if(!is_null($this->layout))
		    {
		        $this->layout = View::make($this->layout, $this->data);
		    }
		}
}
