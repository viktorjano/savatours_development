<?php

class TopOffersController extends SearchableController {

	/**
	 * Display a listing of the resource.
	 * GET /topoffers
	 *
	 * @return Response
	 */
	public function index()
	{
		$topOffers = TopOffer::all();
		
		//dd($topOffers);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /topoffers/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /topoffers
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /topoffers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$topOffer = \TopOffer::find($id);

		$date = explode("/", $topOffer->checkin);
		
		$checkin = $date[2] . $date[1] . $date[0];

		$samo = new \Savatours\Samo\Samo;		
		
        $selected_meals = array();

        $state = \Config::get('site.state_default') ;
        $this->data['meals'] = $samo->getMealTypes($state);

        foreach ($this->data['meals'] as $meal) {
            $selected_meals[] = $meal['Inc'];
        }
        $meals = array();

        foreach ($selected_meals as $k => $v) {
            $meals[] = $samo->getMealId($v);
        }

        $this->data['selected_meals'] = $selected_meals;

        $meal_list = implode(',', $meals);		

        $hotel_list = implode(",", json_decode($topOffer->hotels));

        $this->data['tours'] = $samo->getPrices(array(
                'form' => \Config::get('site.form'), 
                'spo' => null, 
                'owner' => \Config::get('site.owner'), 
                'fr_place' => \Config::get('site.fr_place'), 
                'checkin_start' => $checkin, 
                'checkin_end' => $checkin, 
                'hotel_list' => $hotel_list, 
                'nights_min' => $topOffer->nights, 
                'nights_max' => $topOffer->nights+4, 
                'meal_list' => $meal_list, 
                'adults' => Config::get('site.adults_default'), 
                'children' => 0, 
                'price_min' => null, 
                'price_max' => null, 
                'filter' => 0, 
                'price_page' => 1, 
                'sort_type' => 1, 
                'records_page' => \Config::get('site.records_on_page'), 
                'catalog_db' => \Config::get('site.catalog_db'), 
                'admin_db' => \Config::get('site.admin_db'), 
                'min' => \Config::get('site.min'), 
                'max' => \Config::get('site.max'), 
                'show_request' => \Config::get('site.show_request'), 
                'tour' => \Config::get('site.tour_default'), 
                'business_fr_place' => \Config::get('site.business_fr_place'), 
                'business_min' => \Config::get('site.business_min'), 
                'business_max' => \Config::get('site.business_max'), 
                'max_records' => \Config::get('site.max_records'), 
                'town_from' => \Config::get('site.departure_default'), 
                'state' => \Config::get('site.state_default'), 
                'child1_age' => null, 
                'child2_age' => null, 
                'child3_age' => null, 
                'price_page' => 1
        ));		

		$this->data['tours'] = $samo->filter_cheapest_rooms($this->data['tours']);

		// echo "<pre>";
		// dd($this->data['tours']);

		$this->data['date'] = explode("/", strftime("%A/%-e/%B", strtotime("$checkin")));        

        $this->data['stars'] = \Config::get('site.star_map');

        $this->data['form']['urlParameters'] = array(
            't' => Config::get('site.tour_default'),
            // 'checkin' => $checkin,
            'duration' => $topOffer->nights . '-'. ($topOffer->nights+4),
            'adults' => Config::get('site.adults_default'),
            'children' => Config::get('site.children_default'),
            'child1_age' => -1,
            'child2_age' => -1,
            'child3_age' => -1,
            'from' => Config::get('site.departure_default'),
            's' => Config::get('site.state_default'),

        );

        return View::make('partials.home.hotel_list', $this->data);

	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /topoffers/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /topoffers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /topoffers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}