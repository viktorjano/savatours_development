<?php

class HotelController extends SearchableController {

	public function show($hotelId) {

		$samo = new Savatours\Samo\Samo;

		// Populate form defaults
		$data['form'] = Input::get();


		$hotel = new Hotel;
		$hotelSamoData = $hotel->get($hotelId);

		// get the hotel descriptions
		$hotelDescriptions = $hotel->where('samo_id', '=', $hotelId)->first();

       	$data['hotels'] = $samo->getHotels(Config::get('site.state_default'), Config::get('site.departure_default'), NULL, NULL, NULL, NULL, NULL);

		$data['form']['tours'] = $samo->getTours(Config::get('site.state_default'), Config::get('site.departure_default'));

        $departures = $samo->getDepartureDates(131, Config::get('site.departure_default'), Config::get('site.state_default'));
        sort($departures);

        $data['form']['checkin'] = date("d/m/Y", strtotime($departures[0]));

        Session::push('wishlist', array($hotelId));
        $data['form']['parameters'] = Input::except(array('_token'));

		// return View::make('pages.hotel', $data);
	}
}
