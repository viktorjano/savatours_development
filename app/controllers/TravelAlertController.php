<?php

class TravelAlertController extends \SearchableController {

	/**
	 * Display a listing of the resource.
	 * GET /newsletter
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /newsletter/create
	 *
	 * @return Response
	 */
	public function create()
	{
		// $samo = new \Savatours\Samo\Samo;

		// // $destination_towns = $samo->getDestinationTowns(
		// // 		Config::get('state_default'), 
		// // 		Config::get('town_from'),
		// // 		null,
		// // 		Config::get('tour_default'),
		// // 		null,
		// // 		0
		// // 	);
		// // echo "<pre>";
		// dd($this->data['destinationTowns']);
		return View::make('pages.holiday_alert_register', $this->data);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /newsletter
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /newsletter/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /newsletter/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /newsletter/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /newsletter/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}