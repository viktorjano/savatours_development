<?php

class SPOController extends SearchableController {

	public function bestOffers()
	{
		// $this->data['banner'] = Banner::get()->first();
		
		$samo = new \Savatours\Samo\Samo;

		$this->data['tours'] = $samo->getBestOffers();		
		
		return View::make('pages.bestOffers', $this->data);
	}

}