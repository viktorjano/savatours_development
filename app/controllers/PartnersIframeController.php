<?php

class PartnersIframeController extends SearchableController {

    public function pushime($partner_id) 
    {   
        Debugbar::startMeasure('pushime', __FUNCTION__);
        
        if ( ! Session::has('lastSearch') OR Session::get('lastSearch') != Input::get()) {
            Session::put('lastSearch', Input::get());
        }

        $data['partner_id'] = $partner_id;

        $data['form'] = Input::get();

        $data['form']['parameters'] = Input::except(array('_token'));
        $data['form']['durations'] = Config::get('site.durations');

        $samo = new \Savatours\Samo\Samo;

        $data['form']['tours'] = $samo->getTours(null, Config::get('site.departure_default'));

        $town_from = Input::get('from');

        $tour = intval(Input::get('t'));
        
        $state = Config::get('site.state_default') ;

        $data['meals'] = $samo->getMealTypes($state);

        // Check if the meal filter is set and create and set the meal checkbox accordingly
        if (Input::get('concept')) {
            $meal_groups = Input::get('concept');
            $meals = array();
            foreach ($meal_groups as $k => $v) {
                $meals[] = $samo->getMealId($v);
            }
            // Create meal_list for search
            $meal_list = implode(',', $meals);
            $data['selected_meals'] = $meal_groups;
        } else {
            $selected_meals = array();
            foreach ($data['meals'] as $meal) {
                $selected_meals[] = $meal['Inc'];
            }
            $meals = array();

            foreach ($selected_meals as $k => $v) {
                $meals[] = $samo->getMealId($v);
            }

            $data['selected_meals'] = $selected_meals;

            $meal_list = implode(',', $meals);
        }


        $all_hotels = $samo->getHotels($state, $town_from, NULL, NULL, NULL, NULL, NULL);

        $hotel_list = null;

        if(Input::has('towns')) {
            $hotel_list = array_filter($all_hotels, function($v) { 
                return in_array($v['TownInc'], explode(',', Input::get('towns')));
            });

            if ( $hotel_list !== null ) { 
                $hotel_list = implode(',', array_map( function($el) { return $el['HotelInc'];
              }, $hotel_list));

            }
        }
        
        /*if(Input::get('hotels') !== null) {
            $hotel_list = implode(',', Input::get('hotels'));
        }*/

        // echo "<pre>";
        // dd($hotel_list);


        $nights = explode('-', Input::get('duration', Config::get('site.duration_default')));
        $nights_min = $nights[0];
        $nights_max = $nights[1];
        $form = Config::get('site.form');
        $spo = null;
        $owner = Config::get('site.owner'); // Owner of the flight seats
        $fr_place = Config::get('site.fr_place');
        $date = explode('/', Input::get('checkin'));
        $checkin_start = $date[2] . $date[1] . $date[0];
        
        if ( Input::has('checkin_end')) {
            $date = explode('/', Input::get('checkin_end'));
            $checkin_end = $date[2] . $date[1] . $date[0];
        } else {
            $checkin_end = $checkin_start;    
        }
        
        
        $adults = Input::get('adults');
        $children = Input::get('children');
        $price_min = null;
        $price_max = null;
        $filter = 0;
        $price_page = 1;
        $sort_type = 1;
        $records_on_page = Config::get('site.records_on_page');
        $catalog_db = Config::get('site.catalog_db');
        $admin_db = Config::get('site.admin_db');
        $min = Config::get('site.min');
        $max = Config::get('site.max');
        $show_request = Config::get('site.show_request');
        $business_fr_place = Config::get('site.business_fr_place');
        $business_min = Config::get('site.business_min');
        $business_max = Config::get('site.business_max');
        $max_records = Config::get('site.max_records');

        if (Input::get('child1_age') == false || Input::get('child1_age') == -1) {

            $child1_age = null;
        } else {

            $child1_age = Input::get('child1_age');
        }

        if (Input::get('child2_age') == false || Input::get('child2_age') == -1) {

            $child2_age = null;
        } else {

            $child2_age = Input::get('child2_age');
        }

        if (Input::get('child3_age') == false || Input::get('child3_age') == -1) {

            $child3_age = null;
        } else {

            $child3_age = Input::get('child3_age');
        }
            
        $price_type = null;
        
        // Search
        $data['tours'] = $samo->getPrices(array(
                'form' => $form, 
                'spo' => $spo, 
                'owner' => $owner, 
                'fr_place' => $fr_place, 
                'checkin_start' => $checkin_start, 
                'checkin_end' => $checkin_end, 
                'hotel_list' => $hotel_list, 
                'nights_min' => $nights_min, 
                'nights_max' => $nights_max, 
                'meal_list' => $meal_list, 
                'adults' => $adults, 
                'children' => $children, 
                'price_min' => $price_min, 
                'price_max' => $price_max, 
                'filter' => $filter, 
                'price_page' => (int) $price_page, 
                'sort_type' => $sort_type, 
                'records_page' => $records_on_page, 
                'catalog_db' => $catalog_db, 
                'admin_db' => $admin_db, 
                'min' => $min, 
                'max' => $max, 
                'show_request' => $show_request, 
                'tour' => $tour, 
                'business_fr_place' => $business_fr_place, 
                'business_min' => $business_min, 
                'business_max' => $business_max, 
                'max_records' => $max_records, 
                'town_from' => $town_from, 
                'state' => $state, 
                'child1_age' => $child1_age, 
                'child2_age' => $child2_age, 
                'child3_age' => $child3_age, 
                'price_page' => $price_type
        ));      

        // dd($data['tours']);

        if ( $data['tours']  == null ) {
        
            return View::make('pages.noResults', $this->data);
        
        } else {
            $data['nights_options'] = array_keys(arrayGroup($data['tours'], 'Nights'));
            
            if(Input::has('nights')) {
                $data['tours'] = $samo->filter_nights($data['tours'], Input::get('nights'));
            }
            
            Debugbar::startMeasure('processResults');

            $key = $form . $spo . $owner . $fr_place . $checkin_start . $checkin_end . $hotel_list . $nights_min
                    . $nights_max . $meal_list . $adults . $children . $price_min . $price_max . $filter . $price_page
                    . $sort_type . $records_on_page . $catalog_db . $admin_db . $min . $max . $show_request . $tour
                    . $business_fr_place . $business_min . $business_max . $max_records . $town_from . $state . $child1_age
                    . $child3_age . $child3_age . $price_page;


            $data['tours'] = $this->_filter_cheapest_rooms($data['tours']);

            $data['concepts'] = Config::get('site.concept_map');
            $data['stars'] = Config::get('site.star_map');

            // // Get state data from state id
            $data['state'] = $samo->getState($state);
                    
            if(Cache::has('covers' . $key)) {
                $data['covers'] = Cache::get('covers' . $key);
            } else {
                $data['covers'] = $this->_get_cover_images($data['tours']);
                Cache::add('covers' . $key, $data['covers'], 360);            
            } 

            $data['normal_prices'] = $this->get_normal_prices($data['tours']);      
            
            $data['destinationTowns'] = $samo->getDestinationTowns($state, $town_from, NULL, $tour, NULL, 0);
           

            $data['selectedTowns'] = null;
            
            if ( Input::has('towns') ) $data['selectedTowns'] = explode(',', Input::get('towns'));

            $data['selectedHotels'] = Input::get('hotels');

            // Hotels for search form            
            foreach($all_hotels as $hotel) {
                $data['hotels'][$hotel['HotelInc']] = $hotel['HotelName'];
            }        
            
            Debugbar::startMeasure('getCollections');
            if(Cache::has('collections' . $key)) {
                $data['collections'] = Cache::get('collections' . $key);
            } else {
                $data['collections'] = $this->_get_collections($data['tours']);
                Cache::put('collections' . $key, $data['collections'], 360);            
            }
            Debugbar::stopMeasure('getCollections');

            Debugbar::stopMeasure('processResults');
            Debugbar::stopMeasure('pushime');
            $data['week'] = $this->data['week'];

            $all = $data['tours'];

            $per_page = Input::get('limit', Config::get('site.results_per_page'));
            $page = Input::get('page', 1);
            $paged = $samo->paging($all, $page, $per_page);
            $data['tours'] = $paged['tours'];
                
            $data['number_of_results'] = count($all);
            $data['number_of_pages'] = ceil(count($all)/$per_page);
            $data['result_start'] = $paged['start'];
            $data['result_end'] = $paged['end'];
        
            $data['features'] = $this->_get_features($data['tours']);

            return View::make('pages.resultsPackageIframe', $data);
        }   
    }



    // Accomodation
    public function accomodation($partner_id) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        $partner =  DB::table('partners')
            ->where('id', '=', $partner_id)
            ->get();

        /*$all = \Partner::all();

        $partner = \Partner::where('id', '=', $partner_id)->get();*/

        $data['partner_id'] = $partner[0]->id;
        $data['partner_id_email'] = $partner[0]->email;
        $data['partner_id_phone'] = $partner[0]->phone;

        $data['form'] = Input::all();

        $samo = new \Savatours\Samo\Samo;

        $data['form']['tours'] = $samo->getTours(null, Config::get('site.departure_default'));
        $data['form']['durations'] = Config::get('site.durations');
        $data['form']['parameters'] = Input::except(array('_token', 'claim'));
        $town_from = Input::get('from');

        $tour = intval(Input::get('t', Config::get('site.tour_default')));
        
        $state = Input::get('s', Config::get('site.state_default'));

        $data['meals'] = $samo->getMealTypes($state);

        // Check if the meal filter is set and create and set the meal checkbox accordingly
        if (Input::get('concept')) {

            $meal_groups = Input::get('concept');

            $meals = array();

            foreach ($meal_groups as $k => $v) {

                $meals[] = $samo->getMealId($v);
            }

            // Create meal_list for search
            $meal_list = implode(',', $meals);

            $data['selected_meals'] = $meal_groups;
        } else {

            $selected_meals = array();

            foreach ($data['meals'] as $meal) {
                $selected_meals[] = $meal['Inc'];
            }

            $meals = array();

            foreach ($selected_meals as $k => $v) {
                $meals[] = $samo->getMealId($v);
            }

            $data['selected_meals'] = $selected_meals;

            $meal_list = implode(',', $meals);
        }

        // // Check if the stars filter has been set, if so apply filter
        // if (Input::get('stars')) {

        //     $all_hotels = $samo->getHotels($state, $town_from, NULL, NULL, NULL, NULL, NULL);

        //     $filtered_hotels = $samo->filter_hotels_by_stars($all_hotels, $this->_convert_stars((int) Input::get('stars'), TRUE));

        //     $hotel_ids = $this->_selected_hotel_ids($filtered_hotels);

        //     $hotel_list = implode(',', $hotel_ids);
        // } else {
        //     $hotel_list = null;
        // }

        $hotel_list = null;
        
        if(Input::get('hotels') !== null) {
            $hotel_list = Input::get('hotels');
        }

        $nights = explode('-', Input::get('duration'));
        $nights_min = $nights[0];
        $nights_max = $nights[1];

        $form = Config::get('site.form');
        $spo = null;
        $owner = Config::get('site.owner'); // Owner of the flight seats
        $fr_place = Config::get('site.fr_place');

        $date = explode('/', Input::get('checkin'));
        $checkin_start = $date[2] . $date[1] . $date[0];

        //$convert_date = explode('/', Input::get('latest_return'));
        //$checkin_end = $convert_date[2] . $convert_date[1] . $convert_date[0];
        
        $checkin_end = $checkin_start;

        // Duhet pare se ka nje problem.
//        $checkin_end = date("Ymd", strtotime($convert_date . " - " . $nights_min . " days"));
//        $checkin_end = $latest_return;

        $adults = Input::get('adults');
        $children = Input::get('children');
        $price_min = null;
        $price_max = null;
        $filter = 0;

        $price_page = 1;

        $sort_type = 1;
        $records_on_page = Config::get('site.records_on_page');
        $catalog_db = Config::get('site.catalog_db');
        $admin_db = Config::get('site.admin_db');
        $min = Config::get('site.min');
        $max = Config::get('site.max');
        $show_request = Config::get('site.show_request');
        $business_fr_place = Config::get('site.business_fr_place');
        $business_min = Config::get('site.business_min');
        $business_max = Config::get('site.business_max');
        $max_records = Config::get('site.max_records');

        if (Input::get('child1_age') == false || Input::get('child1_age') == -1) {

            $child1_age = null;
        } else {

            $child1_age = Input::get('child1_age');
        }

        if (Input::get('child2_age') == false || Input::get('child2_age') == -1) {

            $child2_age = null;
        } else {

            $child2_age = Input::get('child2_age');
        }

        if (Input::get('child3_age') == false || Input::get('child3_age') == -1) {

            $child3_age = null;
        } else {

            $child3_age = Input::get('child3_age');
        }

        $price_type = null;

        $data['tours'] = $samo->getPrices(array(
                'form' => $form, 
                'spo' => $spo, 
                'owner' => $owner, 
                'fr_place' => $fr_place, 
                'checkin_start' => $checkin_start, 
                'checkin_end' => $checkin_end, 
                'hotel_list' => $hotel_list, 
                'nights_min' => $nights_min, 
                'nights_max' => $nights_max, 
                'meal_list' => $meal_list, 
                'adults' => $adults, 
                'children' => $children, 
                'price_min' => $price_min, 
                'price_max' => $price_max, 
                'filter' => $filter, 
                'price_page' => (int) $price_page, 
                'sort_type' => $sort_type, 
                'records_page' => $records_on_page, 
                'catalog_db' => $catalog_db, 
                'admin_db' => $admin_db, 
                'min' => $min, 
                'max' => $max, 
                'show_request' => $show_request, 
                'tour' => $tour, 
                'business_fr_place' => $business_fr_place, 
                'business_min' => $business_min, 
                'business_max' => $business_max, 
                'max_records' => $max_records, 
                'town_from' => $town_from, 
                'state' => $state, 
                'child1_age' => $child1_age, 
                'child2_age' => $child2_age, 
                'child3_age' => $child3_age, 
                'price_page' => $price_type
            ));

        $claim =  Input::get('claim', $data['tours'][0]['Cat_Claim']);   

        $data['packet_info'] = $samo->getPacketInfo(array(
                        'Cat_Claim' => $claim, 
                        'Partner' => NULL, 
                        'INTERNET_PARTNER' => NULL,
                        'TOWNFROM' => NULL,
                        'STATE' => NULL
                        ));

        $data['flight_info'] = $samo->getFlightInfo(array('Cat_Claim' => $claim));

        $data['concepts'] = Config::get('site.concept_map');

        $data['stars'] = Config::get('site.star_map');


        // // Get state data from state id
        $data['state'] = $samo->getState($state);
    
        $data['normal_prices'] = $this->get_normal_prices($data['tours']);      
        
        $hotel = new Hotel;
        
        $data['descriptions'] = $hotel
                        ->where('samo_id', '=', $data['tours'][0]['HotelInc'])
                        ->first();

        if ($data['descriptions'] !== NULL) {
            $data['descriptions']->features = json_decode($data['descriptions']->features);
            $data['album'] = $data['descriptions']->albums->first();
        }

        $data['selectedHotels'] = Input::get('hotels');

        $all_hotels = $samo->getHotels($state, $town_from, NULL, NULL, NULL, NULL, NULL);

        foreach($all_hotels as $hotel) {
            $data['hotels'][$hotel['HotelInc']] = $hotel['HotelName'];
        }        

        $data['inWishlist'] = false;

        if(Session::has('wishlist')) {
            $data['inWishlist'] = in_array($data['tours'][0]['HotelInc'], Session::get('wishlist'));
        } 

        $data['tours'] = arrayGroup($data['tours'], 'Nights');
        $data['nights_options'] = array_keys($data['tours']);
        $data['first_key'] = current($data['nights_options']);        

        \Debugbar::stopMeasure('time');

        return View::make('pages.hotelIframe', $data);           
    }


    // Accomodation
    public function book($claim_id) {

        $samo = new \Savatours\Samo\Samo;
        $packet = $samo->getPacketInfo(array(50677179));
        // dd($packet);
        return View::make('pages.booking', $data);
    }

    /***
     *
     * EVERYTHING BELOW SHOULD MOVE TO A MODEL, LIBRARY OR HELPER
     *
     *
     */

    private function _get_collections($tours) {
        \Debugbar::startMeasure('time', __FUNCTION__);

        // $key = __FUNCTION__ + $tours['HotelInc'];
        
        $collections = null;

        foreach($tours as $tour) {
            $hotel = new Hotel;
            $hotel = $hotel->where('samo_id', '=', $tour['HotelInc'])->first();

            if($hotel) 
                $collection = $hotel->collections()->first();
            else 
                $collection = false;

            if($collection)
                $collections[$tour['HotelInc']] = array('name' => $collection->name, 'color' => $collection->color);
            else
                $collections[$tour['HotelInc']] = array('name' => 'Standart', 'color' => '');

        }    

        \Debugbar::stopMeasure('time');

        return $collections;
    }


    private function _get_cover_images($tours) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        $images = array();


        foreach ($tours as $tour) {

            $hotel = Hotel::where('samo_id', '=', $tour['HotelInc'])->first();
            $album = null;
            
            if($hotel) {
                $album = $hotel->albums->first();  
            }          

            if($album) {
                $photo = Photo::find($album->cover);

                if($photo) {
                    $images[$tour['HotelInc']] = $photo->filename;
                } else {
                    $images[$tour['HotelInc']] = null;
                }
            }

            else {
                $images[$tour['HotelInc']] = null;
            }
        }

        \Debugbar::stopMeasure('time');
        
        return $images;
    }


    /* dodadena funkcija */

    private function _get_features($tours) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        $features = array();

        foreach ($tours as $tour) {

            $hotelFeats = Hotel::select('features')->where('samo_id', '=', $tour['HotelInc'])->first();
            /*$album = null;*/
            // dd($tour['HotelInc']);
            //var_dump($hotelFeats);
            if($hotelFeats) {
                $features[$tour['HotelInc']] = json_decode($hotelFeats->features);  
            } else {
                $features[$tour['HotelInc']] = null;
            }
        }

        if($features) {
            return $features;
        }

        else {
            $features = null;
        }

        \Debugbar::stopMeasure('time');
        
    }

    /* dodadena funkcija */

    /**
     * reeturn all checkin dates for the selected tours
     *
     * @param type $tours
     * @return type
     */
    private function _get_checkins($tours) {
        \Debugbar::startMeasure('time', __FUNCTION__);

        $checkins = array();

        foreach ($tours as $tour) {
            if (!array_key_exists($tour['CheckIn'], $checkins)) {
                $checkins[$tour['CheckIn']] = $tour['CheckIn'];
            }
        }

        \Debugbar::stopMeasure('time');

        return $checkins;
    }

    /**
     * return all accomodation types for the selected tours.
     *
     * @param array $tours
     *
     * @return array
     */
    private function _get_accomodations($tours = NULL) {
       
        \Debugbar::startMeasure('time', __FUNCTION__);

        $accomodations = array();

        if ($tours !== NULL) {
            foreach ($tours as $tour) {
                if (!array_key_exists($tour['HtPlaceInc'], $accomodations)) {
                    $accomodations[$tour['HtPlaceInc']] = $tour['HtPlaceName'];
                }
            }
            return $accomodations;
        }

        \Debugbar::stopMeasure('time');

        return null;
    }

    /**
     * return all concepts (meal types) for the selected tours. used for filtering
     *
     * @param type $tours
     * @return null
     */
    private function _get_concepts($tours = NULL) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        $concepts = array();

        if ($tours !== NULL) {
            foreach ($tours as $tour) {
                if (!array_key_exists($tour['MealInc'], $concepts)) {
                    $concepts[$tour['MealInc']] = $tour['MealName'];
                }
            }
            return $concepts;
        }

        \Debugbar::stopMeasure('time');

        return NULL;
    }

    /**
     * Convert meal (concept IDs)
     *
     * @param array $meals
     * @return array
     */
    private function _convert_meal_ids($meals) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        $converted_ids = array();

        foreach ($meals as $meal) {
            $converted_ids[] = $this->samo->get_meal_id($meal['Inc']);
        }

        \Debugbar::stopMeasure('time');

        return $converted_ids;
    }

    /**
     * Takes as input the star count (1-5) and returns array of ids that fall in the given star category.
     * We use this to filter the hotels having the samo star_id in the set of returned values
     * If min is true the function will also return star categories above the indicated
     *
     * @param int $star_count
     * @return array
     */
    private function _convert_stars($star_count) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        // Get all star types
        $samo_stars = $this->samo->get_all_stars();

        // Init empty
        $converted_stars = array();

        // For the given number of stars up to five (to include upper categories)
        for ($i = $star_count; $i <= 5; $i++) {

            // Iterate through star type
            foreach ($samo_stars as $star) {

                // If star category starts with given star category, add to return array
                if (substr($star['name'], 0) == $i) {
                    $converted_stars[] = $star['inc'];
                }
            }
        }

        \Debugbar::stopMeasure('time');

        return $converted_stars;
    }

    /**
     * Extract hotel IDs from hotels array
     *
     * @param type $filtered_hotels
     * @return type
     */
    private function _selected_hotel_ids($filtered_hotels) {

        \Debugbar::startMeasure('time', __FUNCTION__);

        // Init empty
        $hotel_ids = array();

        // Iterate hotels
        foreach ($filtered_hotels as $hotel) {

            // Add hotel id to return array
            $hotel_ids[] = $hotel['HotelInc'];
        }

        \Debugbar::stopMeasure('time');

        return $hotel_ids;
    }

    /**
     * Return the cheapest price for each hotel.
     *
     * @param array $tours
     * @return array
     */
    private function _filter_cheapest_rooms($tours) {
        // Init empty array
        $cheapest = array();

        // Iterate tours
        foreach ($tours as $tour) {

            // If tour is found in array
            if (array_key_exists($tour['HotelInc'], $cheapest)) {

                // If new price is cheaper than existing
                if ($tour['Price'] < $cheapest[$tour['HotelInc']]['Price']) {

                    // Replace
                    $cheapest[$tour['HotelInc']] = $tour;
                }
            } else { // Tour not in array
                // Insert tour in array
                $cheapest[$tour['HotelInc']] = $tour;
            }
        }

        return $cheapest;
    }

    /**
     * Filters the tours based on latest return date
     *
     * @todo test correctnes
     *
     * @param array $tours
     * @param date $latest_return
     * @return array
     */
    private function _filter_latest_return($tours, $latest_return) {

        // Change format of inputed date to SQLSMALLDATE
        $latest = explode('/', $latest_return);
        $latest = $latest[2] . '-' . $latest[1] . '-' . $latest[0];

        // Init empty array
        $filtered = array();

        // Iterate tours
        foreach ($tours as $tour) {

            // Change format of tour date to SQLSMALLDATE
            $checkin_date = explode('/', $tour['CheckIn']);
            $checkin_date = $checkin_date[2] . '-' . $checkin_date[1] . '-' . $checkin_date[0];

            // Calculate return date
            $return_date = strtotime($checkin_date . " + " . $tour['Nights'] . " days");

            // If calculated return date is on or before latest date
            if ($return_date <= strtotime($latest)) {

                // Add to return array
                $filtered[] = $tour;
            }
        }

        return $filtered;
    }

    function get_normal_prices($tours) {

        $samo = new \Savatours\Samo\Samo;

        $normal_prices = array();
        foreach ($tours as $tour) {
            if ($tour['Cat_Claim'] != null) {
                $normal_prices[$tour['Cat_Claim']] = $samo->getNormalPrice($tour);
            }
        }

        return $normal_prices;
    }   
}