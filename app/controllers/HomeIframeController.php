<?php

class HomeIframeController extends SearchableController {

	public function home($partner_id)
	{
		$page = Page::where('slug', '=', 'home')->first();

		$this->data['partner_id'] = $partner_id;

		$this->data['sections'] = json_decode($page->body);

		$this->data['main_banner'] = Banner::where('id', '=', $this->data['sections']->main_banner)->first();
		$this->data['secondary_banner'] = Banner::where('id', '=', $this->data['sections']->secondary_banner)->first();

		// $offers = New TopOffer;
		// $this->data['top_offers'] = $offers->top_offers($sections->top_offers);
		
		return View::make('pages.homeIframe', $this->data);
	}


	public function contact() 
	{

		return View::make('pages.contact', $this->data);
	}



}