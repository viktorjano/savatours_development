<?php

class BlogController extends SearchableController {

	public function index()
	{
		setlocale(LC_TIME, 'sq_AL');

		$posts = Post::where('status', '=', 'PUBLISHED')->get();
		$this->data['posts'] = $posts;
		
		return View::make('pages.blog', $this->data);
	}

	public function show($slug) {
		$post = Post::where('slug', $slug)->first();
		
		$this->data['post'] = $post;

		return View::make('pages.blog-single', $this->data);
	}
}