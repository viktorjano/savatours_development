<?php

class BaseController extends Controller {
	

	public function __construct() 
	{
		
        $lang = Session::get('lang', Config::get('site.locale'));
   

        switch ($lang) {
        	case 'mk':
        		setlocale(LC_ALL, 'mk_MK.utf8');
        		break;
        	case 'sq':
        		setlocale(LC_ALL, 'sq_AL.utf8');
        		break;
        	default:
        		setlocale(LC_ALL, 'sq_AL.utf8');
        		break;
        }		
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		
		// if ( ! is_null($this->layout))
		// {
		// 	$this->layout = View::make($this->layout);
		// }
	}

}