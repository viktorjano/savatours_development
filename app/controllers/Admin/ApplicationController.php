<?php
namespace Admin;
use View;
class ApplicationController extends AdminController {

    /**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin.layouts.master';

	/**
	 * Initiate a new HotelController instance.
	 */
	public function __construct()
	{

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($app_name)
	{
		// $samo = new \Savatours\Samo\Samo;
		// $data['hotels'] = $samo->getHotels(null, null, null);

		// $hotel = New \Hotel;
		// $data['hotels'] = $hotel->getAll();

		return \View::make('admin.iframes.' . $app_name);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return \View::make('admin.hotel.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// $hotel = \Hotel::find($id);
		$hotel = new \Hotel;
		$data['hotel'] = $hotel->get($id);
		$descriptions = $hotel->where('samo_id', '=', $id)->first();
		//dd($descriptions);
		if($descriptions !== null) {
			$data['hotel'] = array_add($data['hotel'], 'features', $descriptions->features);
			$data['hotel'] = array_add($data['hotel'], 'slogan', $descriptions->slogan);						
			$data['hotel'] = array_add($data['hotel'], 'short_description', $descriptions->short_description);
			$data['hotel'] = array_add($data['hotel'], 'full_description', $descriptions->full_description);
		} else {
			$data['hotel'] = array_add($data['hotel'], 'features', '');
			$data['hotel'] = array_add($data['hotel'], 'slogan',  '');						
			$data['hotel'] = array_add($data['hotel'], 'short_description', '');
			$data['hotel'] = array_add($data['hotel'], 'full_description', '');			
		}

		return \View::make('admin.hotel.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'slogan'       => 'required',
			'short_description' => 'required',
			'full_description' => 'required'
		);
		$validator = \Validator::make(\Input::all(), $rules);


		// process the login
		if ($validator->fails()) {
			return \Redirect::to('admin/hotels/' . $id . '/edit')
				->withErrors($validator)
				->withInput(\Input::all());
		} else {
			// store
			$hotel = \Hotel::firstOrCreate(array('samo_id' => $id));			
			$hotel->features = json_encode(\Input::get('features'));
			$hotel->slogan = \Input::get('slogan');
			$hotel->short_description = \Input::get('short_description');
			$hotel->full_description = \Input::get('full_description');
			$hotel->save();

			// redirect
			\Session::flash('success', 'Successfully saved hotel data!');
			return \Redirect::to('admin/hotels');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}