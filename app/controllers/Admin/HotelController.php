<?php
namespace Admin;
use View;
class HotelController extends AdminController {

    /**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin.layouts.master';

	/**
	 * Initiate a new HotelController instance.
	 */
	public function __construct()
	{

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// $samo = new \Savatours\Samo\Samo;
		// $data['hotels'] = $samo->getHotels(null, null, null);
		

		$hotel = New \Hotel;
		$data['hotels'] = $hotel->getAll();
		// dd($data['hotels']);


		return \View::make('admin.hotel.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return \View::make('admin.hotel.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param1  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// $hotel = \Hotel::find($id);
		$hotel = new \Hotel;
		
		$data['hotel'] = $hotel->get($id);

		$descriptions = $hotel->where('samo_id', '=', $id)->first();

		// First lets check if the hotel descriptions exist, if load the related models
		if ($descriptions) {

			$hotelCollections = $descriptions->collections->toArray();
			
			$collections = array();
			foreach ($hotelCollections as $hotelCollection) {
				$collections[] = $hotelCollection['id'];
			}

			$data['hotel'] = array_add($data['hotel'], 'id', $descriptions->id);
			$data['hotel'] = array_add($data['hotel'], 'features', $descriptions->features !== null ? implode(';', json_decode($descriptions->features)) : '');
			$data['hotel'] = array_add($data['hotel'], 'slug', $descriptions->slug);			
			$data['hotel'] = array_add($data['hotel'], 'slogan', $descriptions->slogan);
			$data['hotel'] = array_add($data['hotel'], 'collections', $collections);						
			$data['hotel'] = array_add($data['hotel'], 'short_description', $descriptions->short_description);
			$data['hotel'] = array_add($data['hotel'], 'resort_description', $descriptions->resort_description);
			$data['hotel'] = array_add($data['hotel'], 'rooms_description', $descriptions->rooms_description);
			$data['hotel'] = array_add($data['hotel'], 'beach_pools_description', $descriptions->beach_pools_description);
			$data['hotel'] = array_add($data['hotel'], 'concept_description', $descriptions->concept_description);



			// Load the related albums if descriptions exist
			$album = $descriptions->albums->first();
			if($album) {
				$data['album'] = $album;
			} else {
				$data['album'] = null;
			}
		} else {

			$hotel = new \Hotel(array('samo_id' => $id, 'slug' => slugify($data['hotel']['name'])));
			$hotel->save();

			$data['hotel'] = array_add($data['hotel'], 'slug', '');						
			$data['hotel'] = array_add($data['hotel'], 'features', '');
			$data['hotel'] = array_add($data['hotel'], 'slogan',  '');		
			$data['hotel'] = array_add($data['hotel'], 'collections',  '');													
			$data['hotel'] = array_add($data['hotel'], 'short_description', '');
			$data['hotel'] = array_add($data['hotel'], 'resort_description', '');			
			$data['hotel'] = array_add($data['hotel'], 'rooms_description', '');			
			$data['hotel'] = array_add($data['hotel'], 'beach_pools_description', '');			
			$data['hotel'] = array_add($data['hotel'], 'concept_description', '');			
			
			$data['album'] = null;
			
		}

		// Load all the collections
		$allCollections = \Collection::all()->toArray();
		foreach ($allCollections as $collection) {
			$data['collections'][$collection['id']] =  $collection['name'];
		}
		
		return \View::make('admin.hotel.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'slug' => 'required',
			// 'short_description' => 'required',
			// 'resort_description' => 'required'
		);
		$validator = \Validator::make(\Input::all(), $rules);


		//dd(\Input::get('features'));
		
		// validate
		if ($validator->fails()) {
			return \Redirect::to('admin/hotels/' . $id . '/edit')
				->withErrors($validator)
				->withInput(\Input::all());
		} else {
			// store
			$hotel = \Hotel::firstOrCreate(array('samo_id' => $id));			
			$hotel->features = json_encode(explode(';', \Input::get('features')));
			
			if(\Input::get('collections')) {

				$hotel->collections()->detach();

				foreach(\Input::get('collections') as $collection) {
					$hotel->collections()->attach($collection);
				}
			}
			
			$hotel->slug = \Input::get('slug');
			$hotel->slogan = \Input::get('slogan');
			$hotel->short_description = \Input::get('short_description');
			$hotel->resort_description = \Input::get('resort_description');
			$hotel->rooms_description = \Input::get('rooms_description');
			$hotel->beach_pools_description = \Input::get('beach_pools_description');
			$hotel->concept_description = \Input::get('concept_description');

			$hotel->save();

			// redirect
			\Session::flash('success', 'Successfully saved hotel data!');
			return \Redirect::to('admin/hotels');
		}
	}

	/**
	 * Return images in a json list
	 * 
	 */
	public function jsonPhotos($hotelId = null)
	{
		if ( $hotelId !== null ) {
			$hotel = \Hotel::where('samo_id', '=', $hotelId)->first();
			$album = $hotel->albums->first();

			$jsonPhotos = null;
			
			foreach($album->photos as $photo) {
				$jsonPhotos[] = array('image' => \URL::to('/') . '/' . $photo->filename);
			}
		} else {

			$hotel = new \Hotel;
			$hotels = $hotel->getAll();

			foreach($hotels as $hotel) {
				$hotel = \Hotel::where('samo_id', '=', $hotel['HotelInc'])->first();
	
				if($hotel) {
					$album = $hotel->albums()->first();
					
					foreach($album->photos as $photo) {
						$jsonPhotos[] = array('image' => asset($photo->filename), null, 'folder' => $album->name);
					}
				}
			}

		}

		return json_encode($jsonPhotos);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}