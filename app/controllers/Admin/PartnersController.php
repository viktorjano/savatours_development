<?php
namespace Admin;

class PartnersController extends AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /partners
	 *
	 * @return Response
	 */
	public function index()
	{
			/*$samo = new \Savatours\Samo\Samo;*/
			/*$data['partners'] = $samo->getPartnersInfo();*/

			$data['partners'] = \Partner::all();

			$data['tools'] = '<div class="well well-sm clearfix" data-spy="stickout">        
						    <div class="btn-toolbar" role="toolbar">
						        <div class="btn-group">
						            <a href="'. \URL::to('/admin/partners/create').'" class="btn btn-sm btn-primary">
						                <i class="fa fa-plus"></i> Create new
						            </a>                                       
						        </div>              
						    </div>
						</div>';


			return \View::make('admin.partner.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /newsletter/create
	 *
	 * @return Response
	 */
	public function create()
	{

		$samo = new \Savatours\Samo\Samo;

		$data['partners'] = $samo->getPartnersInfo();

		for($i = 0; $i < count($data['partners']); $i++) {
			$data['names'][] = $data['partners'][$i]['name'];
		};

		return \View::make('admin.partner.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /newsletter
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'email' => 'required',
			'phone' => 'required',
		);


		$validator = \Validator::make($data = \Input::all(), $rules);

		if($validator->fails()) {
			return \Redirect::back()
			->withErrors($validator)
			->withInput(\Input::all());
		} else {

			
			try
			{
				
			    // Create the partner
			    if($partner = \Partner::create(array(
			        'name'       => $data['name'],
			        'email'    => $data['email'],
			        'phone'   => $data['phone'],
			        'website' => $data['website'],
			    ))) {

			    	\Session::flash('success', 'Partner added');
					return \Redirect::to('/admin/partners');
			    }

			     
			}
			catch (Cartalyst\Sentry\Partners\EmailRequiredException $e)
			{
			    echo 'Email field is required.';
			}
			catch (Cartalyst\Sentry\Partners\PhoneRequiredException $e)
			{
			    echo 'Phone field is required.';
			}
			catch (Cartalyst\Sentry\Partners\PartnerExistsException $e)
			{
			    echo 'Partner with this login already exists.';
			}

		}		
	}

	/**
	 * Display the specified resource.
	 * GET /newsletter/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /newsletter/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$partner = \Partner::find($id);

		return \View::make('admin.partner.edit')->withPartner($partner);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /newsletter/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'email' => 'required',
			'phone' => 'required',
		);

		$validator = \Validator::make($data = \Input::all(), $rules);

		if($validator->fails()) {
			return \Redirect::back()
			->withErrors($validator)
			->withInput(\Input::all());
		} else {		
			try
			{
			    // Find the partner using the user id
			    $user = \Partner::find($id);

			    // Update the partner details
			    $user->email = $data['email'];
			    $user->phone = $data['phone'];
			    $user->website = $data['website'];
			    
			    // Update the partner
			    if ($user->save())
			    {
			        \Session::flash('success', 'Partner updated.');
			        return \Redirect::to('/admin/partners');
			    }
			    else
			    {
			        // User information was not updated
			    }
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
			    echo 'User with this login already exists.';
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			    echo 'User was not found.';
			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /newsletter/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}