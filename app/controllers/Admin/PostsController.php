<?php

namespace Admin;

class PostsController extends AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /posts
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['posts'] = \Post::withTrashed()->get();
		
		$data['tools'] = '<div class="well well-sm clearfix" data-spy="stickout">        
						    <div class="btn-toolbar" role="toolbar">
						        <div class="btn-group">
						            <a href="'. \URL::to('/admin/posts/create').'" class="btn btn-sm btn-primary">
						                <i class="fa fa-plus"></i> Create new
						            </a>                                       
						        </div>              
						    </div>
						</div>';
		
		return \View::make('admin.posts.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /posts/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['tools'] = \View::make('admin.includes.form-tools');
		
		return \View::make('admin.posts.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /posts
	 *
	 * @return Response
	 */
	public function store()
	{

		$validator = \Validator::make(\Input::all(), \Post::$rules);

		if ($validator->fails())
		{

			\Session::flash('error', $validator->messages());
			return \Redirect::back()->withErrors($validator)->withInput(\Input::except('image'));
		} 

		$post = new \Post;
		
		// Title
		$post->title = \Input::get('title');
		
		// Image
		if(\Input::hasFile('image')) {
			$file = \Input::file('image');
			$destinationPath = 'images/blog/'.str_random(8);
			$filename = $file->getClientOriginalName();
			$upload_success = \Input::file('image')->move($destinationPath, $filename);
			
			if( $upload_success ) {
				$post->image = $destinationPath . '/' . $filename;
			}

			// Image Alt
			$post->image_alt = $data['image_alt'];
		}
		
		// Youtube video
		if(\Input::has('youtube_video')) {
			$post->youtube_video = $data['youtube_video'];
		}
		
		// Summary
		$post->summary = \Input::get('summary');
		
		//Content
		$post->content = \Input::get('content');
		
		// Link
		if(\Input::has('link_url')) {
			$post->link_text = $data['link_text'];
			$post->link_url = $data['link_url'];
		}
		
		// Is Sticky?
		\Input::has('is_sticky') ? $post->is_sticky = \Input::get('is_sticky') : $post->is_sticky = 0;
		
		// Slug
		$post->slug = \Input::get('slug');
		
		// Page title	
		if(\Input::has('page_title')) {
			$post->page_title = \Input::get('page_title');
		}
	
		// Meta data
		$post->meta_description = \Input::get('meta_description');
		$post->meta_keywords = \Input::get('meta_keywords');
		
		// Status
		$post->status = \Input::get('status');
		
		// Published date
		if(\Input::get('status') == 'PUBLISHED') {
			$post->published_date = new DateTime('now');
		}


		$post->save();

		\Session::flash('success', 'Successfully saved new post.');
		return \Redirect::to('admin/posts');
	}

	/**
	 * Display the specified resource.
	 * GET /posts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /posts/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['post'] = \Post::find($id);
		$data['tools'] = \View::make('admin.includes.form-tools');

		return \View::make('admin.posts.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /posts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$post = \Post::find($id);
		$post->rules['slug'] = 	'required|unique:posts,slug, '.$post->id.'|unique:pages';
		$validator = \Validator::make(\Input::all(), $post->rules);

		if($validator->fails()) {
			\Session::flash('error', $validator->messages());

			return \Redirect::back()->withErrors($validator);
		}

		$post->title =  \Input::get('title');
		$post->summary = \Input::get('summary');
		$post->content = \Input::get('content');
		$post->link_text = \Input::get('link_text');
		$post->link_url = \Input::get('link_url');
		\Input::has('is_sticky') ? $post->is_sticky = \Input::get('is_sticky') : $post->is_sticky = 0;

		// Image
		if(\Input::hasFile('image')) {
			$file = \Input::file('image');
			$destinationPath = 'images/blog/'.str_random(8);
			$filename = $file->getClientOriginalName();
			$upload_success = \Input::file('image')->move($destinationPath, $filename);
			
			if( $upload_success ) {
				$post->image = $destinationPath . '/' . $filename;
			}

			// Image Alt
			//$post->image_alt = \Input::get('image_alt');
		}

		$post->slug = \Input::get('slug');
		// Page title	
		if(\Input::has('page_title')) {
			$post->page_title = \Input::get('page_title');
		}
		// Meta data
		$post->meta_description = \Input::get('meta_description');
		$post->meta_keywords = \Input::get('meta_keywords');
		// Status
		$post->status = \Input::get('status');
		// Published date
		if(\Input::get('status') == 'PUBLISHED' AND $post->published_date == NULL) {
			$post->published_date = new \DateTime('now');
		}

		$post->save();

		\Session::flash('success', 'Successfully updated post!');

		return \Redirect::to('admin/posts');

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /posts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$post = \Post::find($id);

		$post->delete();

		\Session::flash('success', 'Successfully deleted the post.');

		return \Redirect::to('admin/posts');
	}

	/**
	 * Restore specified post
	 * POST /posts/restore/{$id}
	 *
	 * @param int $id
	 * @return Response
	 */
	public function restore($id) {
		$post = \Post::OnlyTrashed()->where('id', $id)->first();
		$post->restore();

		\Session::flash('success', 'Successfully restored the post.');

		return \Redirect::to('admin/posts');
	}


}