<?php
namespace Admin;
class TopOffersController extends AdminController	 {

	/**
	 * Display a listing of the resource.
	 * GET /topoffers
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['topoffers'] = \TopOffer::all();
		$data['tools'] = '<div class="well well-sm clearfix" data-spy="stickout">        
						    <div class="btn-toolbar" role="toolbar">
						        <div class="btn-group">
						            <a href="'. \URL::to('/admin/topoffers/create').'" class="btn btn-sm btn-primary">
						                <i class="fa fa-plus"></i> Create new
						            </a>                                       
						        </div>              
						    </div>
						</div>';
		
		return \View::make('admin.topoffers.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /topoffers/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$hotels = \Hotel::all();

		foreach($hotels as $hotel) {
			
			$hotel_info_samo = $hotel->get($hotel->samo_id);

			$data['hotels'][$hotel->samo_id] = $hotel_info_samo['name'];
		}

		return \View::make('admin.topoffers.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /topoffers
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'hotels' => 'required',
			'checkin' => 'required',
			'nights' => 'required',
		);

		$validator = \Validator::make($data = \Input::all(), $rules);

		if($validator->fails()) {
			return \Redirect::back()
			->withErrors($validator)
			->withInput(\Input::all());
		} else {
			$topoffer = new \TopOffer;
			$topoffer->hotels = json_encode(\Input::get('hotels'));
			$topoffer->checkin = \Input::get('checkin');
			$topoffer->nights = \Input::get('nights');
			$topoffer->save();

			\Session::flash('success', 'Successfully saved topoffer');
			return \Redirect::to('admin/topoffers');
		}		
	}

	/**
	 * Display the specified resource.
	 * GET /topoffers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$topOffer = \TopOffer::find($id);

		$date = explode("/", $topOffer->checkin);
		
		$checkin = $date[2] . $date[1] . $date[0];

		$samo = new \Savatours\Samo\Samo;		
		
        $selected_meals = array();

        $state = \Config::get('site.state_default') ;
        $data['meals'] = $samo->getMealTypes($state);

        foreach ($data['meals'] as $meal) {
            $selected_meals[] = $meal['Inc'];
        }
        $meals = array();

        foreach ($selected_meals as $k => $v) {
            $meals[] = $samo->getMealId($v);
        }

        $data['selected_meals'] = $selected_meals;

        $meal_list = implode(',', $meals);		

        $hotel_list = implode(",", json_decode($topOffer->hotels));

        $data['tours'] = $samo->getPrices(array(
                'form' => \Config::get('site.form'), 
                'spo' => null, 
                'owner' => \Config::get('site.owner'), 
                'fr_place' => \Config::get('site.fr_place'), 
                'checkin_start' => $checkin, 
                'checkin_end' => $checkin, 
                'hotel_list' => $hotel_list, 
                'nights_min' => $topOffer->nights, 
                'nights_max' => $topOffer->nights+4, 
                'meal_list' => $meal_list, 
                'adults' => 2, 
                'children' => 0, 
                'price_min' => null, 
                'price_max' => null, 
                'filter' => 0, 
                'price_page' => 1, 
                'sort_type' => 1, 
                'records_page' => \Config::get('site.records_on_page'), 
                'catalog_db' => \Config::get('site.catalog_db'), 
                'admin_db' => \Config::get('site.admin_db'), 
                'min' => \Config::get('site.min'), 
                'max' => \Config::get('site.max'), 
                'show_request' => \Config::get('site.show_request'), 
                'tour' => \Config::get('site.tour_default'), 
                'business_fr_place' => \Config::get('site.business_fr_place'), 
                'business_min' => \Config::get('site.business_min'), 
                'business_max' => \Config::get('site.business_max'), 
                'max_records' => \Config::get('site.max_records'), 
                'town_from' => \Config::get('site.departure_default'), 
                'state' => \Config::get('site.state_default'), 
                'child1_age' => null, 
                'child2_age' => null, 
                'child3_age' => null, 
                'price_page' => 1
        ));		

		$data['tours'] = $samo->filter_cheapest_rooms($data['tours']);

		// echo "<pre>";
		// dd($data['tours']);

		$data['date'] = explode("/", strftime("%A/%-e/%B", strtotime("$checkin")));        

        $data['stars'] = \Config::get('site.star_map');

        return \View::make('widgets.top_offers', $data);

	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /topoffers/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['topoffer'] = \TopOffer::findOrFail($id);

		$hotels = \Hotel::all();
		

		foreach($hotels as $hotel) {
			$htl = \Hotel::get($hotel->samo_id);
			
			$data['hotels'][$hotel->samo_id] = $htl['name'];
		}


		$data['attachedHotels'][] = array();
		
		$data['attachedHotels'] = json_decode($data['topoffer']->hotels);
		

		// foreach($attachedHotels as $k=>$v) {
			
		// 	$hotel = \Hotel::where('samo_id', '=', $v)->first();
			
		// 	$data['attachedHotels'][] = $hotel->id;
		// }		

		return \View::make('admin.topoffers.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /topoffers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		$rules = array(
			'hotels' => 'required',
			'checkin' => 'required',
			'nights' => 'required',
		);

		$validator = \Validator::make($data = \Input::all(), $rules);

		if($validator->fails()) {
			return \Redirect::back()
			->withErrors($validator)
			->withInput(\Input::all());
		} else {
			$topoffer = \TopOffer::findOrFail($id);
			$topoffer->hotels = json_encode(\Input::get('hotels'));
			$topoffer->checkin = \Input::get('checkin');
			$topoffer->nights = \Input::get('nights');
			$topoffer->save();

			\Session::flash('success', 'Successfully updated topoffer');
			return \Redirect::to('admin/topoffers');
		}		

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /topoffers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}