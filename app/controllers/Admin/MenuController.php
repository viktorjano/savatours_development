<?php
namespace Admin;
use View;
class MenuController extends AdminController {
    
    /**
     * The layout that should be used for responses.
     */
    //protected $layout = 'admin.layouts.master';

	/**
	 * Initiate a new MenuController instance.
	 */
	public function __construct()
	{

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		$data['items'] = \Menu::tree();

		// echo "<pre>";
		// dd($data['items']);
		
		$items = \Menu::orderBy('order')->get(); 
		foreach($items as $item) {
			$parentPagesList[$item->id] = $item->title; 
			$data['menuGroup'][$item->group][] = $item;			
		}		

		$data['parentPagesList'] = $parentPagesList;

        return View::make('admin.menu.index', $data	);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['items'] = \Menu::all();

		foreach($data['items'] as $item) {
			$data['parentPagesList'][$item->id] = $item->title;
		}

		return View::make('admin.menu.create', $data);

		// $rules = array(
		// 	'title' => 'required',
		// 	'url' => 'required',
		// 	'order' => 'required|numeric',
		// 	'parent_id' => 'numeric'
		// );

		// $validator = \Validator::make(\Input::all(), $rules);

		// // validate
		// if ($validator->fails()) {
		// 	return \Redirect::to('admin/menus/')
		// 		->withErrors($validator)
		// 		->withInput(\Input::all());
		// } else {	
		// 	$menu = new \Menu;
		// 	$menu->title = \Input::get('title');
		// 	$menu->url = \Input::get('url');
		// 	$menu->order = 999;
		// 	$menu->parent_id = \Input::get('parent');

		// 	\Session::flash('success', 'Successfully saved hotel data!');
		// 	return \Redirect::to('admin/menus');
		// }
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'title' => 'required',
			'link_url' => 'required',
			//'order' => 'required|numeric',
			'parent_id' => 'numeric',
			//'group' => 'required'
		);

		$validator = \Validator::make(\Input::all(), $rules);

		// validate
		if ($validator->fails()) {
			
			return \Redirect::to('admin/menu/create')
				->withErrors($validator)
				->withInput(\Input::all());
		} else {	
			
			$menu = new \Menu;
			$menu->title = \Input::get('title');
			$menu->url = \Input::get('link_url');
			$menu->order = 999;
			$menu->parent_id = \Input::get('parent');
			$menu->group = 'admin_menu';
			$menu->save();

			\Session::flash('success', 'Successfully saved hotel data!');
			
			return \Redirect::to('admin/menu');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('admin.menu.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// $rules = array(
		// 	'title' => 'required',
		// 	'url' => 'required',
		// 	'order' => 'required|numeric',
		// 	'parent_id' => 'numeric'
		// );
		// $validator = \Validator::make(\Input::all(), $rules);

		// // validate
		// if ($validator->fails()) {
		// 	return \Redirect::to('admin/menus/')
		// 		->withErrors($validator)
		// 		->withInput(\Input::all());
		// } else {	
		// 	$menu = \Menu::find($id)	
		// 	\Session::flash('success', 'Successfully saved hotel data!');
		// 	return \Redirect::to('admin/menus');        }
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
