<?php
namespace Admin;
class UsersController extends AdminController	 {

	/**
	 * Display a listing of the resource.
	 * GET /users
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['users'] = \Sentry::findAllUsers();

		$data['tools'] = '<div class="well well-sm clearfix" data-spy="stickout">        
						    <div class="btn-toolbar" role="toolbar">
						        <div class="btn-group">
						            <a href="'. \URL::to('/admin/users/create').'" class="btn btn-sm btn-primary">
						                <i class="fa fa-plus"></i> Create new
						            </a>                                       
						        </div>              
						    </div>
						</div>';
			
		return \View::make('admin.user.index', $data);

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /users/create
	 *
	 * @return Response
	 */
	public function create()
	{

		return \View::make('admin.user.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /users
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'email' => 'required',
			'password' => 'required|confirmed',
		);

		$validator = \Validator::make($data = \Input::all(), $rules);

		if($validator->fails()) {
			return \Redirect::back()
			->withErrors($validator)
			->withInput(\Input::all());
		} else {
			
			try
			{
			    // Create the user
			    $user = \Sentry::createUser(array(
			        'email'       => $data['email'],
			        'password'    => $data['password'],
			        'activated'   => true,
			        'permissions' => array(
	            	'admin' => 1,
			        ),
			    ));

			    \Session::flash('success', 'User added');

			    return \View::make('admin.user.index'); 
			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
			    echo 'Login field is required.';
			}
			catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
			{
			    echo 'Password field is required.';
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
			    echo 'User with this login already exists.';
			}

		}		
	}

	/**
	 * Display the specified resource.
	 * GET /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

        // return \View::make('widgets.top_offers', $data);

	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /users/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = \Sentry::findUserById($id);

		return \View::make('admin.user.edit')->withUser($user);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'email' => 'required',
			'password' => 'confirmed',
		);

		$validator = \Validator::make($data = \Input::all(), $rules);

		if($validator->fails()) {
			return \Redirect::back()
			->withErrors($validator)
			->withInput(\Input::all());
		} else {		
			try
			{
			    // Find the user using the user id
			    $user = \Sentry::findUserById($id);

			    // Update the user details
			    $user->email = $data['email'];
			    $user->activated = (isset($data['activated'])) ? 1 : 0;
			    
			    if(isset($data['password']))
			    	$user->password = $data['password'];
			    
			    // Update the user
			    if ($user->save())
			    {
			        \Session::flash('success', 'User updated.');
			        return \Redirect::to('/admin/users');
			    }
			    else
			    {
			        // User information was not updated
			    }
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
			    echo 'User with this login already exists.';
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			    echo 'User was not found.';
			}
		}
	}

	public function login() 
	{
		try
		{
			$data = \Input::all();

		    // Login credentials
		    $credentials = array(
		        'email'    => $data['email'],
		        'password' => $data['password'],
		    );

		    // Authenticate the user
		    $user = \Sentry::authenticate($credentials, false);

		    return \Redirect::to('/admin');
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    echo 'Login field is required.';
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		    echo 'Password field is required.';
		}
		catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
		{
		    echo 'Wrong password, try again.';
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    echo 'User was not found.';
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
		    echo 'User is not activated.';
		}

		// The following is only required if the throttling is enabled
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
		    echo 'User is suspended.';
		}
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
		{
		    echo 'User is banned.';
		}			
	}

	public function logout()
	{
		\Sentry::logout();

		return \Redirect::to('/');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}