<?php
namespace Admin;
use View;
class PhotoController extends AdminController {

	/**
	 * Initiate a new PhotoController instance
	 */
	public function __construct()
	{
		// We shall protect the Create, Update, Delete actions
		// $this->beforeFilter('auth', 
		// 	array('only' => array('create', 'store', 'edit','update', 'destroy'))
		// );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('photos.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('photos.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$file = \Input::file('file');
		$destinationPath = 'uploads/'.str_random(8);
		$filename = $file->getClientOriginalName();
		//$extension =$file->getClientOriginalExtension(); 
		$upload_success = \Input::file('file')->move($destinationPath, $filename);

		if( $upload_success ) {

			$photo = new \Photo;
			$photo->filename = $destinationPath . '/' . $filename;
			$photo->title = $filename;
			$photo->sort_order = 1;
			$photo->save();
			

			$hotel = \Hotel::find(\Input::get('hotel'));
			$hotelSamo = new \Hotel;
			$hotelSamo = $hotelSamo->get($hotel->samo_id);
			
			$album = $hotel->albums()->first();

			if($album) {
				$photo->albums()->attach($album);
			} else {
				$album = \Album::create(array('name' => $hotelSamo['name']));
				$hotel->albums()->attach($album);
				$photo->albums()->attach($album);
			} 

		   return \Response::json('success', 200);
		} else {
		   return \Response::json('error', 400);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('photos.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('photos.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//	
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$photo = \Photo::find($id);
		
		// if(unlink($photo->filename)) {
			$photo->albums()->detach();
			$photo->delete();
		// }

		return json_encode(array('success' => true));
	}

}
