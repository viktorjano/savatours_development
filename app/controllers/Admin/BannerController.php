<?php
namespace Admin;
use View;
class BannerController extends AdminController {

	/**
	 * Initiate a new BannerController instance
	 */
	public function __construct()
	{
		// We shall protect the Create, Update, Delete actions
		// $this->beforeFilter('auth', 
		// 	array('only' => array('create', 'store', 'edit','update', 'destroy'))
		// );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['tools'] = '<div class="well well-sm clearfix" data-spy="stickout">        
						    <div class="btn-toolbar" role="toolbar">
						        <div class="btn-group">
						            <a href="'. \URL::to('/admin/banners/create').'" class="btn btn-sm btn-primary">
						                <i class="fa fa-plus"></i> Create new
						            </a>                                       
						        </div>              
						    </div>
						</div>';

		$data['banners'] = \Banner::all();
        return View::make('admin.banner.index', $data);
    	;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['tools'] = '<div class="well well-sm clearfix" data-spy="stickout">        
						    <div class="btn-toolbar" role="toolbar">
						        <div class="btn-group">
						      	<input class="btn btn-primary" type="submit" value="Save" form="banner-form">
						        </div>              
						    </div>
						</div>';		
        return View::make('admin.banner.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array();

		$validator = \Validator::make($data = \Input::all(), $rules);

		if($validator->fails()) {
			return \Redirect::back()->withErrors($validator)->withInput();
		}

		$banner = new \Banner();

		$banner->content = $data['content'];
		$banner->link = $data['link'];
		$banner->expiration = date('Y-m-d H:i:s', strtotime( $data['expiration'] ));

		if(\Input::hasFile('photo')) {
			$img = \Image::make(\Input::file('photo'));

			$destinationPath = 'images/banners/';
			
			$mime = $img->mime(); 
			if ($mime == 'image/jpeg')
			    $extension = '.jpg';
			elseif ($mime == 'image/png')
			    $extension = '.png';
			elseif ($mime == 'image/gif')
			    $extension = '.gif';
			else
			    $extension = ''; 		

			$filename = str_random(8).$extension;

			$upload_success = $img
								  ->crop((int)\Input::get('w'), (int)\Input::get('h'), (int)\Input::get('x'), (int)\Input::get('y'))
								  ->save($destinationPath.$filename);
			if( $upload_success ) {

				$photo = new \Photo;
				$photo->filename = $destinationPath . $filename;
				$photo->title = $filename;
				$photo->sort_order = 1;
				$photo->save();
				
				$album = \Album::where('name', '=', 'banners')->get();

				if($album) {
					$photo->albums()->attach($album);
				} else {
					$album = \Album::create(array('name' => 'banners'));
					$photo->albums()->attach($album);
				} 

				$banner->background_image = $photo->filename;			   
			} else {
			   //return \Response::json('error', 400);
			}

		}

		$banner->save();

		\Session::flash('success', 'Banner created');
		return \Redirect::to('admin/banners/');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('admin.banner.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['banner'] = \Banner::findorFail($id);

		$data['tools'] = '<div class="well well-sm clearfix" data-spy="stickout">        
						    <div class="btn-toolbar" role="toolbar">
						        <div class="btn-group">
						      	<input class="btn btn-primary" type="submit" value="Save" form="banner-form">
						        </div>              
						    </div>
						</div>';	

        return View::make('admin.banner.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array();

        $banner = \Banner::findOrFail($id);

        $validator = \Validator::make($data = \Input::all(), $rules);

        if ($validator->fails()) {
        	\Session::flash('error', 'There are some errors with the inputs.');
            return \Redirect::back()->withErrors($validator)->withInput();
        }
        
        $banner->content = \Input::get('content');
        $banner->link = \Input::get('link');
        $banner->expiration = date('Y-m-d H:i:s', strtotime(\Input::get('expiration')));

		if(\Input::hasFile('photo')) {
			$img = \Image::make(\Input::file('photo'));

			$destinationPath = 'images/banners/';
			
			$mime = $img->mime();
			if ($mime == 'image/jpeg')
			    $extension = '.jpg';
			elseif ($mime == 'image/png')
			    $extension = '.png';
			elseif ($mime == 'image/gif')
			    $extension = '.gif';
			else
			    $extension = ''; 		

			$filename = str_random(8).$extension;

			$upload_success = $img
								  ->crop((int)\Input::get('w'), (int)\Input::get('h'), (int)\Input::get('x'), (int)\Input::get('y'))
								  ->save($destinationPath.$filename);

			if( $upload_success ) {

				$photo = new \Photo;
				$photo->filename = $destinationPath . $filename;
				$photo->title = $filename;
				$photo->sort_order = 1;
				$photo->save();
				
				$album = \Album::where('name', '=', 'banners')->get();

				if($album) {
					$photo->albums()->attach($album);
				} else {
					$album = \Album::create(array('name' => 'banners'));
					$photo->albums()->attach($album);
				} 

				$banner->background_image = $photo->filename;
			  			   
			} else {
			   
				\Session::flash('error', 'Something wrong with the photo.');
		        
		        return \Redirect::back();	
			}

		}

        $banner->save();	

		\Session::flash('success', 'Banner updated.');
        
        return \Redirect::route('admin.banners.index');	        
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		\Banner::destroy($id);

		\Session::flash('success', 'Banner deleted.');
		
		return \Redirect::to('admin/banners');
	}

}
