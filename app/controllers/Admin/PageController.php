<?php
namespace Admin;
use View;
class PageController extends AdminController {

	/**
	 * Initiate a new PageController instance
	 */
	public function __construct()
	{
		// We shall protect the Create, Update, Delete actions
		// $this->beforeFilter('auth', 
		// 	array('only' => array('create', 'store', 'edit','update', 'destroy'))
		// );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pages = \Page::all();
        return View::make('admin.page.index')->with('pages', $pages);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('admin.page.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'layout' => 'required',
			'title' => 'required',
			'slug' => 'required'
		);

		$validator = \Validator::make(\Input::all(), $rules);

		if($validator->fails()) {
			return \Redirect::back()
			->withErrors($validator)
			->withInput(\Input::all());
		} else {
			$page = new \Page;
			$page->layout = \Input::get('layout');
			$page->title = \Input::get('title');
			$page->slug = \Input::get('slug');
			$page->body = \Input::get('body');
			$page->save();

			\Session::flash('success', 'Successfully saved page');
			return \Redirect::to('admin/pages');
		}				
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('admin.page.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['page'] = \Page::find($id);
		
		$banners = \Banner::all();

		$data['banners'] = array();

		foreach ($banners as $banner) {
			$data['banners'][$banner->id] = 'Banner ' . $banner->id; 
		}

		$top_offers = \TopOffer::all();

		$data['top_offers_list'] = array();

		foreach($top_offers as $top_offer) {
			$data['top_offers_list'][$top_offer->id] = '#'. $top_offer->id . ' - '. $top_offer->checkin . ' - ' . $top_offer->nights . ' nights';
		}

		$data['page']->body = json_decode($data['page']->body);  

        return View::make('admin.page.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'layout' => 'required',
			'title' => 'required',
			'slug' => 'required'
		);

		$validator = \Validator::make($data = \Input::all(), $rules);

		if($validator->fails()) {
			return \Redirect::back()
			->withErrors($validator)
			->withInput(\Input::all());
		} else {
			$page = \Page::find($id);
			$page->layout = \Input::get('layout');
			$page->title = \Input::get('title');
			$page->slug = \Input::get('slug');

			$body = array(
				'main_banner' => $data['main_banner'],
				'secondary_banner' => $data['secondary_banner'],
				'top_offers' => $data['top_offers'],
				);

			$page->body = json_encode($body);

			$page->save();

			\Session::flash('success', 'Successfully saved page');
			return \Redirect::to('admin/pages');	
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
