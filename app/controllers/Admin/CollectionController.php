<?php
namespace Admin;
use View;
class CollectionController extends AdminController {

	/**
	 * Initiate a new CollectionController instance
	 */
	public function __construct()
	{
		// We shall protect the Create, Update, Delete actions
				// $this->beforeFilter('auth', 
				// 	array('only' => array('create', 'store', 'edit','update', 'destroy'))
				// );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['tools'] = '<div class="well well-sm clearfix" data-spy="stickout">        
						    <div class="btn-toolbar" role="toolbar">
						        <div class="btn-group">
						            <a href="'. \URL::to('/admin/collections/create').'" class="btn btn-sm btn-primary">
						                <i class="fa fa-plus"></i> Create new
						            </a>                                       
						        </div>              
						    </div>
						</div>';

		$data['collections'] = \Collection::all();
        return View::make('admin.collection.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('admin.collection.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'name' => 'required',
			'slug' => 'required',
			'description' => 'required',
			'color'	=> 'required'
		);

		

		$validator = \Validator::make($data = \Input::all(), $rules);

		if($validator->fails()) {
			return \Redirect::to('admin/collections' . $id . '/edit')
			->withErrors($validator)
			->withInput(\Input::all());
		} else {
			$collection = new \Collection;
			$collection->name = \Input::get('name');
			$collection->slug = \Input::get('slug');
			$collection->description = \Input::get('description');
			$collection->background = $data['background'];
			$collection->save();

			\Session::flash('success', 'Successfully saved collection');
			return \Redirect::to('admin/collections');
		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('collections.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$collection = \Collection::find($id);
		
		$hotels = \Hotel::all();
		
		$data['attachedHotels'] = null;

		foreach($hotels as $hotel) {
			$data['hotels'][$hotel->id] = $hotel->slug;
		}

		$attachedHotels = $collection->hotels;
		
		foreach($attachedHotels as $attachedHotel) {
			$data['attachedHotels'][] = $attachedHotel->id;
		}

       	$data['collection'] = $collection;

        return View::make('admin.collection.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'name' => 'required',
			'slug' => 'required',
			'description' => 'required'
		);

		$validator = \Validator::make($data = \Input::all(), $rules);

		if($validator->fails()) {
			\Session::flash('error', $validator->errors());
			return \Redirect::back()
			->withErrors($validator)
			->withInput(\Input::all());
		} else {
			$collection = \Collection::find($id);
			$collection->name = \Input::get('name');
			$collection->slug = \Input::get('slug');
			$collection->background = $data['background'];
			$collection->description = \Input::get('description');

			$collection->hotels()->detach();
			
			if(\Input::has('hotels')) {
				foreach(\Input::get('hotels') as $hotel) {
					$collection->hotels()->attach($hotel);
				}
			}

			$collection->save();

			\Session::flash('success', 'Successfully updated collection');
			return \Redirect::to('admin/collections');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
