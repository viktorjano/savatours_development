<?php
namespace Admin;
use BaseController;
use View;
class AdminController extends BaseController {

	public function __construct(){

		// $this->beforeFilter(function(){
			
		// 	if(  ! \Sentry::check()) {
		// 		\Redirect::route('home');
		// 	}

		// 	$user = \Sentry::getUser();
		// 	//dd(Route::currentRouteName());
		// 	if( ! $user->hasAccess( \Route::currentRouteName())) {

		// 		\Session::flash('error', 'You are not authorized to do that. Contact your manager if you think this is an error.');

		// 		return \Redirect::back();
		// 	}
		// });

	}
	
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
}
