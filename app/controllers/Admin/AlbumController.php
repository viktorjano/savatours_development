<?php
namespace Admin;
use View;
class AlbumController extends AdminController {

	/**
	 * Initiate a new AlbumController instance
	 */
	public function __construct()
	{
		// We shall protect the Create, Update, Delete actions
		// $this->beforeFilter('auth', 
		// 	array('only' => array('create', 'store', 'edit','update', 'destroy'))
		// );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('albums.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('albums.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{				
        return View::make('albums.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('albums.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function ajaxSetCover() 
	{
		if (\Request::ajax()) {

			$albumId = \Input::get('album');
			$photoId = \Input::get('photo');

			$album = \Album::find($albumId);

			$album->cover = $photoId;

			$album->save();
		}
	}

	public function ajaxOrder()
	{
		if(\Request::ajax()) {
			$ordered = \Input::get('order');
			$i = 1;	
					
			foreach($ordered as $photoId) {
				$photo = \Photo::find($photoId);
				$photo->sort_order = $i;
				$photo->save();
				$i++;
			}

			return \Response::json(array(
				'error' => FALSE,
			), 200);
		}
	}

}
