@extends('layouts.master')
@section('css')
@parent
{{-- <!-- Ionicons -->
<link href="{{ asset('assets/admin/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Ion Slider -->
<link href="{{ asset('assets/admin/css/ionslider/ion.rangeSlider.css') }}" rel="stylesheet" type="text/css" />
<!-- ion slider Nice -->
<link href="{{ asset('assets/admin/css/ionslider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" type="text/css" /> --}}

{{ HTML::style('assets/css/horizontal-calendar.css') }}
{{ HTML::style('assets/js/plugins/nouislider/jquery.nouislider.min.css') }}

@stop

@section('content')
<!--main content-->
<div class="content clearfix">
		{{-- <h1>Contact us</h1> --}}

{{-- 	<!--breadcrumbs-->
	<nav role="navigation" class="breadcrumbs clearfix">
		<!--crumbs-->
		<ul class="crumbs">
			<li><a href="#" title="Home">Home</a></li> 
			<li>Contact</li>                                       
		</ul>
		<!--//crumbs-->
	</nav>
	<!--//breadcrumbs--> --}}
	
	<!--three-fourth content-->
	<section class="three-fourth">
		<!--map-->
		<div class="map-wrap">
			<h2>{{ Lang::get('contact.visit_us') }}</h2>
			<div class="gmap" id="map_canvas"></div>
		</div>
		<!--//map-->		
	</section>		

	<!--sidebar-->
	<aside class="right-sidebar">
		<!--three-fourth content-->	

		<!--contact info-->
		<article class="default">
			<h2>{{ Lang::get('contact.contact_now') }}</h2>
			<p class="phone-green">
				{{ Config::get('site.phone1') }} <br /> {{ Config::get('site.phone2') }} <br /> {{ Config::get('site.mobile') }}</p>
			<p class="email-green"><a href="#">{{ Config::get('site.email') }}</a></p>
		</article>
		<!--//contact info-->

		<!--contact form-->
		<article class="default contact-page">
			<h2>{{ Lang::get('contact.write_to_us') }}</h2>
			{{ Form::open(array('url' => 'contact', 'method' => 'post' , 'id' => 'contactForm')) }}
				<fieldset>
					<div class="f-item active">
						<label for="name">{{ Lang::get('contact.name') }}</label>
						{{ Form::text('name', Input::old('name'), array('required' => 'required'	)) }}
					</div>
					<div class="f-item">
						<label for="email">{{ Lang::get('contact.email') }}</label>
						<input type="email" id="email" name="email" value="" required="required" name="your_message">
					</div>
					<div class="f-item">
						<label for="the_message">{{ Lang::get('contact.message') }}</label>
						<textarea id="the_message" rows="10" cols="10" name="the_message"> </textarea>
					</div>
					<input type="submit" value="{{ Lang::get('contact.send') }}" id="submit" name="submit" class="gradient-button">
				</fieldset>
			{{ Form::close() }}
		</article>
		<!--//contact form-->
	</aside>
	<!--//sidebar-->

</div>

@stop

@section('scripts')
	
	{{ HTML::script('//maps.google.com/maps/api/js?sensor=false') }}
	{{ HTML::script('assets/js/infobox.js') }}

	<script type="text/javascript">
	$(document).ready(function() {
		initialize();
	});

	function initialize() {
		var lat = {{ Config::get('site.lat', '41.32174775') }};
		var lng = {{ Config::get('site.lng', '19.81419213') }};

		var secheltLoc = new google.maps.LatLng(lat, lng);

		var myMapOptions = {
			 zoom: 15
			,center: secheltLoc
			,mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var theMap = new google.maps.Map(document.getElementById("map_canvas"), myMapOptions);


		var marker = new google.maps.Marker({
			map: theMap,
			draggable: true,
			position: new google.maps.LatLng(lat, lng),
			visible: true
		});

		var boxText = document.createElement("div");
		boxText.innerHTML = "<strong>Savatours</strong><br>Rr ”Brigada  VIII” Nr.2,<br>Tirane, Shqiperi";

		var myOptions = {
			 content: boxText
			,disableAutoPan: false
			,maxWidth: 0
			,pixelOffset: new google.maps.Size(-140, 0)
			,zIndex: null
			,closeBoxURL: ""
			,infoBoxClearance: new google.maps.Size(1, 1)
			,isHidden: false
			,pane: "floatPane"
			,enableEventPropagation: false
		};

		google.maps.event.addListener(marker, "click", function (e) {
			ib.open(theMap, this);
		});

		var ib = new InfoBox(myOptions);
		ib.open(theMap, marker);
	}

	$('#contactForm').submit(function() {
		var url = "{{ URL::to('/contact') }}";

		$.ajax({
			'type': 'POST',
			'url': url,
			'data': $('#contactForm').serialize(),
			'success' : function(data) {
				// console.log(data);
				$('#contactForm').html(data.message);
			}
		});

		return false;
	});
	</script>
@stop