@extends('layouts.master')

@section('slider')
	@include('includes.slider')
@stop

@section('content')

@include('pages.main_' . Config::get('site.site_name') )
{{-- {{ dd($sections) }} --}}
{{-- <section class="full"> 
	<div class="hover-top-list">
		<ul>	
			<a href="#">
				<li class="list-item">
					<div class="changeable-class">
						<span class="top-10">TOP 10</span>
						<span class="category">PER VEREN 2015 ›</span>
					</div>
					<span><img src="http://www.dev.odev.al/Banner/HV4.jpg" class="hover-top-list-img" style="-webkit-filter: brightness(1);"></span>
				</li>
			</a>
			<a href="#">
				<li class="list-item">
					<div class="changeable-class">
						<span class="top-10">TOP 10</span>
						<span class="category">PER VEREN 2015 ›</span>
					</div>
					<span><img src="http://www.dev.odev.al/Banner/HV4.jpg" class="hover-top-list-img" style="-webkit-filter: brightness(1);"></span>
				</li>
			</a>
			<a href="#">
				<li class="list-item">
					<div class="changeable-class">
						<span class="top-10">TOP 10</span>
						<span class="category">PER VEREN 2015 ›</span>
					</div>
					<span><img src="http://www.dev.odev.al/Banner/HV4.jpg" class="hover-top-list-img" style="-webkit-filter: brightness(1);"></span>
				</li>
			</a>
			<a href="#">
				<li class="list-item">
					<div class="changeable-class">
						<span class="top-10">TOP 10</span>
						<span class="category">PER VEREN 2015 ›</span>
					</div>
					<span><img src="http://www.dev.odev.al/Banner/HV4.jpg" class="hover-top-list-img" style="-webkit-filter: brightness(1);"></span>
				</li>
			</a>
		</ul>
	</div>			
</section>		 --}}				

@stop

@section('scripts')
<script type="text/javascript">

$(document).ready(function() {

	$(".list-item").mouseenter(function(){
		$(this).children(".changeable-class").addClass("reserve-hover-class");
		$(".hover-top-list-img").not($(this).find('.hover-top-list-img')).css("-webkit-filter","brightness(0.5)");
	});

	$(".list-item").mouseleave(function(){
		$(this).children(".changeable-class").removeClass("reserve-hover-class");
		$(".hover-top-list-img").not($(this).find('.hover-top-list-img')).css("-webkit-filter","brightness(1)");
	});

});

</script>
@stop