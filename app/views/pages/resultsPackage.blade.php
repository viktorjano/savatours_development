@extends('layouts.master')
@section('css')
@parent
{{-- <!-- Ionicons -->
<link href="{{ asset('assets/admin/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Ion Slider -->
<link href="{{ asset('assets/admin/css/ionslider/ion.rangeSlider.css') }}" rel="stylesheet" type="text/css" />
<!-- ion slider Nice -->
<link href="{{ asset('assets/admin/css/ionslider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" type="text/css" /> --}}

{{ HTML::style('assets/css/horizontal-calendar.css') }}
{{ HTML::style('assets/js/plugins/nouislider/jquery.nouislider.min.css') }}

@stop

@section('content')
<!--main content-->
<div class="content clearfix">	
		<!--sidebar-->
			@include('includes.resultsFiltersTop')
			<!-- @include('includes.resultsFilters') -->
		<!--//sidebar-->
		<!-- section previosly three-fourth (also name of the class) content, now full content-->
			<section class="full" style="margin-top: 0; border-top: 1px solid #eee; margin-top:0;">
				<div class="sort-by">
					<h3>
					@if($number_of_results <= 15)
						<strong>{{ $number_of_results }} </strong>
						{{ Lang::get('searchresults.results') }}					
					@else
						<strong>{{ $result_start }} - {{ $result_end }}</strong>	
						{{ Lang::get('searchresults.of') }}
						<strong> {{ strtolower($number_of_results) }}</strong>
						{{ Lang::get('searchresults.results') }}
					@endif
					</h3>						
{{-- 							<ul class="sort">
						<li>Cmimi <a href="#" title="rrites" class="rrites">rrites</a><a href="#" title="zbrites" class="zbrites">zbrites</a></li>
						<li>Yjet <a href="#" title="rrites" class="rrites">rrites</a><a href="#" title="zbrites" class="zbrites">zbrites</a></li>
						<li>Vleresimi <a href="#" title="rrites" class="rrites">rrites</a><a href="#" title="zbrites" class="zbrites">zbrites</a></li>
					</ul> --}}
{{-- 					<ul class="view-type">
						<li class="grid-view active"><a href="#" title="grid view">grid view</a></li>
						<li class="list-view"><a href="#" title="list view">list view</a></li>
						<li class="location-view"><a href="#" title="location view">location view</a></li>
					</ul> --}}
				</div>
				{{-- {{ dd($tours) }} --}}
				<div class="deals clearfix">
					{{-- */$i=1;/* --}}
					@foreach($tours as $tour) 
					<!--deal-->
					<!-- <article class="full {{ $i % 3 == 0 ? 'last' : '' }}"> -->
					{{-- {{ dd($tours) }} --}}
					<article class="full">
						<figure>
							<a href="{{ URL::to('pushime/hotel/?' . http_build_query($form['parameters']) . '&hotels=' . $tour['HotelInc']. '&claim=' . $tour['Cat_Claim']) }}" title="">
							<h1>
								{{{ ucwords(strtolower($tour['HotelName'])) }}}
								<span class="stars">
									@for($j = 1; $j <= $stars[$tour['StarName']]; $j++)
										*
									@endfor
								</span>											 
							</h1>
							</a>
							<!-- <figcaption><i class="fa fa-cutlery"></i> {{{ $concepts[$tour['MealName']] }}}</figcaption> -->				
							<a href="{{ URL::to('pushime/hotel/?' . http_build_query($form['parameters']) . '&hotels=' . $tour['HotelInc']. '&claim=' . $tour['Cat_Claim']) }}" title="">
								@if($covers[$tour['HotelInc']])
								<img src="{{ asset('image/268x152/' . $covers[$tour['HotelInc']]) }}" alt="" width="270" height="152">
								@else 
								<img src="{{ asset('/image/268x152/uploads/wIUzwr5m/2.jpg') }}" alt="" width="270" height="152">
								@endif
							</a>
						</figure>
						<div style="width: 25%; display: inline-block; vertical-align: top; text-align: left; padding-top: 5%;">
							<ul class="features">
								@if($features[$tour['HotelInc']])
									@foreach($features[$tour['HotelInc']] as $feature)
											<li> <span> </span> {{ $feature }}  </li>
									@endforeach
								@endif
							</ul>
						</div>
						<div class="details">								
							<!-- <h1>
								{{{ ucwords(strtolower($tour['HotelName'])) }}}
								<span class="stars">
									@for($j = 1; $j <= $stars[$tour['StarName']]; $j++)
										*
									@endfor
								</span>											 
							</h1> -->
							<span class="address"><i class="fa fa-map-marker"></i> {{{ ucwords(strtolower($tour['HotelTownName'])) }}},  <a href="#">{{{ ucwords(strtolower($state['name'])) }}}</a></span>					
							<span class="collection"> {{{ ucwords(strtolower($collections[$tour['HotelInc']]['name'])) }}}</span>

							<span class="price">
								<!-- <span class="concept">{{-- $concepts[$tour['MealName']] --}}</span>   -->
							<span style="float: left;">{{ Lang::get('searchresults.total_price') }}</span>						  
								<span style="float:right; line-height: 10px;">
									@if($normal_prices[$tour['Cat_Claim']] !== null && $normal_prices[$tour['Cat_Claim']] > $tour['Price']) 
									<span style="text-decoration:line-through; font-size: 1.1em;float:right;">€{{{ $normal_prices[$tour['Cat_Claim']] }}}</span> 
									@endif
									<br />
									<a href="{{ URL::to('pushime/hotel/?' . http_build_query($form['parameters']) . '&hotels=' . $tour['HotelInc']. '&claim=' . $tour['Cat_Claim']) }}" title="Shiko detajet" class="gradient-button right arrow-right"><span style="font-size:12px">€</span> {{{ $tour['Price'] }}}</a>	
								</span>
							</span>									
							<div class="description">
								<p style="width: 68%;">
								{{ Lang::get('searchresults.departure') }} {{ $tour['CheckIn'] }},
								{{ $tour['Nights'] }} {{ Lang::get('searchresults.nights') }} 
								{{ $concepts[$tour['MealName']] }} {{ Lang::get('searchresults.room') }}
								{{ ucwords(strtolower($tour['RoomName'])) }} / {{ ucwords(strtolower($tour['HtPlaceName'])) }} {{ Lang::get('searchresults.for') }} {{ $tour['Adult'] }} {{ Lang::get('searchresults.adults') }}, {{ $tour['Child'] or '0'}} {{ Lang::get('searchresults.children') }}.
								{{--
									<a href="{{{ 'pushime/hotel/?' . http_build_query($form['parameters']) . '&hotels=' . $tour['HotelInc'] }}}">Me shume</a>
								--}}
								</p>
							</div>									
						</div>
					</article>
					<!--//deal-->

					{{-- */$i++;/* --}}							
					@endforeach
					
					<!--bottom navigation-->
					<div class="bottom-nav">					 
						<!--pager-->
						<div class="pager">
							{{-- <span class="first"><a href="{{ URL::to('pushime?' .http_build_query( array_merge(Input::get(), array('page' => 1)))) }}"> {{ Lang::get('searchresults.first_page') }}</a></span> --}}
							<span><a href="{{ URL::to('pushime?' .http_build_query( array_merge(Input::get(), array('page' => Input::get('page')-1)))) }}" style="width: 30px">&lt;</a></span>
							@for($i = 1; $i <= $number_of_pages; $i++)
								@if(Input::get('page') == $i || !Input::has('page') && $i == 1)
									<span class="current">{{$i}}</span>
								@else
									<span><a href="{{ URL::to('pushime?' .http_build_query( array_merge(Input::get(), array('page' => $i)))) }}">{{$i}}</a></span>
								@endif
							@endfor
							<span><a href="{{ URL::to('pushime?' .http_build_query( array_merge(Input::get(), array('page' => Input::get('page')+1)))) }}">&gt;</a></span>
							{{-- <span class="last"><a href="{{ URL::to('pushime?' .http_build_query( array_merge(Input::get(), array('page' => $number_of_pages)))) }}">{{ Lang::get('searchresults.last_page')}}</a></span> --}}
						</div>
						<!--//pager-->							
					</div>
					<!--//bottom navigation-->
{{-- 					
					<!--back up button-->
					<a href="#" class="scroll-to-top" title="Kthehu lart">Kthehu lart</a> 
					<!--//back up button-->			 --}}		
				</div>
			</section>
			<!--//three-fourth content-->
		</div>
		<!--//main content-->
	</div>
</div>
@stop

@section('scripts')        
{{-- <!-- Ion Slider -->
<script src="{{ asset('assets/admin/js/plugins/ionslider/ion.rangeSlider.min.js') }}" type="text/javascript"></script> --}}
<script defer src="{{{ URL::to('/') . '/assets/js/plugins/sticky/jquery.sticky.js' }}}"></script>

{{ HTML::script('assets/js/plugins/nouislider/jquery.nouislider.all.min.js') }}

<script type="text/javascript">

	$(window).load(function() {
    
        $("#priceRange").noUiSlider({
			// start: 127,
			connect: true,
			behaviour: 'tap',
			start: [ 500, 4000 ],
			range: {
				// Starting at 500, step the value by 500,
				// until 4000 is reached. From there, step by 1000.
				'min': [ 0 ],
				'10%': [ 500, 500 ],
				'50%': [ 4000, 1000 ],
				'max': [ 10000 ]
			}		
		});
		
		$('.view-type li:first-child').addClass('active');

		var maxHeight = 0;
			
		$(".three-fourth .one-fourth").each(function(){
			if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
		});

		$(".three-fourth .one-fourth").height(maxHeight);	

    	// $("#sticker").sticky({
    	// 	topSpacing: 0,
    	// 	getWidthFrom: '.sticky-wrapper'
    	// });  	
    });
	
	
	$("input[name='concept[]']").change(function() {
		var concepts = $("input[name='concept[]']:checked").serialize();

		//window.location = window.location.href + serialize(concepts);
		console.log(window.location.href + serialize(concepts));
	}); 

	//towns filter   
	var towns;

	$("input[name='towns']").change(function() {
		towns = $.map($(':checkbox[name=towns]:checked'), function(n, i){
		      return n.value;
		}).join(',');

		//window.location = window.location.href + "&towns=" + towns;
		// console.log(towns);
	});   


   $("input[name='allTowns']").click(function(){

		if(this.checked) {
			$("input[name='towns']").attr('checked', this.checked);	
		}

		//window.location = window.location.href.replace(/&towns=[^&;]*/,'');
    });

   $("#getResults").click(function(){
   		if($("input[name='allTowns']").attr('checked')) {
   			window.location = window.location.href.replace(/&towns=[^&;]*/,'');
   		} else {
   			window.location = window.location.href + "&towns=" + towns;
   		}
   })

	// $("#alltowns").change( function() {
	// 	$("input[name='towns']").prop('checked', $(this).prop('checked'));
	// });

	//toggle towns area up and down
	$("#toggleTownsDisplay").on('click', function(){
		var $self = $(this);
		$(this).next().slideToggle('normal', function(){
			$self.toggleClass('arrowPosition');
		});
	})
			
</script>	

@stop
