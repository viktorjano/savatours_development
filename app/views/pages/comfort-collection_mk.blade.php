@extends('layouts.master')
@section('css')
@parent
{{ HTML::style('assets/css/collections.css') }}
{{ HTML::style('assets/css/tabs-collections.css') }}
@stop
@section('content')
	<!--main-->
	<div class="main" role="main">
		<div class="wrap clearfix">
			<!--main content-->
			<div class="content clearfix" style="background: #fff; padding-top:0;">
		<section class="full" style="margin-top: 0;">
							<section>

								<article class="collection-details" style="background:url({{asset('uploads/QDCfWGKP/genel-14.jpg')}}); background-size: cover;">
									<div class="left-box {{ $name }}">
							
		                    <div class="inner">
		    					<!-- this is to add logo image for other product pages. This alignment might have to be tested.-->
		    					<p>
		    					<img src="{{ asset('assets/images/txt/logo_inverse.png') }}" width="200" height="53" alt="logo">
								</p>

		    					<p class="collection-details-paragraph">COMFORT  хотелите нудат удобен All Inclusive одмор,за нив квалитетот понуден за цената е примарен.Плажата,базените и модерната архитектура во хотелот креираат компактна област полна со живост и забава за сите возрасти.Вашите денови поминати во овие хотели ќе ви поминат и без да сетите,во голем КОМФОРТ!</p>

		    					
				    				</div>
				    				<div class="collection-button">
				    					<a href="#">Најдете Comfort ресорт</a>
		    					</div>
						</div>
		<!-- 							<h1 style="position: absolute; display: block; z-index: 999; background: rgb(211, 239, 251); padding: 10px 20px 10px 10px; margin: 30px 0 0 10px; width: auto; color: #004d6d">Koleksioni Comfort Family</h1>
									<div style="background: #FDFAED;display: block;position: absolute;width: 47%;padding: 15px;top: 290px;margin: 0 0 0 10px;">
										<p>Fama dhe popullariteti që kanë midis pushuesve tanë është pika e përbashkët e hoteleve Comfort. Cilindo që të zgjedhësh, ndjesia që do të të pushtojë është e njëjta: Sikur pushimet të mos mbaronin kurrë! Festo në ambiente përrallore, ekzaltohu në akuaparqet fantastike, argëtohu pa pushim me mijëra zbavitje në ujë e në tokë, shijo larminë e ushqimit dhe të koktejleve freskuese.</p>
										<p class="teaser" style="padding-bottom: 10px;">Per me teper me lirine e formules All Inclusive pa limit!</p>
									</div>	 -->					
								</article>
							</section>

		<div class="reasons">
		<h2><span class="title">Причини за избор на Comfort ресорт</span></h2>
		<p>COMFORT  хотелите нудат удобен All Inclusive одмор,за нив квалитетот понуден за цената е примарен.Плажата,базените и модерната архитектура во хотелот креираат компактна област полна со живост и забава за сите возрасти.Вашите денови поминати во овие хотели ќе ви поминат и без да сетите,во голем КОМФОРТ!</p>
		</div>
{{--		<br>

 		<div class="tabs-container" id="tabs-container">
		    <ul class="tabs-menu">
		        <li class="current"><a href="#tab-1">ATMOSFERE E PAIMITUESHME</a></li>
		        <li><a href="#tab-2">VLERESIM</a></li>
		        <li><a href="#tab-3">ULTRA ALL INCLUSIVE PALIMIT</a></li>
		        <li><a href="#tab-4">DHOMA ME PAMJE SPEKTAKOLARE</a></li>
		    </ul>
		    <br>
		    <div class="tab">
		        <div id="tab-1" class="tab-content" style="display: block;">
		        	<div class="img-container">
		            <img src="http://savatours.com/uploads/jUNpyiDg/6.jpg">
		        </div>
		        <div class="tab-details">
		            <h3>Atmosfere e paimitueshme</h3>
									
									<p>Rezortet Platinum jane feste e vertete per te gjitha shqisat. Ambientet jashte dhe brenda ne harmoni me sherbimin cilesor, formulen Ultra All Inclusive, argetimin dhe relaksin krijojne magjine unike te paimitueshmete ketyre rezorteve. </p>
									<br>
									<br>
					<a href="#"> Најдете Comfort ресорт </a>
									
		        </div>
		        </div>
		        <div id="tab-2" class="tab-content" style="display: none;">
		            <div class="img-container">
		            <img src="http://savatours.com/uploads/Snbzo8A6/13.jpg">
		        </div>
		        <div class="tab-details">
		            <h3>Vleresim</h3>
									
									<p>Per t’u bere pjese e koleksionit Platinum,nje rezort duhet te kete marre vleresim maksimal nga pushuesit tane dhe vleresim maksimal nga Tripadvisor ose Holidaycheck. Shpesh jane pjese e grupeve te famshme si Amara, Gloria, Maxx. </p>
									<br>
									<br>
					<a href="#"> Најдете Comfort ресорт </a>
									
		        </div>
		        
		        </div>
		        <div id="tab-3" class="tab-content" style="display: none;">
		            <div class="img-container">
		            <img src="http://savatours.com/uploads/KApJYCSY/1.jpg">
		        </div>
		        <div class="tab-details">
		            <h3>Ultra All Inclusive</h3>
									
									<p>Togfjaleshi Ultra All Inclusive eshte mbartur ne njenivel tjeter te rezortet Platinum: cilesia me e larte ofrohet pa kursim, pa limit, pa orar; Ju beni banjo dielli, dikush ju ofron nje peshqir te fresket dikush tjeter ju fshin syzet ediellit, nje karroce me fruta te fresketa kalon prane cadres tende. Shijo larmine e sporteve dhe shfaqjet alla Broadway, papaguar bilete. </p>
									<br>
									<br>
					<a href="#"> Најдете Comfort ресорт </a>
									
		        </div>
		        </div>
		        <div id="tab-4" class="tab-content" style="display: none;">
		            <div class="img-container">
		            <img src="http://savatours.com/uploads/GuB4DsGQ/4.jpg" width="645px" height="354">
		        </div>
		        <div class="tab-details">
		            <h3>Dhomat</h3>
									
									<p>Dhomat 5yjore kane pamje spektakolare . Nga deti, nga gjelberimi, nga fushat e golfit, nga mali ose nga pishina. Cfaredo qe te jete eshte clodhese, relaksuese e kendshme dhe ju rigjeneron embush mekenaqesi. Shume ndryshe nga “Land View “ e shume hoteleve. </p>
									<br>
									<br>
					<a href="#"> Најдете Comfort ресорт </a>
									
		        </div>
		        </div>
		    </div>

		</div> --}}
<div style="width:1140px;height:200px;">
	<div class="top-5">
		
			<h4>
			<span class="top-hotel">Resorte</span>
			<span>
			<br>
			<span class="hotelet">Comfort</span>
{{-- 			<br>
			<span class="gold-family">Gold</span>
			</span> --}}
{{-- 			<span class="te-gjitha"></span>
			<br>
			<a href="#"> &gt; Shiko te gjitha</a> --}}
			</h4>

	</div>
	<div class="top-hotel-list">
		
			
				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/DoDAitKj/25437426.jpg') }}" width="145" height="83">
						<span>1</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=237&hotels%5B%5D=51&hotels%5B%5D=786&hotels%5B%5D=874&hotels%5B%5D=882&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Nashira Resort</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
						
				 			<hr>
							<p>Ne oferte!</p> 
						
					</div>
				</div>	



				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/4jeq7UGi/GENERAL%201.jpg') }}" width="145" height="83">
						<span>2</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=237&hotels%5B%5D=51&hotels%5B%5D=786&hotels%5B%5D=874&hotels%5B%5D=882&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Meryan Hotel</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/Z5uOkkke/13800574.jpg') }}" width="145" height="83">
						<span>3</span>
				    </div>

				    <div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=237&hotels%5B%5D=51&hotels%5B%5D=786&hotels%5B%5D=874&hotels%5B%5D=882&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Club Phaselis</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/9zUgL1Gb/5.jpg') }}" width="145" height="83">
						<span>4</span>
				    </div>
				

					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=237&hotels%5B%5D=51&hotels%5B%5D=786&hotels%5B%5D=874&hotels%5B%5D=882&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Sirius Deluxe Hotel</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/XfDXwWkI/43258157%20(1).jpg') }}" width="145" height="83">
						<span>5</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=237&hotels%5B%5D=51&hotels%5B%5D=786&hotels%5B%5D=874&hotels%5B%5D=882&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Sunis Elita Resort</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>	
	</div>
</div>
{{-- 				<div class="footer-img-banner">
					<a href="#"><img src="banner.png"></a>
				</div>
	 --}}



				</section>
			</div>
			<!--//main content-->
		</div>
	</div>
	<!--//main-->
@stop


@section('scripts')
  <script defer src="{{{ URL::to('/') . '/assets/js/plugins/flexslider/jquery.flexslider-min.js' }}}"></script>
  <script defer src="{{{ URL::to('/') . '/assets/js/plugins/sticky/jquery.sticky.js' }}}"></script>

  <script>
		        $(document).ready(function() {
		    $(".tabs-menu a").click(function(event) {
		        event.preventDefault();
		        $(this).parent().addClass("current");
		        $(this).parent().siblings().removeClass("current");
		        var tab = $(this).attr("href");
		        $(".tab-content").not(tab).css("display", "none");
		        $(tab).show();
		    });
		})  
    $(window).load(function(){
      $('.flexslider').flexslider({
	    animation: "slide",
	    controlNav: false,
      	animationLoop: true,	    
      });
    });

    $(document).ready(function() {
    	$("#sticker").sticky({
    		topSpacing: 0,
    		getWidthFrom: '.sticky-wrapper'
    	});
    });
  </script>

@stop

	
