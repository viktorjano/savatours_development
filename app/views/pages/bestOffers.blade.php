@extends('layouts.master')
@section('css')
@parent
<!-- Ionicons -->
<link href="{{ asset('assets/admin/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Ion Slider -->
<link href="{{ asset('assets/admin/css/ionslider/ion.rangeSlider.css') }}" rel="stylesheet" type="text/css" />
<!-- ion slider Nice -->
<link href="{{ asset('assets/admin/css/ionslider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" type="text/css" />
{{ HTML::style('assets/css/horizontal-calendar.css') }}
@stop
@section('content')
			<!--main content-->
{{-- 			<div class="content clearfix">
				<div class="filtri-neteve">
					<div class="filtro-net">Numri i neteve</div>
					<a href="#"><div class="nr-neteve active">7 NET</div></a>
					<a href="#"><div class="nr-neteve">9 NET</div></a>
					<a href="#"><div class="nr-neteve">12 NET</div></a>
				</div>		
				<div class="horizontal-calendar">
						<a href="#">
							<div class="date">
								<span class="dita">E djele</span>
								<br>
								<span class="data">31</span>
								<br>
								<span class="muaji">Maj</span>
							</div>
						</a>
						<a href="#">
							<div class="date no-flight">
								<span class="dita">E Hene</span>
								<br>
								<span class="data">1</span>
								<br>
								<span class="muaji">Qershor</span>
							</div>
						</a>
						<a href="#">
							<div class="date">
								<span class="dita">E Marte</span>
								<br>
								<span class="data">2</span>
								<br>
								<span class="muaji">Qershor</span>
							</div>
						</a>
						<a href="#">
							<div class="date active">
								<span class="dita">E Merkure</span>
								<br>
								<span class="data">3</span>
								<br>
								<span class="muaji">Qershor</span>
							</div>
						</a>
						<a href="#">
							<div class="date">
								<span class="dita">E Enjte</span>
								<br>
								<span class="data">4</span>
								<br>
								<span class="muaji">Qershor</span>
							</div>
						</a>
						<a href="#">
							<div class="date">
								<span class="dita">E Premte</span>
								<br>
								<span class="data">5</span>
								<br>
								<span class="muaji">Qershor</span>
							</div>
						</a>
						<a href="#">
							<div class="date no-flight" title="Nuk ka fluturim kete dite.">
								<span class="dita">E shtune</span>
								<br>
								<span class="data">6</span>
								<br>
								<span class="muaji">Qershor</span>
							</div>
						</a>
				</div>		
				<!--sidebar-->
				<aside class="left-sidebar">
					<div id="sticker" style="margin-bottom: 20px;">
						<article class="refine-search-results">
							<h2>Filtro rezultatet</h2>
							<dl>
								 
								<dt>Cmimi</dt>
								<dd style="display:block; height:auto;">
									<input id="priceRange" type="text" name="priceRange" value="{{ array_values($tours)[0]['Price']; }};{{ end($tours)['Price']; }}" style="display: none;">

								</dd>
									
								<dt>Koncepti</dt>
								<dd style="display:block; height:auto;">
									@foreach($meals as $meal_type)
									<div class="checkbox">
										<input type="checkbox" id="{{$meal_type['Inc']}}" name="concept[]" value="{{$meal_type['Inc']}}" {{in_array($meal_type['Inc'], $selected_meals) ? 'checked' : ''}}/>
										<label for="{{$meal_type['Inc']}}">{{$concepts[$meal_type['Name']]}}</label>
									</div>							
									@endforeach	
								</dd>
							</dl>		
						</article>
					</div>
				</aside> --}}
				<!--//sidebar-->
				<!--three-fourth content-->
					<section class="full">
						<div class="sort-by">
							<h3><strong>{{ count($tours) }} rezultate</strong></h3>						
{{-- 							<ul class="sort">
								<li>Cmimi <a href="#" title="rrites" class="rrites">rrites</a><a href="#" title="zbrites" class="zbrites">zbrites</a></li>
								<li>Yjet <a href="#" title="rrites" class="rrites">rrites</a><a href="#" title="zbrites" class="zbrites">zbrites</a></li>
								<li>Vleresimi <a href="#" title="rrites" class="rrites">rrites</a><a href="#" title="zbrites" class="zbrites">zbrites</a></li>
							</ul> --}}
							<ul class="view-type">
							{{-- 	<li class="grid-view active"><a href="#" title="grid view">grid view</a></li>
								<li class="list-view"><a href="#" title="list view">list view</a></li>
								<li class="location-view"><a href="#" title="location view">location view</a></li> --}}
							</ul>
						</div>
						
						<div class="deals clearfix">
							<pre>
							{{dd($tours)}}
							@foreach($tours as $tour) 

							<!--deal-->
							<article class="one-fourth {{{ $i % 4 == 0 ? 'last' : '' }}}">
								<figure>
									<figcaption><i class="fa fa-cutlery"></i> {{{ $concepts[$tour['MealName']] }}}</figcaption>							
									<a href="{{{ 'pushime/hotel/?' . http_build_query($form['parameters']) . '&hotels=' . $tour['HotelInc'] }}}" title="">
										@if($covers[$tour['HotelInc']])
										<img src="{{ asset('image/268x152/' . $covers[$tour['HotelInc']]) }}" alt="" width="270" height="152">
										@else 
										<img src="asset('image/268x152/uploads/wIUzwr5m/2.jpg')" alt="" width="270" height="152">
										@endif
									</a>
								</figure>
								<div class="details">								
									<h1>
										{{{ ucwords(strtolower($tour['HotelName'])) }}}
										<span class="stars">
											@for($j = 1; $j <= $stars[$tour['StarName']]; $j++)
												*
											@endfor
										</span>											 
									</h1>
									<span class="address"><i class="fa fa-map-marker"></i> {{{ ucwords(strtolower($tour['HotelTownName'])) }}},  <a href="#">{{{ ucwords(strtolower($state['name'])) }}}</a></span>					
									<span class="collection"> {{{ ucwords(strtolower($collections[$tour['HotelInc']]['name'])) }}}</span>

									<span class="price">
										<!-- <span class="concept">{{-- $concepts[$tour['MealName']] --}}</span>   -->
									<span style="float: left;">Cmimi total</span>								 	 
										<span style="float:right; line-height: 10px;">
											@if($normal_prices[$tour['Cat_Claim']] !== null && $normal_prices[$tour['Cat_Claim']] > $tour['Price']) 
											<span style="text-decoration:line-through; font-size: 1.1em;float:right;">€{{{ $normal_prices[$tour['Cat_Claim']] }}}</span> 
											@endif
											<br />
											<a href="{{{ 'pushime/hotel/?' . http_build_query($form['parameters']) . '&hotels=' . $tour['HotelInc'] }}}" title="Shiko detajet" class="gradient-button right arrow-right"><span style="font-size:12px">€</span> {{{ $tour['Price'] }}}</a>											
										</span>
									</span>									
									<div class="description">
										<p style="width: 68%;">
											Nisja {{{ $tour['CheckIn'] }}}, {{{ $tour['Nights'] }}} net 	{{{ $concepts[$tour['MealName']] }}} ne dhome {{{ ucwords(strtolower($tour['RoomName'])) }}} / {{{ ucwords(strtolower($tour['HtPlaceName'])) }}} per {{{ $tour['Adult'] }}} te rritur, {{{ $tour['Child'] or 'pa'}}} femije.
										{{--
											<a href="{{{ 'pushime/hotel/?' . http_build_query($form['parameters']) . '&hotels=' . $tour['HotelInc'] }}}">Me shume</a>
										--}}
										</p>
									</div>									
								</div>
							</article>
							<!--//deal-->

							{{-- */$i++;/* --}}							
							@endforeach
							
							<!--bottom navigation-->
							<div class="bottom-nav">
								<!--back up button-->
								<a href="#" class="scroll-to-top" title="Kthehu lart">Kthehu lart</a> 
								<!--//back up button-->
								{{-- 
								<!--pager-->
								<div class="pager">
									<span class="first"><a href="#">First page</a></span>
									<span><a href="#">&lt;</a></span>
									<span class="current">1</span>
									<span><a href="#">2</a></span>
									<span><a href="#">3</a></span>
									<span><a href="#">4</a></span>
									<span><a href="#">5</a></span>
									<span><a href="#">6</a></span>
									<span><a href="#">7</a></span>
									<span><a href="#">8</a></span>
									<span><a href="#">&gt;</a></span>
									<span class="last"><a href="#">Last page</a></span>
								</div>
								<!--//pager-->
								--}}
							</div>
							<!--//bottom navigation-->
						</div>
					</section>
				<!--//three-fourth content-->
			</div>
			<!--//main content-->
		</div>
	</div>
@stop

@section('scripts')        
		<!-- Ion Slider -->
<script src="{{ asset('assets/admin/js/plugins/ionslider/ion.rangeSlider.min.js') }}" type="text/javascript"></script>
<script defer src="{{{ URL::to('/') . '/assets/js/plugins/sticky/jquery.sticky.js' }}}"></script>

<script type="text/javascript">

	$(document).ready(function() {
        $('.slider').slider();
        /* ION SLIDER */
        $("#priceRange").ionRangeSlider({
            type: 'double',
            prefix: "€",
            prettify: false,
            hasGrid: true
        });

		// $('dt').each(function() {
		// 	var tis = $(this), state = false, answer = tis.next('dd').hide().css('height','auto').slideUp();
		// 	tis.click(function() {
		// 		state = !state;
		// 		answer.slideToggle(state);
		// 		tis.toggleClass('active',state);
		// 	});
		// });
		
		$('.view-type li:first-child').addClass('active');
		
		// $('#star').raty({
		// 	score    : 3,
		// 	starOff : 'images/ico/star-rating-off.png',
		// 	starOn  : 'images/ico/star-rating-on.png',
		// 	click: function(score, evt) {
		// 	alert('ID: ' + $(this).attr('id') + '\nscore: ' + score + '\nevent: ' + evt);
		//   }
		// });
	});
	
    $(document).ready(function() {
		var maxHeight = 0;
			
		$(".three-fourth .one-fourth").each(function(){
			if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
		});

		$(".three-fourth .one-fourth").height(maxHeight);	

    	{{-- Sticky 
    	$("#sticker").sticky({
    		topSpacing: 0,
    		getWidthFrom: '.sticky-wrapper'
    	});  	
    	--}}
    });
</script>	

@stop
