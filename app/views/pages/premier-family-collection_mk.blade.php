@extends('layouts.master')
@section('css')
@parent
{{ HTML::style('assets/css/collections.css') }}
{{ HTML::style('assets/css/tabs-collections.css') }}
@stop
@section('content')
	<!--main-->
	<div class="main" role="main">
		<div class="wrap clearfix">
			<!--main content-->
			<div class="content clearfix" style="background: #fff; padding-top:0;">
		<section class="full" style="margin-top: 0;">
							<section>

								<article class="collection-details" style="background: url('{{asset('image/1048x470/images/collections/premier_family_cover_1.jpg')}}'); background-size: cover;">
									<div class="left-box {{ $name }}">
							
		                    <div class="inner">
		    					<!-- this is to add logo image for other product pages. This alignment might have to be tested.-->
		    					<p>
		    					<img src="http://savatours.com/assets/images/txt/logo_inverse.png" width="200" height="53" alt="logo">
								</p>

		    					<p class="collection-details-paragraph">Славата и популарноста се два најважни аспекти, во вообичаените Premier ресорти. Прославувајте во фабулозна атмосфера; забавувајте се во фантастичниот аква парк, проведувајте се безкрајно во вода и на земја, уживајте во All Inclusive формула без ограничувања.</p>

		    					
				    				</div>
				    				<div class="collection-button">
				    					<a href="#">Најдете Premier Family ресорт</a>
		    					</div>
						</div>
		<!-- 							<h1 style="position: absolute; display: block; z-index: 999; background: rgb(211, 239, 251); padding: 10px 20px 10px 10px; margin: 30px 0 0 10px; width: auto; color: #004d6d">Koleksioni Premier Family Family</h1>
									<div style="background: #FDFAED;display: block;position: absolute;width: 47%;padding: 15px;top: 290px;margin: 0 0 0 10px;">
										<p>Fama dhe popullariteti që kanë midis pushuesve tanë është pika e përbashkët e hoteleve Premier Family. Cilindo që të zgjedhësh, ndjesia që do të të pushtojë është e njëjta: Sikur pushimet të mos mbaronin kurrë! Festo në ambiente përrallore, ekzaltohu në akuaparqet fantastike, argëtohu pa pushim me mijëra zbavitje në ujë e në tokë, shijo larminë e ushqimit dhe të koktejleve freskuese.</p>
										<p class="teaser" style="padding-bottom: 10px;">Per me teper me lirine e formules All Inclusive pa limit!</p>
									</div>	 -->					
								</article>
							</section>

		<div class="reasons">
		<h2><span class="title">Причини за избор на Premier Family ресорт</span></h2>
		<p>Славата и популарноста се два најважни аспекти, во вообичаените Premier ресорти. Прославувајте во фабулозна атмосфера; забавувајте се во фантастичниот аква парк, проведувајте се безкрајно во вода и на земја, уживајте во All Inclusive формула без ограничувања.</p>
		</div>
		<br>

		<div class="tabs-container" id="tabs-container">
		    <ul class="tabs-menu">
		        <li class="current"><a href="#tab-1">Слава</a></li>
		        <li><a href="#tab-2">Akua парк</a></li>
		        <li><a href="#tab-3">All Inclusive без ограничувања</a></li>
		        <li><a href="#tab-4">Забава</a></li>
		    </ul>
		    <br>
		    <div class="tab">
		        <div id="tab-1" class="tab-content" style="display: block;">
		        	<div class="img-container">
		            <img src="http://savatours.com/uploads/jUNpyiDg/6.jpg">
		        </div>
		        <div class="tab-details">
		            <h3>Слава</h3>
									
									<p>Premiere ресортите кои ги избравме остануваат популарни меѓу своите посетители, благодарение на нивните финеси и способноста да исполнат највисоки критериуми за да создадат единствена семејна атмосфера.  </p>
									<br>
									<br>
					<a href="#"> Најдете Premier Family ресорт </a>
									
		        </div>
		        </div>
		        <div id="tab-2" class="tab-content" style="display: none;">
		            <div class="img-container">
		            <img src="http://savatours.com/uploads/Snbzo8A6/13.jpg">
		        </div>
		        <div class="tab-details">
		            <h3>Akua парк</h3>
									
									<p>Се чини дека Premier ресортите се изградени исклучиво за деца: базени, лизгалки со возбудливи имиња и форми, миниклуб ови со богата забавна содржина. Во меѓувреме родители уживаат во деновите од одморот, опуштени и безбедни ... сигурни дека има кој да се грижи за децата. </p>
									<br>
									<br>
					<a href="#"> Најдете Premier Family ресорт </a>
									
		        </div>
		        
		        </div>
		        <div id="tab-3" class="tab-content" style="display: none;">
		            <div class="img-container">
		            <img src="http://savatours.com/uploads/KApJYCSY/1.jpg">
		        </div>
		        <div class="tab-details">
		            <h3>All Inclusive без ограничувања</h3>
									
									<p>Трка помеѓу хотелите, кој ќе понуди повеќе, подобри и поквалитетни усуги вклучени во платената цена. Го знаете чувството кога можете да го најдете секој можен вкус на сладолед, во било кое време од денот; веднаш да ви го послужат вашиот омилен пијалок; да најдете нешто да вкусите во секој час од денот и ноќта.   </p>
									<br>
									<br>
					<a href="#"> Најдете Premier Family ресорт </a>
									
		        </div>
		        </div>
		        <div id="tab-4" class="tab-content" style="display: none;">
		            <div class="img-container">
		            <img src="http://savatours.com/uploads/GuB4DsGQ/4.jpg" width="645px" height="354">
		        </div>
		        <div class="tab-details">
		            <h3>Забава</h3>
									
									<p>Premier Family ресортите ќе ве забавуваат секој миг од денот; повеќе избори на водни игри и игри на земја, музика и анимации; вечерни забави и жива музика. Токму како што сонувавте за вашите денови на одмор. </p>
									<br>
									<br>
					<a href="#"> Најдете Premier Family ресорт </a>
									
		        </div>
		        </div>
		    </div>

		</div>
		<hr>

<div style="width:1140px;height:200px;">
	<div class="top-5">
		
			<h4>
			<span class="top-hotel">Premier</span>
			<span>
			<br>
			<span class="hotelet">Family</span>
{{-- 			<br>
			<span class="gold-family">Gold</span>
			</span> --}}
{{-- 			<span class="te-gjitha"></span>
			<br>
			<a href="#"> &gt; Shiko te gjitha</a> --}}
			</h4>

	</div>
	<div class="top-hotel-list">
		
			
				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/Q91YyAmN/aqua-1.jpg') }}" width="145" height="83">
						<span>1</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=95&hotels%5B%5D=174&hotels%5B%5D=171&hotels%5B%5D=490&hotels%5B%5D=885&hotels%5B%5D=886&hotels%5B%5D=33&hotels%5B%5D=720&hotels%5B%5D=131&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Alibey Club Manavgat</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
						
				 			<hr>
							<p>Ne oferte!</p> 
						
					</div>
				</div>	



				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/QBRjZPE7/9.jpg') }}" width="145" height="83">
						<span>2</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=95&hotels%5B%5D=174&hotels%5B%5D=171&hotels%5B%5D=490&hotels%5B%5D=885&hotels%5B%5D=886&hotels%5B%5D=33&hotels%5B%5D=720&hotels%5B%5D=131&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Gural Premier Belek</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/XcBSWlQu/120_titanic_beach_lara_activities.jpg') }}" width="145" height="83">
						<span>3</span>
				    </div>

				    <div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=95&hotels%5B%5D=174&hotels%5B%5D=171&hotels%5B%5D=490&hotels%5B%5D=885&hotels%5B%5D=886&hotels%5B%5D=33&hotels%5B%5D=720&hotels%5B%5D=131&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Titanic Beach Resort</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/OBZFAIvR/-i-content-1865_2_GGR_General_view2.jpg') }}" width="145" height="83">
						<span>4</span>
				    </div>
				

					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=95&hotels%5B%5D=174&hotels%5B%5D=171&hotels%5B%5D=490&hotels%5B%5D=885&hotels%5B%5D=886&hotels%5B%5D=33&hotels%5B%5D=720&hotels%5B%5D=131&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Gloria Golf Resort</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/8T6Sd4Qb/EUROPEMEDITERRANEANTURKEYCON_TURTURKEY-ANTALYABELEKPAPILLONBELVILAPTHTL.jpg') }}" width="145" height="83">
						<span>5</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=95&hotels%5B%5D=174&hotels%5B%5D=171&hotels%5B%5D=490&hotels%5B%5D=885&hotels%5B%5D=886&hotels%5B%5D=33&hotels%5B%5D=720&hotels%5B%5D=131&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Papillon Belvil Hotel</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>	
	</div>
</div>
<div style="width:1140px;height:200px;">
	<div class="top-5">	</div>
	<div class="top-hotel-list">
		
			
				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/YXYz3S5o/kumkoybest.jpg') }}" width="145" height="83">
						<span>6</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=95&hotels%5B%5D=174&hotels%5B%5D=171&hotels%5B%5D=490&hotels%5B%5D=885&hotels%5B%5D=886&hotels%5B%5D=33&hotels%5B%5D=720&hotels%5B%5D=131&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Sunis Kumkoy Beach</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
						
				 			<hr>
							<p>Ne oferte!</p> 
						
					</div>
				</div>	



				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/fq7f8J1j/10498186_271236256394374_9130771984117048844_o.jpg') }}" width="145" height="83">
						<span>7</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=95&hotels%5B%5D=174&hotels%5B%5D=171&hotels%5B%5D=490&hotels%5B%5D=885&hotels%5B%5D=886&hotels%5B%5D=33&hotels%5B%5D=720&hotels%5B%5D=131&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Magic Life Jacaranda</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/Ubo2MMR6/crop%20(10).jpg') }}" width="145" height="83">
						<span>8</span>
				    </div>

				    <div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=95&hotels%5B%5D=174&hotels%5B%5D=171&hotels%5B%5D=490&hotels%5B%5D=885&hotels%5B%5D=886&hotels%5B%5D=33&hotels%5B%5D=720&hotels%5B%5D=131&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Club Jacaranda Luxury</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/scqq20R7/11011210_827202263994431_8496127167249150200_n.png') }}" width="145" height="83">
						<span>9</span>
				    </div>
				

					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=95&hotels%5B%5D=174&hotels%5B%5D=171&hotels%5B%5D=490&hotels%5B%5D=885&hotels%5B%5D=886&hotels%5B%5D=33&hotels%5B%5D=720&hotels%5B%5D=131&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Amara Club Marine</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>
	</div>
</div>
{{-- 				<div class="footer-img-banner">
					<a href="#"><img src="banner.png"></a>
				</div> --}}
			</section>
			</div>
			<!--//main content-->
		</div>
	</div>
	<!--//main-->
@stop


@section('scripts')
  <script defer src="{{{ URL::to('/') . '/assets/js/plugins/flexslider/jquery.flexslider-min.js' }}}"></script>
  <script defer src="{{{ URL::to('/') . '/assets/js/plugins/sticky/jquery.sticky.js' }}}"></script>

  <script>
	$(document).ready(function() {
		$(".tabs-menu a").click(function(event) {
			event.preventDefault();
			$(this).parent().addClass("current");
			$(this).parent().siblings().removeClass("current");
			var tab = $(this).attr("href");
			$(".tab-content").not(tab).css("display", "none");
			$(tab).show();
		});
	})  
    $(window).load(function(){
      $('.flexslider').flexslider({
	    animation: "slide",
	    controlNav: false,
      	animationLoop: true,	    
      });
    });

    $(document).ready(function() {
    	$("#sticker").sticky({
    		topSpacing: 0,
    		getWidthFrom: '.sticky-wrapper'
    	});
    });
  </script>

@stop

	
