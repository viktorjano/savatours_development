@extends('layouts.master')
@section('content')
<!--main content-->

<div class="content clearfix">		
	<!--three-fourth content-->
		<section class="three-fourth">
		@foreach($posts as $post)
			<!--post-->
			<article class="static-content post">
				<header class="entry-header">
					<h1><a href="{{ URL::to('/blog/' . $post->slug) }}">{{ $post->title }}</a></h1>
					<p class="entry-meta">
						<span class="date">Date: {{ date("F j, Y", strtotime($post->published_date)) }}</span> 
						<span class="author">Autor: {{ $post->author or 'Savatours' }}</span> 
						<span class="tags">Tage:
							<a rel="category tag" title="View all posts in Travel" href="#">Travel</a>, 
							<a rel="category tag" title="View all posts in Photography" href="#">Photography</a>
						</span>
<!-- 						<span class="comments"><a href="#">4 Comments</a></span>
 -->					</p>
				</header>
				<div class="entry-featured">
				@if($post->image)
					<a href="#">
						<figure style="max-height:400px; overflow:hidden">
							<img src="{{ URL::to($post->image) }}" alt="">
						</figure>
					</a>
				@endif
				</div>
				<div class="entry-content">
					<p>{{ $post->content }}</p>
					<a href="blog_single.html" class="gradient-button">Vazhdo leximin</a>
				</div>
			</article>
			<!--//post-->
		@endforeach
			
			<!--bottom navigation-->
			<div class="bottom-nav">
				<!--back up button-->
				<a href="#" class="scroll-to-top" title="Kthehu lart">Kthehu lart</a> 
				<!--//back up button-->
			</div>
			<!--//bottom navigation-->
		</section>
	<!--//three-fourth content-->
	
	<!--sidebar-->
	<aside class="right-sidebar">

		<!--Need Help Booking?-->
		<article class="default clearfix">
			<h2>Need Help Booking?</h2>
			<p>Call our customer services team on the number below to speak to one of our advisors who will help you with all of your holiday needs.</p>
			<p class="number">1- 555 - 555 - 555</p>
		</article>
		<!--//Need Help Booking?-->
		
		<!--Why Book with us?-->
		<article class="default clearfix">
			<h2>Why Book with us?</h2>
			<h3>Low rates</h3>
			<p>Get the best rates, or get a refund.<br>No booking fees. Save money!</p>
			<h3>Largest Selection</h3>
			<p>140,000+ hotels worldwide<br>130+ airlines<br>Over 3 million guest reviews</p>
			<h3>We’re Always Here</h3>
			<p>Call or email us, anytime<br>Get 24-hour support before, during, and after your trip</p>
		</article>
		<!--//Why Book with us?-->
		
		<!--Blog search-->
		<article class="default clearfix">
			<h2>Search</h2>
			<p>Lorem ipsum dolor sit amet</p>
			<div class="f-item active">
				<input type="search" id="blogsearch" name="blogsearch" placeholder="Search the blog">
			</div>
			<div class="f-item">
				<input type="submit" id="blogsearchsubmit" name="blogsearchsubmit" value="Search" class="gradient-button">
			</div>
		</article>
		<!--//Blog search-->
		
		<!--Blog archives-->
		<article class="default clearfix">
			<h2>Archive</h2>
			<ul>
				<li><a href="#">December 2013</a></li>
				<li><a href="#">November 2013</a></li>
				<li><a href="#">October 2013</a></li>
				<li><a href="#">September 2013</a></li>
				<li><a href="#">August 2013</a></li>
			</ul>
		</article>
		<!--//Blog archives-->
		
		<!--Deal of the day-->
		<article class="default clearfix">
			<h2>Deal of the day</h2>
			<div class="deal-of-the-day">
				<a href="hotel.html">
					<figure><img src="images/slider/img4.jpg" alt="" width="230" height="130"></figure>
					<h3>Plaza Resort Hotel &amp; SPA
						<span class="stars">
							<img src="images/ico/star.png" alt="">
							<img src="images/ico/star.png" alt="">
							<img src="images/ico/star.png" alt="">
							<img src="images/ico/star.png" alt="">
						</span>
					</h3>
					<p>From <span class="price">$ 100 <small>/ per night</small></span></p>
					<span class="rating"> 8 /10</span>
				</a>
			</div>
		</article>
		<!--//Deal of the day-->
	</aside>
	<!--//sidebar-->
</div><!--//main content-->
@stop