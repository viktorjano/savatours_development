<section class="offers home clearfix full">			

	<article class="one-half">
		<a href="{{ $secondary_banner->link or '#'}}">
		<img src="{{ asset('image/525x260/' . $secondary_banner->background_image) }}" alt=""/>

		</a>
	</article>

	{{-- Top Offers  1/2 --}}
	{{ $top_offers }}
		 	
	{{-- Collections 1/4 --}}
	@include('widgets.collections_' . Config::get('site.locale'))


</section>

						