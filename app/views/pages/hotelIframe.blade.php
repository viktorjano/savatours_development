@extends('layouts.masterIframe')
@section('css')
@parent
{{ HTML::style('assets/js/plugins/jgallery/jgallery.min.css') }}
<style type="text/css">

.options {
	float: left;
	margin-bottom: 20px;
	width: 100%;
}

.options .nights {
	float: left;
	margin: 0 0 20px 0;	
	width: 100%;
}

.nr-neteve.active {
	background-color: #004EC0 ;
	color: white;
}

.nr-neteve:hover{
	background-color: #004EC0;
	color:white;
}

.package-description {
	float: left;
	margin: 0 0 15px 0;
	font-size: 12px;	
}

.pp_content h2 {
  padding-top: 20px;
  text-align: center;
  font-size: 25px;	
  display: block;
  position: relative;
  clear: both;
}

</style>
@stop




@section('preheader')
<div id="call-now" class="hidden" style="position: relative;">
    <h2>Да го резервирате овој пакет јавете се на:</h2>
    <h2 style="padding-top:10px;"><b>{{ $partner_id_phone }}</b></h2>
    <h2>Или пратете емаил на: </h2>
    <h2 style="padding-top:10px;"><b>{{ $partner_id_email }}</b></h2>
    
    <br />
</div>
@stop

@section('content')

<!--main content-->
<div class="content clearfix" style="background: #fff;">
		<!--hotel details-->
{{-- 		<pre>
		{{dd($packet_info)}} --}}
		<article class="hotel-details header clearfix">
			<h1>{{{ ucwords(strtolower($packet_info[0]['HotelName'])) }}}
			<span class="stars">
				@for($j = 1; $j <= $stars[$tours[$packet_info[0]['Nights']][0]['StarName']]; $j++)
				*
				@endfor
			</span>		
			</h1>
			 {{-- 
			<div class="wish-list">
			  @if($inWishlist)
			  <a href="#" class="gradient-button clean-gray right active" id="addToWishList" data-url="{{ URL::to('/wishlist/remove/' . $tours[$packet_info[0]['Nights']][0]['HotelInc'] ) }}"><i class="fa fa-heart"></i> Hiq</a>
			  @else
			  <a href="#" class="gradient-button clean-gray right" id="addToWishList" data-url="{{ URL::to('/wishlist/add/' . $tours[$packet_info[0]['Nights']][0]['HotelInc'] ) }}"><i class="fa fa-heart-o"></i> Ruaj</a>
			  @endif
			</div>
			--}}
			<span class="address"><i class="fa fa-map-marker"></i> {{{  ucwords(strtolower($tours[$packet_info[0]['Nights']][0]['HotelTownName'])) }}},  {{{ ucwords(strtolower($state['name'])) }}}</span>
		</article>
		<!--//hotel details-->
	<!--hotel three-fourth content-->
	<section class="two-third">
 					
		<article>
			<!--inner navigation-->
			<nav class="inner-nav hotel">
				<ul>
					<li class="availability active"><a href="#permbledhje" title="Permbledhje">Permbledhje</a></li>
					<li class="resort"><a href="#resort_description" title="Resort Description">Rezorti</a></li>
					<li class="rooms"><a href="#rooms_description" title="Rooms Descriptions">Dhomat</a></li>
					<li class="rooms"><a href="#beach_pools_description" title="Beach & Pools Description">Plazhi dhe pishinat</a></li>
					<li class="rooms"><a href="#concept_description" title="Concept Description">Ushqimi dhe pijet</a></li>								
				</ul>
			</nav>
			<!--inner navigation-->
		</article>
		<!--permbledhje-->

		<!--gallery-->
		<article style="margin-bottom: 20px; padding-left: 20px;">
			<div id="gallery">
			@if(isset($album))
				@foreach($album->photos as $photo)
				    <a href="{{ asset( $photo->filename) }}"><img src="{{ asset('/image/75x75/' . $photo->filename) }}" alt="" style="display:none;" /></a>
				@endforeach
			@endif
			</div>	
		</article>	

		<section id="permbledhje" class="tab-content" style="display: block;">	
			@if( Config::get('locale') == 'mk' || Session::get('lang') == 'mk' )
				
				{{ $descriptions->resort_description or '' }}

			@elseif( Config::get('site.locale') == 'sq' || Session::get('lang') == 'sq' || !Session::has('lang'));
				{{ $descriptions->short_description or '' }}			
			@endif
		</section>
		<!--//permbledhje-->
		
{{-- 		<!--pershkrime-->
		<section id="resort_description" class="tab-content" style="display: none;">
			<article>
				{{ $descriptions->resort_description or '' }}
			</article>
		</section>
		<!--//pershkrime-->
		
		<!--dhomat-->
		<section id="rooms_description" class="tab-content" style="display: none;">
			<article>
				{{ $descriptions->rooms_description or '' }}
			</article>
		</section>
		<!--//dhomat-->

		<!--beach_pools_description-->
		<section id="beach_pools_description" class="tab-content" style="display: none;">
			<article>
				{{ $descriptions->beach_pools_description or '' }}
			</article>
		</section>		
		<!--//beach_pools_description-->	

		<!--/concept_description-->
		<section id="concept_description" class="tab-content" style="display: none;">
			<article>
				{{ $descriptions->concept_description or '' }}
			</article>
		</section>		
		<!--//concept_description-->	 --}}						
	</section>
	<!--//hotel content-->

	<!--sidebar-->
	<aside class="right-sidebar one-third">				
		<!--hotel details-->
	<div style="margin-bottom: 20px;">
		<article class="hotel-details clearfix">
		<div style="background: #E7E7E7; padding: 14px 7% 20px; color: #FF3751;">
			<div class="price">
				<span class="currency">€</span>
				<span class="value">{{ $packet_info[0]['Price'] }}</span>
				<span class="total">{{ Lang::get('hotel.package_price') }}</span>
			</div>

			@if($packet_info['normal_price'] !== null && $packet_info['normal_price'] > $packet_info[0]['Price']) 
				<div class="discount">
					<i class="fa fa-arrow-down"></i>
					{{ Lang::get('hotel.save') }} <b>€{{ $packet_info['normal_price'] - $packet_info[0]['Price'] }}</b> {{ Lang::get('hotel.in_total') }}
				</div> 
			@endif
			<div class="buttons">
				<a href="#call-now" class="gradient-button right arrow-right" rel="prettyPhoto">
				{{ Lang::get('hotel.book') }}
				</a>
{{-- 				  @if($inWishlist)
			  <a href="#" class="gradient-button clean-gray right active" id="addToWishList" data-url="{{ URL::to('/wishlist/remove/' . $tours[$packet_info[0]['Nights']][0]['HotelInc'] ) }}"><i class="fa fa-heart"></i> Hiq</a>
			  @else
			  <a href="#" class="gradient-button clean-gray right" id="addToWishList" data-url="{{ URL::to('/wishlist/add/' . $tours[$packet_info[0]['Nights']][0]['HotelInc'] ) }}"><i class="fa fa-heart-o"></i> {{ Lang::get('hotel.save_wishlist') }}</a>
			  @endif
 --}}			
 			</div>						
		</div>
		<div class="included">
			<span class="package-description">
				{{ $packet_info[0]['Adult'] }} {{ Lang::get('hotel.adults') }}, 

				@if($packet_info[0]['Child']  !== 0)
				{{ $packet_info[0]['Child'] }} {{ Lang::get('hotel.children') }}, 
				@endif
				 {{ Lang::get('searchresults.departure') }} {{$packet_info[0]['CheckIn']}} {{ Lang::get('hotel.accommodation') }} {{ ucwords(strtolower($packet_info[0]['RoomName'])) }} ({{ ucwords(strtolower($packet_info[0]['HtPlaceName'])) }}).
		 	</span>		
			<div class="options">
				<span style="font-size:15px;font-weight:600;">{{ Lang::get('hotel.number_of_nights') }}</span><br><br>
				<div class="nights">
					@foreach($nights_options as $nights)
					<a href="{{ URL::to('pushime/hotel/?'.http_build_query($form['parameters']).'&claim='. $tours[$nights][0]['Cat_Claim']) }}">
						<div class="nr-neteve {{$nights==$packet_info[0]['Nights'] ? 'active' : ''}}" style="float:left;font-size: 14px;padding: 4px;margin-right: 20px;  border: 1px solid rgb(169, 169, 169); text-transform:uppercase; border-radius: 3px;">
							{{$nights}} {{ Lang::get('hotel.nights') }}
						</div>
					</a>
					@endforeach
				</div>

				<span style="font-size:15px;font-weight:600;">{{ Lang::get('hotel.change_room') }}</span><br><br>
				<select style="height:27px;-webkit-border-radius: 3px;width:100%;background-color:#EDEBEB;" id="select-room">
						@foreach($tours[$packet_info[0]['Nights']] as $tour)
							<option value="{{URL::to('pushime/hotel/?'.http_build_query($form['parameters']).'&claim='.$tour['Cat_Claim'])}}" {{ $tour['RoomInc'] == $packet_info[0]['RoomInc'] ? 'selected': '' }}>
								{{ ucwords(strtolower($tour['RoomName'])) }} {{ ucwords(strtolower($tour['HtPlaceName'])) }} 
								(+ {{$tour['Price'] - $packet_info[0]['Price']}} €)
							</option>
						@endforeach
{{-- 					<option>Sea Room Double (+ 13 €) </option>
					<option>Standart Jacuzzi Room (+ 50 €)</option>
					<option>Sea Suit Room (+ 70 €)</option> --}}
				</select>
			</div>		

			<div class="description">
				<p>{{ Lang::get('hotel.whats_included') }}</p>
				<div class="included">
					<img width="24" height="24" src="{{{ URL::to('/') . '/assets/images/ico/flight_out.png' }}}">
				<span>{{ Lang::get('hotel.flights') }}</span>
				</div>		

				<div class="included">
					<img width="24" height="24" src="{{{ URL::to('/') . '/assets/images/ico/bus.png' }}}">
				<span>{{ Lang::get('hotel.transfers') }}</span>
				</div>	

				<div class="included">
				<img width="24" height="24" src="{{{ URL::to('/') . '/assets/images/ico/accomodation.png' }}}">
				<span>
					{{ $tours[$packet_info[0]['Nights']][0]['Nights'] }} 
					{{ Lang::get('hotel.nights') }}
				</span>
				</div>

				<div class="included">
				<img width="24" height="24" src="{{{ URL::to('/') . '/assets/images/ico/concept.png' }}}">
				<span> {{{ $concepts[$tours[$packet_info[0]['Nights']][0]['MealName']] }}}</span>
				</div>		

{{-- 							<div class="included">
				<img width="24" height="24" src="{{{ URL::to('/') . '/assets/images/ico/insurance.png' }}}">
				<span>Siguracion shendeti</span>
				</div>	

				<div style="text-align: center; margin: 0 10px 0 0; width: 70px; padding: 0; float: left;">
				<img width="24" height="24" style="float: left;" src="{{{ URL::to('/') . '/assets/images/ico/flight_out.png' }}}">
				<span style="clear: both; float: left; margin: 0px 0 0 0px; font-size: 11px;">Guide shqip</span>
				</div>	 --}}						
			</div>
		</div>
		</article>
	</div>
		<!--//hotel details-->					
		<!--testimonials-->
{{-- 					<article class="testimonials clearfix">
			<blockquote>{{{ $descriptions->slogan or ''}}}</blockquote>
			<span class="name">- Savatours, The Happymakers</span>
		</article> --}}
		<!--//testimonials-->
		
		<!--Why Book with us?-->
		<article class="default clearfix">
			<h2>Karakteristikat e hotelit</h2>
			<ul>
				@if($descriptions !== NULL && $descriptions->features !== NULL)
					@foreach($descriptions->features as $feature) 
						<li>{{{ $feature }}}</li>
					@endforeach
				@endif
			</ul>
		</article>
		<!--//Why Book with us?-->
		
		<!--Need Help Booking?-->
		<article class="default clearfix">
			<h2>{{ Lang::get('hotel.have_a_question') }}</h2>
			<p>{{ Lang::get('hotel.contact_customer_care') }}</p>
			<p class="number">{{ Config::get('site.phone1') }}</p>
		</article>
		<!--//Need Help Booking?-->	


		<div id="thumbnail-nav">
			<img src="http://maps.googleapis.com/maps/api/staticmap?center=36.682655174,29.073952944&amp;zoom=13&amp;size=317x213&amp;markers=color:orange|36.682655174,29.073952944&amp;Client=gme-tuitravelplc&amp;sensor=false" data-dojo-type="tui.widget.LazyLoadImage" data-dojo-props="source:'http://maps.googleapis.com/maps/api/staticmap?center=36.682655174,29.073952944&amp;zoom=13&amp;size=317x213&amp;markers=color:orange|36.682655174,29.073952944&amp;Client=gme-tuitravelplc&amp;sensor=false'" alt="Map of Sensatori Resort Fethiye" width="325" height="213" id="tui_widget_LazyLoadImage_5" lang="en" widgetid="tui_widget_LazyLoadImage_5" class="loaded">
		</div>


		<!--Popular hotels in the area-->
<!-- 					<article class="default clearfix">
			<h2>Hotele te ngjashme</h2>
			<ul class="popular-hotels">
				<li>
					<a href="#">
						<h3>Plaza Resort Hotel &amp; SPA
							<span class="stars">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
							</span>
						</h3>
						<p>From <span class="price">$ 100 <small>/ per night</small></span></p>
						<span class="rating"> 8 /10</span>
					</a>
				</li>
				<li>
					<a href="#">
						<h3>Lorem Ipsum Inn
							<span class="stars">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
							</span>
						</h3>
						<p>From <span class="price">$ 110 <small>/ per night</small></span></p>
						<span class="rating"> 7 /10</span>
					</a>
				</li>
				<li>
					<a href="#">
						<h3>Best Eastern London
							<span class="stars">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
							</span>
						</h3>
						<p>From <span class="price">$ 125 <small>/ per night</small></span></p>
						<span class="rating"> 8 /10</span>
					</a>
				</li>
				<li>
					<a href="#">
						<h3>Plaza Resort Hotel &amp; SPA
							<span class="stars">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
								<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
							</span>
						</h3>
						<p>From <span class="price">$ 100 <small>/ per night</small></span></p>
						<span class="rating"> 8 /10</span>
					</a>
				</li>
			</ul>
			<a href="#" title="Show all" class="show-all">Show all</a>
		</article> -->
		<!--//Popular hotels in the area-->
		
		<!--Deal of the day-->
<!-- 					<article class="default clearfix">
			<h2>Oferta e dites</h2>
			<div class="deal-of-the-day">
				<a href="hotel.html">
					<figure><img src="{{{ URL::to('/') . '/assets/images/slider/img2.jpg' }}}" alt="" width="230" height="130"></figure>
					<h3>Plaza Resort Hotel &amp; SPA
						<span class="stars">
							<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
							<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
							<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
							<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
						</span>
					</h3>
					<p>From <span class="price">$ 100 <small>/ per night</small></span></p>
					<span class="rating"> 8 /10</span>
				</a>
			</div>
		</article> -->
		<!--//Deal of the day-->
	</aside>
	<!--//sidebar-->
</div>
<!--//main content-->
@stop


@section('scripts')
<script defer src="{{{ URL::to('/') . '/assets/js/plugins/flexslider/jquery.flexslider-min.js' }}}"></script>
<script defer src="{{{ URL::to('/') . '/assets/js/plugins/sticky/jquery.sticky.js' }}}"></script>
<script defer src="{{{ URL::to('/') . '/assets/js/plugins/jgallery/jgallery.min.js' }}}"></script>

<script>
	$(document).ready(function() {

		$('#gallery').jGallery({
			height: '450px',
			canZoom: false,
			thumbHeight: 75,
			thumbWidth: 75,
			thumbnailsPosition: 'bottom',
			// tooltipSeeAllPhotos: 'Shiko te gjitha fotografite',
			// tooltipToggleThumbnails: 'Fotografite e vogla',
			// tooltipClose: 'Mbyll',
			preloadAll: false,
			hideThumbnailsOnInit: true,
			slideshow: true,
			slideshowAutostart: true,
			backgroundColor: '#fff',
			browserHistory: false,

		});


		$("#sticker").sticky({
		topSpacing: 0,
		getWidthFrom: '.sticky-wrapper'
		});

		$.ajaxSetup ({
		cache: false
		});

		{{-- Wishlist --}}
		$('#addToWishList').click(function(e) {
		var button = $(this);
		var theUrl = button.data('url');

		button.parent().hide;
		button.parent().html('<img src="http://www.stust.edu.tw/home/images/Loading_Animation.gif" />');

		if(!button.hasClass('active')) {
			  $.ajax({url:theUrl,success:function(result){
			  	button.addClass('active');
			  	button.html('<i class="fa fa-heart"></i> Hiq');
			    // console.log(result);
			  }
			});
		} else {
		  	$.ajax({
			  	url:theUrl,
			  	success:function(result){
				  	button.removeClass('active');
				  	button.html('<i class="fa fa-heart-o"></i> Ruaj');
				    // console.log(result);
			  	}, 
			  	complete: function() {

			  	}
			});		  		
		}

		e.preventDefault();
	});

		$('#select-room').change(function() {
			window.location = $(this).val();
			// console.log($(this).val());
		});

		$("a[rel^='prettyPhoto']").prettyPhoto({
			social_tools: ''
		});		
});
</script>



@stop


