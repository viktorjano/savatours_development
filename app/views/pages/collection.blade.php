@extends('layouts.master')

@section('content')
	<!--main-->
	<div class="main" role="main">
		<div class="wrap clearfix">
			<!--main content-->
			<div class="content clearfix" style="background: #fff;">

					<!--//hotel details-->
				<!--hotel three-fourth content-->
				<section class="three-fourth">
					<section>
						<article class="hotel-details header clearfix" style="padding-right:0px;height: 350px;overflow: hidden;background: url(http://dev.savatours.com/uploads/uDbSeQh3/4.jpg);background-size: cover;">
							<h1 style="position: absolute; display: block; z-index: 999; background: #f7e8ae; padding: 10px 20px 10px 10px; margin: 30px 0 0 10px; width: auto">Koleksioni Platinum</h1>
							<div style="background: #FDFAED;display: block;position: absolute;width: 47%;padding: 15px;top: 290px;margin: 0 0 0 10px;">
								<p>Platinum jane rezortet qe prodhojne eksperienca ekskluzive pushimesh, te pashembullta ne Mesdhe. Nje feste e vertete per te gjitha shqisat, koleksioni Platinum perfshin rezortet luksoze me vleresimin maksimal nga pushuesit tane.</p>
								<p class="teaser">Pergatitu per shijet me speciale, ambiente perrallore, trajtime clodhese, pamje spektakolare dhe standarte te larta sherbimi.</p>
							</div>						
						</article>
					</section>		
					<section class="clearfix tab-content" style="margin-top: 80px;">
						<article class="overview-block">
							<h2>Atmosfere e paimitueshme</h2>
							<div class="text">
							<p>Rezortet Platinum jane feste e vertete per te gjitha shqisat. Ambientet jashte dhe brenda ne harmoni me sherbimin cilesor, formulen Ultra All Inclusive, argetimin dhe relaksin krijojne magjine unike te paimitueshmete ketyre rezorteve. </p>
							</div>
							<img src="http://dev.savatours.com/uploads/jUNpyiDg/6.jpg" style="float: right; width: 340px;">
						</article>	
						<article class="overview-block">
							<h2>Vleresim</h2>
							<div class="text">
							<p style="margin-bottom: 0in">Per  t’u bere pjese e koleksionit Platinum,nje rezort duhet te kete marre vleresim maksimal nga pushuesit tane dhe vleresim maksimal nga Tripadvisor ose Holidaycheck. Shpesh jane pjese e grupeve te famshme si Amara, Gloria, Maxx.</p>
							</div>
							<img src="http://dev.savatours.com/uploads/Snbzo8A6/13.jpg" style="float: right; width: 340px;">
						</article>		
						<article class="overview-block">
							<h2>Ultra All Inclusive palimit</h2>
							<div class="text">
							<p style="margin-bottom: 0in">Togfjaleshi Ultra All Inclusive eshte mbartur ne njenivel tjeter te rezortet Platinum:  cilesia me e larte ofrohet pa kursim, pa limit, pa orar; Ju beni banjo dielli, dikush ju ofron nje peshqir te fresket dikush tjeter ju fshin syzet ediellit, nje karroce me fruta te fresketa kalon prane cadres tende. Shijo larmine e sporteve dhe shfaqjet alla Broadway, papaguar bilete.</p>
							</div>
							<img src="http://dev.savatours.com/uploads/KApJYCSY/1.jpg" style="float: right; width: 340px;">
						</article>			
						<article class="overview-block">
							<h2>Dhoma me pamje spektakolare</h2>
							<div class="text">
							<p style="margin-bottom: 0in">Dhomat 5yjore kane pamje spektakolare . Nga deti, nga gjelberimi, nga fushat e golfit, nga mali ose nga pishina. Cfaredo qe te jete eshte clodhese, relaksuese e kendshme dhe  ju rigjeneron embush mekenaqesi. Shume ndryshe nga “Land View “ e shume hoteleve.</p>
							</div>
							<img src="http://dev.savatours.com/uploads/GuB4DsGQ/4.jpg" style="float: right; width: 340px;">
						</article>																					
					</section>			
				</section>
				<!--//hotel content-->

				<!--sidebar-->
				<aside class="right-sidebar">
				<div class="clearfix"></div><br />
				<article class="hotel-details clearfix">
					<div style="background: #f7e8ae; padding: 14px 7% 20px">
						<div class="price" style="width: 100%;">
						<span class="value" style="display: inline-block;width: 100%;font-size: 2.3em;">Savatours Platinum</span>
						<span class="total" style="width: 100%;">Perzgjedhje e hoteleve me te mire ne Antalja.</span>
						</div>											
					</div>
						<div class="default">

							<ul>	 
								<li>Atmosfere e paimitueshme</li>
								<li>Vleresim maksimal nga klientet</li>	 
								<li>Arkitekture origjinale</li>
								<li>Ultra All Inclusive pa limit</li>
								<li>Pozicion fantastik</li>
								<li>Pishina ne forme lagune</li>
								<li>Dhoma me pamje spektakolare</li>
							</ul>				
							</div>
					</article>			
					<article class="default clearfix">
						<center><a href="#" title="Shiko Hotelet" class="gradient-button" style="font: normal 12px/40px 'OpenSansBold'; height: 40px;">Shiko Hotelet ne koleksion</a></center>
					</article>
					<!--Need Help Booking?-->
					<article class="default clearfix">
						<h2>Keni pyetje?</h2>
						<p>Kontaktoni ekipin tone te kujdesit ndaj klientit ne numrin e meposhtem per tu konsultuar me nje nga specialistet tane te pushimeve.</p>
						<p class="number">+355 42246730</p>
					</article>
					<!--//Need Help Booking?-->					

					<!--Popular hotels in the area-->
					<article class="default clearfix">
						<h2>Hotele te ngjashme</h2>
						<ul class="popular-hotels">
							<li>
								<a href="#">
									<h3>Plaza Resort Hotel &amp; SPA
										<span class="stars">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										</span>
									</h3>
									<p>Nga <span class="price">€ 1109 <small>/ per 7 net</small></span></p>
									<span class="rating"> 8 /10</span>
								</a>
							</li>
							<li>
								<a href="#">
									<h3>Lorem Ipsum Inn
										<span class="stars">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										</span>
									</h3>
									<p>Nga <span class="price">€ 1109 <small>/ per 7 net</small></span></p>
									<span class="rating"> 7 /10</span>
								</a>
							</li>
							<li>
								<a href="#">
									<h3>Best Eastern London
										<span class="stars">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										</span>
									</h3>
									<p>From <span class="price">$ 125 <small>/ per 7 net</small></span></p>
									<span class="rating"> 8 /10</span>
								</a>
							</li>
							<li>
								<a href="#">
									<h3>Plaza Resort Hotel &amp; SPA
										<span class="stars">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										</span>
									</h3>
									<p>Nga <span class="price">€ 1109 <small>/ per 7 net</small></span></p>
									<span class="rating"> 8 /10</span>
								</a>
							</li>
						</ul>
						<a href="#" title="Show all" class="show-all">Shiko te gjithe</a>
					</article>
					<!--//Popular hotels in the area-->
					
					<!--Deal of the day-->
					<article class="default clearfix">
						<h2>Oferta e dites</h2>
						<div class="deal-of-the-day">
							<a href="hotel.html">
								<figure><img src="{{{ URL::to('/') . '/assets/images/slider/img2.jpg' }}}" alt="" width="230" height="130"></figure>
								<h3>Plaza Resort Hotel &amp; SPA
									<span class="stars">
										<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
									</span>
								</h3>
								<p>Nga <span class="price">€ 1109 <small>/ per 7 net</small></span></p>
								<span class="rating"> 8 /10</span>
							</a>
						</div>
					</article>
					<!--//Deal of the day-->
				</aside>
				<!--//sidebar-->
			</div>
			<!--//main content-->
		</div>
	</div>
	<!--//main-->
@stop


@section('scripts')
  <script defer src="{{{ URL::to('/') . '/assets/js/plugins/flexslider/jquery.flexslider-min.js' }}}"></script>
  <script defer src="{{{ URL::to('/') . '/assets/js/plugins/sticky/jquery.sticky.js' }}}"></script>

  <script>
    $(window).load(function(){
      $('.flexslider').flexslider({
	    animation: "slide",
	    controlNav: false,
      	animationLoop: true,	    
      });
    });

    $(document).ready(function() {
    	$("#sticker").sticky({
    		topSpacing: 0,
    		getWidthFrom: '.sticky-wrapper'
    	});
    });
  </script>

@stop

	
