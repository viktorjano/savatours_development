<section class="offers home clearfix full">			

	{{-- @homepage_section('sequence_slider') --}}
	<article class="one-half">
		<a href="{{ $secondary_banner->link or '#'}}">
		{{ $secondary_banner->content }}
		<img src="{{ asset($secondary_banner->background_image) }}" alt=""/>
		</a>
	</article>

	{{-- Top Offers  1/2 --}}
	<article class="one-half" style="overflow-y:scroll;">
		<div class="top-offers-wrap" id="widget_1"></div>
	</article>
			 	
	{{-- Collections 1/4 --}}
	@homepage_section('collections_al')

	@include('includes.newsletter_signup')

</section>