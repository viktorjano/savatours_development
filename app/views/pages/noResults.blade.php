@extends('layouts.master')
@section('css')
@parent
{{-- <!-- Ionicons -->
<link href="{{ asset('assets/admin/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Ion Slider -->
<link href="{{ asset('assets/admin/css/ionslider/ion.rangeSlider.css') }}" rel="stylesheet" type="text/css" />
<!-- ion slider Nice -->
<link href="{{ asset('assets/admin/css/ionslider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" type="text/css" /> --}}

{{ HTML::style('assets/css/horizontal-calendar.css') }}
{{ HTML::style('assets/js/plugins/nouislider/jquery.nouislider.min.css') }}

@stop

@section('content')
<!--main content-->
<div class="content clearfix">	
		<!--sidebar-->
		<aside class="left-sidebar">
{{-- 			<div id="sticker" style="margin-bottom: 20px;">
				<article class="refine-search-results">
					<h2>{{ Lang::get('searchresults.filter_results') }}</h2>
					<a href="#" style="text"><h3>{{ Lang::get('searchresults.clear_all_filters') }}</h3></a>
				</article>
			</div> --}}
{{-- 			<div class="hover-top-list">
				<ul>	
					<li id="category-4">
						<div class="changeable-class-4 reserve-hover-class">
							<span class="top-10">TOP 10</span>
							<span class="category">PER VEREN 2015 ›</span>
						</div>
						<span><img src="http://www.dev.odev.al/Banner/HV4.jpg" id="HV4" class="hover-top-list-item" style="-webkit-filter: brightness(1);"></span>
					</li>
				</ul>
			</div>			 --}}
		</aside>		
		<!--//sidebar-->
		<!--three-fourth content-->
			<section class="three-fourth">
				<div class="sort-by">
					<h3 style="color: #666;">0 {{ trans('searchresults.results') }}. 
{{-- 					Provoni perseri duke pastruar filtrat e Kerkimit</h3> <a href="#" id="clear-filters" class="gradient-button clean-gray" style="margin-top: 9px; height:27px; font-size:1em;"> {{ Lang::get('searchresults.clear_filters') }} --}}
					</a>						
{{-- 							<ul class="sort">
						<li>Cmimi <a href="#" title="rrites" class="rrites">rrites</a><a href="#" title="zbrites" class="zbrites">zbrites</a></li>
						<li>Yjet <a href="#" title="rrites" class="rrites">rrites</a><a href="#" title="zbrites" class="zbrites">zbrites</a></li>
						<li>Vleresimi <a href="#" title="rrites" class="rrites">rrites</a><a href="#" title="zbrites" class="zbrites">zbrites</a></li>
					</ul> --}}
{{-- 					<ul class="view-type">
						<li class="grid-view active"><a href="#" title="grid view">grid view</a></li>
						<li class="list-view"><a href="#" title="list view">list view</a></li>
						<li class="location-view"><a href="#" title="location view">location view</a></li>
					</ul> --}}					
{{-- 					
					<!--back up button-->
					<a href="#" class="scroll-to-top" title="Kthehu lart">Kthehu lart</a> 
					<!--//back up button-->			 --}}	

				</div>
			</section>
			<!--//three-fourth content-->
		</div>

{{-- <section class="full" style="margin-top:5px;">
	<div class="banner-hover" style="background-image: url(http://www.dev.odev.al/Banner/5.jpg);">
		<div class="thumb-list">
			<ul>
				<li><img class="thumb-1" src="1.jpg"></li>
				<li><img class="thumb-2" src="2.jpg"></li>
				<li><img class="thumb-3" src="3.jpg"></li>
				<li><img class="thumb-4" src="4.jpg"></li>
				<li><img class="thumb-5" src="5.jpg"></li>
			</ul>
        </div>
        <div class="top-turkeye">
        	<h1>TOP destinacionet ne Turqi</h1>
       		<ul>
       		<li><i class="fa fa-circle"> </i> Resorte Moderne</li>
       			<li><i class="fa fa-circle"> </i> Ambjente Relaksuese</li>
       				<li><i class="fa fa-circle"> </i> Pishina Gjigande</li>
       					</ul>
        </div>
        <p id="selected-destination">Side</p>
    </div>
	
</section>		 --}}
		<!--//main content-->
	</div>
</div>
@stop

@section('scripts')        
{{-- <!-- Ion Slider -->
<script src="{{ asset('assets/admin/js/plugins/ionslider/ion.rangeSlider.min.js') }}" type="text/javascript"></script> --}}
<script defer src="{{{ URL::to('/') . '/assets/js/plugins/sticky/jquery.sticky.js' }}}"></script>

{{ HTML::script('assets/js/plugins/nouislider/jquery.nouislider.all.min.js') }}

<script type="text/javascript">

	$(window).load(function() {
    
{{--        $("#priceRange").noUiSlider({
			// start: 127,
			connect: true,
			behaviour: 'tap',
			start: [ 500, 4000 ],
			range: {
				// Starting at 500, step the value by 500,
				// until 4000 is reached. From there, step by 1000.
				'min': [ 0 ],
				'10%': [ 500, 500 ],
				'50%': [ 4000, 1000 ],
				'max': [ 10000 ]
			}		
		}); 

--}}
		$('.view-type li:first-child').addClass('active');

		// Make item height equal
		var maxHeight = 0;
		$(".three-fourth .one-fourth").each(function(){
			if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
		});
		$(".three-fourth .one-fourth").height(maxHeight);	

    });
	
</script>	

@stop
