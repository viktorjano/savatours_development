@extends('layouts.master')
@section('css')
@parent
{{ HTML::style('assets/css/collections.css') }}
{{ HTML::style('assets/css/tabs-collections.css') }}
@stop
@section('content')
	<!--main-->
	<div class="main" role="main">
		<div class="wrap clearfix">
			<!--main content-->
			<div class="content clearfix" style="background: #fff; padding-top:0;">
		<section class="full" style="margin-top: 0;">
			<section>
				<article class="collection-details" style="background: url('{{asset('image/1048x470/uploads/A0eXFEyp/il041872.jpg')}}'); background-size: cover;">

					<div class="left-box {{ $name }}">
								
				                <div class="inner">
									<!-- this is to add logo image for other product pages. This alignment might have to be tested.-->
									<p>
										<img src="http://savatours.com/assets/images/txt/logo_inverse.png" width="200" height="53" alt="logo">
									</p>

									<p class="collection-details-paragraph">Gold ресортите нудат уникатно искуство за одмор. Подгответе се себе си за слики од сказните, неповторливи неповторливи вкусови, третмани за опуштање, услуги со висок квалитет и луксузни детали и орнаменти. </p>

									
								</div>
								<div class="collection-button">
									<a href="#">Најдете GOLD ресорт</a>
								</div>
					</div>
					<!-- <h1 style="position: absolute; display: block; z-index: 999; background: rgb(211, 239, 251); padding: 10px 20px 10px 10px; margin: 30px 0 0 10px; width: auto; color: #004d6d">Koleksioni Gold Family</h1>
												<div style="background: #FDFAED;display: block;position: absolute;width: 47%;padding: 15px;top: 290px;margin: 0 0 0 10px;">
													<p>Fama dhe popullariteti që kanë midis pushuesve tanë është pika e përbashkët e hoteleve Gold. Cilindo që të zgjedhësh, ndjesia që do të të pushtojë është e njëjta: Sikur pushimet të mos mbaronin kurrë! Festo në ambiente përrallore, ekzaltohu në akuaparqet fantastike, argëtohu pa pushim me mijëra zbavitje në ujë e në tokë, shijo larminë e ushqimit dhe të koktejleve freskuese.</p>
													<p class="teaser" style="padding-bottom: 10px;">Per me teper me lirine e formules All Inclusive pa limit!</p>
												</div>	 -->					
				</article>
			</section>

		<div class="reasons">
			<h2><span class="title">Причини за избор на Gold ресорт</span></h2>
			<p>Gold ресортите нудат уникатно искуство за одмор. Подгответе се себе си за слики од сказните, неповторливи неповторливи вкусови, третмани за опуштање, услуги со висок квалитет и луксузни детали и орнаменти. </p>
		</div>
		<br>

 		<div class="tabs-container" id="tabs-container">
	    <ul class="tabs-menu">
	        <li class="current"><a href="#tab-1">Волшебна атмосфера</a></li>
	        <li><a href="#tab-2">Спектакуларни локации</a></li>
	        <li><a href="#tab-3">Луксузно сместување</a></li>
	        <li><a href="#tab-4">A LA CARTE</a></li>
	    </ul>
	    <br>
	    <div class="tab">
	        <div id="tab-1" class="tab-content" style="display: block;">
	        	<div class="img-container">
	            <img src="http://savatours.com/uploads/jUNpyiDg/6.jpg">
	        </div>
	        <div class="tab-details">
	            <h3>Волшебна атмосфера</h3>
				<p>Вгнездени во морските градови, преполни со луксуз и неумереност, Gold хотелите будат магични чувства кои не ќе можете да ги заборавите. Прекрасни холови, елегантни ресторани, луксузно уредување, каде луѓето што ги сретнувате се елегантно дотерани и убави, кои се чувствуваат привилегирано, и разгалено.</p>
				<br>
				<br>
				<a href="#"> Најдете GOLD ресорт </a>
								
	        </div>
	        </div>
	        <div id="tab-2" class="tab-content" style="display: none;">
	            <div class="img-container">
	            <img src="http://savatours.com/uploads/Snbzo8A6/13.jpg">
	        </div>
	        <div class="tab-details">
	            <h3>Спектакуларни локации</h3>	
				<p>Gold ресортите се сместени на најфантастичните локации. Уживајте во неограничените простори наменети за опуштање и забава; искористете ги чистите плажи (со сино знаме) кои се протегаат до бесконечноста; романтични глетки и импресивна архитектура. </p>
				<br>
				<br>
				<a href="#"> Најдете GOLD ресорт </a>
								
	        </div>
	        
	        </div>
	        <div id="tab-3" class="tab-content" style="display: none;">
	            <div class="img-container">
	            <img src="http://savatours.com/uploads/KApJYCSY/1.jpg">
	        </div>
	        <div class="tab-details">
	            <h3>Луксузно сместување</h3>
				<p>Дизајнирани со стилски и луксузем мебел, Wi-Fi и спектакуларен поглед, собите во Gold ресортите се само дел од уживањата на одморот. За да доживеете високи дози на луксуз, одберете ги Deluxe собите, вилите со приватни базени и кралските апартмани.  </p>
				<br>
				<br>
				<a href="#"> Најдете GOLD ресорт </a>
								
	        </div>
	        </div>
	        <div id="tab-4" class="tab-content" style="display: none;">
	            <div class="img-container">
	            <img src="http://savatours.com/uploads/GuB4DsGQ/4.jpg" width="645px" height="354">
	        </div>
	        <div class="tab-details">
	            <h3>A la Carte</h3>	
				<p>Базирано на принципот “кога сакате – каде сакате – што сакате” подгответе се себе си да примите максимални уживања на вашиот одмор. Деталите како : некој ви ги чисти наочарите за сонце додека вие се сончате; подвижно масиче со свежо овошје и сокови поминува покрај вашиот чадор – тоа е нормално во Gold ресортите. </p>
				<br>
				<br>
				<a href="#"> Најдете GOLD ресорт </a>
								
	        </div>
	        </div>
	    </div>

	</div> 
	<hr>
	



<div style="width:1140px;height:200px;">
	<div class="top-5">
		
			<h4>
			<span class="top-hotel">Resorte</span>
			<span>
			<br>
			<span class="hotelet">Gold</span>
{{-- 			<br>
			<span class="gold-family">Gold</span>
			</span> --}}
{{-- 			<span class="te-gjitha"></span>
			<br>
			<a href="#"> &gt; Shiko te gjitha</a> --}}
			</h4>

	</div>
	<div class="top-hotel-list">
		
			
				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/qfIKjGiI/11205103_921664514541426_5904576902960676331_n.jpg') }}" width="145" height="83">
						<span>1</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime/hotel/?t=131&hotels%5B0%5D=454&hotels%5B1%5D=45&hotels%5B2%5D=215&hotels%5B3%5D=489&hotels%5B4%5D=13&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6&hotels=489&claim=60589372') }}">Gural Premier Tekirova</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
						
				 			<hr>
							<p>Ne oferte!</p> 
					</div>
				</div>	



				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/kPLkruRQ/327_1_Gloria_Havadan_2011-0001_b.jpg') }}" width="145" height="83">
						<span>2</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime/hotel/?t=131&hotels%5B0%5D=454&hotels%5B1%5D=45&hotels%5B2%5D=215&hotels%5B3%5D=489&hotels%5B4%5D=13&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6&hotels=215&claim=60697426') }}">Gloria Serenity Resort</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/gYLwztBs/genel-8.jpg') }}" width="145" height="83">
						<span>3</span>
				    </div>

				    <div>
						<p><a href="{{ URL::to('pushime/hotel/?t=131&hotels%5B0%5D=454&hotels%5B1%5D=45&hotels%5B2%5D=215&hotels%5B3%5D=489&hotels%5B4%5D=13&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6&hotels=454&claim=60567526') }}">Alibey Resort Side</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/eSsORq2y/3.jpg') }}" width="145" height="83">
						<span>4</span>
				    </div>
				

					<div>
						<p><a href="{{ URL::to('pushime/hotel/?t=131&hotels%5B0%5D=454&hotels%5B1%5D=45&hotels%5B2%5D=215&hotels%5B3%5D=489&hotels%5B4%5D=13&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6&hotels=45&claim=57344434') }}">Amara Dolce Vita</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/wLwaGp7M/10750340_954187811265697_1060915764069434550_o.jpg') }}" width="145" height="83">
						<span>5</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime/hotel/?t=131&hotels%5B0%5D=454&hotels%5B1%5D=45&hotels%5B2%5D=215&hotels%5B3%5D=489&hotels%5B4%5D=13&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6&hotels=13&claim=60627292') }}">Papillon Zeugma</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>	
	</div>
</div>

{{-- 				<div class="footer-img-banner">
					<a href="#"><img src="banner.png"></a>
				</div> --}}
				</section>
			</div>
			<!--//main content-->
		</div>
	</div>
	<!--//main-->
@stop


@section('scripts')
  <script defer src="{{{ URL::to('/') . '/assets/js/plugins/flexslider/jquery.flexslider-min.js' }}}"></script>
  <script defer src="{{{ URL::to('/') . '/assets/js/plugins/sticky/jquery.sticky.js' }}}"></script>

  <script>
		        $(document).ready(function() {
		    $(".tabs-menu a").click(function(event) {
		        event.preventDefault();
		        $(this).parent().addClass("current");
		        $(this).parent().siblings().removeClass("current");
		        var tab = $(this).attr("href");
		        $(".tab-content").not(tab).css("display", "none");
		        $(tab).show();
		    });
		})  
    $(window).load(function(){
      $('.flexslider').flexslider({
	    animation: "slide",
	    controlNav: false,
      	animationLoop: true,	    
      });
    });

    $(document).ready(function() {
    	$("#sticker").sticky({
    		topSpacing: 0,
    		getWidthFrom: '.sticky-wrapper'
    	});
    });
  </script>

@stop

	
