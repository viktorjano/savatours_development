@extends('layouts.master')
@section('css')
@parent
{{ HTML::style('assets/css/rooms.css') }}
{{ HTML::style('assets/css/tabs-rooms.css') }}
{{ HTML::style('assets/css/acc.css') }}
@stop
@section('content')

			<!--main content-->
			<div class="content clearfix" style="background: #fff;">
				<ul id="progress">
				    <li>Hoteli</li>
				    <li class="active">Dhoma</li>
				    <li>Turistet</li>
				</ul>
					<!--hotel details-->
					<article class="hotel-details header clearfix">
						<h1>{{{ ucwords(strtolower($tours[$first_key][0]['HotelName'])) }}}
						<span class="stars">
							@for($j = 1; $j <= $stars[$tours[$first_key][0]['StarName']]; $j++)
							*
							@endfor
						</span>		
						</h1>
						  
{{-- 						<div class="wish-list">
						  @if($inWishlist)
						  <a href="#" class="gradient-button clean-gray right active" id="addToWishList" data-url="{{ URL::to('/wishlist/remove/' . $tours[$first_key][0]['HotelInc'] ) }}"><i class="fa fa-heart"></i> Hiq</a>
						  @else
						  <a href="#" class="gradient-button clean-gray right" id="addToWishList" data-url="{{ URL::to('/wishlist/add/' . $tours[$first_key][0]['HotelInc'] ) }}"><i class="fa fa-heart-o"></i> Ruaj</a>
						  @endif
						</div> --}}
						
{{-- 						<span class="address"><i class="fa fa-map-marker"></i> {{{  ucwords(strtolower($tours[$first_key][0]['HotelTownName'])) }}},  {{{ ucwords(strtolower($state['name'])) }}}</span> --}}
					</article>
					<!--//hotel details-->
				<!--hotel three-fourth content-->
				<section class="three-fourth">
				<div class="tabs-container" id="tabs-container">
				    <ul class="tabs-menu">
				    	@foreach($nights_options as $v)
				        	<li class="{{$v==$packet_info[0]['Nights'] ? 'current' : ''}}"><a href="#{{$v}}nights">{{$v}} NET</a></li>
				   		@endforeach
				    </ul>
					{{-- <br> --}}
					
				    <div class="tab">
				    	@foreach($nights_options as $v)
					        <section id="{{$v}}nights" class="tab-content" style="display:{{ $v == $packet_info[0]['Nights'] ? 'block' : 'none'}}">				
								<ul class="room-types">
									@foreach($tours[$v] as $tour) 
										<!--room-->
										<li class="{{ $tour['Cat_Claim'] == $form['parameters']['claim'] ? 'selected': '' }}">

											<div class="meta">
												<h2>{{{ ucwords(strtolower($tour['RoomName'])) }}} {{ ucwords(strtolower($tour['HtPlaceName'])) }}</h2>

												<p>{{{ $concepts[$tour['MealName']] }}}</p>
												
												@if($tour['Spog'] !== null) 
												<p>{{ $tour['SpogNote'] }}</p>
												@endif

											</div>
											<div class="room-information">

												<div class="row">
													<span class="first">Diferenca:</span>
													<span class="second"> € {{{ $tour['Price'] - $packet_info[0]['Price'] }}}</span>
												</div>								
												<a href="{{ URL::to('pushime/rezervo/?' . http_build_query($form['parameters']) . '&claim='. $tour['Cat_Claim']) }}" class="gradient-button" title="Zgjidh">Zgjidh</a>
											</div>

										</li>
										<!--//room-->
										@endforeach
								</ul>
						</section>	
					@endforeach			
					</div>
				</div>
			</section>



				<!--sidebar-->
				<aside class="right-sidebar">				
					<!--hotel details-->
				<div id="sticker" style="margin-bottom: 20px;">
<article class="booking-details clearfix">

						<h1>Zgjedhja juaj</h1>
{{-- 						<span class="address">Marylebone, London</span>
						<span class="rating"> 8 /10</span> --}}
						<div class="booking-info">
							<h6>Turiste</h6>
							<p>{{{ $tours[$first_key][0]['Adult'] }}} te rritur, {{ $tours[$first_key][0]['Child'] }} femije</p>
							<h6>Fluturimet</h6>
							<p><i class="fa fa-plane"></i> {{ $flight_info[0]['Name'] }} <br /> <i class="fa fa-plane fa-rotate-180"></i> {{ $flight_info[1]['Name'] }}</p>
							<h6>Check-in</h6>
							<p>{{ $packet_info[0]['CheckIn'] }}</p>
							<h6>Check-out</h6>
							<p>{{ $packet_info[0]['CheckOut'] }}</p>
							<h6>Akomodimi</h6>
							<p>{{ $packet_info[0]['Nights'] }} net ne dhome {{ ucwords(strtolower($packet_info[0]['RoomName']))}} {{ $tours[$first_key][0]['HtPlaceName'] }} </p>
						</div>
						<div class="price">

							<p class="total">Cmimi total:  €{{ $packet_info[0]['Price'] }}
							</p>
							<p>asnje kosto shtese</p>
						</div>
						<div class="action">
							<a href="http://dev.savatours.com/pushime/rezervo/?t=131&amp;checkin=03%2F06%2F2015&amp;display_date=M%C3%AB%2C+3+Qershor%2C+2015&amp;flexible-dates=on&amp;duration=6-9&amp;adults=2&amp;children=0&amp;child1_age=-1&amp;child2_age=-1&amp;child3_age=-1&amp;from=3&amp;s=6&amp;hotels=394&amp;hotels=394&amp;claim=58312146" title="Shiko detajet" class="gradient-button arrow-right" style="width: 90%; position:relative;"> Vazhdo Rezervimin</a>
						</div>
					</article>
				</div>
					<!--//hotel details-->					
					<!--testimonials-->
{{-- 					<article class="testimonials clearfix">
						<blockquote>{{{ $descriptions->slogan or ''}}}</blockquote>
						<span class="name">- Savatours, The Happymakers</span>
					</article> --}}
					<!--//testimonials-->
					
					<!--Why Book with us?-->
					<article class="default clearfix">
						<h2>Karakteristikat e hotelit</h2>
						<ul>
							@if($descriptions !== NULL && $descriptions->features !== NULL)
								@foreach($descriptions->features as $feature) 
									<li>{{{ $feature }}}</li>
								@endforeach
							@endif
						</ul>
					</article>
					<!--//Why Book with us?-->
					
					<!--Need Help Booking?-->
					<article class="default clearfix">
						<h2>Keni pyetje?</h2>
						<p>Kontaktoni ekipin tone te kujdesit ndaj klientit ne numrin e meposhtem per tu konsultuar me nje nga specialistet tane te pushimeve.</p>
						<p class="number">+355 42246730</p>
					</article>
					<!--//Need Help Booking?-->					

					<!--Popular hotels in the area-->
<!-- 					<article class="default clearfix">
						<h2>Hotele te ngjashme</h2>
						<ul class="popular-hotels">
							<li>
								<a href="#">
									<h3>Plaza Resort Hotel &amp; SPA
										<span class="stars">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										</span>
									</h3>
									<p>From <span class="price">$ 100 <small>/ per night</small></span></p>
									<span class="rating"> 8 /10</span>
								</a>
							</li>
							<li>
								<a href="#">
									<h3>Lorem Ipsum Inn
										<span class="stars">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										</span>
									</h3>
									<p>From <span class="price">$ 110 <small>/ per night</small></span></p>
									<span class="rating"> 7 /10</span>
								</a>
							</li>
							<li>
								<a href="#">
									<h3>Best Eastern London
										<span class="stars">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										</span>
									</h3>
									<p>From <span class="price">$ 125 <small>/ per night</small></span></p>
									<span class="rating"> 8 /10</span>
								</a>
							</li>
							<li>
								<a href="#">
									<h3>Plaza Resort Hotel &amp; SPA
										<span class="stars">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
											<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										</span>
									</h3>
									<p>From <span class="price">$ 100 <small>/ per night</small></span></p>
									<span class="rating"> 8 /10</span>
								</a>
							</li>
						</ul>
						<a href="#" title="Show all" class="show-all">Show all</a>
					</article> -->
					<!--//Popular hotels in the area-->
					
					<!--Deal of the day-->
<!-- 					<article class="default clearfix">
						<h2>Oferta e dites</h2>
						<div class="deal-of-the-day">
							<a href="hotel.html">
								<figure><img src="{{{ URL::to('/') . '/assets/images/slider/img2.jpg' }}}" alt="" width="230" height="130"></figure>
								<h3>Plaza Resort Hotel &amp; SPA
									<span class="stars">
										<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
										<img src="{{{ URL::to('/') . '/assets/images/ico/star.png' }}}" alt="">
									</span>
								</h3>
								<p>From <span class="price">$ 100 <small>/ per night</small></span></p>
								<span class="rating"> 8 /10</span>
							</a>
						</div>
					</article> -->
					<!--//Deal of the day-->
				</aside>
				<!--//sidebar-->
			</div>
			<!--//main content-->
@stop


@section('scripts')
  <script defer src="{{{ URL::to('/') . '/assets/js/plugins/flexslider/jquery.flexslider-min.js' }}}"></script>
  <script defer src="{{{ URL::to('/') . '/assets/js/plugins/sticky/jquery.sticky.js' }}}"></script>

<script>
    // $(document).ready(function() {
    //   $('.flexslider').flexslider({
	   //  animation: "slide",
	   //  controlNav: "thumbnails",
    //   	animationLoop: true,	    
    //   });
    
        $(document).ready(function() {

		    $(".tabs-menu a").click(function(event) {
		        event.preventDefault();
		        $(this).parent().addClass("current");
		        $(this).parent().siblings().removeClass("current");
		        var tab = $(this).attr("href");
		        $(".tab-content").not(tab).css("display", "none");
		        $(tab).show();
		    });

    	{{-- Sticky div 
    	$("#sticker").sticky({
    		topSpacing: 0,
    		getWidthFrom: '.sticky-wrapper'
    	});
    	--}}

	    $.ajaxSetup ({
	        cache: false
	    });

    	{{-- Wishlist --}}
    	$('#addToWishList').click(function(e) {
    		var button = $(this);
    		var theUrl = button.data('url');

    		//button.parent().hide;
    		// button.parent().html('<img src="http://www.stust.edu.tw/home/images/Loading_Animation.gif" />');

    		if(!button.hasClass('active')) {
			  $.ajax({url:theUrl,success:function(result){
			  	button.addClass('active');
			  	button.html('<i class="fa fa-heart"></i> Hiq');
			    console.log(result);
			  }});
		  	} else {
			  	$.ajax({
				  	url:theUrl,
				  	success:function(result){
					  	button.removeClass('active');
					  	button.html('<i class="fa fa-heart-o"></i> Ruaj');
					    console.log(result);
				  	}, 
				  	complete: function() {

				  	}
				});		  		
		  	}

			e.preventDefault();
    	});

    });
  </script>
<!--
	<script type="text/javascript" charset="utf-8">
	  $(document).ready(function(){
	    $("a[rel^='prettyPhoto']").prettyPhoto();
	  });
	</script>  
	-->

@stop

	
