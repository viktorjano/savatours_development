@extends('layouts.master')
@section('css')
@parent
{{ HTML::style('assets/css/collections.css') }}
{{ HTML::style('assets/css/tabs-collections.css') }}
@stop
@section('content')
	<!--main-->
	<div class="main" role="main">
		<div class="wrap clearfix">
			<!--main content-->
			<div class="content clearfix" style="background: #fff; padding-top:0;">
		<section class="full" style="margin-top: 0;">
							<section>

								<article class="collection-details" style="background:url('{{asset('uploads/v0UXkzXz/13.jpg')}}'); background-size: cover;">
									<div class="left-box {{ $name }}">
							
		                    <div class="inner">
		    					<!-- this is to add logo image for other product pages. This alignment might have to be tested.-->
		    					<p>
		    					<img src="http://savatours.com/assets/images/txt/logo_inverse.png" width="200" height="53" alt="logo">
								</p>

		    					<p class="collection-details-paragraph">Tek Platinum perfshihen rezortet me vleresimin maksimal nga klientet tane. Disa pej tyre jane fenomene te menjehershme. Si mund te jete ndryshe kur gjen ushqim te shijshem, sherbim cilesor dhe nje skuader qe ju mban te argetuar.</p>

		    					
				    				</div>
				    				<div class="collection-button">
				    					<a href="#">Gjej nje Resort Platinum</a>
		    					</div>
						</div>
		<!-- 							<h1 style="position: absolute; display: block; z-index: 999; background: rgb(211, 239, 251); padding: 10px 20px 10px 10px; margin: 30px 0 0 10px; width: auto; color: #004d6d">Koleksioni Platinum Family</h1>
									<div style="background: #FDFAED;display: block;position: absolute;width: 47%;padding: 15px;top: 290px;margin: 0 0 0 10px;">
										<p>Fama dhe popullariteti që kanë midis pushuesve tanë është pika e përbashkët e hoteleve Platinum. Cilindo që të zgjedhësh, ndjesia që do të të pushtojë është e njëjta: Sikur pushimet të mos mbaronin kurrë! Festo në ambiente përrallore, ekzaltohu në akuaparqet fantastike, argëtohu pa pushim me mijëra zbavitje në ujë e në tokë, shijo larminë e ushqimit dhe të koktejleve freskuese.</p>
										<p class="teaser" style="padding-bottom: 10px;">Per me teper me lirine e formules All Inclusive pa limit!</p>
									</div>	 -->					
								</article>
							</section>

		<div class="reasons">
		<h2><span class="title">Disa arsye pse te zgjedhesh Platinum</span></h2>
		<p>Tek Platinum perfshihen rezortet me vleresimin maksimal nga klientet tane. Disa pej tyre jane fenomene te menjehershme. Si mund te jete ndryshe kur gjen ushqim te shijshem, sherbim cilesor dhe nje skuader qe ju mban te argetuar.</p>
		</div>
		<br>

		<div class="tabs-container" id="tabs-container">
		    <ul class="tabs-menu">
		        <li class="current"><a href="#tab-1">MIKPRITJE</a></li>
		        <li><a href="#tab-2">KOMPAKT</a></li>
		        <li><a href="#tab-3">NJE EMER NJE GARANCI</a></li>
		        <li><a href="#tab-4">CIFTET</a></li>
		        <li><a href="#tab-5">FAMILJARET</a></li>
		    </ul>
		    <br>
		    <div class="tab">
		        <div id="tab-1" class="tab-content" style="display: block;">
		        	<div class="img-container">
		            <img src="http://savatours.com/uploads/jUNpyiDg/6.jpg">
		        </div>
		        <div class="tab-details">
		            <h3>Mikpritje</h3>
									
									<p>Rezortet Platinum jane mjeshter te artit te mikpritjes. Menjehere ju ndjeni natyrshem qe i perkisnin rezortit te zgjedhur.</p>
									<br>
									<br>
					<a href="#"> GJEJ NJE RESORT PLATINUM </a>
									
		        </div>
		        </div>
		        <div id="tab-2" class="tab-content" style="display: none;">
		            <div class="img-container">
		            <img src="http://savatours.com/uploads/Snbzo8A6/13.jpg">
		        </div>
		        <div class="tab-details">
		            <h3>Kompakt</h3>				
									<p>Nuk jane te lodhshme dhe te komplikuara. Ne nje territor kompakt, rezortet Platinum, ofrojne gjithe kenaqesite e pushimeve: pishinat, plazhin, lojrat. </p>
									<br>
									<br>
					<a href="#"> GJEJ NJE RESORT PLATINUM </a>
									
		        </div>
		        
		        </div>
		        <div id="tab-3" class="tab-content" style="display: none;">
		            <div class="img-container">
		            <img src="http://savatours.com/uploads/KApJYCSY/1.jpg">
		        </div>
		        <div class="tab-details">
		            <h3>Nje emer nje garanci</h3>
									
									<p>Zakonisht rezortet Platinum u perkasin emrave te njohur te hotelerise. Emra si Gloria, Amara, Sunis, Rubi jane garanci per pushime te paharrueshme. </p>
									<br>
									<br>
					<a href="#"> GJEJ NJE RESORT PLATINUM </a>
									
		        </div>
		        </div>
		        <div id="tab-4" class="tab-content" style="display: none;">
		            <div class="img-container">
		            <img src="http://savatours.com/uploads/GuB4DsGQ/4.jpg" width="645px" height="354">
		        </div>
		        <div class="tab-details">
		            <h3>Ciftet</h3>
									<p>Ideale per ciftet, rezortet Platinum ofrojne ambiente intime e romantike; aktivitete sportive, muzike live dhe argetim deri naten vone. </p>
									<br>
									<br>
					<a href="#"> GJEJ NJE RESORT PLATINUM </a>
									
		        </div>
		        </div>
		        <div id="tab-5" class="tab-content" style="display: none;">
		            <div class="img-container">
		            <img src="http://savatours.com/uploads/GuB4DsGQ/4.jpg" width="645px" height="354">
		        </div>
		        <div class="tab-details">
		            <h3>Familjaret</h3>
									<p>Prinder, zgjidhni nje nga rezortet Platinum per lirine e pushimeve pa portofol,  pa kokecarje. Lerini femijet te argetohen ne miniklube, pishina e lodra disa hapa larg jush dhe ju shijoni pushimet te qete. </p>
									<br>
									<br>
					<a href="#"> GJEJ NJE RESORT PLATINUM </a>
									
		        </div>
		        </div>		        
		    </div>

		</div>
		<hr>

<div style="width:1140px;height:200px;">
	<div class="top-5">
		
			<h4>
			<span class="top-hotel">Resorte</span>
			<span>
			<br>
			<span class="hotelet">Platinum</span>
{{-- 			<br>
			<span class="gold-family">Gold</span>
			</span> --}}
{{-- 			<span class="te-gjitha"></span>
			<br>
			<a href="#"> &gt; Shiko te gjitha</a> --}}
			</h4>

	</div>
	<div class="top-hotel-list">
		
			
				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/6EJM3KsR/asset.4847d.jpg') }}" width="145" height="83">
						<span>1</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=232&hotels%5B%5D=872&hotels%5B%5D=214&hotels%5B%5D=873&hotels%5B%5D=34&hotels%5B%5D=766&hotels%5B%5D=881&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Rubi Platunim Resort</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
						
				 			<hr>
							<p>Ne oferte!</p> 
						
					</div>
				</div>	



				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/X2vG91U5/IMG_6612.jpg') }}" width="145" height="83">
						<span>2</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=232&hotels%5B%5D=872&hotels%5B%5D=214&hotels%5B%5D=873&hotels%5B%5D=34&hotels%5B%5D=766&hotels%5B%5D=881&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Amara Wing Resort</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/8YbMRDYF/crop%20(3).jpg') }}" width="145" height="83">
						<span>3</span>
				    </div>

				    <div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=232&hotels%5B%5D=872&hotels%5B%5D=214&hotels%5B%5D=873&hotels%5B%5D=34&hotels%5B%5D=766&hotels%5B%5D=881&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Gloria Verde Resort</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/9O8wwiWP/allgemein_02.jpg') }}" width="145" height="83">
						<span>4</span>
				    </div>
				

					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=232&hotels%5B%5D=872&hotels%5B%5D=214&hotels%5B%5D=873&hotels%5B%5D=34&hotels%5B%5D=766&hotels%5B%5D=881&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Sunis Evren Resort</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>

				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/c6N5yUby/aycha44.jpg') }}" width="145" height="83">
						<span>5</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=232&hotels%5B%5D=872&hotels%5B%5D=214&hotels%5B%5D=873&hotels%5B%5D=34&hotels%5B%5D=766&hotels%5B%5D=881&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Papillon Ayscha Hotel</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>	
	</div>
</div>
<div style="width:1140px;height:200px;">
	<div class="top-5">	</div>
	<div class="top-hotel-list">
		
			
				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/GxXYPqBe/hotelll.jpg') }}" width="145" height="83">
						<span>6</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=232&hotels%5B%5D=872&hotels%5B%5D=214&hotels%5B%5D=873&hotels%5B%5D=34&hotels%5B%5D=766&hotels%5B%5D=881&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Azzura Deluxe Resort</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
						
				 			<hr>
							<p>Ne oferte!</p> 
						
					</div>
				</div>	



				<div class="top-hotelet">
					<div>
						<img src="{{ asset('image/268x152/uploads/06Lkg46i/15579381548_d3963ff938_c.jpg') }}" width="145" height="83">
						<span>7</span>
				    </div>
				
					<div>
						<p><a href="{{ URL::to('pushime?t=131&hotels%5B%5D=232&hotels%5B%5D=872&hotels%5B%5D=214&hotels%5B%5D=873&hotels%5B%5D=34&hotels%5B%5D=766&hotels%5B%5D=881&checkin=06%2F06%2F2015&display_date=6+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6') }}">Litore Resort & SPA</a>

							<img src="{{ asset('assets/images/ico/5stars.png') }}" style="height:16px;">
						</p>
									<hr>
						<p>Ne oferte!</p>
					</div>
				</div>
	</div>
</div>
{{-- 				<div class="footer-img-banner">
					<a href="#"><img src="banner.png"></a>
				</div> --}}
	



				</section>
			</div>
			<!--//main content-->
		</div>
	</div>
	<!--//main-->
@stop


@section('scripts')
  <script defer src="{{{ URL::to('/') . '/assets/js/plugins/flexslider/jquery.flexslider-min.js' }}}"></script>
  <script defer src="{{{ URL::to('/') . '/assets/js/plugins/sticky/jquery.sticky.js' }}}"></script>

  <script>
	$(document).ready(function() {
		$(".tabs-menu a").click(function(event) {
		event.preventDefault();
		$(this).parent().addClass("current");
		$(this).parent().siblings().removeClass("current");
		var tab = $(this).attr("href");
		$(".tab-content").not(tab).css("display", "none");
		$(tab).show();
		});
	})  
    $(window).load(function(){
      $('.flexslider').flexslider({
	    animation: "slide",
	    controlNav: false,
      	animationLoop: true,	    
      });
    });

    $(document).ready(function() {
    	$("#sticker").sticky({
    		topSpacing: 0,
    		getWidthFrom: '.sticky-wrapper'
    	});
    });
  </script>

@stop

	
