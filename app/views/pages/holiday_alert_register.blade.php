@extends('layouts.master')
@section('css')
@parent
{{ HTML::style('assets/css/holiday_alert.css') }}
@stop
@section('content')
<!--main content-->

<div class="content clearfix" style="">		
	<!--three-fourth content-->
<section class="three-fourth">
	<article class="static-content post" style="padding: 0px; width: 100%;">
		<div class="holiday-alert">
			<div class="alert-header">
				<span style="font-size: 24px;font-weight: bold;">Na lejoni te bejme kerkimin per ju...</span>
				<br>
				<span style="color:grey;font-size: 12px;">Plotesoni pyetjet e meposhtme dhe ne do ju dergojme oferta te personalizuara.</span>
			</div>
			<div class="alert-questions">
				<span class="white">1. </span><span class="question">Kur mund te niseni?</span>
				<div class="answer1">
					{{-- <div class="earliest">  --}}
						<div class="f-item datepicker">
							<input type="hidden" name="earliest" class="datepicker" value="{{{ $form['checkin'] }}}">
							<label for="earliest">Data me e afert qe mund te nisem eshte: </label>
							<div class="datepicker-wrap">
								<input type="text" value="" id="earliest" name="earliest" />
							</div>
						</div>
					{{-- </div> --}}
					<br>
					{{-- <div class="latest"> --}}
						<div class="f-item datepicker">
							<input type="hidden" name="earliest" class="datepicker" value="{{{ $form['checkin'] }}}">
							<label for="latest">Data me e larget qe mund te nisem eshte: </label>
							<div class="datepicker-wrap">
								<input type="text" value="" id="latest" name="latest" />
							</div>
						</div>
					{{-- </div> --}}
				</div>
			</div>
			<div class="alert-questions">
				<span class="white">2. </span><span class="question">Ku deshironi qe te shkoni?</span>
				<div class="answer1"> 
						<div class="f-item">
							<label for="t">Zgjidhni nje nga zonat e mundshme te plazheve te Turqise:</label>
								<select name="t" id="destination_town" class="uniform" style="opacity: 0;">
									<option value="131" selected=""> ANTALYA </option>
								</select>
						</div>
				</div>

			</div>
			<div class="alert-questions">
				<span class="white">3. </span><span class="question">Buxheti juaj</span>
				<div class="answer1">
					<div class="earliest">Sa deshironi te shpenzoni per pushimet tuaja ? 
						&nbsp;&nbsp;&nbsp;<input id="any-price" type="checkbox" name="any-price" style="width: 20px;	">Nuk kam ndonje ide
							<div class="budget-limit">
								Nga <input type="text" name="nga" class="text">€ deri ne <input type="text" name="deri" class="text">€
							</div>															
					</div>
					
				</div>

			</div>


			<div class="alert-questions">
				<span class="white">4. </span><span class="question">Sa shpesh deshironi te merni njoftime me e-mail ?</span>
				<div class="alert-interval">
				
					<div class="f-item">
						<input type="radio" name="alertInterval" id="daily" value="daily" />
						<label for="daily">Cdo dite</label>
					</div>
					<div class="f-item">
						<input type="radio" name="alertInterval" id="2days" value="2days" />
						<label for="2days">Cdo 2 dite</label>
					</div>
					<div class="f-item">
						<input type="radio" name="alertInterval" id="3days" value="daily" />
						<label for="3days">Cdo 3 dite</label>
					</div>																
					<div class="f-item">
						<input type="radio" name="alertInterval" id="weekly" value="weekly" />
						<label for="weekly">Cdo jave</label>
					</div>								
				</div>

			</div>
			<div class="alert-questions">						
				<span class="question"><i class="fa fa-user"> Te dhenat tuaja:</i></span>
				
				<div class="alert-interval">
					<div class="row triplets">
						<div class="f-item active">
							<label for="first_name">Emri</label>
							<input type="text" id="first_name" name="first_name">
						</div>
			
						<div class="f-item last">
							<label for="last_name">Mbiemri</label>
							<input type="text" id="last_name" name="last_name">
						</div>
						<div class="f-item active">
							<label for="first_name">E-mail</label>
							<input type="text" id="first_name" name="first_name">
						</div>

					</div>														    	
{{-- 						    	Emri <input type="text" name="fname" required="" class="text">&nbsp;&nbsp;
			    	Mbiemri <input type="text" name="fname" required="" class="text"><br><br>
			    	E-mail <input type="text" name="email" required="" class="text"><br> --}}							
				</div>
			</div>

			<div class="rregjistrohu">
					<input type="submit" value="Regjistrohu" class="rregjistrohu-button">
			</div>
			
		</div>	
	</article>								
</section>
	<!--//three-fourth content-->
	
<aside class="right-sidebar">				

					<article class="default clearfix">
						<h2>Newsletter</h2>
						<h3>Oferta te personalizuara</h3>
						<p>drejt e ne inbox-in tuaj.</p>
						<h3>Gjithcka nen kontroll</h3>
						<p>zgjidhni ju sa shpesh deshironi te merrni njoftime.</p>
						<h3>Ndryshoni</h3>
						<p>pershtatni preferencat tuaja sa here te deshironi.</p>
						<h3>Eshte zgjedhja juaj</h3>
						<p>ndaloni njoftimet kur te deshironi.</p>						
					</article>					
					<!--Need Help Booking?-->
					<article class="default clearfix">
						<h2>Keni pyetje?</h2>
						<p>Kontaktoni ekipin tone te kujdesit ndaj klientit ne numrin e meposhtem per tu konsultuar me nje nga specialistet tane te pushimeve.</p>
						<p class="number">+355 42246730</p>
					</article>
					<!--//Need Help Booking?-->					

					<!--Popular hotels in the area-->
<!-- 					<article class="default clearfix">
						<h2>Hotele te ngjashme</h2>
						<ul class="popular-hotels">
							<li>
								<a href="#">
									<h3>Plaza Resort Hotel &amp; SPA
										<span class="stars">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
										</span>
									</h3>
									<p>From <span class="price">$ 100 <small>/ per night</small></span></p>
									<span class="rating"> 8 /10</span>
								</a>
							</li>
							<li>
								<a href="#">
									<h3>Lorem Ipsum Inn
										<span class="stars">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
										</span>
									</h3>
									<p>From <span class="price">$ 110 <small>/ per night</small></span></p>
									<span class="rating"> 7 /10</span>
								</a>
							</li>
							<li>
								<a href="#">
									<h3>Best Eastern London
										<span class="stars">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
										</span>
									</h3>
									<p>From <span class="price">$ 125 <small>/ per night</small></span></p>
									<span class="rating"> 8 /10</span>
								</a>
							</li>
							<li>
								<a href="#">
									<h3>Plaza Resort Hotel &amp; SPA
										<span class="stars">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
											<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
										</span>
									</h3>
									<p>From <span class="price">$ 100 <small>/ per night</small></span></p>
									<span class="rating"> 8 /10</span>
								</a>
							</li>
						</ul>
						<a href="#" title="Show all" class="show-all">Show all</a>
					</article> -->
					<!--//Popular hotels in the area-->
					
					<!--Deal of the day-->
<!-- 					<article class="default clearfix">
						<h2>Oferta e dites</h2>
						<div class="deal-of-the-day">
							<a href="hotel.html">
								<figure><img src="http://dev.savatours.com/assets/images/slider/img2.jpg" alt="" width="230" height="130"></figure>
								<h3>Plaza Resort Hotel &amp; SPA
									<span class="stars">
										<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
										<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
										<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
										<img src="http://dev.savatours.com/assets/images/ico/star.png" alt="">
									</span>
								</h3>
								<p>From <span class="price">$ 100 <small>/ per night</small></span></p>
								<span class="rating"> 8 /10</span>
							</a>
						</div>
					</article> -->
					<!--//Deal of the day-->
				</aside>
</div><!--//main content-->
@stop