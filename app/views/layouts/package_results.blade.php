﻿@extends('layouts.master')
	{{-- Nese na duhet te injektojme dicka pas body para kontentit --}}
	@yield('preheader')

	<!--header-->
	<header>
		@include('includes.header')       
	</header>
	<!--//header-->
	
	<!--search-->
	<div class="main-search">
		@include('forms.mainSearch')
	</div>
	<!--//search-->
        
        
	<!--main-->
	<div class="main" role="main">
		<div class="wrap clearfix">
			@yield('content')
		</div>
	</div>
	<!--//main-->

	@section('scripts')
		<script type="text/javascript">
			$(document).ready(function() {
				$('dt').each(function() {
					var tis = $(this), state = false, answer = tis.next('dd').hide().css('height','auto').slideUp();
					tis.click(function() {
						state = !state;
						answer.slideToggle(state);
						tis.toggleClass('active',state);
					});
				});
				
				$('.view-type li:first-child').addClass('active');
				
				// $('#star').raty({
				// 	score    : 3,
				// 	starOff : 'images/ico/star-rating-off.png',
				// 	starOn  : 'images/ico/star-rating-on.png',
				// 	click: function(score, evt) {
				// 	alert('ID: ' + $(this).attr('id') + '\nscore: ' + score + '\nevent: ' + evt);
				//   }
				// });
			});
			
			$(window).load(function () {
				var maxHeight = 0;
					
				$(".three-fourth .one-fourth").each(function(){
					if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
				});
				$(".three-fourth .one-fourth").height(maxHeight);	
			});	
			</script>	
	@stop


