<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Savatours :: The Happymakers</title>

    <meta name="description" content="Savatours :: The Happy makers">
    <meta name="author" content="TSMS">

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
    {{ HTML::style('/assets/css/style-bootstrap.css') }}
</head>
  <body>

    <div class="container-fluid home-page">
    	<div class="row main-menu">
    		<nav class="navbar navbar-inverse">
			    <div class="navbar-header">
			    	<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<img class="logo" src="https://www.savatours.com/assets/images/txt/logo_inverse.png" width="152px">
				</div>
	
	<div class="collapse navbar-collapse js-navbar-collapse">
		<ul class="nav navbar-nav">
			<li class="dropdown mega-dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">PUSHIME <span class="caret"></span></a>				
				<ul class="dropdown-menu mega-dropdown-menu">
					<li class="col-sm-3" style=>
						<ul>
							<li class="dropdown-header">Men Collection</li>                            
                            <div id="menCollection" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                <div class="item active">
                                    <a href="#"><img src="http://placehold.it/254x150/ff3546/f5f5f5/&text=New+Collection" class="img-responsive" alt="product 1"></a>
                                    <h4><small>Summer dress floral prints</small></h4>                                        
                                    <button class="btn btn-primary" type="button">49,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>       
                                </div><!-- End Item -->
                                <div class="item">
                                    <a href="#"><img src="http://placehold.it/254x150/3498db/f5f5f5/&text=New+Collection" class="img-responsive" alt="product 2"></a>
                                    <h4><small>Gold sandals with shiny touch</small></h4>                                        
                                    <button class="btn btn-primary" type="button">9,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>        
                                </div><!-- End Item -->
                                <div class="item">
                                    <a href="#"><img src="http://placehold.it/254x150/2ecc71/f5f5f5/&text=New+Collection" class="img-responsive" alt="product 3"></a>
                                    <h4><small>Denin jacket stamped</small></h4>                                        
                                    <button class="btn btn-primary" type="button">49,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>      
                                </div><!-- End Item -->                                
                              </div><!-- End Carousel Inner -->
                              <!-- Controls -->
                              <a class="left carousel-control" href="#menCollection" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                              </a>
                              <a class="right carousel-control" href="#menCollection" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                              </a>
                            </div><!-- /.carousel -->
                            <li class="divider"></li>
                            <li><a href="#">View all Collection <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
						</ul>
					</li>
					<li class="col-sm-3">
						<ul>
							<li class="dropdown-header">Features</li>
							<li><a href="#">Auto Carousel</a></li>
                            <li><a href="#">Carousel Control</a></li>
                            <li><a href="#">Left & Right Navigation</a></li>
							<li><a href="#">Four Columns Grid</a></li>
							<li class="divider"></li>
							<li class="dropdown-header">Fonts</li>
                            <li><a href="#">Glyphicon</a></li>
							<li><a href="#">Google Fonts</a></li>
						</ul>
					</li>
					<li class="col-sm-3">
						<ul>
							<li class="dropdown-header">Plus</li>
							<li><a href="#">Navbar Inverse</a></li>
							<li><a href="#">Pull Right Elements</a></li>
							<li><a href="#">Coloured Headers</a></li>                            
							<li><a href="#">Primary Buttons & Default</a></li>							
						</ul>
					</li>
					<li class="col-sm-3">
						<ul>
							<li class="dropdown-header">Much more</li>
                            <li><a href="#">Easy to Customize</a></li>
							<li><a href="#">Calls to action</a></li>
							<li><a href="#">Custom Fonts</a></li>
							<li><a href="#">Slide down on Hover</a></li>                         
						</ul>
					</li>
				</ul>				
			</li>
            <li class="dropdown mega-dropdown">
    			<a href="#" class="dropdown-toggle" data-toggle="dropdown">TURE <span class="caret"></span></a>				
				<ul class="dropdown-menu mega-dropdown-menu">
					<li class="col-sm-3">
    					<ul>
							<li class="dropdown-header">Features</li>
							<li><a href="#">Auto Carousel</a></li>
                            <li><a href="#">Carousel Control</a></li>
                            <li><a href="#">Left & Right Navigation</a></li>
							<li><a href="#">Four Columns Grid</a></li>
							<li class="divider"></li>
							<li class="dropdown-header">Fonts</li>
                            <li><a href="#">Glyphicon</a></li>
							<li><a href="#">Google Fonts</a></li>
						</ul>
					</li>
					<li class="col-sm-3">
						<ul>
							<li class="dropdown-header">Plus</li>
							<li><a href="#">Navbar Inverse</a></li>
							<li><a href="#">Pull Right Elements</a></li>
							<li><a href="#">Coloured Headers</a></li>                            
							<li><a href="#">Primary Buttons & Default</a></li>							
						</ul>
					</li>
					<li class="col-sm-3">
						<ul>
							<li class="dropdown-header">Much more</li>
                            <li><a href="#">Easy to Customize</a></li>
							<li><a href="#">Calls to action</a></li>
							<li><a href="#">Custom Fonts</a></li>
							<li><a href="#">Slide down on Hover</a></li>                         
						</ul>
					</li>
                    <li class="col-sm-3">
    					<ul>
							<li class="dropdown-header">Women Collection</li>                            
                            <div id="womenCollection" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                <div class="item active">
                                    <a href="#"><img src="http://placehold.it/254x150/3498db/f5f5f5/&text=New+Collection" class="img-responsive" alt="product 1"></a>
                                    <h4><small>Summer dress floral prints</small></h4>                                        
                                    <button class="btn btn-primary" type="button">49,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>       
                                </div><!-- End Item -->
                                <div class="item">
                                    <a href="#"><img src="http://placehold.it/254x150/ff3546/f5f5f5/&text=New+Collection" class="img-responsive" alt="product 2"></a>
                                    <h4><small>Gold sandals with shiny touch</small></h4>                                        
                                    <button class="btn btn-primary" type="button">9,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>        
                                </div><!-- End Item -->
                                <div class="item">
                                    <a href="#"><img src="http://placehold.it/254x150/2ecc71/f5f5f5/&text=New+Collection" class="img-responsive" alt="product 3"></a>
                                    <h4><small>Denin jacket stamped</small></h4>                                        
                                    <button class="btn btn-primary" type="button">49,99 €</button> <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button>      
                                </div><!-- End Item -->                                
                              </div><!-- End Carousel Inner -->
                              <!-- Controls -->
                              <a class="left carousel-control" href="#womenCollection" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                              </a>
                              <a class="right carousel-control" href="#womenCollection" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                              </a>
                            </div><!-- /.carousel -->
                            <li class="divider"></li>
                            <li><a href="#">View all Collection <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
						</ul>
					</li>
				</ul>				
			</li>
            <li><a href="#" class="mega-menu-list-item">BLOG</a></li>
            <li><a href="#" class="mega-menu-list-item">KONTAKT</a></li>
		</ul>
        <ul class="nav navbar-nav navbar-right">
        </ul>
	</div><!-- /.nav-collapse -->
  </nav>
    	</div>
	
	<div class="row search-panel">
		<div class="col-sm-2">
			 <span class="label label-default" style="">DESTINACIONI</span>
			<select class="selectpicker">
			    <option>Antalja</option>
			</select>
		</div>
		<div class="col-sm-2">
			 <span class="label label-default">HOTELI</span>
			<select>
				<option>1</option>
				<option>2</option>
				<option>3</option>
				<option>4</option>
			</select>
		</div>
		<div class="col-sm-2">
			 <span class="label label-default">DATA E NISJES</span>
			<div class="btn-group">
				 
				<button class="btn btn-default">
					Action
				</button> 
				<button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<li>
						<a href="#">Action</a>
					</li>
					<li class="disabled">
						<a href="#">Another action</a>
					</li>
					<li class="divider">
					</li>
					<li>
						<a href="#">Something else here</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="col-sm-2">
			 <span class="label label-default">KOHEZGJATJA</span>
			<select class="selectpicker">
			    <option>Rreth 1 jave</option>
			    <option>Disa dite</option>
			    <option>Rreth 10 dite</option>
			    <option>Rreth 2 jave</option>
			</select>
		</div>
		<div class="col-sm-2">
			 <span class="label label-default">TE RRITUR</span>
			<select class="selectpicker"> 
				<option>1</option>
				<option>2</option>
				<option>3</option>
				<option>4</option>
				<option>5</option>
			</select>
		</div>
		<div class="col-sm-2">
			 
			<button type="button" class="btn btn-default">
				Kerko
			</button>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="carousel slide" id="carousel-26919">
				<ol class="carousel-indicators">
					<li data-slide-to="0" data-target="#carousel-26919">
					</li>
					<li data-slide-to="1" data-target="#carousel-26919">
					</li>
					<li data-slide-to="2" data-target="#carousel-26919" class="active">
					</li>
				</ol>
				<div class="carousel-inner">
					<div class="item">
						<img alt="Carousel Bootstrap First" src="http://www.savatours-ks.com/images/banners//naojFL6h.jpg">
						<div class="carousel-caption">
							<h4>
								First Thumbnail label
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="Carousel Bootstrap Second" src="http://www.savatours-ks.com/images/banners//naojFL6h.jpg">
						<div class="carousel-caption">
							<h4>
								Second Thumbnail label
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
					<div class="item active">
						<img alt="Carousel Bootstrap Third" src="http://www.savatours-ks.com/images/banners//naojFL6h.jpg">
						<div class="carousel-caption">
							<h4>
								Third Thumbnail label
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
				</div> <a class="left carousel-control" href="#carousel-26919" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-26919" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<img class="img-responsive" src="http://www.dev.odev.al/banners/525x260_new_hotels.jpg">
		</div>
		<div class="col-md-6">
			<ul>
				<li>
					Lorem ipsum dolor sit amet
				</li>
				<li>
					Consectetur adipiscing elit
				</li>
				<li>
					Integer molestie lorem at massa
				</li>
				<li>
					Facilisis in pretium nisl aliquet
				</li>
				<li>
					Nulla volutpat aliquam velit
				</li>
				<li>
					Faucibus porta lacus fringilla vel
				</li>
				<li>
					Aenean sit amet erat nunc 
				</li>
				<li>
					Eget porttitor lorem
				</li>
			</ul>
		</div>
	</div>
	<div class="row collection-panel">
             <div class="col-sm-6 col-md-3">
                <div class="panel panel-default panel-card">
                    <div class="panel-heading">
                        <img src="http://www.savatours.com/image/260x260/uploads/A0eXFEyp/il041872.jpg" />
                        <button class="btn btn-warning btn-sm" role="button">GOLD</button>
                    </div>
                    <div class="panel-body text-center-gold" align="center">
                        <small>Eksperienca luksoze pushimesh</small>
                    </div>
                </div>   
    		</div>
		    <div class="col-sm-6 col-md-3">
        	    <div class="panel panel-default panel-card">
                    <div class="panel-heading">
                        <img src="https://www.savatours.com/image/260x260/images/collections/premier_family_cover_1.jpg" />
                        <button class="btn btn-success btn-sm" role="button">PREMIER FAMILY</button>
                    </div>
                    <div class="panel-body text-center-family" align="center">
                        <small>Atmosfere te paimitueshme familjare</small>
                    </div>
                </div>   
    		</div>
            <div class="col-sm-6 col-md-3">
                <div class="panel panel-default panel-card">
                    <div class="panel-heading">
                        <img src="https://www.savatours.com/image/260x260/images/collections/platinum_cover.jpg" />
                        <button class="btn btn-primary btn-sm" role="button">PLATINUM</button>
                    </div>
                    <div class="panel-body text-center-platinum" align="center">
                        <small>Rezortet me vleresimin me maksimal</small>
                    </div>
                </div>   
    		</div>
            <div class="col-sm-6 col-md-3">
                <div class="panel panel-default panel-card">
                    <div class="panel-heading">
                        <img src="https://www.savatours.com/image/260x260/uploads/QDCfWGKP/genel-14.jpg" />
                        <button class="btn btn-danger btn-sm" role="button">COMFORT</button>
                    </div>
                    <div class="panel-body text-center-comfort" align="center">
                        <small>Pushime te shpenguara ku raporti cilesi/cmim eshte paresor.</small>
                    </div>
                </div>   
    		</div>
	    </div>
	<div class="row register-panel">
		<div class="col-md-6 icons">
                    <ul class="social-network social-circle">
                        <li><a href="http://www.facebook.com" class="icoFacebook" title="Facebook" style="border-radius: 50%;"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="icoYoutube" title="Youtube" style="font-size:26px;border-radius: 50%;"><i class="fa fa-youtube-square"></i></a></li>
                        <li><a href="#" class="icoGoogle" title="Google +" style="border-radius: 50%;"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#" class="icoInstagram" title="Instagram" style="border-radius: 50%;"><i class="fa fa-instagram"></i></a></li>
                    </ul>				
				</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-6">
					<h4>
						Rregjistrohu per oferta
					</h4>
					<p>Oferta te personalizuara, drejt e ne inboxin tuaj.</p>
				</div>
				<div class="col-md-6">
					<a href="#" class="btn btn-outlined btn-theme btn-lg" data-wow-delay="0.7s">Rregjistrohu</a>
				</div>
			</div>
		</div>
		
	</div>
</div>
	<div class="row footer-panel" style="">
		<div class="col-md-3">
			 <span class="label label-default">Savatours</span> 
			<address>
				 <strong>T:</strong> +355 42246730<br> 
				 <strong>E:</strong> info@savatours.com
			</address>
		</div>
		<div class="col-md-3">
			 <span class="label label-default">Kujdesi ndaj klientit</span>
			<ul>
				<li>
					Pyetje te shpejta
				</li>
				<li>
					Si mund te rezervoj ?
				</li>
				<li>
					Menyrat e pageses
				</li>
				<li>
					Keshilla udhetimi
				</li>
			</ul>
		</div>
		<div class="col-md-3">
			 <span class="label label-default">Na ndiqni</span>
			<div class="row">
				<div class="col-md-3">
					<img alt="Bootstrap Image Preview" src="http://www.savatours.com/assets/images/ico/fb_logo.png">
				</div>
				<div class="col-md-3">
					<img alt="Bootstrap Image Preview" src="http://www.savatours.com/assets/images/ico/youtube_logo.png">
				</div>
				<div class="col-md-3">
					<img alt="Bootstrap Image Preview" src="http://www.savatours.com/assets/images/ico/g+_logo.png">
				</div>
				<div class="col-md-3">
					<img alt="Bootstrap Image Preview" src="http://www.savatours.com/assets/images/ico/insta_logo.png">
				</div>
			</div>
		</div>
		<div class="col-md-3">
		</div>
	</div>
	
	{{ HTML::script('https://code.jquery.com/jquery-2.1.4.min.js') }}
	{{ HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js') }}
	
	<script src="js/scripts.js"></script>
    
    <script type='text/javascript'>

   			 $(document).ready(function() {

		         $('.carousel').carousel({
		             interval: 5000
		         })

	   		 });    
	   		 
	</script>

	<script type="text/javascript">
		$(document).ready(function(){
		    
		    $(".dropdown").hover(            
		        function() {
		            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
		            $(this).toggleClass('open');        
		        },
		        function() {
		            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
		            $(this).toggleClass('open');       
		        }
		    
		    );
		});
	</script>

  </body>
</html>