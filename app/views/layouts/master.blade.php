<!DOCTYPE html>
<!--[if IE 7]>    <html class="ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie" lang="en"> <![endif]-->
<!--[if IE]>    <html class="ie" lang="en"> <![endif]-->
<!--[if lt IE9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- <html> -->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="HandheldFriendly" content="True">
<title>{{ $page->title or 'Savatours :: Pushime, Hotele, Fluturime' }}</title>
<meta name="description" content="{{ $meta_description or 'Gjeni pushimet tuaja ideale me Savatours. Paketa All Inclusive per plazh ne rezortet me te mira ne Antalja' }}" />
<meta name="keywords" content="{{ $meta_keywords or 'antalja 2015, antalya, antalja, agjensi udhetimi, agjensi turistike, pushime verore, plazh, oferta speciale, prenoto online, prenotime, rezervime online, ture ne shqiperi, pushime ne turqi, pushime ne antalja, hotelet turke, hotelet shqiptare, savatours, fluturime, charter, turqi, oferta speciale ne turqi, all inclusive, ultra all inclusive' }}"/>
<link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" />	

@section('css')
<link rel="stylesheet" href="{{{ asset('assets/css/prettyPhoto.css') }}}" type="text/css" media="screen" />
{{-- <link rel="stylesheet" href="{{{ asset('assets/js/plugins/flexslider/flexslider.css') }}}" type="text/css" media="screen" /> --}}
<link href="{{ asset('assets/js/plugins/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.css') }}" rel="stylesheet" type="text/css" />	
<link href="{{ asset('assets/js/plugins/jquery-multiselect/jquery.multiselect.css') }}" rel="stylesheet" type="text/css" />	
<link href="{{ asset('assets/js/plugins/jquery-multiselect/jquery.multiselect.filter.css') }}" rel="stylesheet" type="text/css" />	
<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css" media="screen,projection,print" />
<link rel="stylesheet" href="{{ asset('assets/css/theme-blue.css') }}" id="template-color" />	
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">	
{{-- {{ HTML::style('assets/js/plugins/sequence/sequence.css') }} --}}
{{-- {{ HTML::style('assets/js/plugins/cookiecuttr/cookiecuttr.css') }} --}}
{{-- Facebook tags --}}
<meta property="og:site_name" content="{{ $page->title or 'Savatours :: The happymakers' }}"/>

@show
</head>
<body>

	{{-- @yield('preheader') --}}


	@section('notification')
	@if(Config::get('site.site_name') == 'savatours_al')
		<div id="passport-notification" class="hidden" style="position: relative;">
		<p style="line-height: 16px;">
		Të nderuar klientë, nëse jeni të pajisur dhe shfrytëzoni Pasaportën e Republikës së Shqipërisë për udhëtimin tuaj në Turqi, dokumenti juaj i udhëtimit duhet të ketë vlefshmëri të paktën 6 (gjashtë) muaj nga data e kthimit.
		<br /><br />
		 Nëse pasaporta është e vlefshme 2 muaj (60 ditë) nga data e kthimit, ju nuk do të lejoheni të udhëtoni. 
		<br /><br />
		 Nëse pasaporta është e vlefshme 2-6 muaj nga data e kthimit, ju do të lejoheni të udhëtoni kundrejt një pagese shtesë prej rreth 125 të cilën do e paguani në aeroportin e Antaljas.</p>
		<br />
		</div>	
		<a href="#passport-notification" rel="prettyPhoto" style="text-decoration:none;"><div style="background: #565656; color: #fff; padding: 5px; position:" ><span style="padding: 0; color:#fff; font-size: 12px">Informacion i rëndësishëm në lidhje me <b>vlefshmërinë e pasaportës për udhëtimin në Turqi</b>. Kliko këtu për më shumë info.</span></div></a>
	@endif
	@show

	<!--header-->
	<header>
		@include('includes.header')       
	</header>
	<!--//header-->
	
	<!--search-->
	<div class="main-search">
		@section('search')
			@include('forms.mainSearch')
		@show
	</div>
	<!--//search-->
		
	@yield('slider')	
        
        
	<!--main-->
	<div class="main" role="main">
		<div class="wrap clearfix">
			@yield('content')
		</div>
	</div>
	<!--//main-->
	
	<!--footer-->
	<footer>
		@include('includes.footer')
	</footer>
	<!--//footer-->

	{{-- global scripts --}}
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="{{ URL::to('/') . '/assets/js/jquery.ui.datepicker-'. Config::get('site.locale') .'.js' }}"></script>	
    <!-- Bootstrap -->
    <script src="{{ URL::to('/') . '/assets/admin/js/bootstrap.min.js' }}" type="text/javascript"></script>	
	<script type="text/javascript" src="{{ URL::to('/') . '/assets/js/css3-mediaqueries.js' }}"></script>
	<script type="text/javascript" src="{{ URL::to('/') . '/assets/js/plugins/sequence/sequence.jquery-min.js' }}"></script>
	<script type="text/javascript" src="{{ URL::to('/') . '/assets/js/plugins/sequence/sequence.js' }}"></script>
	<script type="text/javascript" src="{{ URL::to('/') . '/assets/js/jquery.uniform.min.js' }}"></script>
	<script type="text/javascript" src="{{ URL::to('/') . '/assets/js/jquery.prettyPhoto.js' }}"></script>
	<script type="text/javascript" src="{{ URL::to('/') . '/assets/js/selectnav.js' }}"></script>
	<script type="text/javascript" src="{{ URL::to('/') . '/assets/js/scripts.js' }}"></script>	
	<script src="{{ asset('assets/js/plugins/jquery-multiselect/jquery.multiselect.min.js') }}"></script>
	<script src="{{ asset('assets/js/plugins/jquery-multiselect/jquery.multiselect.filter.js') }}"></script>


<script type="text/javascript">

$( document ).ready(function() {


		departures = null;			

	    $.ajax({
	      url: "{{ URL::to('/flydays') }}",
	      async: true,
	      dataType: 'json',
	      success: function (flights) {
	        departures = flights.departures;
	      }
	    });

	    function available_departures(date) {
	      ymd =  ('0' + date.getDate()).slice(-2) + "/" + ('0' + (date.getMonth()+1)).slice(-2) + "/" +   date.getFullYear();

	      if ($.inArray(ymd, departures) != -1) {
	        return [true, "","Fly day"];
	      } else {
	        return [false,"","No flight this day"];
	      }
	    }			
		 
		// Main search choose hotel
		$('#search-hotels').multiselect({
			selectedText: "# {{ Lang::get('searchform.of') }} #",
			checkAllText: "{{ Lang::get('searchform.all') }}",
			uncheckAllText: "{{ Lang::get('searchform.none') }}",
			noneSelectedText: "{{ Lang::get('searchform.choose_hotels') }}",
			open: function () {
			    $("input[type='search']:first").focus();                   
			}			   
		}).multiselectfilter({
			label: "{{ Lang::get('searchform.filter') }}",
			placeholder: "{{ Lang::get('searchform.hotel') }}",
			autoReset: true,
		});	

		$('.datepicker-wrap input').each(function(){
			// read the next departure
		    var departure = $.datepicker.parseDate('dd/mm/yy', $('#checkin').val());
				$(this).datepicker({
					dateFormat: "d MM, yy",
			        regional: "{{ Session::get('lang') }}",
			        defaultDate: departure,
					showOn: 'both',
					altField: '#checkin',
					altFormat: "dd/mm/yy",
					buttonImage: "{{ URL::to('/') . '/assets/images/ico/calendar.png' }}",
					buttonImageOnly: true,
					beforeShowDay: available_departures,
				});	
			$.datepicker.setDefaults($.datepicker.regional['{{ Session::get('lang') }}']);

		    $(this).datepicker('setDate', departure);
	     });

		 $('#info-checkin').html($('#display_date').val());		
		
		 // var url = ;
		 @if(isset($sections->top_offers))
		 $('#widget_1').load("{{ URL::to('/topoffers/' . $sections->top_offers) }}");	
		 @endif
	});
</script>

<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-35491766-1', 'auto');
  ga('send', 'pageview');

</script>

<script type="text/javascript">
{{ Config::get('site.lhc') }}
</script>

{{-- page specific scripts --}}
@yield('scripts')

</body>

</html>