
<div style="float:left; width:20%; z-index:1000; border-right:1px solid #eee;">
	<div class="filtri-neteve">
		<div class="filtro-net">{{ Lang::get('searchresults.number_of_nights') }}</div>
		@foreach($nights_options as $nights)
			<a href="{{ URL::to('pushime/?' . http_build_query(array_merge(Input::all(), array('nights' => $nights)))) }}"><div class="nr-neteve {{ $nights == array_values($tours)[0]['Nights'] ? 'active' : '' }}">{{$nights}} {{ Lang::get('searchresults.nights') }}</div></a>
		@endforeach
	</div>	

	<div id="resultsFilter">
		<div id="sticker">
			<article class="refine-search-results">
				<!-- <h2>{{ Lang::get('searchresults.filter_results') }}</h2> -->
				<dl>
					 
	{{-- 				<dt>{{ Lang::get('searchresults.price') }}  </dt>
					<dd style="display:block; height:auto; min-height: 30px;">
						<div id="priceRange"></div>
					</dd> --}}
						
	{{-- 				<dt>{{ Lang::get('searchresults.concept') }}</dt>
					<dd style="display:block; height:auto;">
						@foreach($meals as $meal_type)
						<div class="checkbox">
							<input type="checkbox" id="{{$meal_type['Inc']}}" name="concept[]" value="{{$meal_type['Inc']}}" {{in_array($meal_type['Inc'], $selected_meals) ? 'checked' : ''}}/>
							<label for="{{$meal_type['Inc']}}">{{$concepts[$meal_type['Name']]}}</label>
						</div>							
						@endforeach	
					</dd> --}}
					<dt id="toggleTownsDisplay">{{ Lang::get('searchresults.town') }} </dt>
					<div style="display:none; position:absolute; background-color:#fff; width:21.3%; margin-left:-15px; padding-left:20px; padding-top:5px; z-index:1005; box-shadow:1px 4px 2px rgba(22,22,22,0.3);">
						<dd style="display:block; height:auto;">
							<div class="checkbox">
								@if($destinationTowns == $selectedTowns || empty($selectedTowns))
									<input type="checkbox" id="allTowns" name="allTowns" value="all" />
							    @else 
							    	<input type="checkbox" id="allTowns" name="allTowns" value="all"  />
								@endif
									<label for="allTowns">{{ Lang::get('homepage.all') }}</label>
							</div>										
							@foreach($destinationTowns as $town)
							<div class="checkbox">
								@if( empty($selectedTowns) )
								<input type="checkbox" id="{{$town['TownInc']}}" name="towns" value="{{$town['TownInc']}}" checked  />
								@else
								<input type="checkbox" id="{{$town['TownInc']}}" name="towns" value="{{$town['TownInc']}}" {{ in_array($town['TownInc'], $selectedTowns) ? 'checked' : '' }}  />
								@endif
								<label for="{{$town['TownInc']}}">{{$town['TownName']}}</label>
								
							</div>							
							@endforeach	
						</dd>
						<div>
							<button id='getResults' class='btn btn-primary'>{{ Lang::get('homepage.get_results') }}</button>
						</div>
					</div>								
				</dl>		
			</article>
		</div>
	</div></div>

<div class="horizontal-calendar">
	@foreach($week as $k=>$v)
		@if($v['flyDay'])
			<a href="{{ URL::to('pushime?' . http_build_query( array_merge(Input::except('nights'), array('checkin' => $k)))) }}">
				<div class="date {{ $k == $form['checkin'] ? 'active' : ''}}" title="Ka fluturim">
					<span class="dita">{{ $v['day'][0] }}</span>
					<br>
					<span class="data">{{ $v['day'][1] }}</span>
					<br>
					<span class="muaji">{{ $v['day'][2] }}</span>
				</div>
			</a>
		@else
				<div class="date no-flight {{ $k == $form['checkin'] ? 'active' : ''}}" title="Nuk ka fluturim kete dite">
					<span class="dita">{{ $v['day'][0] }}</span>
					<br>
					<span class="data">{{ $v['day'][1] }}</span>
					<br>
					<span class="muaji">{{ $v['day'][2] }}</span>
				</div>	
		@endif	
	@endforeach
</div>	
