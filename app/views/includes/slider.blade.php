<!--slider-->
<section class="slider clearfix home">		

	<div class="main-banner">
		<a href="{{ $main_banner->link }}">
			{{ $main_banner->content }}
			<img src="{{ asset($main_banner->background_image) }}">
		</a>
	</div>	

</section>
<!--//slider-->		