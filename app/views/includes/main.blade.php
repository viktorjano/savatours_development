
			<!--latest offers-->
			<section class="offers home clearfix full">			
				<article class="one-half">
						@include('partials.home.1')
				</article>							

				<article class="one-half" style="overflow-y:scroll;">
					<div class="top-offers-wrap">
						<section class="top-offers">
							<div class="offer-details">
								<span class="hotel-name">
									<a href="#">Vikingen Quality Resort</a> &nbsp;
								</span>
								<span class="stars">*****</span>
								<div class="hotel-details">
									<span style=""><i class="fa fa-plane"></i> 7 Gusht, 7 net</span> | <i class="fa fa-map-marker"></i> Alanya, Turkey
								</div>								
							</div>
							<a href="#" class="gradient-button right arrow-right">€659 <span class="pp">pp</span></a>
						</section>
						<section class="top-offers">
							<div class="offer-details">
								<span class="hotel-name">
									<a href="#">Rubi Platinum Resort</a> &nbsp;
								</span>
								<span class="stars">*****</span>
								<div class="hotel-details">
									<span style=""><i class="fa fa-plane"></i> 7, 13, 14 Gusht, 7 net</span> | <i class="fa fa-map-marker"></i> Alanya, Turkey
								</div>								
							</div>
							<a href="#" class="gradient-button right arrow-right">€929 <span class="pp">pp</span></a>
						</section>
						<section class="top-offers">
							<div class="offer-details">
								<span class="hotel-name">
									<a href="#">Amara Dolce Vita</a> &nbsp;
								</span>
								<span class="stars">*****</span>
								<div class="hotel-details">
									<span style=""><i class="fa fa-plane"></i> 7 Gusht, 7 net</span> | <i class="fa fa-map-marker"></i> Tekirova, Turkey
								</div>								
							</div>
							<a href="#" class="gradient-button right arrow-right">€1199 <span class="pp">pp</span></a>
						</section>
						<section class="top-offers">
							<div class="offer-details">
								<span class="hotel-name">
									<a href="#">Vikingen Infinity Resort</a> &nbsp;
								</span>
								<span class="stars">*****</span>
								<div class="hotel-details">
									<span style=""><i class="fa fa-plane"></i> 7, 13, 14 Gusht, 7 net</span> | <i class="fa fa-map-marker"></i> Alanya, Turkey
								</div>								
							</div>
							<a href="#" class="gradient-button right arrow-right">€845 <span class="pp">pp</span></a>
						</section>
						<section class="top-offers">
							<div class="offer-details">
								<span class="hotel-name">
									<a href="#">Alibey Club Manavgat</a> &nbsp;
								</span>
								<span class="stars">*****</span>
								<div class="hotel-details">
									<span style=""><i class="fa fa-plane"></i> 7, 13, 14 Gusht, 7 net</span> | <i class="fa fa-map-marker"></i> Side, Turkey
								</div>								
							</div>
							<a href="#" class="gradient-button right arrow-right">€949 <span class="pp">pp</span></a>
						</section>		
						<section class="top-offers">
							<div class="offer-details">
								<span class="hotel-name">
									<a href="#">Titanic Deluxe Belek</a> &nbsp;
								</span>
								<span class="stars">*****</span>
								<div class="hotel-details">
									<span style=""><i class="fa fa-plane"></i> 7 Gusht, 7 net</span> | <i class="fa fa-map-marker"></i> Belek, Turkey
								</div>								
							</div>
							<a href="#" class="gradient-button right arrow-right">€1257 <span class="pp">pp</span></a>
						</section>												
					</div>						
				</article>		
				<!--column-->
				<article class="one-fourth">
					<figure>
						<figcaption>
							<h4 style="color: #004d6d; background: #fef200;">Gold Premier</h4>
						</figcaption>
						<div class="details" style="background: #fef200">
							<p style="color: #004d6d;">Fama dhe popullariteti që kanë midis pushuesve tanë është pika e përbashkët e hoteleve Gold.</p>
						</div>						
						<a href="/collection/goldpremier" title=""><img src="http://dev.savatours.com/image/570x520/uploads/hMOmIqRB/GOLDPREMIE-PHOTO.jpg" alt="" /></a>						
					</figure>


				</article>
				<!--//column-->
				
				<!--column-->
				<article class="one-fourth">
					<figure>
						<figcaption>
							<h4 style="color: #004d6d; background: #d3effb;">Gold Family</h4>
						</figcaption>	
						<div class="details" style="background: #d3effb">
							<p style="color: #004d6d;">Fama dhe popullariteti që kanë midis pushuesve tanë është pika e përbashkët e hoteleve Gold.</p>
						</div>												
						<a href="/collection/goldfamily" title=""><img src="http://dev.savatours.com/image/570x520/uploads/iCq65Qs7/2.jpg" alt="" /></a>
					</figure>
				</article>
				<!--//column-->
				
				<!--column-->
				<article class="one-fourth">
					<figure>
						<figcaption>
							<h4 style="color: #004d6d; background: #f8e9ae;">Platinum</h4>
						</figcaption>	
						<div class="details" style="background: #f8e9ae">
							<p style="color: #004d6d;">Pergatitu per shijet me speciale, ambiente perrallore, trajtime clodhese, pamje spektakolare dhe standarte te larta sherbimi.</p>
						</div>												
						<a href="/collection/platinum" title=""><img src="http://dev.savatours.com/image/570x520/uploads/E4jNFFPy/platinum.jpg" alt="" /></a>
					</figure>
				</article>
				<!--//column-->			
				
				<!--column-->
				<article class="one-fourth last">
					<figure>
						<figcaption>
							<h4 style="color: #fff; background: #92278f ;">Comfort</h4>
						</figcaption>	
						<div class="details" style="background: #92278f">
							<p style="color: #fff;">Hotelet Comfort ofrojne pushime te shpenguara All Inclusive, ku raporti cilesi/cmim eshte paresor.</p>
						</div>												
						<a href="/collection/comfort" title=""><img src="http://dev.savatours.com/image/570x520/uploads/iBIoamEu/comfort-photo.jpg" alt="" /></a>
					</figure>
				</article>
				<!--//column-->					
<!-- 				<article class="one-half">
					<div class="top-offers-wrap">
						<section class="top-offers">
							<div class="offer-details">
								<span class="hotel-name">
									<a href="#">Vikingen Quality Resort</a> &nbsp;
								</span>
								<span class="stars">*****</span>
								<div class="hotel-details">
									<span style=""><i class="fa fa-plane"></i> 26 Korrik, 7 dite</span> | <i class="fa fa-map-marker"></i> Alania, Turkey
								</div>								
							</div>
							<a href="#" class="gradient-button right arrow-right">€985 <span class="pp">pp</span></a>
						</section>
						<section class="top-offers">
							<div class="offer-details">
								<span class="hotel-name">
									<a href="#">Vikingen Quality Resort</a> &nbsp;
								</span>
								<span class="stars">*****</span>
								<div class="hotel-details">
									<span style=""><i class="fa fa-plane"></i> 26 Korrik, 7 dite</span> | <i class="fa fa-map-marker"></i> Alania, Turkey
								</div>								
							</div>
							<a href="#" class="gradient-button right arrow-right">€985 <span class="pp">pp</span></a>
						</section>
						<section class="top-offers">
							<div class="offer-details">
								<span class="hotel-name">
									<a href="#">Vikingen Quality Resort</a> &nbsp;
								</span>
								<span class="stars">*****</span>
								<div class="hotel-details">
									<span style=""><i class="fa fa-plane"></i> 26 Korrik, 7 dite</span> | <i class="fa fa-map-marker"></i> Alania, Turkey
								</div>								
							</div>
							<a href="#" class="gradient-button right arrow-right">€985 <span class="pp">pp</span></a>
						</section>
						<section class="top-offers">
							<div class="offer-details">
								<span class="hotel-name">
									<a href="#">Vikingen Quality Resort</a> &nbsp;
								</span>
								<span class="stars">*****</span>
								<div class="hotel-details">
									<span style=""><i class="fa fa-plane"></i> 26 Korrik, 7 dite</span> | <i class="fa fa-map-marker"></i> Alania, Turkey
								</div>								
							</div>
							<a href="#" class="gradient-button right arrow-right">€985 <span class="pp">pp</span></a>
						</section>
					</div>						
				</article>							

				<article class="one-half">
					<figure>
						<a href="#" title=""><img src="http://dev.savatours.com/image/570x270/uploads/8XUmwyBj/4.jpg" alt="" /></a>
					</figure>
				</article>		 -->
<!-- 				<article class="one-half last">
					<figure><a href="#" title=""><img src="assets/images/slider/img2.jpg" alt="" /></a></figure>
					<div class="details">
						<h4>Oferta m&euml; e mir&euml; e jav&euml;s: Tailand&euml;</h4>
						<a href="#" title="More info" class="gradient-button">M&euml; shum&euml; info</a>
					</div>
				</article>	 -->			
			</section>
			<!--//latest offers-->