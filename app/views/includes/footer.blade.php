		<div class="wrap clearfix">
			<!--column-->
			<article class="one-fourth">
				<h3>Savatours</h3>
				<p> {{ Config::get('site.address') }}</p>
				<p><em>T:</em>{{ Config::get('site.phone1') }}</p>
				<p><em>E:</em> <a href="#" title="{{ Config::get('site.email') }}">{{ Config::get('site.email') }}</a></p>
			</article>
			<!--//column-->
			
			<!--column-->
			<article class="one-fourth">
				<h3>{{ Lang::get('homepage.customer_care') }}</h3>
				<ul>
					<li><a href="#" title="{{ Lang::get('homepage.faq') }}">{{ Lang::get('homepage.faq') }}</a></li>
					<li><a href="#" title="{{ Lang::get('homepage.how_to_book') }}">{{ Lang::get('homepage.how_to_book') }}</a></li>
					<li><a href="#" title="{{ Lang::get('homepage.payment_methods') }}">{{ Lang::get('homepage.payment_methods') }}</a></li>
					<li><a href="#" title="{{ Lang::get('homepage.travel_advice') }}">{{ Lang::get('homepage.travel_advice') }}</a></li>
				</ul>
			</article>
			<!--//column-->
			
			<!--column-->
			<article class="one-fourth">
				<h3>{{ Lang::get('homepage.follow_us') }}</h3>
				<ul class="social">
					<div style="float:left;padding-left:8px;">
						<a href="{{ Config::get('site.facebook_url') }}" target="_blank"><img src="{{ asset('assets/images/ico/fb_logo.png') }}"></a>
					</div>
					<div style="float:left;padding-left:8px;">
						<a href="#"><img src="{{ asset('assets/images/ico/youtube_logo.png') }}"></a>
					</div>
					<div style="float:left;padding-left:8px;">
						<a href="#"><img src="{{ asset('assets/images/ico/g+_logo.png') }}"></a>
					</div>
					<div style="float:left;padding-left:8px;">
						<a href="#"><img src="{{ asset('assets/images/ico/insta_logo.png') }}"></a>
					</div>
				</ul>
			</article>
			<!--//column-->
			
{{-- 			<!--column-->
			<article class="one-fourth last">
				<h3>Mos humbni ofertat ekskluzive</h3>
				<form id="newsletter" action="#" method="post">
					<fieldset>
						<input type="email" id="newsletter_signup" name="newsletter_signup" placeholder="Shkruaj emailin ketu" />
						<input type="submit" id="newsletter_submit" name="newsletter_submit" value="Signup" class="gradient-button" />
					</fieldset>
				</form>
			</article>
			<!--//column-->	 --}}
			
			<section class="bottom">
				<p class="copy">Copyright 2015 Savatours Shpk.</p>
				<nav>
					<ul>
						<li><a href="#" title="{{ Lang::get('homepage.about_us') }}">{{ Lang::get('homepage.about_us') }}</a></li>
						<li><a href="#" title="{{ Lang::get('homepage.partners') }}">{{ Lang::get('homepage.partners') }}</a></li>
						<li><a href="#" title="{{ Lang::get('homepage.customer_care') }}">{{ Lang::get('homepage.customer_care') }}</a></li>
						<li><a href="#" title="{{ Lang::get('homepage.career') }}">{{ Lang::get('homepage.career') }}</a></li>
						<li><a href="#" title="{{ Lang::get('homepage.tos') }}">{{ Lang::get('homepage.tos') }}</a></li>
						<li><a href="#" title="{{ Lang::get('homepage.privacy_policy') }}">{{ Lang::get('homepage.privacy_policy') }}</a></li>
					</ul>
				</nav>
			</section>
		</div>