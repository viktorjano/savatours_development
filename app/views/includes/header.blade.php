<div class="wrap clearfix">
        {{-- Hide the utils for now --}}
        <div class="utils">
            <ul>
                <li><a href="{{{ URL::to('#') }}}" title="{{ Lang::get('homepage.ask_question') }}">{{ Lang::trans('homepage.ask_question') }}</a></li>
                <li><a href="{{{ URL::to('#') }}}" title="{{ Lang::get('homepage.travel_advice') }}">{{ Lang::get('homepage.travel_advice') }}</a></li>
                <li><a href="{{{ URL::to( Config::get('site.b2b_link') ) }}}" target="_blank" title="{{ Lang::get('homepage.b2b_login') }}">{{ Lang::get('homepage.b2b_login') }}</a></li>
            </ul>
        </div>
            <!--logo-->
            <h1 class="logo"><a href="{{ URL::to('/') }}" title="{{ $page->partner_name or 'Savatours' }}"><img src="{{ URL::to('/assets/images/txt/logo_inverse.png') }}" alt="{{ $page->partner_slogan or 'The Happymakers' }}" width="152px"/></a></h1>
            <!--//logo-->
            
            <!--main navigation-->
            <nav class="main-nav" role="navigation" id="nav">
                <ul class="wrap">
                    {{-- <li><a href="#"><i class="fa fa-home"></i></a></li> --}}
                    <li class="active"><a href="{{ URL::to('#') }}" title="{{ Lang::get('homepage.holidays') }}">{{ Lang::get('homepage.holidays') }}</a>
	                       {{-- 
                           <ul>
                                    <li><a href="#">Secondary navigation</a></li>
                                    <li><a href="#">example links</a>
                                            <ul>
                                                    <li><a href="#">Third level navigation</a></li>
                                                    <li><a href="#">example links</a>
                                                            <ul>
                                                                    <li><a href="#">Fourth level navigation</a></li>
                                                                    <li><a href="#">example links</a></li>
                                                            </ul>
                                                    </li>
                                            </ul>
                                    </li>
                            </ul>
                            --}}
                    </li>
                    <li><a href="{{{ URL::to('#') }}}" title="{{ Lang::get('homepage.tours') }}">{{ Lang::get('homepage.tours') }}</a></li>
{{--                                     <li><a href="{{{ URL::to('#') }}}" title="Ture">Krocera</a></li>
--}}               <li><a href="{{ URL::to('#') }}" title="{{ Lang::get('homepage.blog') }}">{{ Lang::get('homepage.blog') }}</a>                                                                        
                    <li><a href="{{ URL::to('/contact') }}" title="{{ Lang::get('homepage.contact') }}">{{ Lang::get('homepage.contact') }}</a></li>
                            {{-- <li><a href="{{{ URL::to('#') }}}" title="Krocera">Krocera</a></li> --}}
{{-- hide for now 	
    <li><a href="car_rental.html" title="Car rental">Car rental</a></li>
        <li><a href="hot_deals.html" title="Hot deals">Hot deals</a></li>
        <li><a href="#" title="holidays">holidays</a></li>
        <li><a href="#" title="Business travel">Business travel</a></li>
        <li><a href="blog.html" title="Blog">Blog</a>
                <ul>
                        <li><a href="blog_single.html" title="Single Post">Single Post</a>
                </ul>
        </li>
        <li><a href="get_inspired.html" title="Get inspired">Get inspired</a></li>
        <li><a href="#" title="Travel guides">Travel guides</a>
                <ul>
                        <li><a href="location.html">Location Details</a></li>
                </ul>
        </li>
    </li>
--}}
                    </ul>
            </nav>
<!--//main navigation-->		
{{-- 	
	<!--ribbon-->
	<div class="ribbon">
		<nav>
			<ul class="profile-nav">
				<li class="active"><a href="#" title="My Account">My Account</a></li>
				<li><a href="login.html" title="Login">Login</a></li>
				<li><a href="my_account.html" title="Settings">Settings</a></li>
			</ul>
			<ul class="lang-nav">
				<li class="active"><a href="#" title="English (US)">English (US)</a></li>
				<li><a href="#" title="English (UK)">English (UK)</a></li>
				<li><a href="#" title="Deutsch">Deutsch</a></li>
				<li><a href="#" title="Italiano">Italiano</a></li>
				<li><a href="#" title="Русский">Русский</a></li>
			</ul>
			<ul class="currency-nav">
				<li class="active"><a href="#" title="$US Dollar">$US Dollar</a></li>
				<li><a href="#" title="€ Euro">€ Euro</a></li>
				<li><a href="#" title="£ Pound">£ Pound</a></li>
			</ul>
		</nav>
	</div>-->
	<!--//ribbon-->
	
	<!--search-->
	<div class="search">
		<form id="search-form" method="get" action="#">
			<input type="search" placeholder="Search entire site here" name="site_search" id="site_search" /> 
			<input type="submit" id="submit-site-search" value="submit-site-search" name="submit-site-search"/>
		</form>
	</div>-->
	<!--//search-->
	
    --}}

	<!--contact-->
	<div class="contact">
            <span class="number"><i class="fa fa-phone-square"></i> {{ Config::get('site.phone1') }}</span>
		    <span><i class="fa fa-envelope-square"></i> {{ Config::get('site.email') }} </span>          
	</div>

    @if(Config::get('site.multilanguage') == true)
        <div class="flags">
            <a href="{{ URL::to('language/sq') }}">
                <img src="{{ asset('assets/images/ico/AL.png') }}" width="18">
            </a>
            <a href="{{ URL::to('language/en') }}">
                <img src="{{ asset('assets/images/ico/GB.png') }}" width="18">
            </a>
            <a href="{{ URL::to('language/mk') }}">
                <img src="{{ asset('assets/images/ico/MK.png') }}" width="18">
            </a>
        </div>  
    @endif
	<!--//contact-->
</div>
