<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="HandheldFriendly" content="True">
<title>{{ $page->title or 'Savatours - Pushime, Hotele, Fluturime' }}</title>
<meta name="description" content="{{ $meta_description or 'Gjeni pushimet tuaja ideale me Savatours. Paketa All Inclusive per plazh ne rezortet me te mira ne Antalja' }}" />
<meta name="keywords" content="{{ $meta_keywords or 'antalja 2014, antalya, antalja, agjensi udhetimi, agjensi turistike, pushime verore, plazh, oferta speciale, prenoto online, prenotime, rezervime online, ture ne shqiperi, pushime ne turqi, pushime ne antalja, hotelet turke, hotelet shqiptare, savatours, fluturime, charter, turqi, oferta speciale ne turqi, all inclusive, ultra all inclusive' }}"/>
<link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" />	

@section('css')
{{ HTML::style('assets/css/prettyPhoto.css') }}
{{ HTML::style('assets/js/plugins/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.css') }}
{{ HTML::style('assets/js/plugins/jquery-multiselect/jquery.multiselect.css') }}
{{ HTML::style('assets/js/plugins/jquery-multiselect/jquery.multiselect.filter.css') }}	
{{ HTML::style('assets/css/style.css') }}
{{ HTML::style('assets/css/theme-blue.css') }}
{{ HTML::style('//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css') }}
@show