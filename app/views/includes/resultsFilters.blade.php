<div id="resultsFilter">
	<div id="sticker" style="margin-bottom: 20px;">
		<article class="refine-search-results">
			<h2>{{ Lang::get('searchresults.filter_results') }}</h2>
			<dl>
				 
{{-- 				<dt>{{ Lang::get('searchresults.price') }}</dt>
				<dd style="display:block; height:auto; min-height: 30px;">
					<div id="priceRange"></div>
				</dd> --}}
					
{{-- 				<dt>{{ Lang::get('searchresults.concept') }}</dt>
				<dd style="display:block; height:auto;">
					@foreach($meals as $meal_type)
					<div class="checkbox">
						<input type="checkbox" id="{{$meal_type['Inc']}}" name="concept[]" value="{{$meal_type['Inc']}}" {{in_array($meal_type['Inc'], $selected_meals) ? 'checked' : ''}}/>
						<label for="{{$meal_type['Inc']}}">{{$concepts[$meal_type['Name']]}}</label>
					</div>							
					@endforeach	
				</dd> --}}
				<dt>{{ Lang::get('searchresults.town') }}</dt>
				<dd style="display:block; height:auto;">
					<div class="checkbox">
					@if($destinationTowns == $selectedTowns || empty($selectedTowns))
						<input type="checkbox" id="allTowns" name="allTowns" value="all" checked />
				    @else 
				    	<input type="checkbox" id="allTowns" name="allTowns" value="all"  />
					@endif
						<label for="allTowns">{{ Lang::get('homepage.all') }}</label>
					</div>											
					@foreach($destinationTowns as $town)
					<div class="checkbox">
						@if( empty($selectedTowns) )
						<input type="checkbox" id="{{$town['TownInc']}}" name="towns" value="{{$town['TownInc']}}" checked  />
						@else
						<input type="checkbox" id="{{$town['TownInc']}}" name="towns" value="{{$town['TownInc']}}" {{ in_array($town['TownInc'], $selectedTowns) ? 'checked' : '' }}  />
						@endif
						<label for="{{$town['TownInc']}}">{{$town['TownName']}}</label>
						
					</div>							
					@endforeach	
				</dd>								
			</dl>		
		</article>
	</div>
</div>