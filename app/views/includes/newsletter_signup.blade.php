
	<section class="full" style="margin-top:5px;">
		<div style="background-color:white;padding:15px;height:32px;">
			<div style="float:left;padding-left:8px;">
				<a href="{{ Config::get('site.facebook_url') }}" target="_blank"><img src="{{ asset('assets/images/ico/fb_logo.png') }}"></a>
			</div>
			<div style="float:left;padding-left:8px;">
				<a href="#"><img src="{{ asset('assets/images/ico/youtube_logo.png') }}"></a>
			</div>
			<div style="float:left;padding-left:8px;">
				<a href="#"><img src="{{ asset('assets/images/ico/g+_logo.png') }}"></a>
			</div>
			<div style="float:left;padding-left:8px;">
				<a href="#"><img src="{{ asset('assets/images/ico/insta_logo.png') }}"></a>
			</div>
			<a href="#{{-- {{URL::to('/holiday_alert') }} --}}">
				<div class="registration_btn">
					 <span >{{ Lang::get('homepage.signup') }}</span>
				</div>
			</a>
			<div class="registration_txt">
				<span>{{ Lang::get('homepage.signup_for_offers') }}</span>
				<br>
				<span>{{ Lang::get('homepage.personalized_offers') }}>
			</div>
		</div>
	</section>