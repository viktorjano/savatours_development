<div id="sequence">
	<ul class="sequence-canvas">
		<li>
			<div class="info animate-in" style="max-width:70%; background: #fff;">
				<h2 style="padding-bottom: 5px;">Alva Donna Exclusive <span class="stars">5*</span>, <span style="border-bottom: 2px solid #FFA800;">€956</span> <span style="font-size:0.5em;">pp</span></h2>
				<p>Oferte speciale per nisje ne <span style="border-bottom: 1px dashed #FFA800;">14 Gusht, 7 net, Ultra All Inclusive</span></p>
			</div>
			<img class="main-image animate-in" src="http://dev.savatours.com/image/570x260/uploads/bRaVOEBr/AlvaDonna.jpg" alt="" />
		</li>
		<li>
			<div class="info animate-in" style="max-width:70%; background: #fff;">
				<h2 style="padding-bottom: 5px;">Gural Premier Belek <span class="stars">5*</span>, <span style="border-bottom: 2px solid #FFA800;">i ri</span></h2>
				<p>Parajse familjare. <span style="border-bottom: 1px dashed #FFA800;">Partner ekskluziv i Savatours.</span></p>
			</div>
			<img class="main-image animate-in" src="http://dev.savatours.com/image/570x260/uploads/bRaVOEBr/AlvaDonna.jpg" alt="" />
		</li>		
	</ul>
</div>