@foreach($tours as $tour)
	<section class="top-offers">
		<div class="offer-details">
			<span class="hotel-name">
				<a href="{{ URL::to('pushime/hotel/?' . http_build_query($form['urlParameters']) . '&hotels=' . $tour['HotelInc']. '&claim=' . $tour['Cat_Claim'] . '&checkin=' . urlencode($tour['CheckIn']) ) }}">{{ ucwords(strtolower($tour['HotelName'])) }}</a> &nbsp;
			</span>
			
			<span class="stars">
				@for( $i = 0; $i < $stars[ $tour['StarName'] ]; $i++ )
					<span class="star">&nbsp;</span>
				@endfor
			</span>

			<div class="hotel-details">
				<span style=""><i class="fa fa-plane"></i> &nbsp;{{ $date['1']. ' ' . ucfirst($date['2']) }}, {{ $tour['Nights'] }} {{ Lang::get('hotel.nights') }}</span> | <i class="fa fa-map-marker"></i> &nbsp;{{ studly_case( strtolower($tour['HotelTownLName']) ) }}, Turkey
			</div>								
		</div>
		<a href="{{ URL::to('pushime/hotel/?' . http_build_query($form['urlParameters']) . '&hotels=' . $tour['HotelInc']. '&claim=' . $tour['Cat_Claim'] . '&checkin=' . urlencode($tour['CheckIn']) ) }}" class="gradient-button right arrow-right">€{{ floor($tour['Price'] / $tour['Adult']) }} <span class="pp">pp</span></a>
	</section>
@endforeach	