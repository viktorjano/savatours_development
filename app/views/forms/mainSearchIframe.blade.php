



{{ Form::open(array('url' => '/pushime/partner/' . $partner_id, 'method' => 'get', 'id' => 'main-search')) }}
<div class="forms">
	<!--form-->
	<div class="form" id="search-form" style="display:block">
		
		<div class="column first destination">
			<div class="f-item">
				<label for="t">{{ Lang::get('searchform.destination') }}</label>
				<select name='t' id="t" class="uniform">
					@if(isset($form['tours']))
						@foreach($form['tours'] as $tour)
						<option value="{{{ $tour['TourInc'] }}}" {{ $tour['TourInc'] == $form['t'] ? 'selected' : '' }}> {{{ $tour['TourName'] }}} </option>
						@endforeach
					@endif
				</select>
			</div>

		</div>

		{{-- @if(isset($selectedHotels) AND isset($hotels)) --}}
		<div class="column hotel">
			<div class="f-item">
				<label for="search-hotels">{{ Lang::get('searchform.hotel') }}</label>
				{{ Form::select('hotels[]', $hotels, $selectedHotels, array('multiple'=>'multiple', 'id'=>'search-hotels')) }}
			</div>
		</div>
		{{--@endif --}}

		<div class="column calendar">
			<div class="f-item datepicker">
				<label for="display_date">{{ Lang::get('searchform.departure_date') }}</label>
				<input type="hidden" name="checkin" id="checkin" value="{{ $form['checkin'] }}">
				<div class="datepicker-wrap">
					<input type="text" value="" id="display_date" name="display_date" />
				</div>
				<span class="flexible-dates">
					<label for="flexible-dates">
						<input type="checkbox" checked="checked" name="flexible-dates" id="flexible-dates"> &nbsp;+/- 3 {{ Lang::get('searchform.days') }}
					</label>
				</span>
			</div>
		</div>

		<div class="column duration">
			<div class="f-item">
				<label for="duration">{{ Lang::get('searchform.duration') }}</label>
				{{ Form::select('duration', $form['durations'], $form['duration'], array('class' => 'uniform', 'id' => 'duration')) }}
			</div>
		</div>

		<div class="column twins pax">
			<div class="f-item spinner">
				<label for="adults">{{ Lang::get('searchform.adults') }}</label>
				<select name="adults" class="uniform" id="adults">
					@for($i = 1; $i <= 5; $i++)
					<option value="{{ $i }}" {{{ $i == $form['adults'] ? 'selected' : '' }}}> {{ $i }}</option>
					@endfor
				</select>
			</div>
			
			<div class="f-item spinner last">
				<label for="children">{{ Lang::get('searchform.children') }} <b>(2-12)</b></label>
				<select name="children" id="children" class="uniform">
					@for($i = 0; $i < 4; $i++)
					<option value="{{ $i }}" {{{ $i == $form['children'] ? 'selected' : '' }}}> {{ $i }}</option>
					@endfor
				</select>				
			</div>
			
			<div class="child-ages" style="{{ $form['children'] == 0 ? 'display:none' : ''}}" id="child_ages">
				<p>{{ Lang::get('searchform.children_ages') }}</p>
				
				<div class="f-item spinner" style="{{ $form['child1_age'] == -1 ? 'display:none' : '' }}" id="child1_age">					
					<select name="child1_age" class="uniform">
					<option value="-1">--</option>									
						@for($i = 2; $i <= 14; $i++)
							<option value="{{{ $i }}}" {{ $i == $form['child1_age'] ? 'selected' : '' }}> {{{ $i }}} </option>
						@endfor
					</select>
				</div>
				
				<div class="f-item spinner" style="{{ $form['child2_age'] == -1 ? 'display:none' : '' }}" id="child2_age">
					<select name="child2_age" class="uniform">
						<option value="-1">--</option>
						@for($i = 2; $i <= 14; $i++)
							<option value="{{{ $i }}}" {{ $i == $form['child2_age'] ? 'selected' : '' }}> {{{ $i }}} </option>
						@endfor
					</select>
				</div>

				<div class="f-item spinner" style="{{ $form['child3_age'] == -1 ? 'display:none' : '' }}" id="child3_age">
					<select name="child3_age" class="uniform">
						<option value="-1">--</option>
						@for($i = 2; $i <= 14; $i++)
							<option value="{{{ $i }}}" {{ $i == $form['child3_age'] ? 'selected' : '' }}> {{{ $i }}} </option>
						@endfor
					</select>
				</div>														
			</div>
		</div>

		<div class="column twins last">
			<input type="submit" value="{{ Lang::get('searchform.search') }}" class="search-submit" id="search-submit" />										
		</div>		
	</div>	

	<!--searchinfo-->
{{-- 	<div class="form search-info" id="search-info" style="display:none;">
		<div class="column first">
			<div class="f-item">
				<label>Destinacioni</label>
				<span class="parameter">{{{ ucwords(strtolower($form['tours'][0]['TourName'])) }}}</span>
			</div>
		</div>
		<div class="column">
			<div class="f-item">
				<label>Hoteli</label>
				<span class="parameter">Amara Dolce Vita</span>
			</div>
		</div>			
		<div class="column">
			<div class="f-item">
				<label>Data e nisjes</label>
				<span id="info-checkin" class="parameter"></span>
			</div>
		</div>	
		<div class="column">
			<div class="f-item">
				<label>Kohezgjatja</label>
				<span class="parameter">Rreth 1 jave</span>
			</div>
		</div>					
		<div class="column twins">
			<div class="f-item">
				<label>Te rritur</label>
				<span class="parameter">{{{ $form['adults'] }}}</span>
			</div>
			<div class="f-item">
				<label>Femije</label>
				<span class="parameter">{{{ $form['children'] }}}</span>
			</div>
		</div>	
		<div class="column twins last">
			<a href="#" class="gradient-button" id="toggle-form">Ndrysho kerkimin</a>										
		</div>													block	</div> --}}

</div>

<input type="hidden" name="from" value="{{{ Config::get('site.departure_default') }}}">
<input type="hidden" name="s" value="{{{ Config::get('site.state_default') }}}">

{{ Form::close() }}