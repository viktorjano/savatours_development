@extends('admin.layouts.forms')

@section('right') {{-- start right --}}
<aside class="right-side">
                <!-- Content Header (Page header) -->
<!--                 <section class="content-header">
                    <h1>
                        Edit Hotel
                        <small>update all hotel attributes</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Hotel</a></li>
                        <li class="active">Edit</li>
                    </ol>
                </section> 
-->

                 <!-- Main content -->
                <section class="content">

                    <div class='row'>
                        <div class='col-md-12'>
                            <div class='box box-primary'>
                                <div class='box-header'>
                                    <h3 class='box-title'>Descriptions</h3>
                                    <!-- tools box -->
                                    <!-- <div class="pull-right box-tools">
                                        <button class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                    </div> --><!-- /. tools -->
                                </div><!-- /.box-header -->
                    {{ Form::open(array('url' => 'admin/hotel/update')) }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Samo ID:</label>
                                    <input type="text" class="form-control " placeholder="4" disabled="">
                                </div>            
                                <div class="form-group">
                                    <label>Hotel Name:</label>
                                    <input type="text" class="form-control" placeholder="Alibey Resort Side" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Slogan:</label>
                                    <input type="text" class="form-control" placeholder="">
                                </div>               
                                <div class="form-group">                                   
                                    <label>Features:</label>
                                    <select class="form-control chosen-select" multiple placeholder="Hotel Features" data-placeholder="Hotel Features">
                                        <option value=""></option>
                                        <option value="United States">United States</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="Afghanistan">Afghanistan</option>
                                        <option value="Aland Islands">Aland Islands</option>
                                        <option value="Albania">Albania</option>
                                        <option value="Algeria">Algeria</option>
                                        <option value="American Samoa">American Samoa</option>
                                        <option value="Andorra">Andorra</option>
                                        <option value="Angola">Angola</option>
                                        <option value="Anguilla">Anguilla</option>
                                        <option value="Antarctica">Antarctica</option>
                                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                        <option value="Argentina">Argentina</option>
                                        <option value="Armenia">Armenia</option>
                                        <option value="Aruba">Aruba</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Austria">Austria</option>
                                        <option value="Azerbaijan">Azerbaijan</option>
                                        <option value="Bahamas">Bahamas</option>
                                        <option value="Bahrain">Bahrain</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Barbados">Barbados</option>
                                        <option value="Belarus">Belarus</option>
                                        <option value="Belgium">Belgium</option>
                                        <option value="Belize">Belize</option>
                                        <option value="Benin">Benin</option>
                                        <option value="Bermuda">Bermuda</option>
                                        <option value="Bhutan">Bhutan</option>
                                        <option value="Bolivia, Plurinational State of">Bolivia, Plurinational State of</option>
                                        <option value="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>
                                        <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                    </select>
                                </div>                                                                        
                                <div class="form-group col-xs-12">                                   
                                    <label>Short Description:</label>                                    
                                        <textarea id="editor1" name="editor1" rows="10" cols="80">
                                           		
                                        </textarea> 
                                </div>       
                                <div class="form-group col-xs-12">                                   
                                    <label>Full Description:</label>                                    
                                        <textarea id="editor2" name="editor1" rows="10" cols="80">
                                                
                                        </textarea> 
                                </div>                            
                                <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                            {{ Form::close() }}                                                           
                            </div><!-- /.box -->
                        </div><!-- /.col-->
                        <div class='col-md-12'>
                            <div class='box box-primary'>
                                <div class='box-header'>
                                   <h3 class='box-title'>Photos</h3>                                 
                                </div><!-- /.box-header --> 
                                <form action="/file-upload" class="dropzone">
                                  <div class="fallback">
                                    <input name="file" type="file" multiple />
                                  </div>
                                </form>                                   
                            </div><!-- /.box -->                           
                        </div><!-- /.col-->                        
                    </div><!-- ./row -->
                </section><!-- /.content -->
            </aside>
@stop {{-- end right --}}