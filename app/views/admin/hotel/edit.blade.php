@extends('admin.layouts.forms')

@section('right') {{-- start right --}}

                <!-- Content Header (Page header) -->

                 <!-- Main content -->
                <section class="content">
                  @if(!$errors->isEmpty()) 
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif                    
                   <div class="row">
                    <div class="col-md-12">
                    <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#descriptions" data-toggle="tab">Descriptions</a></li>
                                    <li><a href="#media" data-toggle="tab">Media Gallery</a></li>

                                </ul>
                                <div class="tab-content">
                                <div class="tab-pane active" id="descriptions">
                                 {{ Form::model($hotel, array('route' => array('admin.hotels.update', $hotel['inc']), 'method' => 'PUT')) }}
                                        <div class="row">
                                            <div class="col-md-1">
                                                <div class="rm-group">
                                                {{ Form::label('ref_id', 'Samo ID') }}
                                                {{ Form::text('ref_id', $hotel['inc'] , array('class' => 'form-control', 'disabled')) }}                                    
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    {{ Form::label('name', 'Name') }}
                                                    {{ Form::text('name', null, array('class' => 'form-control', 'disabled')) }}
                                                </div>
                                            </div>                 
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {{ Form::label('slogan', 'Slogan') }}
                                                    {{ Form::text('slogan', $hotel['slogan'], array('class' => 'form-control')) }}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                {{ Form::label('slug', 'Slug') }}
                                                    <div class="input-group">
                                                    <span class="input-group-addon">www.savatours.com.com/hotel/</span>
                                                    {{ Form::text('slug', slugify($hotel['name']), array('class' => 'form-control')) }}
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {{ Form::label('features', 'Features') }}
                                                    <article id="tags">
                                                        {{ Form::hidden('features', $hotel['features'], array('id' => 'features')) }}
                                                    </article>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {{ Form::label('collections', 'Collections') }}
                                                    {{ Form::select('collections[]', $collections, $hotel['collections'], array('class'=>'form-control chosen-select', 'multiple' => true))
                                                    }}
                                                </div>
                                            </div>     
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                <label>Short Description:</label>               
                                                    <textarea class="ckeditor" id="editor1" name="short_description" rows="10" cols="80">
                                                        {{{ $hotel['short_description'] }}}
                                                    </textarea>    
                                                </div>
                                            </div>          
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                <label>Resort Description:</label>                                    
                                                    <textarea class="ckeditor" id="editor2" name="resort_description" rows="10" cols="80">
                                                        {{ $hotel['resort_description']  }}
                                                    </textarea> 
                                                </div>
                                            </div>                                                         
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                <label>Rooms Description:</label>               
                                                    <textarea class="ckeditor" id="editor3" name="rooms_description" rows="10" cols="80">
                                                        {{{ $hotel['rooms_description'] }}}
                                                    </textarea>    
                                                </div>
                                            </div> 
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                <label>Beach & Pools Description:</label>               
                                                    <textarea class="ckeditor" id="editor4" name="beach_pools_description" rows="10" cols="80">
                                                        {{{ $hotel['beach_pools_description'] }}}
                                                    </textarea>    
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                <label>Concept Description:</label>               
                                            <textarea class="ckeditor" id="editor5" name="concept_description" rows="10" cols="80">
                                                {{{ $hotel['concept_description'] }}}
                                            </textarea>    
                                                </div>
                                            </div> 
                                        </div>                                        

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="well well-sm well-primary">
                                                    <button type="submit" class="btn btn-success">
                                                    <span class="glyphicon glyphicon-floppy-disk"></span>Save
                                                    </button>
                                                    <a href="{{{ URL::to('admin/hotels') }}}" class="btn btn-danger">
                                                    <span class="glyphicon glyphicon-remove"></span>Cancel
                                                    </a>     
                                                </div>
                                            </div>
                                        </div>
                                     {{ Form::close() }}                                  
                                    </div><!-- /.tab-pane -->
                                    <div class="tab-pane" id="media">
                                        <div class="row">
                                            <div class="col-md-6" id="sortable" data-url="{{{ URL::to('admin/albums/order') }}}">
                                            @if(isset($album))
                                                @foreach($album->photos as $photo)
                                                <div class="thumbnail col-md-2 margin" id="{{ $photo->id }}">          
                                                    <img src="{{{ URL::to('/') . '/' . $photo->filename }}}" alt="..." style="height: 140px;" >
                                                    <a href="#" class="del-photo" data-url="{{{ URL::to('admin/photos/' . $photo->id) }}}">Remove</a> 
                                                    <label class="btn btn-default">
                                                        <input type="radio" name="cover" id="cover" value="{{{ $photo->id }}}" data-url="{{{ URL::to('admin/albums/setCover') }}}" data-album="{{{ $album->id  }}}">Album cover
                                                    </label>
                                                </div>
                                                @endforeach
                                            @endif    
                                            </div>
                                            <div class="col-md-6">
                                                @if(isset($hotel['id']))
                                                {{ Form::open(array('route' => 'admin.photos.store', 'files'=> true, 'class' => 'dropzone', 'id' => 'myDropzone')) }}
                                                    <input name="hotel" type="hidden" value="{{{ $hotel['id'] }}}">                                              
                                                      <div class="fallback">
                                                        <input name="file" type="file" multiple />
                                                      </div>
                                                {{ Form::close() }}
                                                @else
                                                    <div class="alert alert-info">
                                                        <i class="fa fa-info"></i>
                                                        You must save the hotel descriptions before being able to add photos
                                                    </div>                                          
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                                    
                                        </div>                                        
                                    </div><!-- /.tab-pane -->
                                </div><!-- /.tab-content -->
                            </div>                    
                        </div>
              </section><!-- /.content -->
@stop {{-- end right --}}

@section('scripts')
@parent
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{{ URL::to('/') . '/assets/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js' }}}" type="text/javascript"></script>
        <script src="{{{ URL::to('/') . '/assets/admin/js/plugins/dropzone/dropzone.js' }}}" type="text/javascript"></script>
        <script src="{{{ URL::to('/assets/admin/js/plugins/chosen/chosen.jquery.min.js') }}}" type="text/javascript"></script>
        <script src="{{{ URL::to('/') . '/assets/admin/js/plugins/select2/select2.js' }}}" type="text/javascript"></script>        
        {{ HTML::script('https://code.jquery.com/ui/1.11.4/jquery-ui.min.js') }}

        <script>
          // myDropzone is the configuration for the element that has an id attribute
          // with the value my-dropzone (or myDropzone)
          Dropzone.options.myDropzone = {
            init: function() {
              this.on("addedfile", function(file) {

                // Create the remove button
                var removeButton = Dropzone.createElement("<button class='btn btn-danger'>Remove</button>");


                // Capture the Dropzone instance as closure.
                var _this = this;

                // Listen to the click event
                removeButton.addEventListener("click", function(e) {
                  // Make sure the button click doesn't submit the form:
                  e.preventDefault();
                  e.stopPropagation();

                  // Remove the file preview.
                  _this.removeFile(file);
                  // If you want to the delete the file on the server as well,
                  // you can do the AJAX request here.
                });

                // Add the button to the file preview element.
                file.previewElement.appendChild(removeButton);
              });
            }
          };
        </script>

        <script type="text/javascript">
            $(function() {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.config.imageBrowser_listUrl = "{{ URL::to('admin/hotels/json_photos/' . $hotel['inc']) }}";              

                // CKEDITOR.config.extraPlugins = 'templates';
                // CKEDITOR.config.extraPlugins = 'filebrowser';
                //bootstrap WYSIHTML5 - text editor
                //$(".textarea").wysihtml5();c
            });

            // Chosen init
            var config = {
              '.chosen-select'           : {},
              '.chosen-select-deselect'  : {allow_single_deselect:true},
              '.chosen-select-no-single' : {disable_search_threshold:10},
              '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
              '.chosen-select-width'     : {width:"95%"}
            }
            for (var selector in config) {
              $(selector).chosen(config[selector]);
            }

        </script>

        <script type="text/javascript">
              $(document).ready(function () {
                $("#features").select2({
                    tags:[],
                    containerCss : {"display":"block"},
                    separator: ";"
                });
              });             
        </script>        

    <script type="text/javascript">
            $(document).ready(function(){

                // Delete the image
                $(".del-photo").click(function (e) {
                    e.preventDefault();
                    
                    var t = $(this);
                    var n = t.parent();
                    var url = t.data('url');
                    
                    $.ajax({ // Starter Ajax Call
                        
                        type: "DELETE", 
                        url: url, 
                        success: function(data) { 
                            var response = eval("(" + data + ')');
                            
                            if(response.success == true) {
                                n.fadeOut(400, function () {
                                    n.remove()
                                })
                            }
                        }
                        
                    });                                
                    return false;
                });

                $("input[name='cover']").on('ifChecked', function(e) {
                    e.preventDefault();

                    var url = $(this).data('url');
                    var album = $(this).data('album');
                    var image = $(this).val();

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {'album': album, 'photo':image },
                        success: function(msg) {
                            // console.log(msg);
                        }
                    });
                    return false;
                });

                $('#sortable').sortable({

                    stop: function(e, ui) {
                        var url = $(this).data('url');
                        var order = $('#sortable').sortable("toArray");
                        
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {'order':order}
                        });
                        console.log($('#sortable').sortable("toArray"));
                    }
                });

            });
    </script>        
@stop
