@extends('admin.layouts.master')

@section('head')
        <!-- bootstrap 3.0.2 -->
        <link href="../../assets/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../../assets/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../../assets/admin/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="../../assets/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../assets/admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
@stop

@section('header')
	@include('admin.includes.header')
@stop

@section('left')
	@include('admin.includes.left')
@stop

@section('right')               
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
            {{-- 
                <div class="box-header">
                    <h3 class="box-title">All Active Hotels</h3>                          
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    @if( \Session::has('success') )
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ \Session::get('success') }}
                        </div>                                     
                    @endif
            --}}                                                          
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Ref. ID</th>
                                <th>Name</th>
                                <th>Official Rating</th>
                                <th>Location</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($hotels as $hotel)
                            <tr>
                                <td>{{{ $hotel['HotelInc'] }}}</td>
                                <td>{{{ $hotel['HotelName'] }}}</td>
                                <td>{{{ $hotel['TownInc'] }}}</td>
                                <td>{{{ $hotel['StarInc'] }}}</td>
                                <td>
                                <a class="btn btn-primary" href="{{{ URL::to('admin/hotels/' . $hotel['HotelInc']) . '/edit' }}}">
                                    <i class="fa fa-edit"></i> Edit
                                </a>  
                                </td>
                            </tr>
                            @endforeach                                            

                        </tbody>
{{--                         <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Official Rating</th>
                                <th>Location</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot> --}}
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section><!-- /.content -->
@stop

@section('scripts')
@parent
        <!-- DATA TABES SCRIPT -->
        <script src="{{{ URL::to('/') . '/assets/admin/js/plugins/datatables/jquery.dataTables.js' }}}" type="text/javascript"></script>
        <script src="{{{ URL::to('/') . '/assets/admin/js/plugins/datatables/dataTables.bootstrap.js' }}}" type="text/javascript"></script>

        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,                    
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
@stop

