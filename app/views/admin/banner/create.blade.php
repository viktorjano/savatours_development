@extends('admin.layouts.master')

@section('head')

        <!-- bootstrap 3.0.2 -->
        <link href="{{{ URL::to('/assets/admin/css/bootstrap.min.css') }}}" rel="stylesheet" type="text/css" />

        <!-- font Awesome -->
        <link href="{{{ URL::to('/assets/admin/css/font-awesome.min.css') }}}" rel="stylesheet" type="text/css" />

        <!-- Ionicons -->
        <link href="{{{ URL::to('/assets/admin/css/ionicons.min.css') }}}" rel="stylesheet" type="text/css" />

		<!-- bootstrap wysihtml5 - text editor -->
        <link href="{{{ URL::to('/assets/admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}}" rel="stylesheet" type="text/css" />

        <!-- Theme style -->
        <link href="{{{ URL::to('/assets/admin/css/AdminLTE.css') }}}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('assets/admin/css/bootstrap-datetimepicker/bootstrap-datetimepicker.css') }}" rel="stylesheet" type="text/css" />

        <link href="{{{ asset( '/assets/admin/js/plugins/chosen/chosen.css' )}}}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        {{ HTML::style('assets/js/plugins/jcrop/jquery.Jcrop.min.css') }}        

@stop

@section('header')
	@include('admin.includes.header')
@stop

@section('left')
	@include('admin.includes.left')
@stop

@section('right')                      	
<div class="row">
<!-- Modal -->
<div class="modal fade" id="cropModal" tabindex="-1" role="dialog" aria-labelledby="cropModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="cropModalLabel">Crop and filter</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-6">                
            <div class="form-group">    
            {{ Form::label('bannerFormat', 'Banner Format') }}  
            {{ Form::select('bannerFormat', array('full' => 'Primary', 'half' => 'Secondary'), array('id' => 'bannerFormat')) }}
            </div>
          </div>
          <div class="col-md-12" id="workspace">
              <img id="crop" style="max-width:80%"/>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="cropButton" data-dismiss="modal">Crop</button>
      </div>
    </div>
  </div>
</div>
<!-- Primary box -->
{{ Form::open(array('url' => 'admin/banners', 'files' => true, 'id' => 'banner-form')) }}
{{ Form::hidden('x', null, array('id' => 'x')) }}
{{ Form::hidden('y', null, array('id' => 'y')) }}
{{ Form::hidden('w', null, array('id' => 'w')) }}
{{ Form::hidden('h', null, array('id' => 'h')) }}                

<div class="col-md-12">
    <div class="form-group col-md-2">
        {{ Form::label('photo', 'Image') }}
        {{ Form::file('photo', array('class' => 'form-control', 'id' => 'photo')) }}
    </div>
    <div class="form-group col-md-2">
        {{ Form::label('start', 'Start') }}                    
        <div class='input-group date' id='start'>                        
            {{ Form::text('start', Input::old('start'), array('class' => 'form-control datetime')) }}
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>   
    <div class="form-group col-md-2">
        {{ Form::label('expiration', 'End') }}                    
        <div class='input-group date' id='end'>                        
            {{ Form::text('expiration', Input::old('expiration'), array( 'class' => 'form-control datetime' )) }}
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>       
    <div class="form-group col-md-4">
        {{ Form::label('link', 'Link') }}
        {{ Form::text('link', Input::old('link'), array('class' => 'form-control')) }}
    </div>    
    <div class="form-group col-md-2">
        {{ Form::label('next', 'Next') }}
        {{ Form::select('next', array('Banner - 1', 'etc...'), 0, array('class' => 'form-control')) }}
    </div>                             
</div>
<div class="col-md-12">                
    <div class="form-group">
        {{ Form::label('content', 'Content') }}
        {{ Form::textarea('content', Input::old('content'), array('class'=>'ckeditor', 'style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px;', 'id' => 'bannerContent', 'contenteditable' => true))
        }}
    </div>
</div>
{{ Form::close() }}

</div>
@stop

@section('scripts')
@parent
<script src="{{ URL::to('/assets/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/admin/js/plugins/bootstrap-datetimepicker/moment.js') }}"></script>
<script src="{{ asset('/assets/admin/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ URL::to('/assets/admin/js/plugins/chosen/chosen.jquery.min.js') }}" type="text/javascript"></script>
{{ HTML::script('assets/js/plugins/jcrop/jquery.Jcrop.min.js') }}

<script type="text/javascript">

$(function() {
    $('#expiration').datetimepicker();
});	


function setCoords(c)
{
  $('#x').val(c.x);
  $('#y').val(c.y);
  $('#w').val(c.w);
  $('#h').val(c.h);
};

$('#photo').change(function() {

  var newPhoto;

  var oFReader = new FileReader();

  oFReader.readAsDataURL(document.getElementById("photo").files[0]);

  oFReader.onload = function(oFREvent) {
    document.getElementById("crop").src = oFREvent.target.result;
  };

  try {
     $("#workspace").html('');
     } catch (Error)
  { }

  $("#workspace").prepend('<img id="crop" />');

  $('#bannerFormat').val($('#bannerFormat option:selected').val()).trigger('change');

  $('#cropModal').modal('show');
});

$('#cropModal').on('show.bs.modal', function () {

   $(this).find('.modal-dialog').css({
          width:'60%', //probably not needed
          height:'auto', //probably not needed 
          'max-height':'100%'
   });
});    

$('#bannerFormat').on('change', function(e) {

  var selected = this.selectedOptions[0].value;

  if ( selected == 'full') {
    $('#crop').Jcrop({
        bgColor: 'black',
        bgOpacity:   .4,
        boxHeight: 500,
        boxWidth: 700,
        aspectRatio: 1048 / 295,
        onSelect: setCoords
    });
  } else {
    $('#crop').Jcrop({
        bgColor: 'black',
        bgOpacity:   .4,
        boxHeight: 500,
        boxWidth: 700,
        aspectRatio: 524 / 260,
        onSelect: setCoords
    });
  }
});

</script>
@stop