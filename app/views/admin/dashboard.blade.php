@extends('admin.layouts.master')

@section('head')
	@include('admin.includes.head_dashboard')
@stop

@section('header')
	@include('admin.includes.header')
@stop

@section('left')
	@include('admin.includes.left')
@stop
        
@section('right')
	@include('admin.includes.right_empty')
@stop

@section('scripts')
	        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="../assets/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="../assets/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="../assets/admin/js/AdminLTE/app.js" type="text/javascript"></script>
@stop

