{{-- @extends('admin.layouts.master') --}}

@section('head')
        <!-- bootstrap 3.0.2 -->
        <link href="../../assets/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../../assets/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../../assets/admin/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="../../assets/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../assets/admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
@stop

@section('header')
	@include('admin.includes.header')
@stop

@section('left')
	@include('admin.includes.left')
@stop

@section('right')       
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body table-responsive">                                
                <table id="banners" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Checkin</th>
                            <th>Hotels</th>
                            <th>Nights</th>
                            <th>Tools</th>
                        </tr>
                    </thead>                  
                    <tbody>
                    @foreach($topoffers as $topoffer)
                        <tr>
                            <td>{{ $topoffer->id }}</td>
                            <td>{{ $topoffer->checkin }}</td>
                            <td>{{ $topoffer->hotels }}</td>
                            <td>{{ $topoffer->nights }}</td>
                            <td>
                            <div class="btn-group">
                                <a class="btn btn-sm btn-default" href="{{{ URL::to('admin/topoffers/' . $topoffer->id . '/edit') }}}">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                                {{ Form::open(array('route' => array('admin.topoffers.destroy', $topoffer->id), 'method' => 'delete')) }}
                                <button type="submit" class="btn btn-sm btn-default ">
                                    <i class="fa fa-times"></i> Delete
                                </button>
                                {{ Form::close() }}
                            </div>
                            </td>                                                        
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
@stop

@section('scripts')
@parent
<!-- DATA TABES SCRIPT -->
<script src="{{{ URL::to('/') . '/assets/admin/js/plugins/datatables/jquery.dataTables.js' }}}" type="text/javascript"></script>
<script src="{{{ URL::to('/') . '/assets/admin/js/plugins/datatables/dataTables.bootstrap.js' }}}" type="text/javascript"></script>

<script type="text/javascript">
$(function() {
    $('#banners').dataTable({
        "bPaginate": true,                    
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": true,
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "aButtons": [ "copy", "print" ]
        }                    
    });
});
</script>
@stop

