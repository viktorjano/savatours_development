@extends('admin.layouts.master')

@section('head')
        <!-- bootstrap 3.0.2 -->
        <link href="{{{ URL::to('/assets/admin/css/bootstrap.min.css') }}}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{{ URL::to('/assets/admin/css/font-awesome.min.css') }}}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{{ URL::to('/assets/admin/css/ionicons.min.css') }}}" rel="stylesheet" type="text/css" />
		<!-- bootstrap wysihtml5 - text editor -->
        <link href="{{{ URL::to('/assets/admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{{ URL::to('/assets/admin/css/AdminLTE.css') }}}" rel="stylesheet" type="text/css" />
        {{ HTML::style('/assets/admin/css/uidatepicker.css') }}
        <link href="{{ asset('assets/admin/css/bootstrap-datetimepicker/bootstrap-datetimepicker.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{{ asset( '/assets/admin/js/plugins/chosen/chosen.css' )}}}" rel="stylesheet" type="text/css" />
        {{ HTML::style('/assets/js/plugins/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.css') }}
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
@stop

@section('header')
	@include('admin.includes.header')
@stop

@section('left')
	@include('admin.includes.left')
@stop

@section('right')                      	
<div class="row">
	<div class="col-md-6">
        <!-- Primary box -->
        <div class="box box-solid">
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                <h3 class="box-title">Create top offers list</h3>
            </div>
             {{ Form::open(array('url' => 'admin/topoffers')) }}
              <div class="box-body">
                    <div class="form-group">
                    	{{ Form::label('hotels', 'Hotels') }}
                        {{ Form::select('hotels[]', $hotels, null, array('class' => 'form-control chosen-select', 'multiple' => true)) }}
                    </div>
                    <div class="form-group">
                        <div class="datepicker-wrap">
                        {{ Form::label('checkin', 'Checkin') }}
                        {{ Form::text('checkin', Input::old('checkin'), array('class' => 'form-control')) }}
                        </div>
                    </div>                    
                    <div class="form-group">
                    	{{ Form::label('nights', 'Nights') }}
                        {{ Form::text('nights', Input::old('nights'), array('class'=>'form-control'))
                        }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('expiration', 'Expiration') }}                    
                        <div class='input-group date' id='expiration'>                        
                            {{ Form::text('expiration', Input::old('expiration'), array('class' => 'form-control')) }}
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>                    
{{--                     <div class="form-group">
                        {{ Form::label('link', 'Link') }}
                        {{ Form::text('link', Input::old('link'), array('class' => 'form-control')) }}
                    </div> --}}
                </div><!-- /.box-body -->
                <div class="box-footer">
                	{{ Form::submit('Create top offers', array('class' => 'btn btn-primary')) }}
                </div>
           {{ Form::close() }}
    	</div>
	</div>
</div>
@stop

@section('scripts')
@parent
<script src="{{{ URL::to('/assets/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}}" type="text/javascript"></script>
<script src="{{ asset('/assets/admin/js/plugins/bootstrap-datetimepicker/moment.js') }}"></script>
<script src="{{ asset('/assets/admin/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}"></script>
<script src="{{{ URL::to('/assets/admin/js/plugins/chosen/chosen.jquery.min.js') }}}" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script src="{{{ URL::to('/assets/admin/js/plugins/chosen/chosen.jquery.min.js') }}}" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
    //bootstrap WYSIHTML5 - text editor
    // $(".textarea").wysihtml5();
    $('#expiration').datetimepicker();
});	

// Chosen init
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}

    departures = null;          

    $.ajax({
      url: "{{ URL::to('/flydays') }}",
      async: true,
      dataType: 'json',
      success: function (flights) {
        departures = flights.departures;
      }
    });

    function available_departures(date) {
      ymd =  ('0' + date.getDate()).slice(-2) + "/" + ('0' + (date.getMonth()+1)).slice(-2) + "/" +   date.getFullYear();

      if ($.inArray(ymd, departures) != -1) {
        return [true, "","Fly day"];
      } else {
        return [false,"","No flight this day"];
      }
    }   

    $('.datepicker-wrap input').each(function(){
        // read the next departure
        var departure = $.datepicker.parseDate('dd/mm/yy', $('#checkin').val());
            $(this).datepicker({
                dateFormat: "d MM, yy",
                regional: "{{ Session::get('lang') }}",
                defaultDate: departure,
                showOn: 'both',
                altField: '#checkin',
                altFormat: "dd/mm/yy",
                buttonImageOnly: true,
                beforeShowDay: available_departures,
            }); 
        $.datepicker.setDefaults($.datepicker.regional["{{ Session::get('lang') }}"]);

        $(this).datepicker('setDate', departure);
     });

     $('#info-checkin').html($('#display_date').val());         
     
    $(document).ready(function () {
        // Chosen init
        var config = {
          '.chosen-select'           : {},
          '.chosen-select-deselect'  : {allow_single_deselect:true},
          '.chosen-select-no-single' : {disable_search_threshold:10},
          '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
          '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
          $(selector).chosen(config[selector]);
        }
    });        
</script>
@stop