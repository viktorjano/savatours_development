@extends('admin.layouts.master')


@section('header')
	@include('admin.includes.header')
@stop


@section('right')
@parent
<div class="row clearfix">
	<div class="col-md-12">
        <div class="box box-primary">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>{{ trans('posts.title') }}</th>
                            <th>{{ trans('posts.status') }}</th>    
                            <th>{{ trans('posts.slug') }}</th>                                                    
                            <th>{{ trans('posts.published_date') }}</th>
                            <th>{{ trans('posts.is_sticky') }}</th>
                            <th width="10%">{{ trans('ui.tools') }}</th>
                        </tr>
                    	@foreach($posts as $post)
                        <tr>
                        	<td>{{ $post->title }}</td>
                            <td><code>{{ $post->slug }}</code></td>                            
                            <td>
                                @if($post->trashed())
                                    <span class="label label-danger label-as-badge">Deleted</span>
                                @else
                                    <span class="label {{$post->status=='DRAFT' ? 'label-warning' : 'label-success'}} label-as-badge">{{ $post->status }}</span>
                                @endif
                            </td>
                        	<td>{{ $post->published_date ? '<i class="fa fa-clock-o"></i> '.$post->published_date : '' }}</td>
                            <td><span class="label {{$post->is_sticky==0 ? 'label-danger' : 'label-success'}} label-as-badge">{{ $post->is_sticky == 0 ? 'No' : 'Yes' }}</span></td>                            
                        	<td width="10%">
                                <span class="btn-group inline">
                                    <a class="btn btn-xs btn-primary" href="{{{ URL::to('admin/posts/' . $post->id) . '/edit' }}}">
                                    <i class="fa fa-edit"></i> Edit
                                    </a>  
                                    @if($post->trashed())
                                    {{ Form::open(array('url' => 'admin/posts/restore/' . $post->id)) }}
                                        <button type="submit" class="btn btn-xs btn-default pull-right">
                                          <span class="fa fa-undo" aria-hidden="true"></span> Restore
                                        </button>                                
                                    {{ Form::close() }}
                                    @else
                                    {{ Form::open(array('url' => 'admin/posts/' . $post->id)) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        <button type="submit" class="btn btn-xs btn-danger pull-right">
                                          <span class="fa fa-times" aria-hidden="true"></span> Delete
                                        </button>                                
                                    {{ Form::close() }}
                                    @endif     
                                </span>                            
							</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
</div>

@stop

@section('scripts')
@parent

@stop


