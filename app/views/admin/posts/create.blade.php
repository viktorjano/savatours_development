@extends('admin.layouts.master')

@section('head')
        <!-- bootstrap 3.0.2 -->
        <link href="{{{ URL::to('/assets/admin/css/bootstrap.min.css') }}}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{{ URL::to('/assets/admin/css/font-awesome.min.css') }}}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{{ URL::to('/assets/admin/css/ionicons.min.css') }}}" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="{{{ URL::to('/assets/admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{{ URL::to('/assets/admin/css/AdminLTE.css') }}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/admin/css/bootstrap-datetimepicker/bootstrap-datetimepicker.css') }}" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
@stop

@section('header')
    @include('admin.includes.header')
@stop

@section('left')
    @include('admin.includes.left')
@stop

@section('right')                       

<div class="row">
{{ Form::open(array('url' => 'admin/posts', 'files' => true)) }}
    <div class="col-md-6">
        <!-- Primary box -->
        <div class="box box-solid">
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                <h3 class="box-title">Content</h3>
            </div>
              <div class="box-body">
                    <div class="form-group col-lg-6 nopadding">
                        {{ Form::label('title', 'Title') }}
                        {{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group col-lg-6">
                        {{ Form::label('slug', 'Slug') }}
                        {{ Form::text('slug', Input::old('slug'), array('class' => 'form-control')) }}
                    </div>                    
                    <div class="form-group">
                        {{ Form::label('summary', 'Summary') }}
                        {{ Form::textarea('summary', Input::old('summary'), array('class'=>'textarea', 'style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px;'))}}
                    </div>                    
                    <div class="form-group">
                        {{ Form::label('content', 'Content') }}
                        {{ Form::textarea('content', Input::old('content'), array('class'=>'textarea', 'style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px;'))}}
                    </div>           
                </div><!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-solid">
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                <h3 class="box-title">Attributes</h3>
            </div>
            <div class="box-body">             
                <div class="row">   
                    <div class="form-group col-md-6">
                        {{ Form::label('is_sticky', 'Is sticky?') }}
                        {{ Form::checkbox('is_sticky', '1', false)}}
                            <p class="help-block">Sticky posts are always 'sticked' to the top of the blog.</p>                        
                    </div>                       
                    <div class="form-group col-md-6">
                        {{ Form::label('published_date', 'Publish date') }}                    
                        <div class='input-group date' id='published_date'>                        
                            {{ Form::text('published_date', Input::old('published_date'), array('class' => 'form-control')) }}
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>     
                </div>             
                <div class="row">
                    <div class="form-group col-md-12">
                        {{ Form::label('status', 'Status') }}                    
                        {{ Form::select('status', array('DRAFT' => 'Draft', 'PUBLISHED' => 'Published'), 'DRAFT', array('class' => 'form-control')) }}
                    </div>                           
                </div>
            </div><!-- /.box-body -->
        </div>
    </div> 
    <div class="col-md-6">
        <div class="box box-solid">
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                <h3 class="box-title">Meta data</h3>
            </div>
            <div class="box-body">             
                <div class="row">
                    <div class="form-group col-md-12">
                        {{ Form::label('meta_description', 'Meta decription') }}
                        {{ Form::textarea('meta_description', Input::old('meta_description'), array('class' => 'form-control', 'size' => '50x2')) }}
                    </div>
                    <div class="form-group col-md-12">
                        {{ Form::label('meta_keywords', 'Meta keywords') }}
                        {{ Form::text('meta_keywords', Input::old('meta_keywords'), array('class' => 'form-control')) }}
                    </div>                
                </div>
            </div><!-- /.box-body -->
        </div>
    </div>     
    <div class="col-md-6">
        <div class="box box-solid">
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                <h3 class="box-title">Photo</h3>
            </div>
              <div class="box-body">      
                    <div class="form-group">
                        <span class="btn btn-default btn-file">
                            Browse <input type="file" name="image">
                        </span>    
                        <p class="help-block">The image must be at least 815px wide (jpeg, png, bmp, or gif).</p>                          
                    </div>         
                </div><!-- /.box-body -->
                    {{ Form::submit('Submit', array('class' => 'btn btn-primary hidden')) }}

        </div>
    </div>

   {{ Form::close() }}
</div>


@stop

@section('scripts')
@parent
<script src="{{{ URL::to('/assets/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}}" type="text/javascript"></script>
<script src="{{ asset('/assets/admin/js/plugins/bootstrap-datetimepicker/moment.js') }}"></script>
<script src="{{ asset('/assets/admin/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
$(function() {
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
    $('#published_date').datetimepicker();
}); 
</script>
@stop