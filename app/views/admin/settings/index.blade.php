@extends('admin.layouts.master')


@section('header')
	@include('admin.includes.header')
@stop


@section('right')
<!-- Right side column. Contains the navbar and content of the page -->
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ trans('settings.title') }}
        <small>{{ trans('setings.headline') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row clearfix">
	<div class="col-md-12">
        <div class="box box-primary">
            <!-- <div class="box-header"><h3 class="box-title">{{ trans('website') }}</h3></div> --> 
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>{{ trans('settings.group') }}</th>
                            <th>{{ trans('settings.value') }}</th>
                            <th>{{ trans('settings.description') }}</th>
                        </tr>
                    	@foreach($settings as $k=>$v)
                        <tr>
                    	   <td>{{ ucwords($k) }}</td>
                            <td><code>{{ json_encode($v) }}</code></td>
                            <td>description</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
</div>

</section><!-- /.content -->
@stop

@section('scripts')
@parent

@stop


