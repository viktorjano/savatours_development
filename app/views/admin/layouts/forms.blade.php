@extends('admin.layouts.master')

@section('head')
        <!-- bootstrap 3.0.2 -->
        <link href="{{{ URL::to('/') . '/assets/admin/css/bootstrap.min.css' }}}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{{ URL::to('/') . '/assets/admin/css/font-awesome.min.css' }}}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{{ URL::to('/') . '/assets/admin/css/ionicons.min.css' }}}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{{ URL::to('/') . '/assets/admin/css/AdminLTE.css' }}}" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="{{{ URL::to('/') . '/assets/admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css' }}}" rel="stylesheet" type="text/css" />
        <link href="{{{ URL::to('/') . '/assets/admin/js/plugins/dropzone/dropzone.css' }}}" rel="stylesheet" type="text/css" />
        <link href="{{{ URL::to('/') . '/assets/admin/js/plugins/chosen/chosen.css' }}}" rel="stylesheet" type="text/css" />
        <link href="{{{ URL::to('/') . '/assets/admin/js/plugins/select2/select2.css' }}}" rel="stylesheet" type="text/css" />        
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
<style>
/* Special grid styles
-------------------------------------------------- */

.show-grid {
  margin-top: 10px;
  margin-bottom: 20px;
}
.show-grid [class*="span"] {
  background-color: #eee;
  text-align: center;
  -webkit-border-radius: 3px;
     -moz-border-radius: 3px;
          border-radius: 3px;
  min-height: 40px;
  line-height: 40px;
}
.show-grid [class*="span"]:hover {
  background-color: #ddd;
}
.show-grid .show-grid {
  margin-top: 0;
  margin-bottom: 0;
}
.show-grid .show-grid [class*="span"] {
  margin-top: 5px;
}
.show-grid [class*="span"] [class*="span"] {
  background-color: #ccc;
}
.show-grid [class*="span"] [class*="span"] [class*="span"] {
  background-color: #999;
}
</style>        
@stop

@section('right')
	@parent
@stop

