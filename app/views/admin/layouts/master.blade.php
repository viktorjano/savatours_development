<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Savatours | Dashboard</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
{{-- head section holds the styles etc --}}
@section('head')
@include('admin.includes.head_dashboard') 
@show
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<header class="header">
    @include('admin.includes.header')
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    @section('left')
    <!-- Left side column. contains the logo and sidebar -->            
    @include('admin.includes.left')
    @show

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">  
    <!-- Right side column. Contains the navbar and content of the page -->
    <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ trans('page.title') }}
                <small>{{ trans('page.headline') }}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

        
        <!-- Notifications -->
        @include('admin.layouts.notifications')
        <!-- ./ notifications -->    
        
        {{ $tools or '' }}
        
        @section('right')           
        @show

        </section><!-- /.content -->

    </aside><!-- /.right-side -->

</div><!-- ./wrapper -->

@section('scripts')
    <!-- jQuery 2.0.2 -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="{{ URL::to('/assets/admin/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL::to('/assets/admin/js/AdminLTE/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/admin/js/AdminLTE/restfulizer.js') }}"></script> 
    {{ HTML::script('assets/admin/js/plugins/ckeditor/ckeditor.js') }}
    {{ HTML::script('assets/admin/js/plugins/ckeditor/adapters/jquery.js') }}

@show

</body>
</html>