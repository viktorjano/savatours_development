@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissable">
  <i class="fa fa-check"></i>	
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>{{trans('pages.flashsuccess')}}</strong> {{ $message }}
</div>
{{ Session::forget('success') }}
@endif

@if ($message = Session::get('error'))
<div class="alert alert-danger alert-dismissable">
  <i class="fa fa-ban"></i>	
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>{{trans('pages.flasherror')}}:</strong> {{ $message }}
</div>
{{ Session::forget('error') }}
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissable">
  <i class="fa fa-warning"></i>	
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>{{trans('pages.flashwarning')}}:</strong> {{ $message }}
</div>
{{ Session::forget('warning') }}
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info alert-dismissable">
  <i class="fa fa-info"></i>	
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>{{trans('pages.flashinfo')}}:</strong> {{ $message }}
</div>
{{ Session::forget('info') }}
@endif