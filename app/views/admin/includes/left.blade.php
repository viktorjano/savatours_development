            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
<!--                     <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{{ asset('assets/admin/img/avatar3.png') }}}" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, {{{ $user->username or 'human'  }}} </p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div> -->
                    <!-- search form -->
<!--                     <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form> -->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="{{{ URL::to('admin/') }}}">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-shopping-cart"></i>
                                <span>Products</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="{{{ URL::to('admin/hotels') }}}">
                                        <i class="fa fa-edit"></i>
                                        <span>Hotels</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{{ URL::to('admin/collections') }}}">
                                        <i class="fa fa-w fa-folder"></i>
                                        <span>Collections</span>
                                    </a>
                                </li>
                            </ul>
                        </li>                        
                        <li>
                            <a href="{{ URL::to('admin/pages') }}">
                                <i class="fa fa-th"></i> <span>Pages</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('admin/posts') }}">
                                <i class="fa fa-newspaper-o"></i> <span>Blog posts</span>
                            </a>
                        </li>
                       <li>
                            <a href="{{{ URL::to('admin/banners') }}}">
                                <i class="fa fa-image"></i> <span>Banners</span>
                            </a>
                        </li>
                       <li>
                            <a href="{{{ URL::to('admin/topoffers') }}}">
                                <i class="fa fa-bar-chart"></i> <span>Top Offers</span>
                            </a>
                        </li>   
                       <li>
                            <a href="{{{ URL::to('admin/users') }}}">
                                <i class="fa fa-users"></i> <span>Users</span>
                            </a>
                        </li>  
                        <li>
                            <a href="{{{ URL::to('admin/partners') }}}">
                                <i class="fa fa-users"></i> <span>Partners</span>
                            </a>
                        </li>                                                                        
{{--                        <li>
                            <a href="{{{ URL::to('admin/settings') }}}">
                                <i class="fa fa-cogs"></i> <span>Settings</span>
                            </a>
                        </li>                                                                        
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-desktop"></i>
                                <span>Other Apps</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{{ URL::to('admin/apps/freepbx') }}}"><i class="fa fa-angle-double-right"></i> PBX Telephony</a></li>
                            </ul>
                        </li> --}}
                        <!--<li class="treeview">
                            <a href="#">
                                <i class="fa fa-table"></i> <span>Tables</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/tables/simple.html"><i class="fa fa-angle-double-right"></i> Simple tables</a></li>
                                <li><a href="pages/tables/data.html"><i class="fa fa-angle-double-right"></i> Data tables</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="pages/calendar.html">
                                <i class="fa fa-calendar"></i> <span>Calendar</span>
                                <small class="badge pull-right bg-red">3</small>
                            </a>
                        </li>
                        <li>
                            <a href="pages/mailbox.html">
                                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                                <small class="badge pull-right bg-yellow">12</small>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span>Examples</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/examples/invoice.html"><i class="fa fa-angle-double-right"></i> Invoice</a></li>
                                <li><a href="pages/examples/login.html"><i class="fa fa-angle-double-right"></i> Login</a></li>
                                <li><a href="pages/examples/register.html"><i class="fa fa-angle-double-right"></i> Register</a></li>
                                <li><a href="pages/examples/lockscreen.html"><i class="fa fa-angle-double-right"></i> Lockscreen</a></li>
                                <li><a href="pages/examples/404.html"><i class="fa fa-angle-double-right"></i> 404 Error</a></li>
                                <li><a href="pages/examples/500.html"><i class="fa fa-angle-double-right"></i> 500 Error</a></li>                                
                                <li><a href="pages/examples/blank.html"><i class="fa fa-angle-double-right"></i> Blank Page</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>