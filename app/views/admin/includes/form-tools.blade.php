<div class="well well-sm clearfix" data-spy="stickout">        
    <div class="btn-toolbar" role="toolbar">
        <div class="btn-group">
            <a href="javascript:void(0);" onclick="$('form:first').submit();" class="btn btn-sm btn-primary">
                <i class="fa fa-check"></i> Save
            </a>                                       
        </div>
        <div class="btn-group">          
            <a href="javascript:void(0);" onclick="history.go(-1);" class="btn btn-sm btn-danger">
                <i class="fa fa-times"></i> Cancel
            </a>                                    
        </div>                
    </div>
</div>