@extends('admin.layouts.master')

@section('head')
        <!-- bootstrap 3.0.2 -->
        <link href="../../assets/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../../assets/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../../assets/admin/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="../../assets/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../assets/admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
@stop

@section('header')
	@include('admin.includes.header')
@stop

@section('left')
	@include('admin.includes.left')
@stop

@section('right')
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                    <i class="fa fa-building-o"></i>                        
                        Menus
                        <small>manage menus</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Menus</a></li>
                        <li class="active">Index</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">                   
                    @if(!$errors->isEmpty()) 
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif                	
                    <div class="row">
						<div class="col-md-6">
                            <!-- Primary box -->
                            <div class="box box-solid">
                                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                                    <h3 class="box-title">Add menu item</h3>
                                </div>
                                  {{ Form::open(array('url' => 'admin/menu')) }}
                                  <div class="box-body">
                                        <div class="form-group">
                                        	{{ Form::label('title', 'Title') }}
                                        	{{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}
                                        </div>
                                        <div class="form-group">
                                        	{{ Form::label('link_url', 'Url') }}
                                        	{{ Form::text('link_url', Input::old('link_url'), array('class' => 'form-control')) }}
                                        </div>
                                        <div class="form-group">
                                        	{{ Form::label('parent', 'Parent menu') }}
                                            {{ Form::select('parent', $parentPagesList, Input::old('parent'), array('class'=>'form-control chosen-select'))
                                            }}
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                    	{{ Form::submit('Create the menu item', array('class' => 'btn btn-primary')) }}
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                               {{ Form::close() }}
                        	</div>
                    	</div>                    	
						<div class="col-md-6">
                            <!-- Primary box -->
                            <div class="box box-primary">
                                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                                    <h3 class="box-title">Admin area menu</h3>
                                </div>
                                <div class="box-body">
									<table class="table">
									      <thead>
									        <tr></tr>
									          <th>#</th>
									          <th>Title</th>
									          <th>Url</th>
											  <th>Parent</th>          
									        </tr>
									      </thead>
									      <tbody>
								      @foreach($items as $item)
									        <tr id="{{{ $item->id }}}">
										          <td>{{{ $item->id }}}</td>
									          <td>{{{ $item->title }}}</td>
									          <td>{{{ $item->url }}}</td>
									          <td>{{{ $item->parent_id }}}</td>
									        </tr>
							        	@endforeach
									      </tbody>
									    </table>
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                    <code>.box-footer</code>
                                </div><!-- /.box-footer-->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop

@section('scripts')
@parent
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script type="text/javascript">
		$("tbody").sortable({
	        update: function( event, ui ) {
	            var order = $(this).sortable('toArray');
	            //console.log(order);	
	            $.post('{{{ URL::to('admin/menu/sort') }}}', { order: order });
	        }
    	}); 	
	</script>
@stop

