{{-- @extends('admin.layouts.master') --}}

@section('head')
@parent
        <!-- DATA TABLES -->
        <link href="{{ URL::to('/assets/admin/css/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
        <!-- jQuery UI -->
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style>
            .ui-state-highlight { height: 40px; line-height: 36px; }
            .popover { min-width: 400px ! important; }
        </style>
@stop

@section('header')
	@include('admin.includes.header')
@stop

@section('left')
	@include('admin.includes.left')
@stop

@section('right')
    
<!-- Right side column. Contains the navbar and content of the page -->
<pre>
{{-- dd($items->toArray()) --}}
<div class="row">
<div class="col-md-6">
    <!-- Primary box -->
    <div class="box box-primary">
        <div class="box-header">
            <i class="ion ion-clipboard"></i>
            <h3 class="box-title">Admin Menu</h3>
            <div class="box-tools pull-right">
                <a id="popover" class="btn btn-default pull-right" data-placement="left"><i class="fa fa-plus"></i> Add item</a>
        <div id="popover-head" class="hide">New  item</div>
            <div id="popover-content" class="hide">
            {{ Form::open(array('url' => 'admin/menu')) }}
              <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('title', 'Title') }}
                        {{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('link_url', 'Url') }}
                        {{ Form::text('link_url', Input::old('link_url'), array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('parent', 'Parent menu') }}
                        {{ Form::select('parent', $parentPagesList, Input::old('parent'), array('class'=>'form-control chosen-select'))
                        }}
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    {{ Form::submit('Create item', array('class' => 'btn btn-primary')) }}
                </div>
            {{ Form::close() }}
                </div>                                        
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="panel-group" id="accordion">
            @foreach($menuGroup as $group => $items)                               
              <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#{{{ $group }}}">
                            Menu: {{{ $group }}}
                        </a>
                      </h4>
                    </div>
                    <div id="{{{ $group }}}" class="panel-collapse collapse in">
                      <div class="panel-body">
                        <ul class="todo-list sortable">
                          @foreach($items as $item)
                            <li id="{{{ $item->id }}}" class="ui-state-default">
                                <!-- drag handle -->
                                <span class="handle">
                                    <i class="fa fa-ellipsis-v"></i>
                                    <i class="fa fa-ellipsis-v"></i>
                                </span>                                              
                                <!-- todo text -->
                                <span class="text">{{{ $item->title }}}</span>
                                <!-- Emphasis label -->
                                <code><i class="fa fa-link"></i> {{{ $item->url }}}</code>
                                <!-- General tools such as edit or delete-->
                                <div class="tools">
                                    <i class="fa fa-edit"></i>
                                    <i class="fa fa-trash-o"></i>
                                </div>
                            </li>                                      
                            @endforeach                                        
                        </ul>
                      </div>
                    </div>
                  </div>
            @endforeach                                          
              </div>   
            </div>                                   
        </div><!-- /.box-body -->
        <div class="box-footer clearfix no-border">
        <code>footer</code>
        </div>
    </div>
</div>                    	
@stop

@section('scripts')
@parent
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script>
        $( document ).ready(function(){        
                $( ".sortable" ).sortable({
                        placeholder: "ui-state-highlight",
            	        update: function( event, ui ) {
            	            var order = $(this).sortable('toArray');
            	            // console.log(order);	
            	            $.post('{{{ URL::to('admin/menu/sort') }}}', { order: order });
            	        }
            	}); 	

                $( ".sortable" ).disableSelection();

                $('#popover').popover({ 
                    html : true,
                    title: function() {
                      return $("#popover-head").html();
                    },
                    content: function() {
                      return $("#popover-content").html();
                    }
                });          
          });
    </script>
@stop