@extends('admin.layouts.master')

@section('head')
        <!-- bootstrap 3.0.2 -->
        <link href="{{{ URL::to('/assets/admin/css/bootstrap.min.css') }}}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{{ URL::to('/assets/admin/css/font-awesome.min.css') }}}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{{ URL::to('/assets/admin/css/ionicons.min.css') }}}" rel="stylesheet" type="text/css" />
		<!-- bootstrap wysihtml5 - text editor -->
        <link href="{{{ URL::to('/assets/admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{{ URL::to('/assets/admin/css/AdminLTE.css') }}}" rel="stylesheet" type="text/css" />

        <link href="{{{ URL::to('/assets/admin/js/plugins/chosen/chosen.css') }}}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
@stop

@section('header')
	@include('admin.includes.header')
@stop

@section('left')
	@include('admin.includes.left')
@stop

@section('right')                      	
<div class="row">
	<div class="col-md-6">
        <!-- Primary box -->
        <div class="box box-solid">
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                <h3 class="box-title">Edit partner</h3>
            </div>
             {{ Form::model($partner, array('route' => array('admin.partners.update', $partner->id), 'method' => 'PUT')) }}
              <div class="box-body">
                    <div class="form-group">
                    	{{ Form::label('email', 'Email') }}
                    	{{ Form::text('email', Input::old('email'), array('class' => 'form-control')) }}
                    </div>                  

                    <div class="form-group">
                        {{ Form::label('phone', 'Phone') }}
                        {{ Form::text('phone', Input::old('phone'), array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('website', 'Website') }}
                        {{ Form::text('website', Input::old('website'), array('class' => 'form-control')) }}
                    </div>                                        
                </div><!-- /.box-body -->

                <div class="box-footer">
                	{{ Form::submit('Update partner', array('class' => 'btn btn-primary')) }}
                <a href="{{{ URL::to('admin/partners') }}}" class="btn btn-danger">
                    Cancel
                </a>  
                </div>
    	</div>
	</div>     
</div>
@stop

@section('scripts')
@parent
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script src="{{{ URL::to('/assets/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}}" type="text/javascript"></script>
<script src="{{{ URL::to('/assets/admin/js/plugins/chosen/chosen.jquery.min.js') }}}" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
    });	

    $(document).ready(function () {
    // Chosen init
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
    });             

</script>      
@stop