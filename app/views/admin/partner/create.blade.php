@extends('admin.layouts.master')

@section('head')
        <!-- bootstrap 3.0.2 -->
        <link href="{{{ URL::to('/assets/admin/css/bootstrap.min.css') }}}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{{ URL::to('/assets/admin/css/font-awesome.min.css') }}}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{{ URL::to('/assets/admin/css/ionicons.min.css') }}}" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
        <link href="{{{ URL::to('/assets/admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{{ URL::to('/assets/admin/css/AdminLTE.css') }}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/admin/css/bootstrap-datetimepicker/bootstrap-datetimepicker.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{{ asset( '/assets/admin/js/plugins/chosen/chosen.css' )}}}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
@stop

@section('header')
  @include('admin.includes.header')
@stop

@section('left')
  @include('admin.includes.left')
@stop

@section('right')                       
<div class="row">
  <div class="col-md-6">
        <!-- Primary box -->
        <div class="box box-solid">
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                <h3 class="box-title">Add partner</h3>
            </div>
             {{ Form::open(array('url' => 'admin/partners')) }}
              <div class="box-body">
                    <div class="form-group">
                      {{ Form::label('id', 'Name') }} <br>
                        {{ Form::select('id', [null => 'Select a partner...'] + $names, null, array('class' => 'form-control')) }}
                    </div>

                    {{ Form::hidden('name', '', array('id' => 'name')) }}

                    <div class="form-group">
                      {{ Form::label('email', 'Email') }}
                        {{ Form::text('email', '', array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('phone', 'Phone') }}
                        {{ Form::text('phone', '', array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('website', 'Website') }}
                        {{ Form::text('website', '', array('class' => 'form-control')) }}
                    </div>                                        
                </div><!-- /.box-body -->
                <div class="box-footer">
                  {{ Form::submit('Add partner', array('class' => 'btn btn-primary')) }}
                </div>
           {{ Form::close() }}
      </div>
  </div>
</div>
@stop

@section('scripts')
@parent
<script src="{{{ URL::to('/assets/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}}" type="text/javascript"></script>
<script src="{{ asset('/assets/admin/js/plugins/bootstrap-datetimepicker/moment.js') }}"></script>
<script src="{{ asset('/assets/admin/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}"></script>
<script src="{{{ URL::to('/assets/admin/js/plugins/chosen/chosen.jquery.min.js') }}}" type="text/javascript"></script>

<script type="text/javascript">
$(function() {
    //bootstrap WYSIHTML5 - text editor
    // $(".textarea").wysihtml5();
    $('#expiration').datetimepicker();
}); 

// Chosen init
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
</script>

<!--populate fields after select field change-->
<script>

  //take the php vars and convert them to javascript vars
  var names = {{ json_encode($names); }};
  var partners = {{ json_encode($partners); }};

  var select = $('select'), i, partner_name;

  select.on('change', function() {
    partner_name = $("select option:selected").html();

    //set the iterator to start from beggining
    i = 0;

    //itterate through the list of names
    while(i < names.length) {
      if(names[i] === partner_name) {

        $('#name').val($("select option:selected").html());

        $('#email').val(partners[i].email);
        $('#phone').val(partners[i].phones);
        i = 0;
        break;
      } else {
        $('#email').val('');
        $('#phone').val('');
        i++;
      };
      
    };

  });

</script>
@stop