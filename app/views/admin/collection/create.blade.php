@extends('admin.layouts.master')

@section('head')
        <!-- bootstrap 3.0.2 -->
        <link href="../../assets/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../../assets/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../../assets/admin/css/ionicons.min.css" rel="stylesheet" type="text/css" />
		<!-- bootstrap wysihtml5 - text editor -->
        <link href="../../assets/admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../assets/admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
@stop

@section('header')
	@include('admin.includes.header')
@stop

@section('left')
	@include('admin.includes.left')
@stop

@section('right')                                         	
                    <div class="row">
						<div class="col-md-6">
                            <!-- Primary box -->
                            <div class="box box-solid">
                                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                                    <h3 class="box-title">Add menu item</h3>
                                </div>
                                  {{ Form::open(array('url' => 'admin/collections')) }}
                                  <div class="box-body">
                                        <div class="form-group">
                                        	{{ Form::label('name', 'Name') }}
                                        	{{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
                                        </div>
                                        <div class="form-group">
                                        	{{ Form::label('slug', 'Slug') }}
                                        	{{ Form::text('slug', Input::old('slug'), array('class' => 'form-control')) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('background', 'Color') }}
                                            {{ Form::text('background', Input::old('background'), array('class' => 'form-control')) }}
                                        </div>                                                         
                                        <div class="form-group">
                                        	{{ Form::label('description', 'Description') }}
                                            {{ Form::textarea('description', Input::old('description'), array('class'=>'ckeditor', 'style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px;'))
                                            }}
                                        </div>
                                    </div><!-- /.box-body -->
                                    <div class="col-md-6">
                                        <!-- Primary box -->
                                        <div class="box box-solid">
                                            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                                                <h3 class="box-title">Hotels</h3>
                                            </div>
                                              <div class="box-body">
                                                    <div class="form-group">
                                                        {{ Form::label('hotels', 'Hotels in this collection') }}
                                                        {{-- {{ Form::select('hotels[]', $hotels, array('class' => 'form-control chosen-select', 'multiple' => true)) }} --}}
                                                    </div>
                                                </div><!-- /.box-body -->
                                           {{ Form::close() }}
                                        </d                                    
                                    <div class="box-footer">
                                    	{{ Form::submit('Create collection', array('class' => 'btn btn-primary')) }}
                                    <a href="{{{ URL::to('admin/collections') }}}" class="btn btn-danger">
                                        Cancel
                                    </a>  
                                    </div>
                               {{ Form::close() }}
                        	</div>
                    	</div>
                    </div>
@stop

@section('scripts')
@parent

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script src="../../assets/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        CKEDITOR.config.imageBrowser_listUrl = "{{ URL::to('admin/hotels/json_photos/145') }}";
    });	
</script>
@stop

