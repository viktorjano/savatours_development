@extends('admin.layouts.master')

@section('head')
        <!-- bootstrap 3.0.2 -->
        <link href="../../assets/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../../assets/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../../assets/admin/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../assets/admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
@stop

@section('header')
	@include('admin.includes.header')
@stop

@section('left')
	@include('admin.includes.left')
@stop

@section('right')           
<div class="row">
    <div class="col-xs-12">
		<div class="box">
            <div class="box-body table-responsive no-padding">                                       
                <table class="table table-hover">
                    <tbody><tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>Description</th>
                        <th>Hotels</th>
                        <th>Tools</th>
                    </tr>
                    @foreach($collections as $collection)
                    <tr>
                        <td>{{{ $collection->id }}}</td>
                        <td>{{{ $collection->name }}}</td>
                        <td><code>{{{ $collection->slug }}}</code></td>
                        <td>{{ $collection->description }}</td>
                        <td>{{ count( $collection->hotels ) }}</td>
                        <td><a class="btn btn-primary" href="{{{ URL::to('admin/collections/' . $collection->id . '/edit') }}}"><i class="fa fa-edit"></i> Edit</a></td>
                    </tr>
                    @endforeach
                </tbody></table>
            </div><!-- /.box-body -->
        </div>
    </div>
</div>
@stop

@section('scripts')
@parent
@stop

