@extends('admin.layouts.forms')
@section('')
@section('right')                                           
<div class="row">
    <div class="col-md-3">
        <!-- Primary box -->
        <div class="box box-solid">
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                <h3 class="box-title">General</h3>
            </div>
            {{ Form::model($page, array('route' => array('admin.pages.update', $page->id), 'method' => 'PUT')) }}
              <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('layout', 'Layout') }}
                        {{ Form::text('layout', Input::old('layout'), array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('title', 'Title') }}
                        {{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}
                    </div>                                        
                    <div class="form-group">
                        {{ Form::label('slug', 'Slug') }}
                        {{ Form::text('slug', Input::old('slug'), array('class' => 'form-control')) }}
                    </div>
{{--                     <div class="form-group">
                        {{ Form::label('body', 'Body') }}
                        {{ Form::textarea('body', Input::old('body'), array('class'=>'textarea', 'style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px;'))
                        }}
                    </div> --}}
                </div><!-- /.box-body -->

                <div class="box-footer">
                    {{ Form::submit('Edit page', array('class' => 'btn btn-primary')) }}
                    <a href="{{{ URL::to('admin/pages') }}}" class="btn btn-danger">
                        Cancel
                    </a>  
                </div>

        </div>
    </div>
    <div class="col-md-9">
        <!-- Primary box -->
        <div class="box box-solid">
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                <h3 class="box-title">Content (Widgets)</h3>
            </div>
            <div class="box-body">
                <!-- Bootstrap 3 panel list. -->
                <ul id="draggablePanelList" class="list-unstyled">
                    <li class=" panel panel-info col-md-12">
                        <div class="panel-heading">Main Banner</div>
                        <div class="panel-body">{{ Form::select('main_banner', $banners, $page->body->main_banner) }}</div>
                    </li>
                    <li class="panel panel-info col-md-6">
                        <div class="panel-heading">Secondary Banner</div>
                        <div class="panel-body">{{ Form::select('secondary_banner', $banners, $page->body->secondary_banner) }}</div>
                    </li>
                    <li class=" panel panel-info col-md-6">
                        <div class="panel-heading">Top Offers</div>
                        <div class="panel-body"> {{ Form::select('top_offers', $top_offers_list, $page->body->top_offers) }}</div>
                    </li>
                    <li class="panel panel-info col-md-3">
                        <div class="panel-heading">1/4</div>
                        <div class="panel-body">Content ...</div>
                    </li>
                    <li class="panel panel-info col-md-3">
                        <div class="panel-heading">1/4</div>
                        <div class="panel-body">Content ...</div>
                    </li>
                    <li class="panel panel-info col-md-3">
                        <div class="panel-heading">1/4</div>
                        <div class="panel-body">Content ...</div>
                    </li>
                    <li class="panel panel-info col-md-3">
                        <div class="panel-heading">1/4</div>
                        <div class="panel-body">Content ...</div>
                    </li>  
                </ul>
            </div>
           {{ Form::close() }}            
    </div>    
</div>
@stop

@section('scripts')
@parent

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

    <script type="text/javascript">

        jQuery(function($) {

            var panelList = $('#draggablePanelList');

            panelList.sortable({
                // Only make the .panel-heading child elements support dragging.
                // Omit this to make then entire <li>...</li> draggable.
                handle: '.panel-heading', 
                update: function() {
                    $('.panel', panelList).each(function(index, elem) {
                         var $listItem = $(elem),
                             newIndex = $listItem.index();

                         // Persist the new indices.
                    });
                }
            });
        });    
    </script>        
@stop


