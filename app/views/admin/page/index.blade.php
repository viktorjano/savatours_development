@extends('admin.layouts.master')


@section('header')
	@include('admin.includes.header')
@stop


@section('right')
@parent
<div class="row clearfix">
	<div class="col-md-12">
        <div class="box box-primary">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>{{ trans('pages.layout') }}</th>
                            <th>{{ trans('pages.title') }}</th>
                            <th>{{ trans('pages.slug') }}</th>
                            <th>{{ trans('ui.tools') }}</th>
                        </tr>
                    	@foreach($pages as $page)
                        <tr>
                        	<td>{{ $page->layout }}</td>
                        	<td>{{ $page->title }}</td>
                        	<td><code>{{ $page->slug }}</code></td>
                        	<td>
                                <a class="btn btn-primary" href="{{{ URL::to('admin/pages/' . $page->id) . '/edit' }}}">
                                <i class="fa fa-edit"></i> Edit
                                </a>  
							</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
</div>

@stop

@section('scripts')
@parent

@stop


