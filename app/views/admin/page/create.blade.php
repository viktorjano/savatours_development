@extends('admin.layouts.master')

@section('head')
        <!-- bootstrap 3.0.2 -->
        <link href="../../assets/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../../assets/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../../assets/admin/css/ionicons.min.css" rel="stylesheet" type="text/css" />
		<!-- bootstrap wysihtml5 - text editor -->
        <link href="../../assets/admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../assets/admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
@stop

@section('header')
	@include('admin.includes.header')
@stop

@section('left')
	@include('admin.includes.left')
@stop

@section('right')                                         	
<div class="row">
	<div class="col-md-6">
        <!-- Primary box -->
        <div class="box box-solid">
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Edit Admin area menu">
                <h3 class="box-title">Add new page</h3>
            </div>
              {{ Form::open(array('url' => 'admin/pages')) }}
              <div class="box-body">
                    <div class="form-group">
                    	{{ Form::label('layout', 'Layout') }}
                    	{{ Form::text('layout', Input::old('layout'), array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('title', 'Title') }}
                        {{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}
                    </div>                                        
                    <div class="form-group">
                    	{{ Form::label('slug', 'Slug') }}
                    	{{ Form::text('slug', Input::old('slug'), array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                    	{{ Form::label('body', 'Body') }}
                        {{ Form::textarea('body', Input::old('body'), array('class'=>'textarea', 'style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px;'))
                        }}
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                	{{ Form::submit('Create page', array('class' => 'btn btn-primary')) }}
                <a href="{{{ URL::to('admin/pages') }}}" class="btn btn-danger">
                    Cancel
                </a>  
                </div>
           {{ Form::close() }}
    	</div>
	</div>
</div>
@stop

@section('scripts')
@parent

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script src="../../assets/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

	<script type="text/javascript">
            $(function() {
                //bootstrap WYSIHTML5 - text editor
                $(".textarea").wysihtml5();
            });	
	</script>
@stop

