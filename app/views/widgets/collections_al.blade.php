<!--column-->
<article class="one-fourth">
	<figure>
		<figcaption>
			<h4 style="color: #fff; background: #c69f46;">Gold</h4>
		</figcaption>
		<div class="details" style="background: #c69f46">
			<p style="color: #fff;">Eksperienca luksoze pushimesh.</p>
		</div>						
		<a href="/collection/gold" title=""><img src="{{ asset('image/260x260/uploads/A0eXFEyp/il041872.jpg') }}" alt="" /></a>						
	</figure>
</article>
<!--//column-->
	
<!--column-->
<article class="one-fourth">
	<figure>
		<figcaption>
			<h4 style="color: #fff; background: #36ab49;">Premier Family</h4>
		</figcaption>	
		<div class="details" style="background: #36ab49">
			<p style="color: #fff;">Pusho ne nje atmosfere te paimitueshme familjare.</p>
		</div>												
		<a href="/collection/premier-family" title=""><img src="{{ asset('image/260x260/images/collections/premier_family_cover_1.jpg') }}" alt="" /></a>
	</figure>
</article>
<!--//column-->	

<!--column-->
<article class="one-fourth">
	<figure>
		<figcaption>
			<h4 style="color: #fff; background: #4e4d66;">Platinum</h4>
		</figcaption>	
		<div class="details" style="background: #4e4d66;">
			<p style="color: #fff;">Rezortet me vleresimin maksimal  nga pushuesit.</p>
		</div>												
		<a href="/collection/platinum" title=""><img src="{{ asset('image/260x260/images/collections/platinum_cover.jpg') }}" alt="" /></a>
	</figure>
</article>

<!--column-->
<article class="one-fourth last">
	<figure>
		<figcaption>
			<h4 style="color: #fff; background: #ef3d55 ;">Comfort</h4>
		</figcaption>	
		<div class="details" style="background: #ef3d55">
			<p style="color: #fff;">Pushime te shpenguara ku raporti cilesi/cmim eshte paresor.</p>
		</div>												
		<a href="/collection/comfort" title=""><img src="{{ asset('image/260x260/uploads/QDCfWGKP/genel-14.jpg') }}" alt="" /></a>
	</figure>
</article>
<!--//column-->	

{{-- <article class="one-half">
	<div class="main-banner-description">
		<div style="width:  350px;">
			<h2>Gural Premier Belek 5*</h2>	
			<h2><span class="price">20% ulje </span>për gjithë verën</h2>
			<h2>#2FemijeFalas</h2>
			<h4>* Oferta skadon ne 31 Maj</h4>
		</div>
	</div>
	
	<a href="http://www.savatours.com/pushime/hotel/?t=131&hotels%5B0%5D=490&checkin=03%2F06%2F2015&display_date=3+Qershor%2C+2015&flexible-dates=on&duration=6-9&adults=2&children=0&child1_age=-1&child2_age=-1&child3_age=-1&from=3&s=6&hotels=490&claim=60588630">		<img src="{{ URL::to('image/524x260/images/banners/GURALIMG2.jpg') }}" />
	</a>
</article> --}}

